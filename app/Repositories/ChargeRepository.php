<?php


namespace App\Repositories;


use App\Charge;

class ChargeRepository
{
    /**
     * @var Charge
     */
    private $charge;

    /**
     * ChargeRepository constructor.
     * @param Charge $charge
     */
    public function __construct(Charge $charge)
    {

        $this->charge = $charge;
    }

    public function charge()
    {
        return $this->charge->newQuery();
    }
}
