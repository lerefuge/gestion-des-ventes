<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;


class ChefLieuDistrict extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;

    public function setLibelleAttribute($value)
    {
        return $this->attributes['libelle'] = STR::upper($value);
    }

    public function district()
    {
        return $this->belongsTo(District::class);
    }

    public function regions()
    {
        return $this->hasMany(Region::class,'chef_lieu_id','id');
    }


}
