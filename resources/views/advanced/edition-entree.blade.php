@extends('template.theme')


@section('font-title')

@endsection
@push('scripts')

    <script type="text/javascript">

        $(function () {
            $('.selectPicker').select2({
                title: "Liste d'élement ...",
                header: 'Selectionnez un élement dans la liste',
                liveSearch: true,
                width: 'resolve'
            });
            var field__caisse = $('#field_caisse');
            var field_facturee = $('#field_facture');
            let type_vehicule_id_field = $('#type_vehicule_id');
            var type_subvention_id = $('#type_subvention_id');
            var prix_base = $('#prix_base');
            var prix_negocie = $('#prix_negocie');
            var montant_supporte = $('#montant_supporte');
            var num_bon_caisse_field = $('#numero_bon_caisse');
            var $field_caisse = document.querySelector('#field_caisse');
            var $montant_supporte_cimaf = document.querySelector('#montant_supporte_cimaf');
            var $prix_negocie = document.querySelector('#prix_negocie'); // prix du voyage
            var $montant_supporte = document.querySelector('#montant_supporte'); // montant supporté par le client
            var $avance = document.querySelector('#avance'); // Avance versee au client
            var $reste = document.querySelector('#reste'); // Avance versee au client
          //  $field_caisse.style.display = "{{ $entree->subvention &&  $entree->subvention->statut ==true ? 'inline': 'none' }}";
            var radio = $('input.paiementStatus');
            // num_bon_caisse_field.attr('required', 'required');
            @if($entree->subvention && $entree->subvention->statut==true )
                num_bon_caisse_field.removeAttr('disabled');
            @else
                num_bon_caisse_field.attr('disabled', 'disabled');
            @endif


            $('#montant_supporte').on('blur',function(e) {
              let prix_voyage = parseInt($prix_negocie.value);
              let  prix_client = parseInt(e.target.value);
                let mtn = prix_voyage - prix_client;
                $montant_supporte_cimaf.value =  '';    //parseInt()
                $montant_supporte_cimaf.value =  mtn;    //parseInt()
                $avance.value = mtn;
                $avance.setAttribute('max',mtn);

                //console.log($avance);
            }).trigger('change');

            $('#avance').on('blur',function(e) {
                $reste.value=$montant_supporte_cimaf.value - $avance.value;
            }).trigger('change');






            type_vehicule_id_field.on('change', function (e) {
                e.preventDefault();
                let type_vehicule_id = e.target.value;

                num_bon_caisse_field.attr('disabled', 'disabled');
                if (type_vehicule_id === '1') {
                    num_bon_caisse_field.removeAttr('required');
                    num_bon_caisse_field.attr('disabled', 'disabled');
                    type_subvention_id.removeAttr('required');
                    type_subvention_id.attr('disabled', 'disabled');
                    prix_base.removeAttr('required');
                    prix_base.attr('disabled', 'disabled');
                    prix_negocie.removeAttr('required');
                    prix_negocie.attr('disabled', 'disabled');
                    montant_supporte.removeAttr('required');
                    montant_supporte.attr('disabled', 'disabled');
                    radio.attr('disabled', 'disabled')

                } else {
                    num_bon_caisse_field.attr('required', 'required');
                   // num_bon_caisse_field.removeAttr('disabled');
                    type_subvention_id.attr('required', 'required');
                    type_subvention_id.removeAttr('disabled');
                    prix_base.attr('required', 'required');
                    prix_base.removeAttr('disabled');
                    prix_negocie.attr('required', 'required');
                    prix_negocie.removeAttr('disabled');
                    montant_supporte.attr('required', 'required');
                    montant_supporte.removeAttr('disabled');
                    radio.removeAttr('disabled')
                }
            }).trigger('change');
            @if($entree->subvention && $entree->subvention->statut==true )
            num_bon_caisse_field.removeAttr('disabled');
            @else
            num_bon_caisse_field.attr('disabled', 'disabled');
            @endif

            radio.on('change', function (event) {

                let status = event.target.value;
                // console.log(event)
                //
                if (status === 'unpaid') {
                    $field_caisse.style.display = 'none';
                    num_bon_caisse_field.removeAttr('required');
                    num_bon_caisse_field.attr('disabled', 'disabled');
                } else if (status === 'paid') {
                    $field_caisse.style.display = 'inline';
                    num_bon_caisse_field.attr('required', 'required');
                    num_bon_caisse_field.removeAttr('disabled');

                }
            }).trigger('checked',);

        })
        ;

    </script>

@endpush


@section('content-body')

    <div class="row">
        <div class="col-md-12 ">



            <section class="card">
                <header class="card-header clearfix">


                    <h2 class="card-title">
                        Editer une entrée </h2>
                    <div class="card-subtitle">
                        <form method="post" action="{{ route('exploitation.deletion-pesee') }}" class="form-inline pull-right">
                            @method('delete')
                            @csrf
                            <input type="hidden" name="pesee_id" value="{{ $entree->id }}">
                            <button type="submit" data-toggle="confirmation" class="btn btn-xs btn-danger"><i
                                    class="fa fa-trash"></i>
                            </button>
                        </form>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                </header>
                {!! Form::open(['url'=>route('exploitation.edition-pesee',$entree),'class'=>'form-horizontal','method'=>'PUT']) !!}
                <div class="card-body">
                    <div class="row form-group">
                        <div class="col-2">
                            <div class="form-group">
                                <label class="col-form-label" for="numero_bl">N°BL</label>
                                <input type="text" name="numero_bl" value="{{ old('numero_bl',$entree->numero_bl) }}"
                                       id="numero_bl" class="form-control"
                                       autocomplete="off"
                                >

                                @error('numero_bl','pesee')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group">

                                <label for="distributeur_id" class="control-label">Distributeur : </label><br>
                                {!! Form::select('distributeur_id', $distributeurs->pluck('nom','id'),
                                $entree->distributeur_id, ['distributeur_id'=>'distributeur_id','class'=>'form-control selectPicker
                                 show-tick',' data-live-search' => true, 'data-live-search-normalize'=>true,
                                'required'=>'required',]) !!}
                                @error('distributeur_id','pesee')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group">

                                <label for="transporteur_id" class="control-label">Transporteur : </label><br>
                                {!! Form::select('transporteur_id', $transporteurs->pluck('nom','id'),
                                $entree->transporteur_id, ['transporteur_id'=>'transporteur_id','class'=>'form-control selectPicker
                                 show-tick',' data-live-search' => true, 'data-live-search-normalize'=>true,
                                'required'=>'required',]) !!}
                                @error('transporteur_id','pesee')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group">

                                <label for="destination_id" class="control-label">Destination : </label><br>
                                {!! Form::select('destination_id', $destinations->pluck('libelle','id'),
                                $entree->destination_id, ['destination_id'=>'destination_id','class'=>'form-control selectPicker
                                 show-tick',' data-live-search' => true, 'data-live-search-normalize'=>true,
                                'required'=>'required',]) !!}
                                @error('destination_id','pesee')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group">

                                <label for="produit_id" class="control-label">Produit : </label><br>
                                {!! Form::select('produit_id', $produits->pluck('libelle','id'),
                                $entree->produit_id, ['produit_id'=>'produit_id','class'=>'form-control selectPicker
                                 show-tick',' data-live-search' => true, 'data-live-search-normalize'=>true,
                                'required'=>'required',]) !!}



                                @error('produit_id','pesee')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-1">
                            <div class="form-group">
                                <label for="quantite" class="control-label">Quantité : </label><br>


                                {!! Form::number('quantite', $entree->quantite,
                                ['id'=>'quantite','class'=>'form-control',
                                'required'=>'required',]) !!}
                                @error('quantite','pesee')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">


                        <div class="col-2">
                            <div class="form-group">
                                <label class="col-form-label" for="date_pesee_entree">Date entrée </label>
                                <div class="input-group">
														<span class="input-group-prepend">
															<span class="input-group-text">
																<i class="fas fa-calendar-alt"></i>
															</span>
														</span>
                                    <input type="text" name="date_pesee_entree"
                                           value="{{ old('date_pesee_entree',$entree->date_entree->format('Y-m-d')) }}"
                                           id="date_pesee_entree"
                                           data-plugin-datepicker class="form-control" autocomplete="off" required

                                           data-plugin-options='{"orientation":"bottom","autoclose":true,"language":"en","todayHighlight":true,"format":"yyyy-mm-dd"}'
                                           readonly
                                    >
                                </div>
                                @error('date_pesee_entree','pesee')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group">
                                <label class="col-form-label" for="date_pesee_sortie">Date sortie </label>
                                <div class="input-group">
														<span class="input-group-prepend">
															<span class="input-group-text">
																<i class="fas fa-calendar-alt"></i>
															</span>
														</span>
                                    <input type="text" name="date_pesee_sortie"
                                           value="{{ old('date_pesee_sortie',$entree->date_sortie->format('Y-m-d')) }}"
                                           id="date_pesee_sortie" class="form-control" autocomplete="off" required
                                           data-plugin-datepicker

                                           data-plugin-options='{"orientation":"bottom","autoclose":true,"language":"en","todayHighlight":true,"format":"yyyy-mm-dd"}'
                                           readonly
                                    >
                                </div>
                                @error('date_pesee_sortie','pesee')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group">
                                <label class="col-form-label" for="heure_pesee_entree">Heure entrée </label>
                                <div class="input-group">
														<span class="input-group-prepend">
															<span class="input-group-text">
																<i class="fas fa-clock"></i>
															</span>
														</span>
                                    <input type="text" name="heure_pesee_entree"
                                           value="{{ old('heure_pesee_entree',Carbon\Carbon::parse($entree->heure_pesee_entree)->format('H:i:s')) }}"
                                           id="heure_pesee_entree"
                                           data-plugin-timepicker class="form-control"
                                           data-plugin-options='{ "showMeridian": false,"minuteStep": 1 }'
                                    >
                                </div>
                                @error('heure_pesee_entree','pesee')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group">
                                <label class="col-form-label" for="heure_pesee_sortie">Heure sortie </label>
                                <div class="input-group">
														<span class="input-group-prepend">
															<span class="input-group-text">
																<i class="fas fa-clock"></i>
															</span>
														</span>
                                    <input type="text" name="heure_pesee_sortie"
                                           value="{{ old('heure_pesee_sortie',$entree->heure_pesee_sortie) }}"
                                           id="heure_pesee_sortie" class="form-control" autocomplete="off"
                                           data-plugin-timepicker
                                           data-plugin-options='{ "showMeridian": false ,"minuteStep": 1 }'
                                           readonly
                                    >
                                </div>
                                @error('heure_pesee_sortie','pesee')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-2">
                            <div class="form-group">
                                <label for="poids_pesee_entree" class="control-label">Poids entrée : </label>

                                <div class="input-group">
														<span class="input-group-prepend">
															<span class="input-group-text">
																<i class="fas fa-weight-hanging"></i>
															</span>
														</span>
                                    {!! Form::text('poids_pesee_entree', $entree->poids_entree,
                                    ['poids_pesee_entree'=>'poids_pesee_entree','class'=>'form-control',
                                    'required'=>'required',]) !!}
                                </div>
                                @error('poids_pesee_entree','pesee')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group">
                                <label for="poids_pesee_sortie" class="control-label">Poids sortie </label>
                                <div class="input-group">
														<span class="input-group-prepend">
															<span class="input-group-text">
																<i class="fas fa-weight"></i>
															</span>
														</span>


                                    {!! Form::text('poids_pesee_sortie', $entree->poids_sortie,
                                    ['poids_pesee_sortie'=>'poids_pesee_sortie','class'=>'form-control',
                                    'required'=>'required',]) !!}
                                    @error('poids_pesee_sortie','pesee')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                        </div>
                        <div class="col-2">
                            <div class="form-group">
                                <label class="col-form-label" for="poids_net">Poids net </label>
                                <div class="input-group">
														<span class="input-group-prepend">
															<span class="input-group-text">
																<i class="fas fa-weight"></i>
															</span>
														</span>


                                    <input type="text" name="poids_net"
                                           value="{{ $entree->poids_net }}"
                                           id="poids_net" class="form-control" autocomplete="off" required

                                    >
                                </div>

                                @error('poids_net','pesee')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group">
                                <label class="col-form-label" for="qte_bl">Poids BL </label>
                                <div class="input-group">
														<span class="input-group-prepend">
															<span class="input-group-text">
																<i class="fas fa-weight"></i>
															</span>
														</span>


                                    <input type="text" name="qte_bl"
                                           value="{{ $entree->qte_bl }}"
                                           id="qte_bl" class="form-control" autocomplete="off" required

                                    >
                                </div>

                                @error('qte_bl','pesee')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group">
                                <label class="col-form-label" for="qte_pesee">Poids Pésé </label>
                                <div class="input-group">
														<span class="input-group-prepend">
															<span class="input-group-text">
																<i class="fas fa-weight"></i>
															</span>
														</span>


                                    <input type="text" name="qte_pesee"
                                           value="{{ $entree->qte_pesee }}"
                                           id="qte_pesee" class="form-control" autocomplete="off" required

                                    >
                                </div>

                                @error('qte_pesee','pesee')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group">
                                <label class="col-form-label" for="date_pesee">Date  </label>
                                <div class="input-group">
														<span class="input-group-prepend">
															<span class="input-group-text">
																<i class="fas fa-calendar-alt"></i>
															</span>
														</span>
                                    <input type="text" name="date_pesee"
                                           value="{{ old('date_pesee',$entree->date_pesee->format('Y-m-d')) }}"
                                           id="date_pesee"
                                           data-plugin-datepicker class="form-control" autocomplete="off" required

                                           data-plugin-options='{"orientation":"bottom","autoclose":true,"language":"en","todayHighlight":true,"format":"yyyy-mm-dd"}'
                                           readonly
                                    >
                                </div>
                                @error('date_pesee','pesee')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group">
                                <label class="col-form-label" for="heure_debut_chargement">Heure debut Chargmt </label>
                                <div class="input-group">
														<span class="input-group-prepend">
															<span class="input-group-text">
																<i class="fas fa-clock"></i>
															</span>
														</span>
                                    <input type="text" name="heure_debut_chargement"
                                           value="{{ old('heure_debut_chargement',$entree->heure_debut_chargement) }}"
                                           id="heure_debut_chargement" data-plugin-timepicker class="form-control"
                                           data-plugin-options='{ "showMeridian": false,"minuteStep": 1 }'
                                           readonly
                                    >
                                </div>
                                @error('heure_debut_chargement','pesee')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group">
                                <label class="col-form-label" for="heure_fin_chargement">Heure Fin Chargment </label>
                                <div class="input-group">
														<span class="input-group-prepend">
															<span class="input-group-text">
																<i class="fas fa-clock"></i>
															</span>
														</span>
                                    <input type="text" name="heure_fin_chargement"
                                           value="{{ old('heure_fin_chargement',$entree->heure_fin_chargement) }}"
                                           id="heure_fin_chargement" data-plugin-timepicker class="form-control"
                                           data-plugin-options='{ "showMeridian": false,"minuteStep": 1 }' autocomplete="off"
                                           readonly
                                    >
                                </div>
                                @error('heure_fin_chargement','pesee')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                    </div>

                </div>

                    <footer class="card-footer">
                        <div class="row justify-content-end">
                            {!! Form::hidden('pesee_id',$entree->id) !!}
                            <button type="submit" data-toggle="confirmation" class="btn btn-dark"><i
                                    class="fas fa-save"></i> Mettre à jour
                            </button>


                        </div>
                    </footer>
                {!! Form::close() !!}
            </section>


        </div>
    </div>


@endsection
