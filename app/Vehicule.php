<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Vehicule extends Model
{
    protected $guarded = ['id'];
    use SoftDeletes;

    public function setLibelleAttribute($value)
    {
        return $this->attributes['libelle'] = STR::upper($value);
    }

    public function pesee()
    {
        return $this->belongsTo(Pesee::class);
    }

    public function typeVehicule()
    {
        return $this->belongsTo(TypeVehicule::class);
    }
}
