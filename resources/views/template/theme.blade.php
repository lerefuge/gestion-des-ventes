<!doctype html >
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="fixed sidebar-light {{ $app->environment('production') ? 'sidebar-left-collapsed' : '' }} ">
<head>

    <!-- Basic -->
    <meta charset="UTF-8">

    <title></title>
    <meta name="keywords" content="HTML5 Admin Template"/>
    <meta name="description" content="Porto Admin - Responsive HTML5 Template">
    <meta name="author" content="okler.net">
    <meta name="csrf-token" content="{!! csrf_token() !!}">
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>

    <!-- Web Fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light"
          rel="stylesheet" type="text/css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/all.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/ajax-bootstrap-select.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('porto/css/theme.css') }}"/>
    <link rel="stylesheet" href="{{ asset('porto/css/custom.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/porto-vendors.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/notify.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/csspin-round.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/bootstrap-select.min.css') }}"/>
    @notifyCss
    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{ asset('css/porto-theme.css') }}"/>
    <style>
        .swal-icon--custom>img{
            max-height: 250px;
            border-radius: 50%;
        }
        .smiley-alert {
            z-index: 2000;
        }
        .bootstrap-select {
            width: 100% !important;
        }
    </style>
@yield('css-libs')
<!-- Head Libs -->
    <script src="{{ asset('porto/vendor/modernizr/modernizr.js')}}"></script>

{{--    <script src="{{ asset('porto/vendor/jquery/jquery.min.js') }}"></script>--}}
</head>
<body>
<div class="cp-spinner cp-round"></div>
<div id="app">
    <section class="body">

        <!-- start: header -->
        <header class="header">
            <div class="logo-container">
                <a href="{{ route('application.index') }}" class="logo">

                    <img src="{{ asset('images/logo-cimaf.jpg') }}" width="180" height="35" alt="Logo Cimaf "/>
                </a>
                <div class="d-md-none toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html"
                     data-fire-event="sidebar-left-opened">
                    <i class="fas fa-bars" aria-label="Toggle sidebar"></i>
                </div>
            </div>

            <!-- start: search & user box -->
            <div class="header-right">

                {{--            <form action="pages-search-results.html" class="search nav-form">--}}
                {{--                <div class="input-group">--}}
                {{--                    <input type="text" class="form-control" name="q" id="q" placeholder="Search...">--}}
                {{--                    <span class="input-group-append">--}}
                {{--								<button class="btn btn-default" type="submit"><i class="fas fa-search"></i></button>--}}
                {{--							</span>--}}
                {{--                </div>--}}
                {{--            </form>--}}

                <span class="separator"></span>

                <ul class="notifications">
                    {{--                @include('template.partials.notifications')--}}
                </ul>

                <span class="separator"></span>

                <div id="userbox" class="userbox">
                    <a href="#" data-toggle="dropdown">
                        <figure class="profile-picture">
{{--                            <img src="img/!logged-user.jpg" alt="Joseph Doe" class="rounded-circle"--}}
{{--                                 data-lock-picture="img/!logged-user.jpg"--}}
{{--                            />--}}
                        </figure>
                        <div class="profile-info" data-lock-name="{{ auth()->user()->name }}" data-lock-email="{{ auth()->user()->email }}">
                            <span class="name">{{ auth()->user()->name }}</span>
                            <span class="role">{{ auth()->user()->roles->implode('name',' | ') }}</span>
                        </div>

                        <i class="fa custom-caret"></i>
                    </a>

                    <div class="dropdown-menu">
                        <ul class="list-unstyled mb-2">
                            <li class="divider"></li>
                            <li>
                                <a role="menuitem" tabindex="-1" href="{{ route('user.profile') }}"><i class="fas fa-user"></i>
                                    Mon Profile</a>
                            </li>
                            <li>
                                <a role="menuitem" tabindex="-1" href="#" data-lock-screen="true"><i
                                        class="fas fa-lock"></i> Verrouiller ma session</a>
                            </li>
                            <li>
                                <a role="menuitem" tabindex="-1" href="{{ route('utilisateur.logout') }}"><i class="fas fa-power-off"></i>
                                    Deconnexion</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- end: search & user box -->
        </header>
        <!-- end: header -->

        <div class="inner-wrapper">
            <!-- start: sidebar -->
            <aside id="sidebar-left" class="sidebar-left">

                <div class="sidebar-header">
                    <div class="sidebar-title">
                        Navigation
                    </div>
                    <div class="sidebar-toggle d-none d-md-block" data-toggle-class="sidebar-left-collapsed"
                         data-target="html" data-fire-event="sidebar-left-toggle">
                        <i class="fas fa-bars" aria-label="Toggle sidebar"></i>
                    </div>
                </div>

                <div class="nano">
                    <div class="nano-content">
                        <nav id="menu" class="nav-main" role="navigation">
                            @include('template.partials.left-menu')
                        </nav>

                        <hr class="separator"/>

                        {{--                    @include('template.partials.sidebar-widgets)--}}
                    </div>


                </div>

            </aside>
            <!-- end: sidebar -->

            <section role="main" class="content-body">

                <header class="page-header">
                    <h2></h2>


                    {{--                <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>--}}

                </header>

            @include('flash::message')

            <!-- start: page -->
            @yield('content-body')
            <!-- end: page -->
            </section>
        </div>

        <aside id="sidebar-right" class="sidebar-right">
            {{--        @include('template.partials.sidebar-right)--}}
        </aside>

    </section>
</div>


<!-- Vendor -->

<script src="{{ asset('js/porto-vendors.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/porto-theme.js') }}"></script>
<script src="{{ asset('js/sweetalert.min.js') }}"></script>
<script src="{{ asset('js/notify.js') }}"></script>
<script src="{{ asset('js/ajax-bootstrap-select.min.js') }}"></script>
<script src="{{ asset('js/icheck.min.js') }}"></script>
<script src="{{ asset('js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('assets/SpiderWebtr/isAuth/isAuth.js') }}"></script>
<script src="{{ asset('js/bootstrap-confirmation.min.js') }}"></script>
<script src="{{ asset('js/popper.min.js') }}"></script>

<!-- Theme Base, Components and Settings -->
@include('notify::messages')
@yield('js-links')
@stack('scripts')
@stack('script')
@notifyJs
<script>
    // Maintain Scroll Position
    if (typeof localStorage !== 'undefined') {
        if (localStorage.getItem('sidebar-left-position') !== null) {
            var initialPosition = localStorage.getItem('sidebar-left-position'),
                sidebarLeft = document.querySelector('#sidebar-left .nano-content');

            sidebarLeft.scrollTop = initialPosition;
        }
    }

    (function ($) {

        'use strict';
        var flashInit = function () {
            $('#flash-overlay-modal').modal();
        };
        var bootstrapConfirmInit = function () {
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]',
                // other options
                animation: true,
                placement: 'top',
                trigger: 'click',
                title: 'Confirmez-vous cette action ?',
                btnOkLabel: '<i class="icon-ok-sign icon-white"></i> Oui',
                btnCancelLabel: '<i class="icon-remove-sign"></i> Non',
                singleton: true
            });
        };
        var user={
            name:"{{auth()->user()->name}}",
            email:"{{auth()->user()->email}}",

        };
        defineIsAuth(user,{
            texts:{ //change strings to your language
                placeholder:"Tapez votre mot de  Passe",
                wrong:"Mot de passe erroné",
                error:"Error",
                button:"Se reconnecter"
            },
            // loginField:"username" //your laravel login field
        });

        $(function () {
            flashInit();
            bootstrapConfirmInit();
        });
    }).apply(this, [jQuery]);


</script>
</body>
</html>
