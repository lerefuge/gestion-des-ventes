<?php


namespace App\Repositories;


use App\Pesee;
use Illuminate\Database\Eloquent\Builder as BuilderAlias;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class PeseeRepository
{
    /**
     * @var Pesee
     */
    private $pesee;

    /**
     * PeseeRepository constructor.
     * @param Pesee $pesee
     */
    public function __construct(Pesee $pesee)
    {

        $this->pesee = $pesee;
    }

    /**
     * @return Pesee|BuilderAlias
     */
    public function pesee()
    {
        return $this->pesee->newQuery();
    }

    public function loadBl()
    {
        return $this->pesee()->select(['id', 'numero_bl']);
    }

    /*
     *
     */
    public function loadCimentQualite($start,$end=null)
    {
       return $this->_getProduit($start,$end)->addSelect('pesees.produit_id')->distinct()->get();

    }


    /**
     * Recupère le point d'un transporteur
     * en fonction des parametres fournis
     * @return BuilderAlias|Builder
     */
    private function pointTransporteur()
    {
        return $this->pesee()
            ->select("transporteurs.nom", "transporteurs.code", "produits.libelle", "subventions.numero_bon_caisse",
                DB::raw('SUM(prix_base) as prix_base'),
                DB::raw('SUM(prix_negocie) as prix_negocie'),
                DB::raw('SUM(montant_supporte) as montant_supporte'),
                DB::raw('SUM(montant_supporte_cimaf) as montant_supporte_cimaf'),
                DB::raw('SUM(pesees.quantite) as tonnage')
            )
            ->join('subventions', 'pesees.id', '=', 'subventions.pesee_id', 'inner')
            ->join('transporteurs', 'transporteurs.id', '=', 'pesees.transporteur_id', 'inner')
            ->join('produits', 'produits.id', '=', 'pesees.produit_id', 'inner')
            ->groupBy("transporteurs.nom")
            ->groupBy("transporteurs.code")
            ->groupBy("produits.libelle")
            ->groupBy("subventions.numero_bon_caisse")
            ->orderBy("transporteurs.nom");
    }

    /**
     * Requete
     * @return Pesee|BuilderAlias|Builder
     */
    private function historiqueTransporteur()
    {
        return $this->pesee()
//            ->selectRaw("transporteurs.nom, transporteurs.code, produits.libelle
//                , prix_base as prix_base,
//                prix_negocie as prix_negocie,
//                montant_supporte as montant_supporte,
//                subventions.numero_bon_caisse,
//                montant_supporte_cimaf as montant_supporte_cimaf,
//                pesees.quantite as tonnage,pesees.date_pesee"
//            )
            ->selectRaw("transporteurs.nom, transporteurs.code, produits.libelle,pesees.date_pesee")
            ->join('subventions', 'pesees.id', '=', 'subventions.pesee_id', 'inner')
            ->join('transporteurs', 'transporteurs.id', '=', 'pesees.transporteur_id', 'inner')
            ->join('produits', 'produits.id', '=', 'pesees.produit_id', 'inner')
            ->orderBy("pesees.date_pesee")
            ->orderBy("transporteurs.nom")
            //->groupBy("subventions.numero_bon_caisse")
            ->groupBy("transporteurs.nom")
            ->groupBy("transporteurs.code")
            ->groupBy("produits.libelle")
            ->groupBy("pesees.date_pesee");
    }

    /**
     * @return BuilderAlias|Builder
     */
    private function pointDistributeur()
    {
        return $this->pesee()
            ->select("distributeurs.nom", "distributeurs.code", "produits.libelle", "transporteurs.nom as transporteur", "subventions.numero_bon_caisse",
                DB::raw('SUM(prix_base) as prix_base'),
                DB::raw('SUM(prix_negocie) as prix_negocie'),
                DB::raw('SUM(montant_supporte) as montant_supporte'),
                DB::raw('SUM(montant_supporte_cimaf) as montant_supporte_cimaf'),
                DB::raw('SUM(pesees.quantite) as tonnage')
            )
            ->join('subventions', 'pesees.id', '=', 'subventions.pesee_id', 'inner')
            ->join('distributeurs', 'distributeurs.id', '=', 'pesees.distributeur_id', 'inner')
            ->join('transporteurs', 'transporteurs.id', '=', 'pesees.transporteur_id', 'inner')
            ->join('produits', 'produits.id', '=', 'pesees.produit_id', 'inner')
            ->groupBy("transporteurs.nom")
            ->groupBy("distributeurs.nom")
            ->groupBy("distributeurs.code")
            ->groupBy("produits.libelle")
            ->groupBy("subventions.numero_bon_caisse")
            ->orderBy("distributeurs.nom");
    }

    /**
     * @param $transporteur_id
     * @param $date
     * @param $subvention
     * @return BuilderAlias|BuilderAlias[]|\Illuminate\Database\Eloquent\Collection|Builder|Builder[]|Collection
     */
    public function pointJourTransporteur($transporteur_id = null, $date, $subvention = null)
    {
        $query = $this->pointTransporteur();
        if (!is_null($transporteur_id)) {
            if (is_array($transporteur_id)) {
                $query = $query->whereIn('pesees.transporteur_id', $transporteur_id, 'and');
            } else {
                $query = $query->where('pesees.transporteur_id', '=', $transporteur_id);
            }
        }

        if (!is_null($subvention)) {
            $query = $query->where('subventions.type_subvention_id', '=', $subvention);
        }
        $query = $query->where('pesees.date_pesee', '=', $date)->get();

        return $query;
    }

    /**
     * @param null $transporteur_id
     * @param $date
     * @param null $subvention
     * @return BuilderAlias|BuilderAlias[]|\Illuminate\Database\Eloquent\Collection|Builder|Collection
     */
    public function historiqueJourTransporteur($transporteur_id = null, $date, $subvention = null)
    {
        $query = $this->historiqueTransporteur();
        if (!is_null($transporteur_id)) {
            if (is_array($transporteur_id)) {
                $query = $query->whereIn('pesees.transporteur_id', $transporteur_id, 'and');
            } else {
                $query = $query->where('pesees.transporteur_id', '=', $transporteur_id);
            }
        }

        if (!is_null($subvention)) {
            $query = $query->where('subventions.type_subvention_id', '=', $subvention);
        }
        $query = $query->where('pesees.date_pesee', '=', $date)
            ->get();

        return $query;
    }

    public function expedies($start , $end,$zones=null)
    {
       $query =  $this->_getProduit($start,$end)->selectRaw(DB::raw('SUM(pesees.qte_bl) AS livre')
        );
       if(isset($zones) && !empty($zones)){
           $query = $query->join('destinations', 'destinations.id', '=', 'pesees.destination_id')
               ->join('regions', 'destinations.region_id', '=', 'regions.id')
               ->join('chef_lieu_districts', 'regions.chef_lieu_id', '=', 'chef_lieu_districts.id')
               ->whereIn('pesees.destination_id',$zones);
       }
        $query = $query->groupBy(['produits.libelle'])->get();

       return $query;
    }

    public function venteJournaliere($date)
    {
        $query = $this->_ventes();
        $ventes = $query->where('date_pesee', '=', $date);
        $produit =$this->_getProduit($date);
        $produit_vendus = $produit->selectRaw(DB::raw('SUM(pesees.qte_bl) AS livre'))->groupBy(['produits.libelle'])->get();
        return [
            'ventes' => $ventes->get(),
            'produits' => $produit_vendus,
            'distributeurs' => $this->_groupByDistributeur($produit),
            'transporteurs' => $this->_groupByTransporteur($produit),
        ];
    }

    public function ventePeriodique(string $start, string $end)
    {
        $query = $this->_ventes();
        $ventes = $query
            ->whereBetween('date_pesee',[$start,$end])
           //->where('date_pesee', '<=', $end)
        ;
        $produit =$this->_getProduitByPeriode($start,$end);
        $produit_vendus = $produit->selectRaw(DB::raw('SUM(pesees.qte_bl) AS livre'))->groupBy(['produits.libelle'])->get();
        return [
            'ventes' => $ventes->get(),
            'produits' => $produit_vendus,
            'distributeurs' => $this->_groupByDistributeur($produit),
            'transporteurs' => $this->_groupByTransporteur($produit),
        ];
    }

    public function venteAnnuelle( $start,$ends)
    {

        $query = $this->_ventes();
        $ventes = $query
            ->whereBetween('date_pesee',[$start,$ends])
        ;
        $produit =$this->_getProduitByYear($start,$ends);
        $produit_vendus = $produit->selectRaw(DB::raw('SUM(pesees.qte_bl) AS livre'))->groupBy(['produits.libelle'])->get();
        return [
            'ventes' => $ventes->get(),
            'produits' => $produit_vendus,
            'distributeurs' => $this->_groupByDistributeur($produit),
            'transporteurs' => $this->_groupByTransporteur($produit),
        ];
    }
    private function _getProduit($start,$end=null)
    {
        $query =  $this->pesee()->select("produits.libelle");
        if(!is_null($end)){
            $query = $query->whereBetween('date_pesee',[$start,$end]);
        }else{
            $query = $query->where('date_pesee', '=', $start);
        }
          return $query  ->join('produits', 'produits.id', '=', 'pesees.produit_id');
    }
    private function _getProduitByPeriode($start,$end)
    {
        return  $this->pesee()->select("produits.libelle")
            ->whereBetween('date_pesee', [$start,$end])

            ->join('produits', 'produits.id', '=', 'pesees.produit_id');
    }

    private function _getProduitByYear($year_start,$year_end)
    {
        return  $this->pesee()->select("produits.libelle")
            ->whereBetween('date_pesee', [$year_start,$year_end])
         //   ->where('date_pesee', '>=', $year_end)
            ->join('produits', 'produits.id', '=', 'pesees.produit_id');
    }



    private function _groupByTransporteur(BuilderAlias $builder)
    {
        $transporteurs = $builder->selectRaw(DB::raw('SUM(pesees.qte_bl) AS livre'))->addSelect('transporteurs.nom')
        ->leftJoin('transporteurs', 'transporteurs.id', '=', 'pesees.transporteur_id')
        ->groupBy('transporteurs.nom')
            ->get()->sortByDesc('livre');
        return $transporteurs;
    }

    public function _groupByDistributeur(BuilderAlias $builder)
    {
        $distibuteurs = $builder->selectRaw(DB::raw('SUM(pesees.qte_bl) AS livre'))->addSelect('distributeurs.nom')
            ->leftJoin('distributeurs', 'distributeurs.id', '=', 'pesees.distributeur_id')
            ->groupBy('distributeurs.nom')->get()->sortByDesc('livre');
        return $distibuteurs;
    }

    public function _groupByCiment(BuilderAlias $builder)
    {
        $distibuteurs = $builder->selectRaw(DB::raw('SUM(pesees.qte_bl) AS expedie'))
         //   ->leftJoin('distributeurs', 'distributeurs.id', '=', 'pesees.distributeur_id')
            ->groupBy('produits.libelle')->get()->sortByDesc('expedie');
        return $distibuteurs;
    }

    /**
     *
     */
    private function _ventes()
    {
        return $this->pesee()->select(
            "pesees.id",
            "pesees.numero_bl",
            "pesees.qte_bl",
            "pesees.poids_entree",
            "pesees.poids_sortie",
            "pesees.qte_pesee",
            "pesees.heure_pesee_entree",
            "pesees.heure_pesee_sortie",
            "pesees.date_entree",
            "pesees.date_sortie",
            "distributeurs.nom AS distributeur",
            "transporteurs.nom as transporteur",
            "produits.libelle AS ciment",
            "vehicules.immatriculation AS immatriculation",
            "destinations.libelle AS destination"
        )
            ->join('produits', 'produits.id', '=', 'pesees.produit_id')
            ->leftJoin('distributeurs', 'distributeurs.id', '=', 'pesees.distributeur_id')
            ->leftJoin('transporteurs', 'transporteurs.id', '=', 'pesees.transporteur_id')
            ->leftJoin('vehicules', 'pesees.id', '=', 'vehicules.pesee_id')
            ->leftJoin('destinations', 'destinations.id', '=', 'pesees.destination_id');

    }

    /**
     * @param null $distributeur
     * @param $date
     * @param null $subvention
     * @return BuilderAlias|BuilderAlias[]|\Illuminate\Database\Eloquent\Collection|Builder|Collection
     */
    public function pointJourDistributeur($distributeur = null, $date, $subvention = null)
    {
        $query = $this->pointDistributeur();
        if (!is_null($distributeur)) {
            if (is_array($distributeur)) {
                $query = $query->whereIn('pesees.distributeur_id', $distributeur, 'and');
            } else {
                $query = $query->where('pesees.distributeur_id', '=', $distributeur);
            }
        }

        if (!is_null($subvention)) {
            $query = $query->where('subventions.type_subvention_id', '=', $subvention);
        }
        $query = $query->where('pesees.date_pesee', '=', $date)->get();

        return $query;
    }

    /**
     * @param $transporteur_id
     * @param $start
     * @param $end
     * @param $subvention
     * @return BuilderAlias|BuilderAlias[]|\Illuminate\Database\Eloquent\Collection|Builder|Collection
     */
    public function pointHebdoTransporteur($transporteur_id, $start, $end, $subvention = null)
    {

        $query = $this->pointTransporteur();
        if (!is_null($transporteur_id)) {
            if (is_array($transporteur_id)) {
                $query = $query->whereIn('pesees.transporteur_id', $transporteur_id, 'or');
            } else {
                $query = $query->where('pesees.transporteur_id', '=', $transporteur_id);
            }
        }

        if (!is_null($subvention)) {
            $query = $query->where('subventions.type_subvention_id', '=', $subvention);
        }
        $query = $query
            ->where('pesees.date_pesee', '>=', $start)
            ->where('pesees.date_pesee', '<=', $end)
            ->get();

        return $query;
    }

    /**
     * @param $transporteur_id
     * @param $start
     * @param $end
     * @param null $subvention
     * @return Pesee|Pesee[]|BuilderAlias|BuilderAlias[]|\Illuminate\Database\Eloquent\Collection|Builder|Builder[]|Collection
     */
    public function historiqueHebdoTransporteur($transporteur_id, $start, $end, $subvention = null)
    {

        $query = $this->historiqueTransporteur();
        if (!is_null($transporteur_id)) {
            if (is_array($transporteur_id)) {
                $query = $query->whereIn('pesees.transporteur_id', $transporteur_id, 'or');
            } else {
                $query = $query->where('pesees.transporteur_id', '=', $transporteur_id);
            }
        }

        if (!is_null($subvention)) {
            $query = $query->where('subventions.type_subvention_id', '=', $subvention);
        }
        $query = $query
            ->where('pesees.date_pesee', '>=', $start)
            ->where('pesees.date_pesee', '<=', $end)
            ->get();

        return $query;
    }


    /**
     * @param $distributeur_id
     * @param $start
     * @param $end
     * @param null $subvention
     * @return BuilderAlias|BuilderAlias[]|\Illuminate\Database\Eloquent\Collection|Builder|Collection
     */
    public function pointHebdoDistributeur($distributeur_id, $start, $end, $subvention = null)
    {
        $query = $this->pointDistributeur();
        if (!is_null($distributeur_id)) {
            if (is_array($distributeur_id)) {
                $query = $query->whereIn('pesees.distributeur_id', $distributeur_id, 'and');
            } else {
                $query = $query->where('pesees.distributeur_id', '=', $distributeur_id);
            }
        }

        if (!is_null($subvention)) {
            $query = $query->where('subventions.type_subvention_id', '=', $subvention);
        }
        $query = $query
            ->where('pesees.date_pesee', '>=', $start)
            ->where('pesees.date_pesee', '<=', $end)
            ->get();

        return $query;
    }

     /**
     * @param $day
     * @param $month
     * @param array $produit_id
     * @param null $year
     * @param null $destination_id
     * @return \App\Pesee|\App\Pesee[]|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Query\Builder|\Illuminate\Database\Query\Builder[]|\Illuminate\Support\Collection
     */
    public function _expedition($month,$day=null, array $produit_id, $year = null, $destination_id = null)
    {
        $query = $this->pesee()
            ->join('produits', 'produits.id', '=', 'pesees.produit_id')
            ->groupBy('pesees.date_pesee');
        if (!is_null($day)) {
            $query =$query->whereDay('pesees.date_pesee', $day);
        }
        if (!is_null($year)) {
            $query = $query->whereYear('pesees.date_pesee', $year);
        } else {
            $query = $query->whereYear('pesees.date_pesee', now()->format('Y'));
        }

        if (!is_null($destination_id) && !empty($destination_id)) {
            $query = $query->join('destinations', 'destinations.id', '=', 'pesees.destination_id')
                ->join('regions', 'destinations.region_id', '=', 'regions.id')
                ->join('chef_lieu_districts', 'regions.chef_lieu_id', '=', 'chef_lieu_districts.id')
                ->whereIn('pesees.destination_id', $destination_id);
        }
        $query = $query ->whereMonth('pesees.date_pesee', $month)
            ->whereIn('produits.id', $produit_id)
            ->addSelect(DB::raw('SUM(qte_bl) AS expedies'))
            ->orderBy('pesees.date_pesee')->get('expedies')->map(function ($q) {
                return round($q->expedies, 2);
            });
        return $query;
    }


}
