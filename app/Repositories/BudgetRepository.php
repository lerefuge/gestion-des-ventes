<?php


namespace App\Repositories;


use App\Budget;

class BudgetRepository
{

    /**
     * @var Budget
     */
    private $budget;

    public function __construct(Budget $budget)
    {

        $this->budget = $budget;
    }

    public function budget()
    {
        return $this->budget->newQuery();

    }

}
