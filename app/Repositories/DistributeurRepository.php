<?php


namespace App\Repositories;


use App\Distributeur;

class DistributeurRepository
{
    /**
     * @var Distributeur
     */
    private $distributeur;

    /**
     * DistributeurRepository constructor.
     * @param Distributeur $distributeur
     */
    public function __construct(Distributeur $distributeur)
    {

        $this->distributeur = $distributeur;
    }

    public function distributeur()
    {
        return $this->distributeur->newQuery();
    }
}
