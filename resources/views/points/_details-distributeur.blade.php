<div class="row">
    <div class="col">
        <section class="card">
            <header class="card-header">
                <div class="card-actions">
                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                    <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                </div>

                <h2 class="card-title">Points Distributeurs

                    @if($case =='daily')
                        du {{ date('d/m/Y', strtotime(request('date_pesee_entree'))) }}
                    @elseif ($case=='weekly')
                        du {{ date('d/m/Y', strtotime(request('start'))) }}  au {{ date('d/m/Y', strtotime(request('end'))) }}
                    @endif
                    <sup class="text-danger">*</sup></h2>
                <p class="card-subtitle text-danger">
                    NB: Sous réserve, des entrées comptabilisées
                </p>
            </header>
            <div class="card-body">
                <table class="table table-bordered table-condensed table-striped mb-0 datatable-default">
                    <thead>
                    <tr class="small">
                        <th>#</th>
                        <th>Code Distributeur</th>
                        <th>Distributeur</th>
                        <th>Transporteur</th>
                        <th>Qualité Ciment</th>
                        <th>Quantité</th>
                        <th>Tarif de base</th>
                        <th>Prix du voyage</th>
                        <th>Montant Supporté par le client</th>
                        <th>Montant Supporté par CIMAF</th>

                    </tr>
                    </thead>
                    <tbody>
                    @php
                        $total_tonnage = $total_prix_base = $total_prix_negocie = $total_montant_supporte = $total_montant_supporte_cimaf = 0                       @endphp
                    @foreach($results as $result)
                        <tr class="text-center small ">
                            <td>{{ $loop->index + 1  }}</td>
                            <td>{{ $result->code }}</td>
                            <td>{{ $result->nom }}</td>
                            <td>{{ $result->transporteur }}</td>
                            <td>{{ $result->libelle }}</td>
                            <td>
                                @php($total_tonnage += $result->tonnage)
                                {{ $result->tonnage }}
                            </td>
                            <td>
                                @php($total_prix_base += $result->prix_base)
                                {{ numberFormat($result->prix_base,0,'',' ') }}
                            </td>
                            <td>
                                @php($total_prix_negocie += $result->prix_negocie)
                                {{ numberFormat($result->prix_negocie,0,'',' ')}}
                            </td>
                            <td>
                                @php($total_montant_supporte += $result->montant_supporte)
                                {{ numberFormat($result->montant_supporte,0,'',' ') }}
                            </td>
                            <td>
                                @php($total_montant_supporte_cimaf += $result->montant_supporte_cimaf)
                                {{ numberFormat($result->montant_supporte_cimaf,0,'',' ') }}
                            </td>
                        </tr>
                    @endforeach
                    <tr class="text-center small " role="row">
                        <td colspan="5" class="sorting_1 text-right">Total</td>
                        <td>

                            {{$total_tonnage   }}</td>
                        <td>{{ numberFormat($total_prix_base,0,'',' ') }}</td>
                        <td>{{  numberFormat($total_prix_negocie,0,'',' ') }}</td>
                        <td>{{  numberFormat($total_montant_supporte,0,'',' ') }}</td>
                        <td>{{ numberFormat($total_montant_supporte_cimaf,0,'',' ')  }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </section>
    </div>
</div>

