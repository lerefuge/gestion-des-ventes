<?php

namespace App\Imports;

use App\Repositories\DestinationRepository;
use App\Repositories\DistributeurRepository;
use App\Repositories\PeseeRepository;
use App\Repositories\ProduitRepository;
use App\Repositories\RegionRepository;
use App\Repositories\SubventionRepository;
use App\Repositories\TransporteurRepository;
use App\Repositories\VehiculeRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use  RandomLib\Factory;

class MassDataPeseeImport extends DefaultValueBinder implements ToCollection, WithHeadingRow, WithColumnFormatting, WithChunkReading, ShouldQueue
{

    use Importable;

    /**
     * @var ProduitRepository
     */
    private $produitRepository;
    /**
     * @var RegionRepository
     */
    private $regionRepository;
    /**
     * @var SubventionRepository
     */
    private $subventionRepository;
    /**
     * @var TransporteurRepository
     */
    private $transporteurRepository;
    /**
     * @var DistributeurRepository
     */
    private $distributeurRepository;
    /**
     * @var DestinationRepository
     */
    private $destinationRepository;
    /**
     * @var PeseeRepository
     */
    private $peseeRepository;
    /**
     * @var VehiculeRepository
     */
    private $vehiculeRepository;

    public function __construct(ProduitRepository $produitRepository,
                                RegionRepository $regionRepository,
                                SubventionRepository $subventionRepository,
                                TransporteurRepository $transporteurRepository,
                                DistributeurRepository $distributeurRepository,
                                DestinationRepository $destinationRepository, PeseeRepository $peseeRepository, VehiculeRepository $vehiculeRepository)
    {


        $this->produitRepository = $produitRepository;
        $this->regionRepository = $regionRepository;
        $this->subventionRepository = $subventionRepository;
        $this->transporteurRepository = $transporteurRepository;
        $this->distributeurRepository = $distributeurRepository;
        $this->destinationRepository = $destinationRepository;
        $this->peseeRepository = $peseeRepository;
        $this->vehiculeRepository = $vehiculeRepository;
    }

    public function chunkSize(): int
    {
        return 1000;
    }

    /**
     * @param Collection $collection
     * @throws \Exception
     */
    public function collection(Collection $collection)
    {
        DB::beginTransaction();
        foreach ($collection as $row) {
            // dd($row);


            // dd($quantite,$poid_net);
            if ($row->filter()->isNotEmpty()) {
                $distributeur = $this->distributeurRepository->distributeur()->firstOrCreate(['nom' => trim($row['client'], " \t\n\r")], ['nom' => trim($row['client'], " \t\n\r")]);
                $transporteur = $this->transporteurRepository->transporteur()->firstOrCreate(['nom' => trim($row['transporteur'], " \t\n\r")], ['nom' => trim($row['transporteur'], " \t\n\r")]);
                $produit = $this->produitRepository->produit()->firstOrCreate(['libelle' => trim($row['produit'], " \t\n\r")], ['libelle' => trim($row['produit'], " \t\n\r")]);
                $destination = $this->destinationRepository->destination()->firstOrCreate(['libelle' => trim($row['destination'], " \t\n\r")], ['libelle' => trim($row['destination'], " \t\n\r")]);

                $poid_net = trim($row['poids_net'], " \t\n\r");
                if ($poid_net == 0) {
                    $quantite = 0;
                } elseif ($poid_net > 0) {
                    if (Str::contains($produit->libelle, ['VRAC','CHF'])) {
                        $quantite = $poid_net / 1000;

                    } else {
                        if ($poid_net < 1000) {
                            $quantite = $poid_net / 1000 ;
                        } else {
                            $quantite = round($poid_net / 1000, 0, PHP_ROUND_HALF_UP);
                        }
                    }
                    $qte_bl = $quantite;
                }
                if($row['n0_bl'] == '' || $row['n0_bl'] == NULL){
                   $numBL = Str::random(10);
                }else{
                    $numBL = $row['n0_bl'];
                }

                $data = [
                    'date_pesee' => Date::excelToDateTimeObject($row['date_p2'])->format('Y-m-d'),
                    'numero_bl' => $numBL,
                    'quantite' => $quantite,
                    'qte_pesee' => $quantite,
                    'qte_bl' => $qte_bl,
                    'poids_entree' => $row['poids_p1'],
                    'poids_sortie' => $row['poids_p2'],
                    'poids_net' => $poid_net,
                    'date_entree' => Date::excelToDateTimeObject($row['date_p1'])->format('Y-m-d'),
                    'heure_pesee_entree' => Date::excelToDateTimeObject($row['heure_p1'])->format('H:i:s'),
                    'date_sortie' => Date::excelToDateTimeObject($row['date_p2'])->format('Y-m-d'),
                    'heure_pesee_sortie' => Date::excelToDateTimeObject($row['heure_p2'])->format('H:i:s'),
                    'distributeur_id' => $distributeur->id ,
                    'transporteur_id' => $transporteur->id ,
                    'produit_id' => $produit->id,
                    'destination_id' => $destination->id ,
                ];

                //verifions si le numero bl existe deja

                $presence = $this->peseeRepository->pesee()->where('numero_bl', '=', $row['n0_bl'])->first();
                if ($presence) {
                    $toUpdate = Arr::except($data, 'numero_bl');
                    $presence->update($toUpdate);
                } else {
                    if ($pesse = $this->peseeRepository->pesee()->create($data)) {
                        $driver = [
                            'chauffeur' => $row['chauffeur'],
                            'immatriculation' => $row['vehicule'],
                            'contact' => $row['observations'],
                            'type_vehicule_id' => 1,
                            'pesee_id' => $pesse->id,
                        ];
                        $this->vehiculeRepository->vehicule()->create($driver);
                        $this->subventionRepository->subvention()->create(['pesee_id' => $pesse->id]);
                        DB::commit();
                    }
                }

            } else {
                DB::rollBack();
            }
        }
    }

    /**
     * @return array
     */
    public function columnFormats(): array
    {
        // TODO: Implement columnFormats() method.
    }

}
