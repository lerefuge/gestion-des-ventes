<?php

namespace App\Imports;

use App\Pesee;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use App\Repositories\PeseeRepository;
use App\Repositories\RegionRepository;
use App\Repositories\ProduitRepository;
use Maatwebsite\Excel\Concerns\ToModel;
use App\Repositories\VehiculeRepository;
use App\Repositories\ChauffeurRepository;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\DefaultValueBinder;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use App\Repositories\SubventionRepository;
use App\Repositories\DestinationRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Repositories\DistributeurRepository;
use App\Repositories\TransporteurRepository;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithMappedCells;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
/**/
class PeseesImport extends DefaultValueBinder implements ToCollection,  WithHeadingRow, WithColumnFormatting, WithChunkReading, ShouldQueue
{
    /**
     * @var ProduitRepository
     */
    private $produitRepository;
    /**
     * @var ChauffeurRepository
     */
    private $chauffeurRepository;
    /**
     * @var RegionRepository
     */
    private $regionRepository;
    /**
     * @var DestinationRepository
     */
    private $destinationRepository;
    /**
     * @var TransporteurRepository
     */
    private $transporteurRepository;
    /**
     * @var DistributeurRepository
     */
    private $distributeurRepository;
    /**
     * @var PeseeRepository
     */
    private $peseeRepository;
    /**
     * @var Request
     */
    private $request;
    /**
     * @var VehiculeRepository
     */
    private $vehiculeRepository;
    /**
     * @var SubventionRepository
     */
    private $subventionRepository;

    /**
     * PeseesImport constructor.
     * @param ProduitRepository $produitRepository
     * @param RegionRepository $regionRepository
     * @param SubventionRepository $subventionRepository
     * @param TransporteurRepository $transporteurRepository
     * @param DistributeurRepository $distributeurRepository
     * @param DestinationRepository $destinationRepository
     * @param PeseeRepository $peseeRepository
     * @param VehiculeRepository $vehiculeRepository
     * @param $request
     */
    public function __construct(ProduitRepository $produitRepository,
                                RegionRepository $regionRepository,
                                SubventionRepository $subventionRepository,
                                TransporteurRepository $transporteurRepository,
                                DistributeurRepository $distributeurRepository,
                                DestinationRepository $destinationRepository,
                                PeseeRepository $peseeRepository,
                                VehiculeRepository $vehiculeRepository, $request)
    {

        $this->produitRepository = $produitRepository;
        $this->regionRepository = $regionRepository;
        $this->destinationRepository = $destinationRepository;
        $this->transporteurRepository = $transporteurRepository;
        $this->distributeurRepository = $distributeurRepository;
        $this->peseeRepository = $peseeRepository;
        $this->request = $request;
        $this->vehiculeRepository = $vehiculeRepository;
        $this->subventionRepository = $subventionRepository;
    }

    public function columnFormats(): array
    {
        return [
            'I' => NumberFormat::FORMAT_DATE_DDMMYYYY,
//            'C' => NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE,
            'F' => NumberFormat::FORMAT_NUMBER,
        ];
    }

    /**
     * @param Collection $collection
     * @throws Exception
     */
    public function collection(Collection $collection)
    {
        $date = $this->request['date'];
        $date_pesee = Carbon::parse($date)->format('Y-m-d');
        DB::beginTransaction();
        foreach ($collection as $row) {
            if ($row->filter()->isNotEmpty()) {
                $distributeur = $this->distributeurRepository->distributeur()->firstOrCreate(['nom' => trim($row['client'], " \t\n\r")], ['nom' => trim($row['client'], " \t\n\r")]);
                $transporteur = $this->transporteurRepository->transporteur()->firstOrCreate(['nom' => trim($row['transporteur'], " \t\n\r")], ['nom' => trim($row['transporteur'], " \t\n\r")]);
                $produit = $this->produitRepository->produit()->firstOrCreate(['libelle' => trim($row['produit'], " \t\n\r")], ['libelle' => trim($row['produit'], " \t\n\r")]);
                $destination = $this->destinationRepository->destination()->firstOrCreate(['libelle' => trim($row['destination'], " \t\n\r")], ['libelle' => trim($row['destination'], " \t\n\r")]);
                $poid_net = $row['poids_net'];
                if ($poid_net == 0) {
                    $quantite = 0;
                } elseif ($poid_net > 0) {
                    if (Str::contains($produit->libelle, ['VRAC','CHF'])) {
                        $quantite = $poid_net / 1000;

                    } else {
                        if ($poid_net < 1000) {
                            $quantite = $poid_net / 1000 ;
                        } else {
                            $quantite = round($poid_net / 1000, 0, PHP_ROUND_HALF_UP);
                        }
                    }
                    $qte_bl = $quantite;
                }
                if($row['n0_bl'] == '' || $row['n0_bl'] == NULL){
                   $numBL = Str::random(10);

                }else{
                    $numBL = $row['n0_bl'];
                }
                $quantite = $poid_net < 1000 ? $poid_net / 1000 : round($poid_net / 1000, 0, PHP_ROUND_HALF_UP);
                $data = [
                    'date_pesee' =>$date_pesee,
                    'numero_bl' => $numBL,
                    'quantite' => $quantite,
                    'qte_pesee' => $quantite,
                    'qte_bl' => $qte_bl,
                    'poids_entree' => $row['poids_p1'],
                    'poids_sortie' => $row['poids_p2'],
                    'poids_net' => $poid_net,
                    'date_entree' => Date::excelToDateTimeObject($row['date_p1'])->format('Y-m-d'),
                    'heure_pesee_entree' => Date::excelToDateTimeObject($row['heure_p1'])->format('H:i:s'),
                    'date_sortie' => Date::excelToDateTimeObject($row['date_p2'])->format('Y-m-d'),
                    'heure_pesee_sortie' => Date::excelToDateTimeObject($row['heure_p2'])->format('H:i:s'),
                    'distributeur_id' => $distributeur->id,
                    'transporteur_id' => $transporteur->id,
                    'produit_id' => $produit->id,
                    'destination_id' => $destination->id,
                ];

                //verifions si le numero bl existe deja
               $pesee = $this->peseeRepository->pesee()->updateOrCreate(['numero_bl'=>$numBL],$data);
               if ($pesee){
                   $driver = [
                            'chauffeur' => $row['chauffeur'],
                            'immatriculation' => $row['vehicule'],
                            'contact' => $row['observations'],
                            'type_vehicule_id' => 1,
                            'pesee_id' => $pesee->id,
                        ];
                   $this->vehiculeRepository->vehicule()->create($driver);
                   $this->subventionRepository->subvention()->updateOrCreate(['pesee_id' => $pesee->id],['pesee_id' => $pesee->id]);
               }

//                $presence = $this->peseeRepository->pesee()->where('numero_bl', '=', $numBL)->first();
//                if ($presence) {
//                    $toUpdate= Arr::except($data, 'numero_bl');
//                    $presence->update($toUpdate);
//                } else {
//                    if ($pesse = $this->peseeRepository->pesee()->create($data)) {
//
//
//                        create(['pesee_id' => $pesse->id]);
//
//                    }
//                }
                DB::commit();
            } else {
                DB::rollBack();
            }
        }
    }


    /**
     * @return int
     */
    public function chunkSize(): int
    {
        return 1000;
    }
}
