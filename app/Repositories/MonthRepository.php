<?php


namespace App\Repositories;


use App\Month;

class MonthRepository
{

    /**
     * @var Month
     */
    private $month;

    public function __construct(Month $month)
    {

        $this->month = $month;
    }

    public function month()
    {
        return $this->month->newQuery();
    }
}
