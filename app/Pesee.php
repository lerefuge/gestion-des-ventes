<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pesee extends Model
{
    use SoftDeletes;
    protected $guarded =['id'];
    protected $dates =['date_pesee','date_sortie','date_entree'];


    public function transporteur()
    {
        return $this->belongsTo(Transporteur::class);
    }

    public function distributeur()
    {
        return $this->belongsTo(Distributeur::class);
    }

    public function produit()
    {
        return $this->belongsTo(Produit::class);
    }

    public function destination()
    {
        return $this->belongsTo(Destination::class);
    }

    public function charges()
    {
        return $this->hasOne(Charge::class);
    }
    public function vehicule()
    {
        return $this->hasOne(Vehicule::class);
    }

    public function subvention()
    {
        return $this->hasOne(Subvention::class);
    }

    public function tonnage()
    {
        return $this->hasOne(Tonnage::class);
    }

    public  function expedies()
    {
        return self::belongsTo(Destination::class)->sum('qte_bl');
    }
}
