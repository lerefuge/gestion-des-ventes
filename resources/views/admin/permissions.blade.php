@extends('template.theme')


@section('font-title')

@endsection
@push('scripts')



@endpush


@section('content-body')

    <div class="row">
        <div class="col-md-5">
            @if($permission->id)
            {!! Form::open(['url'=>route('admin.enregistrement-permission-edit',$permission->id),'class'=>'form-horizontal','method'=>'PUT']) !!}
                @method('put')
            @else
            {!! Form::open(['url'=>route('admin.enregistrement-permissions'),'class'=>'form-horizontal']) !!}
            @endif
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title"> Enregistrement des permissions </h2>

                </header>
                <div class="card-body">
                    <div class="form-group row">
                        <label class="col-sm-4 control-label text-sm-right pt-2"> permissions <span
                                class="required">*</span></label>
                        <div class="col-sm-8">
                            {!! Form::text('name',$permission->name,['class'=>'form-control', 'required'=>true, 'autocomplete'=>'off','id'=>'','title'=>"Enregistrer  un permission",'placeholder'=>"Enregistrer un permission"]) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 control-label text-sm-right pt-2"> Guard <span
                                class="required">*</span></label>
                        <div class="col-sm-8">
                            {!! Form::text('guard',$permission->guard_name,['class'=>'form-control', 'required'=>true, 'autocomplete'=>'off','id'=>'','title'=>"Guard",'placeholder'=>"Guard"]) !!}
                        </div>
                    </div>

                </div>
                <footer class="card-footer">
                    <div class="row justify-content-end">
                        <div class="col-sm-9">
                            @if($permission->id)
                                <button type="submit" class="btn btn-primary" data-toggle="confirmation">Mettre à jour</button>
                            @else
                                <button type="submit" class="btn btn-primary" data-toggle="confirmation">Enregistrer</button>
                                <button type="reset" class="btn btn-default">Effacer</button>
                            @endif
                        </div>
                    </div>
                </footer>
            </section>
            {!! Form::close() !!}

        </div>
        <div class="col-md-7">
            <div class="row">
                <div class="col">
                    <section class="card">
                        <header class="card-header">
                            <div class="card-actions">
                                <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                            </div>

                            <h2 class="card-title">Liste des permissions</h2>
                        </header>
                        <div class="card-body">
                            <table class="table table-bordered table-striped mb-0 datatable-default">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Guard</th>
                                    <th>Autorisations</th>
                                    <th class="">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($permissions as $permission)
                                    <tr>
                                        <td>{{ $loop->index + 1 }}</td>
                                        <td>{{ $permission->guard_name }}</td>
                                        <td>{{ $permission->name }}</td>
                                        <td class="flex text-center">
                                            <a href="{{ route('admin.enregistrement-permission-edit',$permission) }}" class="on-default edit-row" data-toggle="confirmation"><i
                                                    class="fas fa-pencil-alt"></i></a>
{{--                                            <a href="{{ route('admin.enregistrement-permission-permissions',$permission) }}" class=""><i--}}
{{--                                                    class="fas fa-user-edit"></i></a>--}}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>





@endsection
