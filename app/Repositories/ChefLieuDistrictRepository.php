<?php


namespace App\Repositories;


use App\ChefLieuDistrict;

class ChefLieuDistrictRepository
{
    /**
     * @var ChefLieuDistrict
     */
    private $chefLieuDistrict;

    /**
     * ChefLieuDistrictRepository constructor.
     * @param ChefLieuDistrict $chefLieuDistrict
     */
    public function __construct(ChefLieuDistrict $chefLieuDistrict)
    {

        $this->chefLieuDistrict = $chefLieuDistrict;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function chefLieuDistrict()
    {
        return $this->chefLieuDistrict->newQuery();
    }
}
