<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBudgetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('budgets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('officiel')->nullable();
            $table->string('challenge')->nullable();
            $table->unsignedInteger('produit_id')->nullable();
            $table->unsignedInteger('entite_id')->nullable();
            $table->unsignedInteger('month_id')->nullable();
            $table->unsignedInteger('annee_id')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('budgets');
    }
}
