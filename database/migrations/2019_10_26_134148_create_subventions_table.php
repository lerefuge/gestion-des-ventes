<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubventionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subventions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->float('prix_base')->default(0)->nullable();
            $table->float('prix_negocie')->default(0)->nullable();
            $table->float('montant_supporte')->default(0)->nullable();
            $table->string('numero_bon_caisse')->nullable();
            $table->unsignedInteger('type_subvention_id')->nullable();
            $table->unsignedInteger('pesee_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subventions');
    }
}
