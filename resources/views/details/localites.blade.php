@extends('template.theme')


@section('font-title')

@endsection
@push('scripts')
    @include('_dataTable\details\_js_script',['data_table_url'=>$data_table_url])
    <script type="text/javascript">
        (function () {

            'use strict';
            // Select 2 Fields
            $('select[data-plugin-selectTwo]').on('change', function () {
                $(this).valid();
            });


        }).apply(this, [jQuery]);
        $(document).ready(function () {

            $('.selectPicker').select2({
                placeholder: {
                        id: '', // the value of the option
                        text: 'Selectionnez un élement dans la liste'
                    },
                                    placeholder: "",
               // header: '',
                liveSearch: true,
                width: 'resolve'
            });
            $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
                localStorage.setItem('activeTab', $(e.target).attr('href'));
            });
            var activeTab = localStorage.getItem('activeTab');
            if (activeTab) {
                $('#navTabs a[href="' + activeTab + '"]').tab('show');
            }
        });
    </script>

@endpush


@section('content-body')
    <div class="row">
        <div class="col-6 offset-3">
            <div class="tabs">
                <ul class="nav nav-tabs" id="navTabs">

                    <li class="nav-item">
                        <a class="nav-link" href="#monthly" data-toggle="tab">Mensuel</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#yearly" data-toggle="tab">Annuel</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="card-subtitle">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>

                    <div id="monthly" class="tab-pane active">
                        {!! Form::open(['url'=>route('detail.localite'),'class'=>'daily-form',"id"=>'daily-form']) !!}
                        <section class="card">
                            <header class="card-header">
                                <div class="card-actions">
                                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                    <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                                </div>

                                <h2 class="card-title"> Consulter les details par localités </h2>
                                <p class="card-subtitle">
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif
                                    </p>
                            </header>
                            <div class="card-body">
                                <div class="form-row">
                                    <div class="col ">
                                        <div class="form-group">
                                            <label class="col-form-label" for="localite_ids">Localite </label>
                                            <div class="input-group">
														<span class="input-group-prepend">
															<span class="input-group-text">
																<i class="fas fa-map-marked-alt"></i>
															</span>
														</span>
                                                {!! Form::select('localite_ids[]', $localites->pluck('libelle','id'),
                                               request('localite_ids'), ['id'=>'','class'=>'form-control selectPicker
                                                 show-tick',' data-live-search' => true, 'multiple', 'data-live-search-normalize'=>true,'title'=>'Selectionner une localité',
                                               ]) !!}
                                                @error('localite_ids')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror

                                            </div>
                                            @error('date_month')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col ">
                                        <div class="form-group">
                                            <label class="col-form-label" for="date_month">Date </label>
                                            <div class="input-group">
														<span class="input-group-prepend">
															<span class="input-group-text">
																<i class="fas fa-calendar-alt"></i>
															</span>
														</span>
                                                <input type="text" name="date_month"
                                                       value="{{ old('date_month',request('date_month')) }}"
                                                       id="date_month" class="form-control" autocomplete="off"
                                                       required
                                                       data-plugin-datepicker
                                                       data-date-view="months"
                                                       data-date-view-mode="months"
                                                       data-date-min-view-mode="months"
                                                       data-date-format="mm/yyyy"
                                                       data-plugin-options='{"orientation":"bottom","autoclose":true}'
                                                >
                                            </div>
                                            @error('date_month')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <footer class="card-footer">
                                <div class="row ">
                                    <div class="col-sm-9 align-content-md-end">
                                        <input type="hidden" name="case" value="monthly">
                                        <button type="submit" class="btn btn-primary"><i
                                                class="fas search"></i> Rechercher
                                        </button>


                                    </div>
                                </div>
                            </footer>
                        </section>
                        {!! Form::close() !!}
                    </div>

                                       <div id="yearly" class="tab-pane ">
                                           {!! Form::open(['url'=>route('detail.localite'),'class'=>'yearly-form',"id"=>'yearly-form']) !!}
                                           <section class="card">
                                               <header class="card-header">
                                                    <div class="card-actions">
                                                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                                                    </div>

                                                    <h2 class="card-title"> Consulter Ventes </h2>
                                                    <p class="card-subtitle">
                                                    @if ($errors->any())
                                                        <div class="alert alert-danger">
                                                            <ul>
                                                                @foreach ($errors->all() as $error)
                                                                    <li>{{ $error }}</li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                        @endif
                                                        </p>
                                                </header>
                                                <div class="card-body">
                                                    <div class="form-row">
                                                        <div class="col ">
                                                                <label class="col-form-label" for="localite_ids">Localite </label>
                                                                <div class="input-group">
                                                                            <span class="input-group-prepend">
                                                                                <span class="input-group-text">
                                                                                    <i class="fas fa-map-marked-alt"></i>
                                                                                </span>
                                                                            </span>
                                                                    {!! Form::select('localite_ids[]', $localites->pluck('libelle','id'),
                                                                   request('localite_ids'), ['id'=>'localite_ids','class'=>'form-control selectPicker
                                                                     show-tick',' data-live-search' => true, 'multiple', 'data-live-search-normalize'=>true,'title'=>'Selectionner une localité',
                                                                   'style'=>'width:80%']) !!}
                                                                    @error('localite_ids')
                                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                                    @enderror

                                                                </div>

                                                        </div>
                                                        <div class="col">
                                                            <div class="form-group">
                                                                <label class="col-form-label" for="date_year">Date </label>
                                                                <div class="input-group">
                    														<span class="input-group-prepend">
                    															<span class="input-group-text">
                    																<i class="fas fa-calendar-alt"></i>
                    															</span>
                    														</span>
                                                                    <input type="text" name="date_year"
                                                                           value="{{ old('date_year',request('date_year')) }}"
                                                                           id="date_year" class="form-control" autocomplete="off"
                                                                           required
                                                                           data-plugin-datepicker
                                                                           data-date-view="years"
                                                                           data-date-view-mode="years"
                                                                           data-date-min-view-mode="years"
                                                                           data-date-format="yyyy"
                                                                           data-plugin-options='{"orientation":"bottom","autoclose":true}'
                                                                    >
                                                                </div>
                                                                @error('date_year')
                                                                <div class="alert alert-danger">{{ $message }}</div>
                                                                @enderror
                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>
                                                <footer class="card-footer">
                                                    <div class="row ">
                                                        <div class="col-sm-9 align-content-md-end">
                                                            <input type="hidden" name="case" value="yearly">
                                                            <button type="submit" class="btn btn-primary"><i
                                                                    class="fas search"></i> Rechercher
                                                            </button>


                                                        </div>
                                                    </div>
                                                </footer>
                                            </section>
                                            {!! Form::close() !!}
                                        </div>

                </div>
            </div>
        </div>

    </div>
@include('_dataTable.details.files',['data_table_url'=>$data_table_url])
@endsection
