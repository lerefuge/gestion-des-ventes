<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Distributeur extends Model
{
    protected $guarded=['id'];

    use SoftDeletes;

    public function setNomAttribute($value)
    {
        return $this->attributes['nom'] = STR::upper($value);
    }

    public function setReferentAttribute($value)
    {
        return $this->attributes['referent'] = STR::upper($value);
    }
}
