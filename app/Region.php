<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Region extends Model
{
    protected $guarded = ['id'];

    public function setLibelleAttribute($value)
    {
        return $this->attributes['libelle'] = STR::upper($value);
    }

    public function chefLieu()
    {
        return $this->belongsTo(ChefLieuDistrict::class,'chef_lieu_id','id');
    }

    public function pesees()
    {
        return $this->hasManyThrough(Pesee::class,Destination::class);
    }

    public function localites()
    {
        return $this->hasMany(Destination::class);
    }
}
