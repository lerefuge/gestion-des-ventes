<?php


namespace App\Repositories;


use App\Produit;

class ProduitRepository
{
    /**
     * @var Produit
     */
    private $produit;

    /**
     * ProduitRepository constructor.
     * @param Produit $produit
     */
    public function __construct(Produit $produit)
    {
        $this->produit = $produit;
    }

    public function produit()
    {
        return $this->produit->newQuery();
    }
}
