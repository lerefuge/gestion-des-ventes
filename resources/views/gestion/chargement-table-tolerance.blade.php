@extends('template.theme')


@section('font-title')

@endsection
@push('scripts')



@endpush


@section('content-body')

    <div class="row">
        <div class="col-md-6 offset-md-3">

            {!! Form::open(['url'=>route('gestion.load-table-tolerance'),'class'=>'form-horizontal','files'=>true]) !!}

            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title"> Chargement de table de tolérance </h2>
                    <p class="card-subtitle">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                    </p>
                </header>
                <div class="card-body">
                    <div class="row form-group">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="col-form-label" for="file">Importer le fichier</label>

                                    <input type="file"  id="file" data-plugin-file name="table_tolerance" class="form-control-file">
                                @error('file')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                    </div>



                </div>
                <footer class="card-footer">
                    <div class="row justify-content-end">
                        <div class="col-sm-9">

                                <button type="submit" class="btn btn-primary" data-toggle="confirmation"><i class="fas fileupload"></i> Charger le fichier</button>
                                <button type="reset" class="btn btn-default">Effacer</button>

                        </div>
                    </div>
                </footer>
            </section>
            {!! Form::close() !!}

        </div>
    </div>





@endsection
