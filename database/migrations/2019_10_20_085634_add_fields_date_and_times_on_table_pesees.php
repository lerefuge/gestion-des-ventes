<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsDateAndTimesOnTablePesees extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pesees', function (Blueprint $table) {
            $table->string('immatriculation')->nullable();
            $table->string('poids_entree')->nullable();
            $table->string('poids_sortie')->nullable();
            $table->string('poids_net')->nullable();
            $table->string('date_entree')->nullable();
            $table->string('date_sortie')->nullable();
            $table->string('heure_pesee_entree')->nullable();
            $table->string('heure_pesee_sortie')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pesees', function (Blueprint $table) {
            $table->dropColumn(
                [
                    'immatriculation',
                    'poids_entree',
                    'poids_sortie',
                    'date_entree',
                    'date_sortie',
                    'heure_pesee_entree',
                    'heure_pesee_sortie'
                ]
            );
        });
    }
}
