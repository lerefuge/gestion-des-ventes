<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddQteBlFieldsOnTablePesees extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pesees', function (Blueprint $table) {
            $table->float('qte_bl')->nullable();
            $table->float('qte_pesee')->nullable();
            $table->float('ecart')->nullable();
            $table->string('heure_debut_chargement')->nullable();
            $table->string('heure_fin_chargement')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pesees', function (Blueprint $table) {
            $table->dropColumn(['qte_bl','qte_pesee','ecart','heure_debut_chargement','heure_fin_chargement']);
        });
    }
}
