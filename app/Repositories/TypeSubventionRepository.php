<?php


namespace App\Repositories;


use App\TypeSubvention;
use App\Vehicule;

class TypeSubventionRepository
{
    /**
     * @var TypeSubvention
     */
    private $typeSubvention;


    /**
     * TypeSubventionRepository constructor.
     * @param TypeSubvention $typeSubvention
     */
    public function __construct(TypeSubvention $typeSubvention)
    {

        $this->typeSubvention = $typeSubvention;
    }

    public function typeSubvention()
    {
        return $this->typeSubvention->newQuery();
    }
}
