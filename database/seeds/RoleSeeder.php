<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert(['name'=>'Admin','guard_name'=>'web']);
        DB::table('roles')->insert(['name'=>'Superviseur','guard_name'=>'web']);
        DB::table('roles')->insert(['name'=>'Guest','guard_name'=>'web']);
    }
}
