<?php

namespace App\Http\Controllers\DataTables;

use App\Http\Controllers\Controller;
use App\Repositories\PeseeRepository;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Yajra\DataTables\Facades\DataTables;

class DataTableDetailsController extends Controller
{
    /**
     * @var PeseeRepository
     */
    private $peseeRepository;
    /**
     * @var Builder
     */
    private $builder;

    /**
     * DataTableDetailsController constructor.
     * @param PeseeRepository $peseeRepository
     * @param Builder $builder
     */
    public function __construct(PeseeRepository $peseeRepository)
    {
        $this->middleware('auth');
        $this->peseeRepository = $peseeRepository;
    }

    public function detailMensuelle(Request $request)
    {
        $destination = $request->localite_ids;
        $debut = date('Y-m', $request->debut);
        $days = Carbon::parse($debut)->daysInMonth;
        $query = $this->peseeRepository->pesee()

        ->selectRaw('pesees.date_pesee,produits.libelle AS qualite,destinations.libelle AS destination')
            ->whereBetween('pesees.date_pesee', ["{$debut}-01", "{$debut}-{$days}"])
            ->join('produits', 'produits.id', '=', 'pesees.produit_id')
            ->join('destinations', 'destinations.id', '=', 'pesees.destination_id')
            ->groupBy('pesees.date_pesee')
            ->groupBy('produits.libelle')
            ->groupBy('destinations.libelle')
            ->addSelect(DB::raw('SUM(qte_bl) AS expedies'))
            ->orderBy('pesees.date_pesee')
            ;
        if (!is_null($request->localite_ids)) {
            $query = $query->whereIn('pesees.destination_id', $destination);
        }
        $query =  $query->get();
        $datatable = DataTables::collection($query)
        ->addColumn('expedies', function ($q) {
            return $q->expedies;
        })
        ->addColumn('qualite', function ($q) {
            return $q->qualite;
        })
        ->addColumn('date_pesee', function ($q) {
            return $q->date_pesee->format('d/m/Y');
        })
       ;
        return $datatable->make(true);
    }

    public function detailAnnuelle(Request $request)
    {
        //dd($request->all());
        $destination = $request->localite_ids;
        $year = $request->date_year ?? now()->format('Y');
        $start_year = "01-01-" . $year;
        $end_year = "31-12-" . $year;
        $query = $this->peseeRepository->pesee()
        ->selectRaw('pesees.date_pesee,produits.libelle AS qualite,destinations.libelle AS destination')
            ->whereBetween('pesees.date_pesee', ["{$start_year}", "{$end_year}"])
            ->join('produits', 'produits.id', '=', 'pesees.produit_id')
            ->join('destinations', 'destinations.id', '=', 'pesees.destination_id')
            ->groupBy('pesees.date_pesee')
            ->groupBy('produits.libelle')
            ->groupBy('destinations.libelle')
            ->addSelect(DB::raw('SUM(qte_bl) AS expedies'))
            ->orderBy('pesees.date_pesee')
            ;
        if (!is_null($request->localite_ids)) {
            $query = $query->whereIn('pesees.destination_id', $destination);
        }
        $query =  $query->get();
        $datatable = DataTables::collection($query)
        ->addColumn('expedies', function ($q) {
            return $q->expedies;
        })
        ->addColumn('qualite', function ($q) {
            return $q->qualite;
        })
        ->addColumn('date_pesee', function ($q) {
            return $q->date_pesee->format('d/m/Y');
        })

       ;
        return $datatable->make(true);
    }
}
