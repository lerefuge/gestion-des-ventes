@section('css-libs')
    <style>
        tfoot > tr > th > input {
            width: 120px;
        }
    </style>
@endsection

@push('scripts')

    @include('expedition._includes._js_script',['data_table_url'=>$data_table_url])
    <script type="text/javascript">
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });
    </script>
@endpush
<div class="row">
    <div class="col">
        <section class="card">
            <header class="card-header">
                <div class="card-actions">
                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                    <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                </div>
                <h2 class="card-title">{{ $periode }}
                    <sup class="text-danger">*</sup></h2>
                <p class="card-subtitle text-danger">
                    NB: Sous réserve, des entrées comptabilisées
                </p>
            </header>
            <div class="card-body table-responsive">
                @include('expedition._includes._table')
            </div>
        </section>
    </div>
</div>
<div class="row">
    <div class="col-md-6 pull-right">
        <section class="card">
            <header class="card-header">
                <div class="card-actions">
                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                    <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                </div>

                <h2 class="card-title">Expédié par qualité
                    <sup class="text-danger">*</sup></h2>
                <p class="card-subtitle text-danger">
                    NB: Sous réserve, des entrées comptabilisées
                </p>
            </header>
            <div class="card-body">
                <table class="table table-bordered small table-striped table-condensed datatable-default mb-0 ">
                    <thead>
                    <tr class="small">
                        <th>#</th>
                        <th>Ciment</th>
                        <th>Quantité</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($expedies as $vendu)
                        <tr>
                            <td>{{ $loop->index +1 }}</td>
                            <td>{{$vendu->libelle}}</td>
                            <td class="text-center">{{  number_format($vendu->livre,2,',',' ')}}</td>
                        </tr>

                    @endforeach
                    <tr>
                        <td colspan="2" class="text-right">Total</td>
                        <td class="text-center">{{ number_format($expedies->sum('livre'),2,',',' ') }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </section>
    </div>
</div>
