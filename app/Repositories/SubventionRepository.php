<?php


namespace App\Repositories;


use App\Subvention;
use App\TypeSubvention;
use App\Vehicule;

class SubventionRepository
{

    /**
     * @var Subvention
     */
    private $subvention;

    /**
     * SubventionRepository constructor.
     * @param Subvention $subvention
     */
    public function __construct(Subvention $subvention)
    {
        $this->subvention = $subvention;
    }

    public function subvention()
    {
        return $this->subvention->newQuery();
    }
}
