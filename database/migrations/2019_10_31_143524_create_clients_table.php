<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('matricule')->nullable()->index();
            $table->string('nom')->nullable();
            $table->string('tel')->nullable();
            $table->string('contact')->nullable();
            $table->unsignedInteger('region_id')->nullable();
            $table->unsignedInteger('destination_id')->nullable();
            $table->unsignedInteger('distributeur_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
