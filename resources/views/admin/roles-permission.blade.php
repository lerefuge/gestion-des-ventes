@extends('template.theme')


@section('font-title')

@endsection
@push('scripts')



@endpush


@section('content-body')

    <div class="row">
        <div class="col-md-5">
            @if($role->id)
            {!! Form::open(['url'=>route('admin.enregistrement-role-edit',$role->id),'class'=>'form-horizontal','method'=>'PUT']) !!}
                @method('put')
            @endif
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title"> Enregistrement des roles </h2>

                </header>
                <div class="card-body">
                    <div class="form-group row">
                        <label class="col-sm-4 control-label text-sm-right pt-2"> roles <span
                                class="required">*</span></label>
                        <div class="col-sm-8">
                            {!! Form::text('name',$role->name,['class'=>'form-control',  'readonly'=>true,'disabled'=>true,  'autocomplete'=>'off','id'=>'','title'=>"Enregistrer  un role",'placeholder'=>"Enregistrer un role"]) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 control-label text-sm-right pt-2"> Guard <span
                                class="required">*</span></label>
                        <div class="col-sm-8">
                            {!! Form::text('guard',$role->guard_name,['class'=>'form-control', 'readonly'=>true,'disabled'=>true,  'autocomplete'=>'off','id'=>'','title'=>"Guard",'placeholder'=>"Guard"]) !!}
                        </div>
                    </div>

                </div>
                <footer class="card-footer">
                    <div class="row justify-content-end">
                        <div class="col-sm-9">
                            @if($role->id)
                                <button type="submit" class="btn btn-primary" data-toggle="confirmation">Mettre à jour</button>

                            @endif
                        </div>
                    </div>
                </footer>
            </section>
            {!! Form::close() !!}

        </div>
        <div class="col-md-7">
            <div class="row">
                <div class="col">
                    <section class="card">
                        <header class="card-header">
                            <div class="card-actions">
                                <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                            </div>

                            <h2 class="card-title">Liste des roles</h2>
                        </header>
                        <div class="card-body">
                            <table class="table table-bordered table-striped mb-0 datatable-default">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Guard</th>
                                    <th>Roles</th>
                                    <th>Premission</th>

                                    <th class="">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($roles as $role)
                                    <tr>
                                        <td>{{ $loop->index + 1 }}</td>
                                        <td>{{ $role->guard_name }}</td>
                                        <td>{{ $role->name }}</td>
                                        <td>{{ $role->permissions->implode('name',', ') }}</td>
                                        <td class="flex text-center">
                                            <a href="{{ route('admin.enregistrement-role-edit',$role) }}" class="on-default edit-row" data-toggle="confirmation"><i
                                                    class="fas fa-pencil-alt"></i></a>
                                            <a href="{{ route('admin.enregistrement-role-permissions',$role) }}" class=""><i
                                                    class="fas fa-user-edit"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>





@endsection
