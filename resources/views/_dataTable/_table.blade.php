<table class="table small table-bordered table-condensed table-striped mb-0 " id="dataTableVente">
    <thead>
    <tr class="small">

        <th>N° BL</th>
        <th>Distributeur</th>
        <th>Transporteur</th>
        <th>Immat. Vehicule</th>
        <th>Qualité Ciment</th>
        <th style="width:50%">Quantité BL</th>
        <th>Quantité Pesee</th>
        <th>Date Entree</th>
        <th>Heure Entree</th>
        <th>Date Sortie</th>
        <th>Heure Sortie</th>
        <th>Destination</th>
        <th style="width:10%"></th>
    </tr>
    </thead>
    <tbody class="text-center"></tbody>
    <tfoot>
    <tr class="small">

        <th>N° BL</th>
        <th>Distributeur</th>
        <th>Transporteur</th>
        <th>Immat. Vehicule</th>
        <th>Qualité Ciment</th>
        <th>Quantité BL</th>
        <th>Quantité Pesee</th>
        <th>Date Entree</th>
        <th>Heure Entree</th>
        <th>Date Sortie</th>
        <th>Heure Sortie</th>
        <th>Destination</th>
        <th></th>
    </tr>
    <tr>
        <td colspan="11">Total</td>
        <td class="somme_vente text-center"></td>
    </tr>

    </tfoot>
</table>

