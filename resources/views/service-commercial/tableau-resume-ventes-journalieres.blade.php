@extends('template.theme')


@section('font-title')

@endsection

@section('css-libs')

@endsection
@push('scripts')

    <script type="text/javascript">
        (function () {

            'use strict';

            // basic

            // Select 2 Fields
            $('select[data-plugin-selectTwo]').on('change', function () {
                $(this).valid();
            });
            $('.selectPicker').select2({
                title: "Liste d'élement ...",
                placeholder: 'Selectionnez un élement dans la liste',
                liveSearch: true,allowClear: true,
                width: 'resolve'
            });

            $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
                localStorage.setItem('activeTab', $(e.target).attr('href'));
            });
            var activeTab = localStorage.getItem('activeTab');
            if (activeTab) {
                $('#navTabs a[href="' + activeTab + '"]').tab('show');
            }

        }).apply(this, [jQuery]);

    </script>
@endpush


@section('content-body')
<div class="row">
    <div class="col-8 offset-2">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
</div>
                   <div class="col-6 offset-3">

                    <div class="tabs">
                        <ul class="nav nav-tabs" id="navTabs">

                           <li class="nav-item">
                                <a class="nav-link current" href="#monthly" data-toggle="tab">Mois</a>
                            </li>
                        </ul>
                        <div class="tab-content">

                            <div id="monthly" class="tab-pane active">
                                {!! Form::open(['url'=>route('commercial.tableau-ventes-journaliere'),'class'=>'monthly-form',"id"=>'monthly-form']) !!}
                                <section class="card">
                                    <header class="card-header">
                                        <div class="card-actions">
                                            <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                            <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                                        </div>

                                        <h2 class="card-title"> Consulter Ventes </h2>
                                        <p class="card-subtitle">

                                            </p>
                                    </header>
                                    <div class="card-body">
                                        <div class="form-row">

                                            <div class="col">
                                                <div class="form-group">
                                                    <label class="col-form-label" for="date_year">Date </label>
                                                    <div class="input-group">
														<span class="input-group-prepend">
															<span class="input-group-text">
																<i class="fas fa-calendar-alt"></i>
															</span>
														</span>
                                                        <input type="text" name="date_month"
                                                               value="{{ old('date_month',request('date_month')) }}"
                                                               id="date_month" class="form-control" autocomplete="off"
                                                               required
                                                               data-plugin-datepicker
                                                               data-date-view="months"
                                                               data-date-view-mode="months"
                                                               data-date-min-view-mode="months"
                                                               data-date-format="mm/yyyy"
                                                               data-plugin-options='{"orientation":"bottom","autoclose":true}'
                                                        >
                                                    </div>
                                                    @error('date_month')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>


                                        </div>
                                        {{-- <div class="row">
                                            <div class="col">

                                                <label class=" control-label"> Zones </label>
                                                {!! Form::select('zone_id[]',$zones->pluck('libelle','id'),request('zone_id'),['class'=>'form-control selectPicker', 'required'=>true,'multiple'=>true,'style'=>'width:98%'
                                                         ]
                                                          ) !!}

                                            </div>
                                        </div> --}}
                                        <hr>
                                    </div>
                                    <footer class="card-footer">
                                        <div class="row ">
                                            <div class="col-sm-9 align-content-md-end">
                                                <input type="hidden" name="case" value="yearly">
                                                <button type="submit" class="btn btn-primary"><i
                                                        class="fas search"></i> Rechercher
                                                </button>


                                            </div>
                                        </div>
                                    </footer>
                                </section>
                                {!! Form::close() !!}
                            </div>

                        </div>
                    </div>
                </div>
<hr>
@inject('expedition', 'App\Repositories\PeseeRepository')

<table class="table small table-striped table-bordered table-condensed  mb-0 " >
    <tbody>
      <tr class="text-center">
        <th scope="col">MOIS</th>
        <th scope="col" colspan="4">SAC&nbsp;</th>
        <th scope="col" colspan="4">VRAC&nbsp;</th>
        
        <th scope="col">&nbsp;TOTAL</th>
      </tr>
      <tr class="text-center">
      <th scope="row">  </th>
        <td>&nbsp;CPJ 32.5</td>
        <td>CPJ 42.5&nbsp;</td>
        <td>&nbsp;CPA ROBUSTO</td>
        <td>CHF&nbsp;</td>
        <td>&nbsp;CPJ 32.5</td>
        <td>CPJ 42.5&nbsp;</td>
        <td>CPA ROBUSTO&nbsp;</td>
        <td>CHF&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      @php
          $cpj_32_sac_mois = $cpj_42_sac_mois=  $cpa_sac_mois = $chf_sac_mois = $cpj_32_vrac_mois = $cpj_42_vrac_mois=  $cpa_vrac_mois = $chf_vrac_mois = 0;
      @endphp
      @for($j=1;$j<=$nbr_jours;$j++)
      @php
          $jour = $j<10 ? "0{$j}" : $j;

          $cpj_32_sac_jour = $expedition->_expedition($month,$jour,[2])->sum(); // 32.5
          $cpj_32_sac_mois += $cpj_32_sac_jour;

          $cpj_42_sac_jour = $expedition->_expedition($month,$jour,[1])->sum(); // 42.5
          $cpj_42_sac_mois +=$cpj_42_sac_jour;

          $cpa_sac_jour = $expedition->_expedition($month,$jour,[3])->sum(); // cpa
          $cpa_sac_mois += $cpa_sac_jour;

          $chf_sac_jour = $expedition->_expedition($month,$jour,[0])->sum(); // chf_sac
          $chf_sac_mois += $chf_sac_jour;


          $cpj_32_vrac_jour = $expedition->_expedition($month,$jour,[0])->sum(); // 32.5
          $cpj_32_vrac_mois += $cpj_32_vrac_jour;

          $cpj_42_vrac_jour = $expedition->_expedition($month,$jour,[4])->sum(); // 42.5
          $cpj_42_vrac_mois +=$cpj_42_vrac_jour;

          $cpa_vrac_jour = $expedition->_expedition($month,$jour,[5])->sum(); // cpa
          $cpa_vrac_mois += $cpa_vrac_jour;

          $chf_vrac_jour = $expedition->_expedition($month,$jour,[6])->sum(); // chf_sac
          $chf_vrac_mois += $chf_vrac_jour;



          $expedie_sac_mois = $cpj_32_sac_mois + $cpj_42_sac_mois + $cpa_sac_mois + $chf_sac_mois ;
          $expedie_vrac_mois = $cpj_32_vrac_mois + $cpj_42_vrac_mois + $cpa_vrac_mois + $chf_vrac_mois ;
          $expedie_mesuel =  $expedie_sac_mois + $expedie_vrac_mois ;

      @endphp
      <tr class="text-center">
        <th scope="row"> {{ "{$jour}/{$month}/{$year}"  }}</th>
        <td>{{  number_format($cpj_32_sac_jour,2,',',' ') }}</td>
        <td>{{ number_format($cpj_42_sac_jour,2,',',' ') }}</td>
        <td>{{ number_format($cpa_sac_jour,2,',',' ') }}</td>
        <td>{{ number_format( $chf_sac_jour,2,',',' ') }}</td>
        <td>{{  number_format($cpj_32_vrac_jour,2,',',' ') }}</td>
        <td>{{ number_format($cpj_42_vrac_jour,2,',',' ') }}</td>
        <td>{{ number_format($cpa_vrac_jour,2,',',' ') }}</td>
        <td>{{ number_format( $chf_vrac_jour,2,',',' ') }}</td>
        <td>{{ number_format($expedition->_expedition($month,$jour,[1,2,3,4,5,6])->sum(),2,',',' ') }}</td> 
      </tr>
      @endfor
      <tr class="text-center info">
        <th scope="row">&nbsp;TOTAL</th>
       
        <td>{{ number_format($cpj_32_sac_mois,2,',',' ') }}</td>
        <td>{{ number_format($cpj_42_sac_mois,2,',',' ') }}</td>
        <td>{{ number_format($cpa_sac_mois,2,',',' ') }}</td>
        <td>{{ number_format($chf_sac_mois,2,',',' ') }}</td>
        <td>{{ number_format($cpj_32_vrac_mois,2,',',' ') }}</td>
        <td>{{ number_format($cpj_42_vrac_mois,2,',',' ') }}</td>
        <td>{{ number_format($cpa_vrac_mois ,2,',',' ') }}</td>
        <td>{{ number_format($chf_vrac_mois,2,',',' ') }}</td>
        <td>{{ number_format($expedie_mesuel,2,',',' ') }}</td> 
      </tr>
      <tr class="text-center success">
        <th scope="row">TOTAL&nbsp;</th>
        <td colspan="4">{{ number_format($expedie_sac_mois,2,',',' ') }}</td>
        <td colspan="4">{{ number_format($expedie_vrac_mois,2,',',' ') }}</td>      
        
        <td>{{ number_format($expedie_mesuel,2,',',' ') }}</td>
      </tr>
      <tr class="text-center warning">
        <th scope="row">TOTAL&nbsp;</th>
        <td colspan="8">{{ number_format($expedie_mesuel,2,',',' ') }}</td>
        
        <td>&nbsp;</td>
      </tr>
    </tbody>
  </table>
  

@endsection
