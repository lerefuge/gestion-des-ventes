<?php

namespace App\Imports;

use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use App\Repositories\PeseeRepository;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
//, WithChunkReading, ShouldQueue
class TablePeseeImport implements ToCollection, WithHeadingRow, WithColumnFormatting
{
    /**
     * @var PeseeRepository
     */
    private $peseeRepository;

    public function __construct(PeseeRepository $peseeRepository)
    {

        $this->peseeRepository = $peseeRepository;
    }
//    public function chunkSize(): int
//    {
//        return 1000;
//    }
    /**
     * @param Collection $collection
     * @throws Exception
     */
    public function collection(Collection $collection)
    {
        DB::beginTransaction();
        foreach ($collection as $row) {
            if ($row->filter()->isNotEmpty()) {
                $numero_bl = $row['n0_bl'];
                $values =[
                    'date_pesee' => Date::excelToDateTimeObject($row['date'])->format('Y-m-d'),
                    'qte_bl' => $row['quantite_bl'],
                    'qte_pesee' => $row['quantite_pesee'],
                    'heure_debut_chargement' => Date::excelToDateTimeObject($row['heure_debut_chargement'])->format('H:i:s'),
                    'heure_fin_chargement' => Date::excelToDateTimeObject($row['heure_fin_de_chargement'])->format('H:i:s'),
                ];
                 $this->peseeRepository->pesee()->updateOrCreate(['numero_bl'=>$numero_bl],$values);
                    DB::commit();

            } else {
                DB::rollBack();
            }
        };

    }

    /**
     * @return array
     */
    public function columnFormats(): array
    {
        // TODO: Implement columnFormats() method.
    }
}
