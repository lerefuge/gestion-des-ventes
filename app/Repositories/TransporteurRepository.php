<?php


namespace App\Repositories;


use App\Transporteur;

class TransporteurRepository
{

    /**
     * @var Transporteur
     */
    private $transporteur;

    /**
     * TransporteurRepository constructor.
     * @param Transporteur $transporteur
     */
    public function __construct(Transporteur $transporteur)
    {

        $this->transporteur = $transporteur;
    }

    public function transporteur()
    {
        return $this->transporteur->newQuery();
    }
}
