<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Transporteur extends Model
{
    protected $guarded =['id'];

    public function setCodeAttribute($value)
    {
        return $this->attributes['code'] = STR::upper($value);
    }

    public function seNomAttribute($value)
    {
        return $this->attributes['nom'] = STR::upper($value);
    }
}
