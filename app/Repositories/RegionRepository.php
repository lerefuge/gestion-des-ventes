<?php


namespace App\Repositories;


use App\Region;

class RegionRepository
{
    /**
     * @var Region
     */
    private $region;

    /**
     * RegionRepository constructor.
     * @param Region $region
     */
    public function __construct(Region $region)
    {

        $this->region = $region;
    }

    public function region()
    {
        return $this->region->newQuery();
    }
}
