<?php


namespace App\Traits;


trait SnipetsTraits
{

    /**
     * @param $message
     * @param string $level
     * @param null $validator
     * @return \Illuminate\Http\RedirectResponse
     */
    private function _redirectWhenFail($message="Veuillez corriger le erreurs SVP !" , $level='info' ,$validator = null)
    {
        flash("{$message}", $level);
        $redirect = redirect()->back()->withInput();
        if ($validator)
            $redirect = $redirect->withErrors($validator)->withInput();
        return $redirect;
    }
}
