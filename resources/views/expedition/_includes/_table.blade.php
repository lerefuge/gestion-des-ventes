<table class="table small table-bordered table-condensed  mb-0 " id="dataTableExpeditions">
    <thead>
    <tr class="small">

        <th>N° BL</th>
        <th>Distributeur</th>
        <th>Transporteur</th>
        <th>Immat. Vehicule</th>
        <th>Qualité Ciment</th>
        <th>Quantité BL</th>
        <th>Quantité Pesee</th>
        <th>Ecart</th>
        <th>Date Entree</th>
        <th>Date Sortie</th>
        <th>Heure Entree</th>
        <th>Heure Sortie</th>
        <th>TPS Attente</th>
        <th>Début Chargement</th>
        <th>Fin Chargement</th>
        <th>TPS Chargement</th>
        <th></th>
    </tr>
    </thead>
    <tbody class="text-center"></tbody>
    <tfoot>
    <tr class="small">

        <th>N° BL</th>
        <th>Distributeur</th>
        <th>Transporteur</th>
        <th>Immat. Vehicule</th>
        <th>Qualité Ciment</th>
        <th>Quantité BL</th>
        <th>Quantité Pesee</th>
        <th>Ecart</th>
        <th>Date Entree</th>
        <th>Date Sortie</th>
        <th>Heure Entree</th>
        <th>Heure Sortie</th>
        <th>TPS Attente</th>
        <th>Début Chargement</th>
        <th>Fin Chargement</th>
        <th>TPS Chargement</th>
        <th></th>
    </tr>
    <tr>
        <td colspan="5">Total</td>
        <td  class="somme_vente text-left"></td>
        <td colspan="11" ></td>
    </tr>
    </tfoot>
</table>
