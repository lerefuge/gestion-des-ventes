@extends('template.theme')


@section('font-title')

@endsection
@push('scripts')



@endpush


@section('content-body')
{{--    <header class="page-header">--}}
{{--        <h2>{{ $page_header_title ?? '' }}</h2>--}}

{{--        --}}{{--        <div class="right-wrapper text-right">--}}
{{--        --}}{{--            <ol class="breadcrumbs">--}}
{{--        --}}{{--                <li>--}}
{{--        --}}{{--                    <a href="index.html">--}}
{{--        --}}{{--                        <i class="fas fa-home"></i>--}}
{{--        --}}{{--                    </a>--}}
{{--        --}}{{--                </li>--}}
{{--        --}}{{--                <li><span>Pages</span></li>--}}
{{--        --}}{{--                <li><span>Blank Page</span></li>--}}
{{--        --}}{{--            </ol>--}}

{{--        --}}{{--            <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>--}}
{{--        --}}{{--        </div>--}}
{{--        <hr>--}}
{{--    </header>--}}
    <div class="row">
        <div class="col-md-5">
            @if($produit->id)
            {!! Form::open(['url'=>route('admin.enregistrement-produit-edit',$produit->id),'class'=>'form-horizontal','method'=>'PUT']) !!}
                @method('put')
            @else
            {!! Form::open(['url'=>route('admin.enregistrement-produit'),'class'=>'form-horizontal']) !!}
            @endif
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title"> Enregistrement Qualité de ciment </h2>
                    <p class="card-subtitle">
                        Basic validation will display a label with the error after the form control.
                    </p>
                </header>
                <div class="card-body">
                    <div class="form-group row">
                        <label class="col-sm-4 control-label text-sm-right pt-2"> Qualité de ciment <span
                                class="required">*</span></label>
                        <div class="col-sm-8">
                            {!! Form::text('qualite',$produit->libelle,['class'=>'form-control', 'required'=>true, 'autocomplete'=>'off','id'=>'','title'=>"Enregistrer la qualité de ciment",'placeholder'=>"Enregistrer la qualité de ciment"]) !!}
                        </div>
                    </div>

                </div>
                <footer class="card-footer">
                    <div class="row justify-content-end">
                        <div class="col-sm-9">
                            @if($produit->id)
                                <button type="submit" class="btn btn-primary" data-toggle="confirmation">Mettre à jour</button>
                            @else
                                <button type="submit" class="btn btn-primary" data-toggle="confirmation">Enregistrer</button>
                                <button type="reset" class="btn btn-default">Effacer</button>
                            @endif
                        </div>
                    </div>
                </footer>
            </section>
            {!! Form::close() !!}

        </div>
        <div class="col-md-7">
            <div class="row">
                <div class="col">
                    <section class="card">
                        <header class="card-header">
                            <div class="card-actions">
                                <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                            </div>

                            <h2 class="card-title">Qualités Ciments</h2>
                        </header>
                        <div class="card-body">
                            <table class="table table-bordered table-striped mb-0 datatable-default">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Qualité ciment</th>

                                    <th class="">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($produits as $produit)
                                    <tr>
                                        <td>{{ $loop->index + 1 }}</td>
                                        <td>{{ $produit->libelle }}</td>
                                        <td class="flex text-center">
                                            <a href="{{ route('admin.enregistrement-produit-edit',$produit) }}" class="on-default edit-row" data-toggle="confirmation"><i
                                                    class="fas fa-pencil-alt"></i></a>
                                            <a href="#" class="on-default remove-row"><i
                                                    class="far fa-trash-alt"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>





@endsection
