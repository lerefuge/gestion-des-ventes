<?php


namespace App\Repositories;


use App\Tonnage;

class TonnageRepository
{
    /**
     * @var Tonnage
     */
    private $tonnage;

    /**
     * TonnageRepository constructor.
     * @param Tonnage $tonnage
     */
    public function __construct(Tonnage $tonnage)
    {

        $this->tonnage = $tonnage;
    }

    public function tonnage()
    {
        return $this->tonnage->newQuery();
    }

}
