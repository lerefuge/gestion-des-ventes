@extends('template.theme')


@section('font-title')

@endsection
@push('scripts')

    <script type="text/javascript">
        $(function () {
            $('.selectPicker').select2({
                title: "Liste d'élément ...",
                header: 'Selectionnez un élément dans la liste',
                liveSearch: true,
                width: 'resolve'
            });
        });

    </script>

@endpush


@section('content-body')

    {{--    <div class="row">--}}
    {{--        <div class="col-md-5">--}}
    {{--            @if($user->id)--}}
    {{--            {!! Form::open(['url'=>route('admin.enregistrement-user-edit',$user->id),'class'=>'form-horizontal','method'=>'PUT']) !!}--}}
    {{--                @method('put')--}}
    {{--            @else--}}
    {{--            {!! Form::open(['url'=>route('admin.enregistrement-user'),'class'=>'form-horizontal']) !!}--}}
    {{--            @endif--}}
    {{--            <section class="card">--}}
    {{--                <header class="card-header">--}}
    {{--                    <div class="card-actions">--}}
    {{--                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>--}}
    {{--                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>--}}
    {{--                    </div>--}}

    {{--                    <h2 class="card-title"> Enregistrement des utilisateur </h2>--}}

    {{--                </header>--}}
    {{--                <div class="card-body">--}}
    {{--                    @if ($errors->any())--}}
    {{--                        <div class="alert alert-danger">--}}
    {{--                            <ul>--}}
    {{--                                @foreach ($errors->all() as $error)--}}
    {{--                                    <li>{{ $error }}</li>--}}
    {{--                                @endforeach--}}
    {{--                            </ul>--}}
    {{--                        </div>--}}
    {{--                    @endif--}}
    {{--                    <div class="row form-group">--}}
    {{--                        <div class="col-8">--}}

    {{--                            <div class="form-group">--}}
    {{--                                <label for="name" class="control-label">NOM : </label>--}}
    {{--                                {!! Form::text('name',$user->nom,--}}
    {{--                                ['id'=>'nom','class'=>'form-control','required'=>'required','placeholder'=>'Nomuser du user']) !!}--}}
    {{--                                @error('nom')--}}
    {{--                                <div class="alert alert-danger">{{ $message }}</div>--}}
    {{--                                @enderror--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                        <div class="col">--}}

    {{--                            <div class="form-group">--}}
    {{--                                <label for="email" class="control-label">identifiant : </label>--}}
    {{--                                {!! Form::text('email',$user->email,--}}
    {{--                                ['id'=>'email','class'=>'form-control','placeholder'=>'Contacts du user']) !!}--}}
    {{--                                @error('email')--}}
    {{--                                <div class="alert alert-danger">{{ $message }}</div>--}}
    {{--                                @enderror--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                        <--}}
    {{--                    </div>--}}


    {{--                </div>--}}
    {{--                <footer class="card-footer">--}}
    {{--                    <div class="row justify-content-end">--}}
    {{--                        <div class="col-sm-9">--}}
    {{--                            @if($user->id)--}}
    {{--                                <button type="submit" class="btn btn-primary" data-toggle="confirmation">Mettre à jour</button>--}}
    {{--                            @else--}}
    {{--                                <button type="submit" class="btn btn-primary" data-toggle="confirmation">Enregistrer</button>--}}
    {{--                                <button type="reset" class="btn btn-default">Effacer</button>--}}
    {{--                            @endif--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                </footer>--}}
    {{--            </section>--}}
    {{--            {!! Form::close() !!}--}}

    {{--        </div>--}}
    {{--        <div class="col-md-7">--}}
    {{--            <div class="row">--}}
    {{--                <div class="col">--}}
    {{--                    <section class="card">--}}
    {{--                        <header class="card-header">--}}
    {{--                            <div class="card-actions">--}}
    {{--                                <a href="#" class="card-action card-action-toggle" data-card-toggle></a>--}}
    {{--                                <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>--}}
    {{--                            </div>--}}

    {{--                            <h2 class="card-title">Localités enregistrées</h2>--}}
    {{--                        </header>--}}
    {{--                        <div class="card-body">--}}
    {{--                            <table class="table table-bordered table-striped mb-0 datatable-default">--}}
    {{--                                <thead>--}}
    {{--                                <tr>--}}
    {{--                                    <th>#</th>--}}
    {{--                                    <th>Nom</th>--}}

    {{--                                    <th>Email</th>--}}


    {{--                                    <th class="">Actions</th>--}}
    {{--                                </tr>--}}
    {{--                                </thead>--}}
    {{--                                <tbody>--}}
    {{--                                @foreach($users as $user)--}}
    {{--                                    <tr>--}}
    {{--                                        <td>{{ $loop->index + 1 }}</td>--}}
    {{--                                        <td>{{ $user->nom }}</td>--}}
    {{--                                        <td>{{ $user->email }}</td>--}}


    {{--                                        <td class="flex text-center">--}}
    {{--                                            <a href="{{ route('admin.enregistrement-user-edit',$user) }}" class="on-default edit-row" data-toggle="confirmation"><i--}}
    {{--                                                    class="fas fa-pencil-alt"></i></a>--}}
    {{--                                            <a href="#" class="on-default remove-row"><i--}}
    {{--                                                    class="far fa-trash-alt"></i></a>--}}
    {{--                                        </td>--}}
    {{--                                    </tr>--}}
    {{--                                @endforeach--}}
    {{--                                </tbody>--}}
    {{--                            </table>--}}
    {{--                        </div>--}}
    {{--                    </section>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div>--}}
    <div class="row">
        <div class="col col-md-5">
            <div class="panel card-sign">

                <div class="card-body">
                                @if($user->id)
                                {!! Form::open(['url'=>route('admin.enregistrement-user-edit',$user->id),'class'=>'form-horizontal','method'=>'PUT']) !!}
                                    @method('put')
                                @else
                                {!! Form::open(['url'=>route('admin.enregistrement-user'),'class'=>'form-horizontal']) !!}
                                @endif
                        <div class="form-group mb-3">

                            <label for="name" class="control-label">Nom utilisateur : </label>
                            {!! Form::text('name',$user->name,
                            ['id'=>'name','class'=>'form-control form-control-lg','required'=>'required','placeholder'=>'Nom utilisateur']) !!}
                            @error('name')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group mb-3">

                            <label for="email" class="control-label">Identifiant utilisateur : </label>
                            {!! Form::email('email',$user->email,
                            ['id'=>'email','class'=>'form-control form-control-lg','placeholder'=>"Identifiant utilisateur "]) !!}
                            @error('email')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>



                        <div class="row">

                            <div class="col-sm-6 text-right">
                                @if($user->id)
                                <button type="submit" class="btn btn-primary  mt-2" data-toggle="confirmation">Mettre à jour
                                </button>
                                @else
                                    <button type="submit" class="btn btn-primary  mt-2" data-toggle="confirmation">
                                        Enregistrer
                                    </button>
                                    <button type="reset" class="btn btn-default  mt-2">Effacer</button>
                                @endif

                            </div>
                        </div>


                   {!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="col col-md-7">
            <div class="row">
                <div class="col">
                    <section class="card">
                        <header class="card-header">
                            <div class="card-actions">
                                <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                            </div>

                            <h2 class="card-title">Localités enregistrées</h2>
                        </header>
                        <div class="card-body">
                            <table class="table table-bordered table-striped mb-0 datatable-default">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nom</th>

                                    <th>Email</th>


                                    <th class="">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{ $loop->index + 1 }}</td>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>


                                        <td class="flex text-center">
                                            <a href="{{ route('admin.enregistrement-user-edit',$user) }}"
                                               class="on-default edit-row" data-toggle="confirmation"><i
                                                    class="fas fa-pencil-alt"></i></a>
                                            <a href="#" class="on-default remove-row"><i
                                                    class="far fa-trash-alt"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>




@endsection
