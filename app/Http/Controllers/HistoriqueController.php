<?php

namespace App\Http\Controllers;

use App\Repositories\DistributeurRepository;
use App\Repositories\PeseeRepository;
use App\Repositories\RegionRepository;
use App\Repositories\TransporteurRepository;
use App\Repositories\TypeSubventionRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HistoriqueController extends Controller
{
    /**
     * @var TransporteurRepository
     */
    private $transporteurRepository;
    /**
     * @var DistributeurRepository
     */
    private $distributeurRepository;
    /**
     * @var RegionRepository
     */
    private $regionRepository;
    /**
     * @var TypeSubventionRepository
     */
    private $typeSubventionRepository;
    /**
     * @var PeseeRepository
     */
    private $peseeRepository;

    /**
     * HistoriqueController constructor.
     * @param TransporteurRepository $transporteurRepository
     * @param DistributeurRepository $distributeurRepository
     * @param RegionRepository $regionRepository
     * @param TypeSubventionRepository $typeSubventionRepository
     * @param PeseeRepository $peseeRepository
     */
    public function __construct(TransporteurRepository $transporteurRepository,
                                DistributeurRepository $distributeurRepository,
                                RegionRepository $regionRepository,
                                TypeSubventionRepository $typeSubventionRepository, PeseeRepository $peseeRepository)
    {
        $this->middleware('auth');
        $this->transporteurRepository = $transporteurRepository;
        $this->distributeurRepository = $distributeurRepository;
        $this->regionRepository = $regionRepository;
        $this->typeSubventionRepository = $typeSubventionRepository;
        $this->peseeRepository = $peseeRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function transporteur()
    {
        $data['results']=false;
        $data['transporteurs']=$this->transporteurRepository->transporteur()->get()->sortBy('name');
        $data['type_subventions'] = $this->typeSubventionRepository->typeSubvention()->get();
        return view('historique.transporteur')->with($data);
    }

    public function historiqueTransporteur(Request $request)
    {
        //  dd($request->all());
        $data['results'] =[];
        $case = $request->case;
        $data['case'] =$case;
        $transporteur_id = $request->transporteur_id;

        $type_subvention_id = $request->type_subvention_id;


        if ($case=='daily'){
            $date = Carbon::parse($request->date_pesee_entree)->format('Y-m-d');
            $points =   $this->peseeRepository->historiqueJourTransporteur($transporteur_id,$date,$type_subvention_id);
        }
        if ($case=='weekly'){
            $start = Carbon::parse($request->start)->format('Y-m-d');
            $end = Carbon::parse($request->end)->format('Y-m-d');
            $points =   $this->peseeRepository->historiqueHebdoTransporteur($transporteur_id,$start,$end,$type_subvention_id);
        }
        // dd($points);
        $data['transporteurs']=$this->transporteurRepository->transporteur()->get()->sortBy('name');
        $data['type_subventions'] = $this->typeSubventionRepository->typeSubvention()->get();
        if($points->isNotEmpty()) {
            $data['results'] = $points;
        }else{
            flash('Aucune donnée correspondante a ce jour trouvée');
            return redirect()->back();
        }
        return view('historique.transporteur')->with($data);
    }


}
