<?php

/**
 *  Route liée aux action du service commercial
 */
Route::group(['prefix' => 'departement-commercial','middleware'=>'auth'], function () {

    Route::get('recherche','ServiceCommercialController@recherche')->name('commercial.recherche');
    Route::post('recherche','ServiceCommercialController@recherchePesee')->name('commercial.recherche');


    Route::get('synthese-data','ServiceCommercialController@synthese')->name('commercial.synthese');
    Route::get('pesee','ServiceCommercialController@pesee')->name('commercial.pesee');
    Route::post('pesee','ServiceCommercialController@consultation')->name('commercial.pesee');
    Route::get('tableau-resume-ventes','ServiceCommercialController@resumeDesVentes')->name('commercial.tableau-ventes');
    Route::post('tableau-resume-ventes','ServiceCommercialController@pointResumeDesVentes')->name('commercial.tableau-ventes');
    Route::get('tableau-resume-ventes-journaliere','ServiceCommercialController@resumeDesVentesJournaliere')->name('commercial.tableau-ventes-journaliere');
    Route::post('tableau-resume-ventes-journaliere','ServiceCommercialController@resumeDesVentesJournalierePost')->name('commercial.tableau-ventes-journaliere');


    Route::get('localite','ServiceCommercialController@localites')->name('commercial.localite');
    Route::post('localite','ServiceCommercialController@enregistrementLocalites')->name('commercial.localite');
    Route::get('edition-localite-{localite}','ServiceCommercialController@editionLocalites')->name('commercial.edition-localite')->where('localite', '[0-9]+');
    Route::put('edition-localite-{localite}','ServiceCommercialController@updateLocalites')->name('commercial.edition-localite')->where('localite', '[0-9]+');
    Route::delete('localite','ServiceCommercialController@deletionLocalites')->name('commercial.deletion-localite');

    Route::get('distributeur','ServiceCommercialController@distributeur')->name('commercial.distributeur');
    Route::post('distributeur','ServiceCommercialController@enregistrementDistributeur')->name('commercial.distributeur');
    Route::get('edition-distributeur-{distributeur}','ServiceCommercialController@editionDistributeur')->name('commercial.edition-distributeur')->where('distributeur', '[0-9]+');
    Route::put('edition-distributeur-{distributeur}','ServiceCommercialController@updateDistributeur')->name('commercial.edition-distributeur')->where('distributeur', '[0-9]+');
    Route::delete('distributeur','ServiceCommercialController@deleteDistributeur')->name('commercial.deletion-distributeur');
});
