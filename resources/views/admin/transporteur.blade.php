@extends('template.theme')


@section('font-title')

@endsection
@push('scripts')



@endpush


@section('content-body')

    <div class="row">
        <div class="col-md-5">
            @if($transporteur->id)
            {!! Form::open(['url'=>route('admin.enregistrement-transporteur-edit',$transporteur->id),'class'=>'form-horizontal','method'=>'PUT']) !!}
                @method('put')
            @else
            {!! Form::open(['url'=>route('admin.enregistrement-transporteur'),'class'=>'form-horizontal']) !!}
            @endif
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title"> Enregistrement  transporteurs </h2>

                </header>
                <div class="card-body">
                    <div class="row form-group">
                        <div class="col-4">

                            <div class="form-group">
                                <label for="code" class="control-label">CODE : </label>
                                {!! Form::text('code', $transporteur->code ,
                                ['id'=>'code','class'=>'form-control','placeholder'=>"Code Distributeur",'required'=>'required',
                                'required'=>'required']) !!}
                                @error('code')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-8">

                            <div class="form-group">
                                <label for="nom" class="control-label">NOM : </label>
                                {!! Form::text('nom',$transporteur->nom,
                                ['id'=>'nom','class'=>'form-control','required'=>'required']) !!}
                                @error('nom')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
{{--                        <div class="col-6">--}}

{{--                            <div class="form-group">--}}
{{--                                <label for="telephone" class="control-label">Telephone principal : </label>--}}
{{--                                {!! Form::text('telephone', $transporteur->telephone ,--}}
{{--                                ['id'=>'telephone','class'=>'form-control','placeholder'=>"N° Principal du transporteur",'required'=>'required',--}}
{{--                                'required'=>'required']) !!}--}}
{{--                                @error('telephone')--}}
{{--                                <div class="alert alert-danger">{{ $message }}</div>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}
                        <div class="col">

                            <div class="form-group">
                                <label for="contacts" class="control-label">Contacts : </label>
                                {!! Form::text('contacts',$transporteur->contacts,
                                ['id'=>'contacts','class'=>'form-control','placeholder'=>'Contacts du transporteur']) !!}
                                @error('contacts')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>

                </div>
                <footer class="card-footer">
                    <div class="row justify-content-end">
                        <div class="col-sm-9">
                            @if($transporteur->id)
                                <button type="submit" class="btn btn-primary" data-toggle="confirmation">Mettre à jour</button>
                            @else
                                <button type="submit" class="btn btn-primary" data-toggle="confirmation">Enregistrer</button>
                                <button type="reset" class="btn btn-default">Effacer</button>
                            @endif
                        </div>
                    </div>
                </footer>
            </section>
            {!! Form::close() !!}

        </div>
        <div class="col-md-7">
            <div class="row">
                <div class="col">
                    <section class="card">
                        <header class="card-header">
                            <div class="card-actions">
                                <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                            </div>

                            <h2 class="card-title">Transporteurs enregistrées</h2>
                        </header>
                        <div class="card-body">
                            <table class="table table-bordered table-striped mb-0 datatable-default">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Code</th>
                                    <th>NOM</th>
                                    <th>Contact</th>

                                    <th class="">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($transporteurs as $transporteur)
                                    <tr>
                                        <td>{{ $loop->index + 1 }}</td>
                                        <td>{{ $transporteur->code }}</td>
                                        <td>{{ $transporteur->nom }}</td>
                                        <td>{{ $transporteur->contacts }}</td>
                                        <td class="flex text-center">
                                            <a href="{{ route('admin.enregistrement-transporteur-edit',$transporteur) }}" class="on-default edit-row" data-toggle="confirmation"><i
                                                    class="fas fa-pencil-alt"></i></a>
                                            <a href="#" class="on-default remove-row"><i
                                                    class="far fa-trash-alt"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>





@endsection
