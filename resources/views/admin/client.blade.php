@extends('template.theme')


@section('font-title')

@endsection
@push('scripts')

    <script type="text/javascript">
        $(function () {
            $('.selectPicker').select2({
                title: "Liste d'élément ...",
                header: 'Selectionnez un élément dans la liste',
                liveSearch: true,
                width: 'resolve'
            });
        });

    </script>

@endpush


@section('content-body')

    <div class="row">
        <div class="col-md-5">
            @if($client->id)
            {!! Form::open(['url'=>route('admin.enregistrement-client-edit',$client->id),'class'=>'form-horizontal','method'=>'PUT']) !!}
                @method('put')
            @else
            {!! Form::open(['url'=>route('admin.enregistrement-client'),'class'=>'form-horizontal']) !!}
            @endif
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title"> Enregistrement des clients </h2>

                </header>
                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="row form-group">
                        <div class="col-4">

                            <div class="form-group">
                                <label for="region_id" class="control-label">Regions </label>
                                {!! Form::select('region_id',$regions->pluck('libelle','id') , $client->region_id ,
                                ['id'=>'region_id','class'=>'form-control selectPicker','placeholder'=>"Region",'required'=>'required',
                                'required'=>'required']) !!}
                                @error('region_id')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-4">

                            <div class="form-group">
                                <label for="destination_id" class="control-label">Localité : </label>
                                {!! Form::select('destination_id',$destinations->pluck('libelle','id') , $client->destination_id ,
                                 ['id'=>'destination_id','class'=>'form-control selectPicker','placeholder'=>"Localité",'required'=>'required',
                                 'required'=>'required']) !!}
                                @error('destination_id')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-4">

                            <div class="form-group">
                                <label for="distributeur_id" class="control-label">Distributeur : </label>
                                {!! Form::select('distributeur_id',$distributeurs->pluck('nom','id') , $client->distributeur_id ,
                                 ['id'=>'distributeur_id','class'=>'form-control selectPicker','placeholder'=>"Distributeur",'required'=>'required',
                                 'required'=>'required']) !!}
                                @error('distributeur_id')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-8">

                            <div class="form-group">
                                <label for="nom" class="control-label">NOM : </label>
                                {!! Form::text('nom',$client->nom,
                                ['id'=>'nom','class'=>'form-control','required'=>'required','placeholder'=>'Nomclient du client']) !!}
                                @error('nom')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col">

                            <div class="form-group">
                                <label for="contacts" class="control-label">Contacts : </label>
                                {!! Form::text('contacts',$client->contact,
                                ['id'=>'contacts','class'=>'form-control','placeholder'=>'Contacts du client']) !!}
                                @error('contacts')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>


                </div>
                <footer class="card-footer">
                    <div class="row justify-content-end">
                        <div class="col-sm-9">
                            @if($client->id)
                                <button type="submit" class="btn btn-primary" data-toggle="confirmation">Mettre à jour</button>
                            @else
                                <button type="submit" class="btn btn-primary" data-toggle="confirmation">Enregistrer</button>
                                <button type="reset" class="btn btn-default">Effacer</button>
                            @endif
                        </div>
                    </div>
                </footer>
            </section>
            {!! Form::close() !!}

        </div>
        <div class="col-md-7">
            <div class="row">
                <div class="col">
                    <section class="card">
                        <header class="card-header">
                            <div class="card-actions">
                                <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                            </div>

                            <h2 class="card-title">Localités enregistrées</h2>
                        </header>
                        <div class="card-body">
                            <table class="table table-bordered table-striped mb-0 datatable-default">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nom</th>
                                    <th>Matricule</th>
                                    <th>Region</th>
                                    <th>Localite</th>
                                    <th>Ditributeur</th>


                                    <th class="">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($clients as $client)
                                    <tr>
                                        <td>{{ $loop->index + 1 }}</td>
                                        <td>{{ $client->nom }}</td>
                                        <td>{{ $client->matricule}}</td>
                                        <td>{{ $client->region->libelle }}</td>
                                        <td>{{ $client->destination->libelle ?? null }}</td>
                                        <td>{{ $client->distributeur->nom }}</td>

                                        <td class="flex text-center">
                                            <a href="{{ route('admin.enregistrement-client-edit',$client) }}" class="on-default edit-row" data-toggle="confirmation"><i
                                                    class="fas fa-pencil-alt"></i></a>
                                            <a href="#" class="on-default remove-row"><i
                                                    class="far fa-trash-alt"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>





@endsection
