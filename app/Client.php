<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Client extends Model
{
    protected $guarded=['id'];



    public function setNomAttribute($value)
    {
        return $this->attributes['nom'] = STR::upper($value);

    }

    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    public function destination()
    {
        return $this->belongsTo(Destination::class);
    }

    public function distributeur()
    {
        return $this->belongsTo(Distributeur::class);
    }

    public function tonnage()
    {
        return $this->hasOne(Tonnage::class);
    }
}
