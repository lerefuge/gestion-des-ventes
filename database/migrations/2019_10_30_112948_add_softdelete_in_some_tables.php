<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSoftdeleteInSomeTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('type_subventions', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('vehicules', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('type_vehicules', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('subventions', function (Blueprint $table) {
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('type_subventions', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('vehicules', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('type_vehicules', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('subventions', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
    }
}
