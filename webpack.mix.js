const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    // .sass('resources/sass/theme.scss', 'public/porto/css')
    // .sass('resources/sass/custom.scss', 'public/porto/css')

    .combine([
        "public/porto/vendor/bootstrap/css/bootstrap.css",
        "public/porto/vendor/animate/animate.css",
        "public/porto/vendor/font-awesome/css/all.min.css",
        "public/porto/vendor/magnific-popup/magnific-popup.css",
        "public/porto/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css",
        "public/porto/vendor/jquery-ui/jquery-ui.css",
        "public/porto/vendor/jquery-ui/jquery-ui.theme.css",
        "public/porto/vendor/select2-bootstrap-theme/select2-bootstrap.min.css",
        "public/porto/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css",
        "public/porto/vendor/bootstrap-multiselect/css/bootstrap-multiselect.css",
        "public/porto/vendor/bootstrap-select/css/bootstrap-select.min.css",
        "public/porto/vendor/morris/morris.css",
        "public/porto/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css",
        "public/porto/vendor/bootstrap-timepicker/css/bootstrap-timepicker.css",
        "public/porto/vendor/select2/css/select2.css",
        "public/porto/vendor/select2-bootstrap-theme/select2-bootstrap.min.css",
        "public/porto/vendor/datatables/media/css/dataTables.bootstrap4.css",
        "public/porto/vendor/pnotify/pnotify.custom.css",

    ], 'public/css/porto-vendors.css')
    .combine([
        "public/porto/vendor/morris/morris.css",
        "public/porto/vendor/chartist/chartist.min.css",
    ], 'public/css/charts.css')
    .combine([
        "public/porto/vendor/jquery-appear/jquery.appear.js",
        "public/porto/vendor/jquery.easy-pie-chart/jquery.easypiechart.js",
        "public/porto/vendor/flot/jquery.flot.js",
        "public/porto/vendor/flot.tooltip/jquery.flot.tooltip.js",
        "public/porto/vendor/flot/jquery.flot.pie.js",
        "public/porto/vendor/flot/jquery.flot.categories.js",
        "public/porto/vendor/flot/jquery.flot.resize.js",
        "public/porto/vendor/jquery-sparkline/jquery.sparkline.js",
        "public/porto/vendor/raphael/raphael.js",
        "public/porto/vendor/morris/morris.js",
        "public/porto/vendor/gauge/gauge.js",
        "public/porto/vendor/snap.svg/snap.svg.js",
        "public/porto/vendor/liquid-meter/liquid.meter.js",
        "public/porto/vendor/chartist/chartist.js",
    ], 'public/js/charts.js')

    .combine([
        "public/porto/css/skins/default.css",
    ], 'public/css/porto-theme.css')

    .combine([
        "public/porto/vendor/jquery/jquery.js",
        "public/porto/vendor/jquery-browser-mobile/jquery.browser.mobile.js",
        "public/porto/vendor/popper/umd/popper.min.js",
        "public/porto/vendor/bootstrap/js/bootstrap.js",
        "public/porto/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js",
        "public/porto/vendor/bootstrap-select/js/bootstrap-select.min.js",
        "public/porto/vendor/bootstrap-timepicker/js/bootstrap-timepicker.js",
        // "public/porto/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.fr.min.js",
        // "public/porto/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.en.min.js",
        "public/porto/vendor/common/common.js",
        "public/porto/vendor/moment/moment.js",
        "public/porto/vendor/nanoscroller/nanoscroller.js",
        "public/porto/vendor/magnific-popup/jquery.magnific-popup.js",
        "public/porto/vendor/jquery-placeholder/jquery.placeholder.js",
        "public/porto/vendor/select2/js/select2.js",
        "public/porto/vendor/bootstrap-multiselect/js/bootstrap-multiselect.js",
        "public/porto/vendor/jquery-validation/jquery.validate.js",
        "public/porto/vendor/jquery-maskedinput/jquery.maskedinput.js",
        "public/porto/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js",
        "public/porto/vendor/pnotify/pnotify.custom.js",
        "public/porto/vendor/datatables/media/js/jquery.dataTables.min.js",
        "public/porto/vendor/datatables/media/js/dataTables.bootstrap4.min.js",
        "public/porto/vendor/datatables/extras/TableTools/Buttons-1.4.2/js/dataTables.buttons.min.js",
        "public/porto/vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.bootstrap4.min.js",
        "public/porto/vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.html5.min.js",
        "public/porto/vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.print.min.js",
        "public/porto/vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.colVis.min.js",
        "public/porto/vendor/datatables/extras/TableTools/JSZip-2.5.0/jszip.min.js",
        "public/porto/vendor/datatables/extras/TableTools/pdfmake-0.1.32/pdfmake.min.js",
        "public/porto/vendor/datatables/extras/TableTools/pdfmake-0.1.32/vfs_fonts.js",
        "public/porto/vendor/datatables/extras/TableTools/sum.js",

        "public/porto/vendor/ios7-switch/ios7-switch.js",

    ], 'public/js/porto-vendors.js')
    .combine([
        "public/porto/js/theme.js",
        "public/porto/js/custom.js",
        "public/porto/js/theme.init.js",

    ], 'public/js/porto-theme.js');

