<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignFieldsOnTableVehicule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vehicules', function (Blueprint $table) {
            $table->dropColumn(['libelle']);
            $table->string('chauffeur')->nullable();
            $table->string('immatriculation')->nullable();
            $table->string('contact')->nullable();
            $table->unsignedInteger('pesee_id')->nullable();
            $table->unsignedInteger('type_vehicule_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vehicules', function (Blueprint $table) {
            $table->string('libelle')->nullable();
            $table->dropColumn(['chauffeur',
                'immatriculation',
                'contact',
                'pesee_id',
                'type_vehicule_id']);
        });
    }
}
