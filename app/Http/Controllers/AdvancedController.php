<?php

namespace App\Http\Controllers;

use App\Repositories\ClientRepository;
use App\Repositories\DestinationRepository;
use App\Repositories\DistributeurRepository;
use App\Repositories\PeseeRepository;
use App\Repositories\ProduitRepository;
use App\Repositories\SubventionRepository;
use App\Repositories\TransporteurRepository;
use App\Repositories\TypeSubventionRepository;
use App\Repositories\TypeVehiculeRepository;
use App\Repositories\VehiculeRepository;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Carbon\Carbon;
class AdvancedController extends Controller
{
    /**
     * @var PeseeRepository
     */
    private $peseeRepository;
    /**
     * @var ClientRepository
     */
    private $clientRepository;
    /**
     * @var TypeVehiculeRepository
     */
    private $typeVehiculeRepository;
    /**
     * @var TypeSubventionRepository
     */
    private $typeSubventionRepository;
    /**
     * @var ProduitRepository
     */
    private $produitRepository;
    /**
     * @var DistributeurRepository
     */
    private $distributeurRepository;
    /**
     * @var TransporteurRepository
     */
    private $transporteurRepository;
    /**
     * @var DestinationRepository
     */
    private $destinationRepository;
    /**
     * @var VehiculeRepository
     */
    private $vehiculeRepository;
    /**
     * @var SubventionRepository
     */
    private $subventionRepository;


    /**
     * AdvancedController constructor.
     * @param PeseeRepository $peseeRepository
     * @param ClientRepository $clientRepository
     * @param ProduitRepository $produitRepository
     * @param VehiculeRepository $vehiculeRepository
     * @param SubventionRepository $subventionRepository
     * @param TransporteurRepository $transporteurRepository
     * @param DestinationRepository $destinationRepository
     * @param TypeVehiculeRepository $typeVehiculeRepository
     * @param TypeSubventionRepository $typeSubventionRepository
     * @param DistributeurRepository $distributeurRepository
     */
    public function __construct(
        PeseeRepository $peseeRepository,
        ClientRepository $clientRepository,
        ProduitRepository $produitRepository,
        VehiculeRepository $vehiculeRepository,
        SubventionRepository $subventionRepository,
        TransporteurRepository $transporteurRepository,
        DestinationRepository $destinationRepository,
        TypeVehiculeRepository $typeVehiculeRepository,
        TypeSubventionRepository $typeSubventionRepository,
        DistributeurRepository $distributeurRepository
    ) {
        $this->peseeRepository = $peseeRepository;
        $this->clientRepository = $clientRepository;
        $this->typeVehiculeRepository = $typeVehiculeRepository;
        $this->typeSubventionRepository = $typeSubventionRepository;
        $this->produitRepository = $produitRepository;
        $this->distributeurRepository = $distributeurRepository;
        $this->transporteurRepository = $transporteurRepository;
        $this->destinationRepository = $destinationRepository;
        $this->vehiculeRepository = $vehiculeRepository;
        $this->subventionRepository = $subventionRepository;
    }


    public function index(){
        return $this->recherchePesee();
    }

    public function recherchePesee()
    {
        $data['entrees'] = null;
        return view('advanced.recherche-pesee')->with($data);
    }

    public function pesees(Request $request)
    {
        $case = $request->case;
        $data['case']= $case;
        if($case=='numero_bl'){
        $pesee_id = $request->get('pesee_id');
        $validator = validator($request->all(), ['pesee_id' => 'required|integer'], ['pesee_id.required' => 'Veuillez selectionner le numero du BL']);
        if ($validator->fails()) {
            return $this->_redirectWhenFail($validator);
        }
        $result = $this->peseeRepository->pesee()->where('pesees.id', '=', $pesee_id)
        ->with(['transporteur', 'produit', 'distributeur', 'destination', 'destination.region', 'charges', 'vehicule', 'vehicule.typeVehicule', 'subvention', 'subvention.typeSubvention'])
        ->get();
        if ($result) {
            $data['entrees'] = $result;
            return view('advanced.recherche-pesee')->with($data);
        } else {
            flash('Desolé, aucune entrée trouvées pour cette date', 'info');
            return redirect()->back();
        }
    }else{
            $date= Carbon::parse($request->date_jour)->format('Y-m-d');
            $data['date_pesee'] = $date;
            $validator = validator($request->all(), ['date_jour' => 'required'], ['date_jour.required' => 'Veuillez selectionner la date du jour']);
        if ($validator->fails()) {
            return $this->_redirectWhenFail($validator);
        }
        $result = $this->peseeRepository->pesee()->where('pesees.date_pesee', '=', $date)

        ->with(['transporteur', 'produit', 'distributeur', 'destination', 'destination.region', 'charges', 'vehicule', 'vehicule.typeVehicule', 'subvention', 'subvention.typeSubvention'])
        ->get();
        if ($result) {
            $data['entrees'] = $result;
            return view('advanced.recherche-pesee')->with($data);
        } else {
            flash('Desolé, aucune entrée trouvées pour cette date', 'info');
            return redirect()->back();
        }
        }
    }

    public function editionPesee(int $pesee_id)
    {
        $result = $this->peseeRepository->pesee()
            ->with(['transporteur', 'produit', 'distributeur', 'destination', 'vehicule', 'subvention.typeSubvention'])->where('id', '=', $pesee_id)->first();
        if ($result) {
            $data['clients'] = $this->clientRepository->client();
            $data['type_vehicules'] = $this->typeVehiculeRepository->typeVehicule();
            $data['distributeurs'] = $this->distributeurRepository->distributeur()->get()->sortBy('nom');
            $data['type_subventions'] = $this->typeSubventionRepository->typeSubvention()->get();
            $data['transporteurs'] = $this->transporteurRepository->transporteur()->get();
            $data['destinations'] = $this->destinationRepository->destination()->get();
            $data['produits'] = $this->produitRepository->produit()->get();
            $data['entree'] = $result;
            return view('advanced.edition-entree')->with($data);
        } else {
            flash('Desolé, aucune entrée trouvées pour cette date', 'info');
            return redirect()->back();
        }
    }

    public function putEditionPesee(Request $request, int $pesee_id)
    {
        #dd($request->produit_id);
        $validator = validator(
            $request->all(),
            [
            'quantite' => 'required',
            'qte_pesee' => 'required',
            'numero_bl' => 'required',
            'qte_bl' => 'required',
            'pesee_id' => 'required',
            'distributeur_id' => 'nullable|integer',
            'transporteur_id' => 'nullable|integer',
            'destination_id' => 'nullable|integer',
            'produit_id' => 'required|integer',
            'date_pesee_entree' => 'required|date|date_format:Y-m-d',
        //    'heure_debut_chargement' => 'required|date|date_format:Y-m-d',

        ],
            [
                'produit_id.required' => 'Veuillez preciser le produit',
                'quantite.required' => 'Veuillez preciser la quantité',
                'numero_bl.required' => 'Veuillez preciser lnuméro du BL',
                'qte_pesee.required' => 'Veuillez preciser la quantité affichée à la pésée',
                'qte_bl.required' => 'Veuillez preciser la quantité affichée sur le BL ',
                'date_pesee_entree.required' => 'Veuillez preciser la date de la pesée',
                'date_pesee_entree.date' => 'Veuillez preciser une date correcte',
                'date_pesee_entree.date_format' => 'Veuillez preciser la date au bon format',

            ]
        );
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator, 'pesee')->withInput();
        }
        $pesee_id = $request->pesee_id;
        $pesee= $this->peseeRepository->pesee()->where('id', '=', $pesee_id)->first();
        if ($pesee) {
            # dd($request->all());
            $pesee->update([
                'produit_id'=>$request->produit_id,
                'transporteur_id'=>$request->transporteur_id,
                'distributeur_id'=>$request->distributeur_id,
                'destination_id'=>$request->destination_id,
                'date_pesee'=>$request->date_pesee,
                'quantite'=>$request->quantite,
                'qte_pesee'=>$request->qte_pesee,
                'qte_bl'=>$request->qte_bl,
                'numero_bl'=>$request->numero_bl,
                'poids_entree' => $request->poids_pesee_entree,
                'poids_sortie' => $request->poids_pesee_sortie,
                'poids_net' => $request->poids_net,
                'date_entree' => $request->date_pesee_entree,
                'heure_pesee_entree' => $request->heure_pesee_entree,
                'date_sortie' => $request->date_pesee_sortie,
                'heure_pesee_sortie' => $request->heure_pesee_sortie,
                'heure_debut_chargement' => $request->heure_debut_chargement,
                'heure_fin_chargement' => $request->heure_fin_chargement,

            ]);
            smilify('success', "Enregistrement mis à jour");
            return redirect()->route('exploitation.edition-pesee', $pesee_id);
        }
        smilify('info', "Enregistrement non mis à jour");
        return redirect()->route('exploitation.edition-pesee', $pesee_id);
    }

    public function deletePesee(Request $request)
    {
        //  dd($request->all());
        $pesee_id = $request->pesee_id;
        $pesee= $this->peseeRepository->pesee()->where('id', '=', $pesee_id)->first();
        if ($pesee) {
            $vehicule = $this->vehiculeRepository->vehicule()->where('pesee_id', '=', $pesee_id);
            $subvention =  $this->subventionRepository->subvention()->where('pesee_id', '=', $pesee_id);

            if ($vehicule->delete() ||  $subvention->delete()) {
                $pesee->delete();
                smilify('success', "Enregistrement supprimé");
                return redirect()->route('recherche.pesee');
            }
        }
    }

    public function deletePeseeByDate (Request $request){

        $date = Carbon::parse($request->date_pesee_del)->format('Y-m-d');
        $pesees = $this->peseeRepository->pesee()->where('date_pesee', '=', $date);
        $pesees_ids = $pesees->get(['id'])->toArray();
        if($pesees->forceDelete()){
            $this->vehiculeRepository->vehicule()->whereIn('pesee_id',$pesees_ids)->forceDelete();
            $this->subventionRepository->subvention()->whereIn('pesee_id',$pesees_ids)->forceDelete();

        }
        smilify('success', "Enregistrement supprimé");
        return redirect()->route('recherche.pesee');
    }

    public function loadBonLivraison(Request $request)
    {
        $field = 'numero_bl';
        $terms = $request->get('q');
        $query = $this->peseeRepository->loadBl();
        if (!is_null($terms)) {
            $query = $query->where(function ($query) use ($field, $terms) {
                $query->where($field, 'like', "{$terms}%");
            });
        }
        $data = $query->get()->take(10)->toArray();
        //dd($data);
        return response()->json($data, 200, [
            'Content-Type' => 'application/json'
        ]);
    }
}
