<script type="text/javascript" defer="true">

    (function ($) {

        'use strict';

        var datatableVente = function (idSelector) {
            var $table = $(idSelector);

            var table = $table.dataTable({
                //"dom": '<"text-right mb-md"T><"row"<"col-lg-6"l><"col-lg-6"f>><"table-responsive"t>p',
                "dom": '<"dt-buttons"B><"clear">lfrtip',
                "language": {
                    "sProcessing": '<i class="fas fa-spinner fa-spin"></i> Loading',
                    "sSearch": "Rechercher&nbsp;:",
                    "sLengthMenu": "Afficher _MENU_ &eacute;l&eacute;ments",
                    "sInfo": "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    "sInfoEmpty": "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                    "sInfoFiltered": "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    "sInfoPostFix": "",
                    "sLoadingRecords": "Chargement en cours...",
                    "sZeroRecords": "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    "sEmptyTable": "Aucune donn&eacute;e disponible dans le tableau",
                    "oPaginate": {
                        "sFirst": "Premier",
                        "sPrevious": "Pr&eacute;c&eacute;dent",
                        "sNext": "Suivant",
                        "sLast": "Dernier"
                    },
                    "oAria": {
                        "sSortAscending": ": activer pour trier la colonne par ordre croissant",
                        "sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
                    }
                },
                @hasanyrole('Super Admins|Admins|Superviseur')
                "buttons": ['print', 'excel', 'pdf', {
                    "text": 'Recharger',
                    action: function (e, dt, node, config) {
                        dt.ajax.reload();
                    }
                }],
                @endhasanyrole
                "processing": true,
                "serverSide": true,
                "paging": true, // Allow data to be paged
                "lengthChange": true,
                "searching": true, // Search box and search function will be actived
                "ordering": true,
                "info": true,
                "autoWidth": true,
                'order': [[7, 'asc'],[8, 'asc']],
                "lengthMenu": [[10, 25, 50, 100, 200, 300, 500, -1], [10, 25, 50, 100, 200, 300, 500, "All"]],
                "ajax": {
                    "url": "{{$data_table_url}}",
                    "method": 'POST',
                    @if(isset($zone_id))
                    "data" : {
                          'zone_id':   @json($zone_id)  ,
                       },
                   @endif
                    "dataType": 'json',
                },
                "deferRender": true,

                "scrollCollapse": true,
                "scroller": true,
                "search": {
                    "regex": true
                },
                "aoColumnDefs": [
                    {"bSortable": false, 'aTargets': [0, 1]},
                    {"className": "dt-center", "aTargets": [0, 1, 2, 3]},
                ],
                @include('_dataTable._columns-table')


                "responsive": !0,
                "keys": true,
                "stateSave": true,
                drawCallback: function () {
                var api = this.api();
                var nombre = api.column( 5, {page:'current'} ).data().sum().toFixed(2) ;


                $('.somme_vente').html(new Intl.NumberFormat('fr-FR',{ style: 'decimal' }).format(nombre)+' Tonnes')
               },
                fnInitComplete: function (settings, json) {
                    // select 2
                    if ($.isFunction($.fn['select2'])) {
                        $('.dataTables_length select', settings.nTableWrapper).select2({
                            theme: 'bootstrap',
                            minimumResultsForSearch: -1
                        });
                    }


                    var options = $('table', settings.nTableWrapper).data('plugin-options') || {};

                    // search
                    var $search = $('.dataTables_filter input', settings.nTableWrapper);

                    $search
                        .attr({
                            placeholder: typeof options.searchPlaceholder !== 'undefined' ? options.searchPlaceholder : 'Search...'
                        })
                        .removeClass('form-control-sm').addClass('form-control pull-right');

                    if ($.isFunction($.fn.placeholder)) {
                        $search.placeholder();
                    }

                    this.api().columns().every(function () {
                        var column = this;
                        var input = document.createElement("input");

                        $(input).appendTo($(column.footer()).empty())
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex($(this).val());
                                column.search(val ? val : '', true, false).draw();
                            });
                    });
                },
            });
        };

        $(function () {
            datatableVente('#dataTableVente');
        });

    }).apply(this, [jQuery]);
</script>
