<?php

namespace App\Http\Controllers;

use App\Repositories\PeseeRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

class VenteController extends Controller
{


    /**
     * @var PeseeRepository
     */
    private $peseeRepository;

    /**
     * VenteController constructor.
     * @param PeseeRepository $peseeRepository
     */
    public function __construct(PeseeRepository $peseeRepository)
    {

        $this->peseeRepository = $peseeRepository;
    }

    /**
     * @deprecated
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $date = now()->subDay()->format('Y-m-d');
        $data['data_table_url'] = route('datatable.vente-jour', strtotime($date));
        $day = Carbon::parse($date)->format('d/m/Y');
        $data['periode'] = " Point des ventes de la journée du {$day} ";
        $data['expedies'] = $this->peseeRepository->expedies($date, $date);
        return view('ventes.index')->with($data);
    }

    /**
     * @deprecated
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function consultation(Request $request)
    {
//        $period = $request->case;
//        switch ($period):
//            case  'daily';
//                $date = Carbon::parse($request->date_jour)->format('Y-m-d');
//                $day = Carbon::parse($date)->format('d/m/Y');
//                $data['periode'] = " Point des ventes de la journée du {$day} ";
//                $data['data_table_url'] = route('datatable.vente-jour', strtotime($date));
//                $data['expedies'] = $this->peseeRepository->expedies($date, $date);
//                break;
//            case  'period';
//                $start_period = Carbon::parse($request->start)->format('Y-m-d');
//                $end_period = Carbon::parse($request->end)->format('Y-m-d');
//                $data['data_table_url'] = route('datatable.vente-periode', ['debut' => strtotime($start_period), 'fin' => strtotime($end_period)]);
//                $start = Carbon::parse($start_period)->format('d/m/Y');
//                $fin = Carbon::parse($end_period)->format('d/m/Y');
//                $data['periode'] = " Point des ventes de la période du {$start} au {$fin} ";
//                $data['expedies'] = $this->peseeRepository->expedies($start_period, $end_period);
//                break;
//            case  'monthly';
//                $date_send = explode('/', $request->date_month);
//                $correct_date = $date_send[1] . '-' . $date_send['0'] . '-01';
//                $month = $correct_date;
//                $date_month = Carbon::parse($month)->format('Y-m-d');
//                $start_month = $date_month;
//                $end_month = Carbon::parse($start_month)->endOfMonth()->format('Y-m-d');
//                $data['data_table_url'] = route('datatable.vente-mensuelle', ['debut' => strtotime($start_month), 'fin' => strtotime($end_month)]);
//                $start = Carbon::parse($start_month)->format('d/m/Y');
//                $fin = Carbon::parse($end_month)->format('d/m/Y');
//                $data['periode'] = " Point des ventes de la période du {$start} au {$fin} ";
//                $data['expedies'] = $this->peseeRepository->expedies($start_month, $end_month);
//                break;
//            case  'yearly';
//                $start_year = "01-01-" . $request->date_year;
//                $end_year = "31-12-" . $request->date_year;
//                $date_year_start = Carbon::parse($start_year)->format('Y-m-d');
//                $date_year_end = Carbon::parse($end_year)->format('Y-m-d');
//                $data['data_table_url'] = route('datatable.vente-annuelle', ['debut' => strtotime($date_year_start), 'fin' => strtotime($date_year_end)]);
//                $start = Carbon::parse($date_year_start)->format('d/m/Y');
//                $fin = Carbon::parse($date_year_end)->format('d/m/Y');
//                $data['periode'] = " Point des ventes de la période du {$start} au {$fin} ";
//                $data['expedies'] = $this->peseeRepository->expedies($date_year_start, $date_year_end);
//                break;
//            default:
//                $date = Carbon::parse($request->date_jour)->format('Y-m-d');
//                $day = Carbon::parse($date)->format('d/m/Y');
//                $data['periode'] = " Point des ventes de la journée du {$day} ";
//                $data['data_table_url'] = route('datatable.vente-jour', strtotime($date));
//                $data['expedies'] = $this->peseeRepository->expedies($date, $date);
//        endswitch;
        return view('ventes.index')->with($data);
    }

}
