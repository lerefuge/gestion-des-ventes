<?php

namespace App\Http\Controllers;

use App\Repositories\ChefLieuDistrictRepository;
use App\Repositories\DestinationRepository;
use App\Repositories\DistributeurRepository;
use App\Repositories\PeseeRepository;
use App\Repositories\RegionRepository;
use App\Traits\ExportableGraphData;
use App\Traits\SnipetsTraits;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ExpeditionController extends Controller
{
    //
    use SnipetsTraits, ExportableGraphData;
    /**
     * @var Carbon
     */
    private $date;

    /**
     * @var Carbon
     */
    private $yesterday;

    /**
     * @var PeseeRepository
     */
    private $peseeRepository;
    /**
     * @var Request
     */
    private $period;
    /**
     * @var ChefLieuDistrictRepository
     */
    private $chefLieuDistrictRepository;
    /**
     * @var DestinationRepository
     */
    private $destinationRepository;
    /**
     * @var RegionRepository
     */
    private $regionRepository;
    /**
     * @var DistributeurRepository
     */
    private $distributeurRepository;

    /**
     * ExpeditionController constructor.
     * @param PeseeRepository $peseeRepository
     * @param ChefLieuDistrictRepository $chefLieuDistrictRepository
     * @param DestinationRepository $destinationRepository
     * @param RegionRepository $regionRepository
     * @param DistributeurRepository $distributeurRepository
     */
    public function __construct(PeseeRepository $peseeRepository,
                                ChefLieuDistrictRepository $chefLieuDistrictRepository,
                                DestinationRepository $destinationRepository,
                                RegionRepository $regionRepository,
                                DistributeurRepository $distributeurRepository)
    {

        $this->date =  now();
        $this->yesterday = $this->date->subDay();
        $this->peseeRepository = $peseeRepository;
        $this->chefLieuDistrictRepository = $chefLieuDistrictRepository;
        $this->destinationRepository = $destinationRepository;
        $this->regionRepository = $regionRepository;
        $this->distributeurRepository = $distributeurRepository;
    }
    /**
     * @param string $date
     * @param string $dateFormat
     * @return string
     */
    private function parsedDate(string $date, $dateFormat = 'Y-m-d')
    {
        return Carbon::parse($date)->format($dateFormat);
    }

    public function recherche()
    {
        $data['zones'] = $this->chefLieuDistrictRepository->chefLieuDistrict();
        return view('expedition.recherche-expedies')->with($data);
    }

    public function postRecherche(Request $request)
    {
        $case = $request->case;
        $data['case'] = $case;
        $data['zones'] = $this->chefLieuDistrictRepository->chefLieuDistrict();
        if ($case == 'numero_bl') {
            $pesee_id = $request->get('pesee_id');
            $validator = validator($request->all(), ['pesee_id' => 'required|integer'], ['pesee_id.required' => 'Veuillez selectionner le numero du BL']);
            if ($validator->fails()) {
                $this->_redirectWhenFail("Veuillez corriger le erreurs SVP !", 'warning', $validator);
            }
            $result = $this->peseeRepository->pesee()->where('pesees.id', '=', $pesee_id)
                ->with(['produit', 'distributeur', 'destination', 'vehicule', 'transporteur'])
                ->get();
            if ($result) {
                $data['expedies'] = $this->peseeRepository->pesee()->where('pesees.id', '=', $pesee_id)->get();
                return view('expedition.recherche-expedies')->with($data);
            } else {
                flash('Desolé, aucune entrée trouvées pour cette date', 'info');
                return redirect()->back();
            }
        } elseif ($case == 'period') {
            $date = Carbon::parse($request->date_jour)->format('Y-m-d');
            $data['date_pesee'] = $date;
            $validator = validator($request->all(), ['date_jour' => 'required'], ['date_jour.required' => 'Veuillez selectionner la date du jour']);
            if ($validator->fails()) {
                $this->_redirectWhenFail("Veuillez corriger le erreurs SVP !", 'warning', $validator);
            }
            $result = $this->peseeRepository->pesee()->where('pesees.date_pesee', '=', $date)
                ->with(['produit', 'distributeur', 'destination', 'vehicule', 'transporteur'])
                ->get();
            if ($result) {
                $data['expedies'] = $result;
                return view('expedition.recherche-expedies')->with($data);
            } else {
                flash('Desolé, aucune entrée trouvées pour cette date', 'info');
                return redirect()->back();
            }
        } else
            return view('expedition.recherche-expedies')->with($data);
    }

    public function pointExpedition()
    {
        $date = $this->parsedDate($this->yesterday);
        $day = $this->yesterday->format('d/m/Y');
        $data['zones'] = $this->chefLieuDistrictRepository->chefLieuDistrict();
        $data['data_table_url'] = route('datatable.expeditions-jour', strtotime($date));
        $data['periode'] = " Point des expédiés de la journée du {$day} ";
        $data['expedies'] = $this->peseeRepository->expedies($date, $date);
        return view('expedition.expedies')->with($data);
    }

    public function consultation(Request $request)
    {
        $this->period = $request->case;
        $zone_id = $request->zone_id;
        switch ($this->period):
            case  'daily';
                $date = $this->parsedDate($this->yesterday);
                $day = $this->yesterday->format('d/m/Y');
                $data['periode'] = " Point des expédiés de la journée du {$day} ";
                $data['data_table_url'] = route('datatable.expeditions-jour', strtotime($date));
                $data['expedies'] = $this->peseeRepository->expedies($date, $date, $zone_id);
                break;
            case  'period';
           // dd($request->start);
                $start_period = $this->parsedDate($request->start, 'Y-m-d');
                $end_period = $this->parsedDate($request->end, 'Y-m-d');
                $data['data_table_url'] = route('datatable.expeditions-periode', ['debut' => strtotime($start_period), 'fin' => strtotime($end_period)]);
                $start = $this->parsedDate($request->start, 'd/m/Y');
                $fin = $this->parsedDate($request->end, 'd/m/Y');
                $data['periode'] = " Point des expédiés de la période du {$start} au {$fin} ";
                $data['expedies'] = $this->peseeRepository->expedies($start_period, $end_period, $zone_id);
                break;
            case  'monthly';
                $date_send = explode('/', $request->date_month);
                $correct_date = $date_send[1] . '-' . $date_send['0'] . '-01';
                $month = $correct_date;
                $date_month = Carbon::parse($month)->firstOfMonth()->format('Y-m-d');
                $start_month = $date_month;
                $end_month = Carbon::parse($start_month)->endOfMonth()->format('Y-m-d');
                $data['data_table_url'] = route('datatable.expeditions-mensuelle', ['debut' => strtotime($start_month), 'fin' => strtotime($end_month)]);
                $start = Carbon::parse($start_month)->format('d/m/Y');
                $fin = Carbon::parse($end_month)->format('d/m/Y');
                $data['periode'] = " Point des expédiés de la période du {$start} au {$fin} ";
                $data['expedies'] = $this->peseeRepository->expedies($start_month, $end_month, $zone_id);
                break;
            case  'yearly';
                $start_year = "01-01-" . $request->date_year;
                $year = Carbon::parse($start_year);
                $date_year_start = $year->startOfYear()->format('Y-m-d');
                $date_year_end =$year->endOfYear()->format('Y-m-d'); 
                $data['data_table_url'] = route('datatable.expeditions-annuelle', ['debut' => strtotime($date_year_start), 'fin' => strtotime($date_year_end)]);
                $start = Carbon::parse($date_year_start)->format('d/m/Y');
                $fin = Carbon::parse($date_year_end)->format('d/m/Y');
                $data['periode'] = " Point des expédiés de la période du {$start} au {$fin} ";
                $data['expedies'] = $this->peseeRepository->expedies($date_year_start, $date_year_end, $zone_id);
                break;
            default:
                $date = Carbon::parse($request->date_jour)->format('Y-m-d');
                $day = Carbon::parse($date)->format('d/m/Y');
                $data['periode'] = " Point des expédiés de la journée du {$day} ";
                $data['data_table_url'] = route('datatable.expeditions-jour', strtotime($date));
                $data['expedies'] = $this->peseeRepository->expedies($date, $date, $zone_id);
        endswitch;
        $data['zones'] = $this->chefLieuDistrictRepository->chefLieuDistrict();
        return view('expedition.expedies')->with($data);
    }

    public function toleranceCamions()
    {

    }

}


