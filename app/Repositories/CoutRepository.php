<?php


namespace App\Repositories;


use App\Cout;

class CoutRepository
{

    /**
     * @var Cout
     */
    private $cout;

    /**
     * CoutRepository constructor.
     * @param Cout $cout
     */
    public function __construct(Cout $cout)
    {

        $this->cout = $cout;
    }

    public function cout()
    {
        return $this->cout->newQuery();
    }
}
