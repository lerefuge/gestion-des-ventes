<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;


    public function redirectPath()
    {
        return route('application.index');
    }

    public function __construct()
    {
        $this->middleware('guest')
            ->except(['logout']);
    }


    public function showLoginForm()
    {
        return view('auth.login');
    }


    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        $name = $user->name;
        smilify('success',"Bienvenue {$name} ⚡️");
    }
    /**
     * Log the user out of the application.
     *
     * @param Request $request
     * @return Response
     */
    public function logout(Request $request)
    {
        $name = auth('web')->user()->name;
        $this->guard()->logout();

        $request->session()->invalidate();
        smilify('success',"Bye {$name} ⚡️");
        return redirect()->route('utilisateur.login');
    }
}
