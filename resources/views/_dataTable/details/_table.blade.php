
<section class="card">
    <header class="card-header">
        <div class="card-actions">
            <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
            <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
        </div>

        <h2 class="card-title"> Ventes </h2>
        <p class="card-subtitle">

            </p>
    </header>
    <div class="card-body table-responsive">
<table class="table small table-bordered table-condensed table-striped mb-0 " id="dataTableDetails">
    <thead>
    <tr class="small">

        <th>Date Pesee</th>
        <th>Qualité Ciment</th>
        <th>Destination</th>
        <th>Quantité Expediée</th>


    </tr>
    </thead>
    <tbody class="text-center"></tbody>
    <tfoot>
        <tr class="small text-center">

            <th>Date Pesee</th>
            <th>Qualité Ciment</th>
            <th>Destination</th>
            <th>Quantité Expediée</th>


        </tr>
        <tr>
            <td colspan="3">Total</td>
            <td class="somme_vente text-center"></td>
        </tr>
        </tfoot>
</table>
    </div>
</section>
