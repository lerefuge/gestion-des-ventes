<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeseesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pesees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('date_pesee')->nullable();
            $table->string('numero_bl')->nullable();
            $table->float('quantite')->nullable();
            $table->string('chauffeur')->nullable();
            $table->unsignedInteger('distributeur_id')->nullable()->index();
            $table->unsignedInteger('transporteur_id')->nullable()->index();
            $table->unsignedInteger('produit_id')->nullable()->index();
            $table->unsignedInteger('destination_id')->nullable()->index();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pesees');
    }
}
