<?php


namespace App\Repositories;


use App\Chauffeur;

class ChauffeurRepository
{
    /**
     * @var Chauffeur
     */
    private $chauffeur;

    /**
     * ChauffeurRepository constructor.
     * @param Chauffeur $chauffeur
     */
    public function __construct(Chauffeur $chauffeur)
    {

        $this->chauffeur = $chauffeur;
    }

    public function chauffeur()
    {
        return $this->chauffeur->newQuery();
    }
}
