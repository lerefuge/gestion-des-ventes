<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ApplicationController@index')->name('application.index')->middleware('auth');
Route::post('historique', 'ApplicationController@historique')->name('application.historique')->middleware('auth');


Route::group(['prefix' => 'services','middleware'=>'auth'], function () {
    Route::get('qualite-de-ciment', 'AdminController@produits')->name('admin.enregistrement-produit');
    Route::post('qualite-de-ciment', 'AdminController@postProduits')->name('admin.enregistrement-produit');
    Route::get('edition-qualite-de-ciment-{produit_id}', 'AdminController@editProduits')->name('admin.enregistrement-produit-edit')->where('produit_id', '[0-9]+');
    Route::put('edition-qualite-de-ciment-{produit_id}', 'AdminController@updateProduits')->name('admin.enregistrement-produit-edit')->where('produit_id', '[0-9]+');

    Route::get('region', 'AdminController@region')->name('admin.enregistrement-region');
    Route::post('region', 'AdminController@postRegion')->name('admin.enregistrement-region');
    Route::get('edition-region-{region_id}', 'AdminController@editRegion')->name('admin.enregistrement-region-edit')->where('region_id', '[0-9]+');
    Route::put('edition-region-{region_id}', 'AdminController@patchRegion')->name('admin.enregistrement-region-edit')->where('region_id', '[0-9]+');

    Route::get('type_vehicule', 'AdminController@type_vehicule')->name('admin.enregistrement-type_vehicule');
    Route::post('type_vehicule', 'AdminController@postTypeVehicule')->name('admin.enregistrement-type_vehicule');
    Route::get('edition-type_vehicule-{type_vehicule_id}', 'AdminController@editTypeVehicule')->name('admin.enregistrement-type_vehicule-edit')->where('type_vehicule_id', '[0-9]+');
    Route::put('edition-type_vehicule-{type_vehicule_id}', 'AdminController@patchTypeVehicule')->name('admin.enregistrement-type_vehicule-edit')->where('type_vehicule_id', '[0-9]+');

    Route::get('destination', 'AdminController@destination')->name('admin.enregistrement-destination');
    Route::post('destination', 'AdminController@postDestination')->name('admin.enregistrement-destination');
    Route::get('edition-destination-{destination_id}', 'AdminController@editDestination')->name('admin.enregistrement-destination-edit')->where('destination_id', '[0-9]+');
    Route::put('edition-destination-{destination_id}', 'AdminController@patchDestination')->name('admin.enregistrement-destination-edit')->where('destination_id', '[0-9]+');

    Route::get('distributeur', 'AdminController@distributeur')->name('admin.enregistrement-distributeur');
    Route::post('distributeur', 'AdminController@postDistributeur')->name('admin.enregistrement-distributeur');
    Route::get('edition-distributeur-{distributeur_id}', 'AdminController@editDistributeur')->name('admin.enregistrement-distributeur-edit')->where('distributeur_id', '[0-9]+');
    Route::put('edition-distributeur-{distributeur_id}', 'AdminController@patchDistributeur')->name('admin.enregistrement-distributeur-edit')->where('distributeur_id', '[0-9]+');


    Route::get('transporteur', 'AdminController@transporteur')->name('admin.enregistrement-transporteur');
    Route::post('transporteur', 'AdminController@postTransporteur')->name('admin.enregistrement-transporteur');
    Route::get('edition-transporteur-{transporteur_id}', 'AdminController@editTransporteur')->name('admin.enregistrement-transporteur-edit')->where('transporteur_id', '[0-9]+');
    Route::put('edition-transporteur-{transporteur_id}', 'AdminController@patchTransporteur')->name('admin.enregistrement-transporteur-edit')->where('transporteur_id', '[0-9]+');

    Route::get('client', 'AdminController@client')->name('admin.enregistrement-client');
    Route::post('client', 'AdminController@postClient')->name('admin.enregistrement-client');
    Route::get('edition-client-{client_id}', 'AdminController@editClient')->name('admin.enregistrement-client-edit')->where('client_id', '[0-9]+');
    Route::put('edition-client-{client_id}', 'AdminController@patchClient')->name('admin.enregistrement-client-edit')->where('client_id', '[0-9]+');

    Route::get('utilisateur', 'AdminController@user')->name('admin.enregistrement-user');
    Route::post('utilisateur', 'AdminController@postUser')->name('admin.enregistrement-user');
    Route::get('edition-utilisateur-{user_id}', 'AdminController@editUser')->name('admin.enregistrement-user-edit')->where('user_id', '[0-9]+');
    Route::put('edition-utilisateur-{user_id}', 'AdminController@patchUser')->name('admin.enregistrement-user-edit')->where('user_id', '[0-9]+');

    Route::get('creation-roles', 'AdminController@roles')->name('admin.enregistrement-roles');
    Route::post('creation-roles', 'AdminController@roleSave')->name('admin.enregistrement-roles');
    Route::get('editer-role-{role_id}', 'AdminController@roleEdit')->name('admin.enregistrement-role-edit')->where('role_id', '[0-9]+');
    Route::put('editer-role-{role_id}', 'AdminController@putRole')->name('admin.enregistrement-role-edit')->where('role_id', '[0-9]+');


    Route::get('permission-utilisateur', 'AdminController@permissions')->name('admin.enregistrement-permissions');
    Route::post('permission-utilisateur', 'AdminController@savePermission')->name('admin.enregistrement-permissions');
    Route::get('permission-utilisateur-{permission}', 'AdminController@permissionsEdit')->name('admin.enregistrement-permission-edit')->where('permission', '[0-9]+');
    Route::put('permission-utilisateur-{permission}', 'AdminController@permissionsPut')->name('admin.enregistrement-permission-edit')->where('permission', '[0-9]+');


    Route::get('role-permission-{role_id}', 'AdminController@rolesPermissions')->name('admin.enregistrement-role-permissions')->where('role_id', '[0-9]+');


    Route::get('attribution-roles', 'AdminController@rolesAttribution')->name('admin.user-roles');
    Route::get('attribution-roles-user-{user_id}', 'AdminController@rolesAttributionUser')->name('admin.user-roles-attibution')->where('user_id', '[0-9]+');
    Route::post('attribution-roles-user-{user_id}', 'AdminController@postRolesAttributionUser')->name('admin.user-roles-attibution')->where('user_id', '[0-9]+');

    Route::get('couts-ciments', 'AdminController@couts')->name('admin.enregistrement-couts');
    Route::post('couts-ciments', 'AdminController@postCoutsCiments')->name('admin.enregistrement-couts');
    Route::get('budget-annuel', 'AdminController@getBudgetAnnuel')->name('admin.enregistrement-budget-annuel');
    Route::post('budget-annuel', 'AdminController@postBudgetAnnuel')->name('admin.enregistrement-budget-annuel');


    Route::get('district', 'AdminController@district')->name('admin.enregistrement-district');
    Route::post('district', 'AdminController@postDistrict')->name('admin.enregistrement-district');
    Route::get('edition-district-{district_id}', 'AdminController@editDistrict')->name('admin.enregistrement-district-edit')->where('district_id', '[0-9]+');
    Route::put('edition-district-{district_id}', 'AdminController@patchDistrict')->name('admin.enregistrement-district-edit')->where('district_id', '[0-9]+');

    Route::get('chef-lieu-district', 'AdminController@chefLieuDistrict')->name('admin.enregistrement-chef-lieu-district');
    Route::post('chef-lieu-district', 'AdminController@postChefLieuDistrict')->name('admin.enregistrement-chef-lieu-district');
    Route::get('chef-lieu-district-{chef_lieu_id}', 'AdminController@editChefLieuDistrict')->name('admin.enregistrement-chef-lieu-district-edit')->where('chef_lieu_id', '[0-9]+');
    Route::put('chef-lieu-district-{chef_lieu_id}', 'AdminController@putChefLieuDistrict')->name('admin.enregistrement-chef-lieu-district-edit')->where('chef_lieu_id', '[0-9]+');

});

Route::group(['prefix' => 'gestion','middleware'=>'auth'], function () {
    Route::get('chargement-table-de-pesee', 'GestionController@index')->name('gestion.load-file');
    Route::post('chargement-table-de-pesee', 'GestionController@traitement')->name('gestion.load-file');

    Route::get('chargement-table-de-tolerance', 'GestionController@tableTolerance')->name('gestion.load-table-tolerance');
    Route::post('chargement-table-de-tolerance', 'GestionController@chargementTableTolerance')->name('gestion.load-table-tolerance');

    Route::get('chargement-donnees-en-masse', 'GestionController@loadMassData')->name('gestion.load-mass-datas');
    Route::post('chargement-donnees-en-masse', 'GestionController@chargeMassData')->name('gestion.load-mass-datas');

    Route::get('recherche-standard', 'GestionController@recherche')->name('gestion.recherche');
    Route::post('recherche-standard', 'GestionController@postRecherche')->name('gestion.recherche');

    Route::post('recherche-advanced', 'GestionController@postAdvanced')->name('gestion.recherche-avancee');
    Route::get('edition-entree-{entree}', 'GestionController@editionEntree')->name('gestion.edition-entree')->where('entree', '[0-9]+');
    Route::post('charge-livraison', 'GestionController@chargeLivraison')->name('gestion.charge-livraison');
});

Route::group(['prefix' => 'recherches-avancees','middleware'=>'auth'], function () {
    Route::get('numero-bon-livraison', 'RechercheAvanceeController@numeroBonLivraison')->name('recherche.numero-bon-livraison');
});

Route::group(['prefix' => 'exploitations','middleware'=>'auth'], function () {
    Route::get('/', 'AdvancedController@recherchePesee')->name('recherche.pesee');
    Route::get('pesees', 'AdvancedController@recherchePesee')->name('recherche.pesee');
    Route::post('pesees', 'AdvancedController@pesees')->name('recherche.pesee');
    Route::get('edition-pesee-{pesee_id}', 'AdvancedController@editionPesee')->name('exploitation.edition-pesee');
    Route::put('edition-pesee-{pesee_id}', 'AdvancedController@putEditionPesee')->name('exploitation.edition-pesee');
    Route::post('bon-de-livraison', 'AdvancedController@loadBonLivraison')->name("recherche.load-bon-livraison");
    Route::delete('suppression-pesee', 'AdvancedController@deletePesee')->name("exploitation.deletion-pesee");

    Route::delete('suppression-pesee', 'AdvancedController@deletePesee')->name("exploitation.deletion-pesee");
    Route::delete('suppression-pesee-par-date', 'AdvancedController@deletePeseeByDate')->name("exploitation.deletion-pesee-by-date");
});

//Auth::routes();

Route::group(['prefix' => 'espace-prive'], function () {
    Route::get('mon-profil', 'UserController@profile')->name('user.profile');
    Route::post('mon-profil', 'UserController@updateProfile')->name('user.profile');
});
Route::group(['prefix' => 'authentification'], function () {
    Route::get('utilisateur', 'Auth\LoginController@showLoginForm')->name('utilisateur.login');
    Route::post('utilisateur', 'Auth\LoginController@login')->name('utilisateur.login');
    Route::get('deconnexion-utilisateur', 'Auth\LoginController@logout')->name('utilisateur.logout');
    Route::get('lock-session-utilisateur', 'Auth\LoginController@locked')->name('utilisateur.locked');
    Route::post('lock-session-utilisateur', 'Auth\LoginController@unlock')->name('utilisateur.locked');
});

Route::group(['prefix' => 'points',], function () {
    Route::get('point-transporteur', 'PointController@transporteur')->name('point.transporteur');
    Route::post('point-transporteur', 'PointController@pointTransporteur')->name('point.transporteur');

    Route::get('point-distributeur', 'PointController@distributeur')->name('point.distributeur');
    Route::post('point-distributeur', 'PointController@pointDistributeur')->name('point.distributeur');

    Route::get('point-localite', 'PointController@localite')->name('point.localite');
    Route::post('point-localite', 'PointController@pointLocalite')->name('point.localite');

    Route::get('point-region', 'PointController@region')->name('point.region');
    Route::post('point-region', 'PointController@pointRegion')->name('point.region');
});

Route::group(['prefix' => 'historiques',], function () {
    Route::get('historique-transporteur', 'HistoriqueController@transporteur')->name('historique.transporteur');
    Route::post('historique-transporteur', 'HistoriqueController@historiqueTransporteur')->name('historique.transporteur');
});

Route::group(['prefix'=>'bilans',], function () {
});

Route::group(['prefix'=>'details',], function () {
    Route::get('localite', 'DetailsController@localite')->name('detail.localite');
    Route::post('localite', 'DetailsController@getDetailLocalite')->name('detail.localite');
});

Route::group(['prefix'=>'points-pesees',], function () {
    Route::get('pesee-journaliere', 'PeseeController@peseeJournaliere')->name('pesee.journaliere');
});
Route::group(['prefix'=>'ventes',], function () {
    Route::get('index', 'VenteController@index')->name('vente.index');
    Route::get('vente-journaliere', 'VenteController@index')->name('vente.index');
    Route::post('vente-journaliere', 'VenteController@consultation')->name('vente.journaliere');
    Route::get('vente-periodique', 'VenteController@index')->name('vente.periode');
    Route::post('vente-periodique', 'VenteController@consultation')->name('vente.periode');
    Route::get('vente-mensuelle', 'VenteController@index')->name('vente.month');
    Route::post('vente-mensuelle', 'VenteController@consultation')->name('vente.month');
    Route::get('vente-annuelle', 'VenteController@index')->name('vente.year');
    Route::post('vente-annuelle', 'VenteController@consultation')->name('vente.year');
});


Route::group(['prefix'=>'departement-commercial',], function () {
    Route::get('ventes-mensuelle', 'CommercialController@venteMensuelles')->name('sce-commercial.vente-mensuelle');
});

Route::group(
    ['namespace' => '\Haruncpi\LaravelLogReader\Controllers',
    'middleware' => ['auth']
    ],
    function () {
        Route::get(config('laravel-log-reader.view_route_path'), 'LogReaderController@getIndex');
        Route::post(config('laravel-log-reader.view_route_path'), 'LogReaderController@postDelete');
        Route::get(config('laravel-log-reader.api_route_path'), 'LogReaderController@getLogs');
    }
);
