<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Budget extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;

    public function produit()
    {
        return $this->belongsTo(Produit::class);
    }

    public function month()
    {
        return $this->belongsTo(Month::class);
    }

    public function annee()
    {
        return $this->belongsTo(Annee::class);
    }
}
