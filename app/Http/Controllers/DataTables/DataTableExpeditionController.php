<?php

namespace App\Http\Controllers\DataTables;

use App\Traits\CanUsedRoles;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Auth\AuthManager;
use App\Http\Controllers\Controller;
use App\Repositories\PeseeRepository;
use Illuminate\Database\Eloquent\Collection;
use Yajra\DataTables\Facades\DataTables;

class DataTableExpeditionController extends Controller
{
    use CanUsedRoles;

    /**
     * @var PeseeRepository
     */
    private $peseeRepository;

    /**
     *@var AuthManager
     */
    private $authManager;

    /**
     * VenteController constructor.
     * @param PeseeRepository $peseeRepository
     * @param AuthManager $authManager
     */
    public function __construct(PeseeRepository $peseeRepository,AuthManager $authManager)
    {

       $this->authManager = $authManager;
       // $this->middleware('auth');
        $this->peseeRepository = $peseeRepository;
    }

    private function _getExpediesQuery(){
        return $this->peseeRepository->pesee()->select(
            "pesees.id",
            "pesees.numero_bl",
            "pesees.qte_bl AS quantite_bl",
            "pesees.poids_entree",
            "pesees.poids_sortie",
            "pesees.qte_pesee as quantite_pesee",
            "pesees.heure_pesee_entree",
            "pesees.heure_debut_chargement",
            "pesees.heure_pesee_sortie",
            "pesees.heure_fin_chargement",
            "pesees.date_entree",
            "pesees.date_sortie",
            "distributeurs.nom AS distributeur",
            "transporteurs.nom as transporteur",
            "produits.libelle AS ciment",
            "vehicules.immatriculation AS immatriculation"

        )
            ->join('produits', 'produits.id', '=', 'pesees.produit_id')
            ->leftJoin('distributeurs', 'distributeurs.id', '=', 'pesees.distributeur_id')
            ->leftJoin('transporteurs', 'transporteurs.id', '=', 'pesees.transporteur_id')
            ->leftJoin('vehicules', 'pesees.id', '=', 'vehicules.pesee_id')
            ->leftJoin('destinations', 'destinations.id', '=', 'pesees.destination_id')

            ;

    }

    private function _expediesDataTableColumn($ventes)
    {

        return DataTables::collection($ventes)
            ->addColumn('id', function ($q) {
                return $q->id;
            })
            ->addColumn('numero_bl', function ($q) {
                return $q->numero_bl;
            })
            ->addColumn('distributeur', function ($q) {
                return "{$q->distributeur}";
            })->addColumn('transporteur', function ($q) {
                return "{$q->transporteur}";
            })
            ->addColumn('immatriculation', function ($q) {
                return "{$q->immatriculation}";
            })
            ->addColumn('ciment', function ($q) {
                return "{$q->ciment}";
            })
            ->addColumn('quantite_bl', function ($q) {
                return "{$q->quantite_bl}";
            })
            ->addColumn('quantite_pesee', function ($q) {
                return "{$q->quantite_pesee}";
            })
            ->addColumn('date_entree', function ($q) {
                return "{$q->date_entree->format('d/m/Y')}";
            })
            ->addColumn('heure_entree', function ($q) {
                return "{$q->heure_pesee_entree}";
            })
            ->addColumn('date_sortie', function ($q) {
                return $q->date_sortie->format('d/m/Y');
            })->addColumn('heure_sortie', function ($q) {
                return $q->heure_pesee_sortie;
            })->addColumn('debut_chargement', function ($q) {
                return $q->heure_debut_chargement;
            })->addColumn('fin_chargement', function ($q) {
                return $q->heure_fin_chargement;
            })->addColumn('ecart', function ($q) {
                $ecart = $q->quantite_pesee >0 ?  ($q->quantite_pesee - $q->quantite_bl) / $q->quantite_pesee : 0;
                $res =  round($ecart,5);

                return $res*100 ."%";
            })
//            ->setRowClass(function ($q) {
//                $ecart = $q->quantite_pesee > 0 ?  ($q->quantite_pesee - $q->quantite_bl) / $q->quantite_pesee : 0;
//                $res =  round($ecart,5);
//                $res = $res * 100;
//                    if($res<= 0.02){
//                        return 'alert-danger';
//                    }elseif ($res >=0.8){
//                        return 'alert-warning';
//                    }
//
//               // return $res <= 0.02 == 0 ? 'alert-success' : 'alert-warning';
//            })
            ->addColumn('tps_attente', function ($q) {
                $heure_entree = $q->heure_pesee_entree ?? '00:00:00';
                $heure_sortie = $q->heure_pesee_sortie ?? '00:00:00';
              $debut =    Carbon::parse("{$q->date_entree->format('Y-m-d')} {$heure_entree}" );
              $fin =    Carbon::parse("{$q->date_sortie->format('Y-m-d')} {$heure_sortie}" );

            return date("H:i:s", strtotime($debut->diff($fin)->format("%H:%i:%s")));
            })
            ->addColumn('tps_chargement', function ($q) {
                $debut_chargement = $q->heure_debut_chargement ?? '00:00:00';
                $fin_chargement = $q->heure_fin_chargement ?? '00:00:00';

                $debut =   Carbon::parse("{$q->date_entree->format('Y-m-d')} {$debut_chargement}" );
                $fin =   Carbon::parse("{$q->date_sortie->format('Y-m-d')} {$fin_chargement}" );
                return date("H:i:s", strtotime($debut->diff($fin)->format("%H:%i:%s")));
            })

            ->addColumn('destination', function ($q) {
                return $q->destination;
            })

            ->editColumn('id', function ($q) {
                return $q->id;
            });
    }





    private function _makeDataTable(Collection $collection)
    {
        /**
         * @var AuthManager
         */
        $user =  $this->authManager->user();

        return $this->_expediesDataTableColumn($collection)
            ->addColumn('action', function ($q) use($user) {
//                $route = route("exploitation.edition-pesee", $q->id);
//                if ($user->hasanypermission('Edition données commerciales')):
//                    return '<a href="' . $route . '" target="_blank" class="btn btn-xs btn-info"><i class="fas fa-edit"></i></a>';
//                endif;
//                $this->userCanUsePermissions($user,$route,['Edition données commerciales']);
            })
            ->startsWithSearch()
            ->rawColumns(['action',])
            ->escapeColumns(['action',])
            ->toJson();
    }
    public function expediesJournaliere(Request $request)
    {
        $date = date('Y-m-d', $request->datetime);
        $expedies = $this->_getExpediesQuery()
            ->where('date_pesee','=',$date)
            ->get();
        #ddd($expedies);
        return $this->_makeDataTable($expedies);
    }
    public function expediesPeriodique(Request $request)
    {
        $debut = date('Y-m-d', $request->debut);
        $fin = date('Y-m-d', $request->fin);
        $expedies = $this->_getExpediesQuery()
            ->whereBetween('date_pesee',[$debut,$fin])
            ->get();
        return $this->_makeDataTable($expedies);
    }
    public function expediesMensuelle(Request $request)
    {
        $debut = date('Y-m-d', $request->debut);
        $fin = date('Y-m-d', $request->fin);
        $expedies = $this->_getExpediesQuery()
            ->whereBetween('date_pesee',[$debut,$fin])
            ->get();
        return $this->_makeDataTable($expedies);
    }
    public function expediesAnnuelle(Request $request)
    {
        $debut = date('Y-m-d', $request->debut);
        $fin = date('Y-m-d', $request->fin);
        $expedies = $this->_getExpediesQuery()
            ->whereBetween('date_pesee',[$debut,$fin])
            ->get();
        return $this->_makeDataTable($expedies);
    }
}
