<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class District extends Model
{
    protected $fillable=['libelle'];
    public $timestamps = false;

    public function setLibelleAttribute($value)
    {
        return $this->attributes['libelle'] = STR::upper($value);
    }

    public function chef_lieu_districts()
    {
        return $this->hasOne( ChefLieuDistrict::class);
    }
}
