@extends('template.theme')


@section('font-title')

@endsection
@push('scripts')

    <script type="text/javascript">
        (function () {

            'use strict';

            // basic
            $("#weekly-form").validate({
                highlight: function (label) {
                    $(label).closest('.form-group').removeClass('has-success').addClass('has-error');
                },
                success: function (label) {
                    $(label).closest('.form-group').removeClass('has-error');
                    label.remove();
                },
                errorPlacement: function (error, element) {
                    var placement = element.closest('.input-group');
                    if (!placement.get(0)) {
                        placement = element;
                    }
                    if (error.text() !== '') {
                        placement.after(error);
                    }
                }
            });

            // validation summary
            var $summaryForm = $("#summary-form");
            $summaryForm.validate({
                errorContainer: $summaryForm.find('div.validation-message'),
                errorLabelContainer: $summaryForm.find('div.validation-message ul'),
                wrapper: "li"
            });

            // checkbox, radio and selects
            $("#chk-radios-form, #selects-form").each(function () {
                $(this).validate({
                    highlight: function (element) {
                        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                    },
                    success: function (element) {
                        $(element).closest('.form-group').removeClass('has-error');
                    },
                    errorPlacement: function (error, element) {
                        var placement = $(element).parent();

                        placement.append(error);
                    }
                });
            });

            // Select 2 Fields
            $('select[data-plugin-selectTwo]').on('change', function () {
                $(this).valid();
            });



        }).apply(this, [jQuery]);
        $(document).ready(function () {

            $('.selectPicker').select2({
                title: "Liste d'élement ...",
                header: 'Selectionnez un élement dans la liste',
                liveSearch: true,
                width: 'resolve'
            });
            $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
                localStorage.setItem('activeTab', $(e.target).attr('href'));
            });
            var activeTab = localStorage.getItem('activeTab');
            if (activeTab) {
                $('#navTabs a[href="' + activeTab + '"]').tab('show');
            }
        });
    </script>

@endpush


@section('content-body')

    <div class="row">
        <div class="col-6 offset-3">
            <div class="tabs">
                <ul class="nav nav-tabs" id="navTabs">
                    <li class="nav-item ">
                        <a class="nav-link" href="#daily" data-toggle="tab"> Journalière</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#period" data-toggle="tab">Periode</a>
                    </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#monthly" data-toggle="tab">Mensuel</a>
                                        </li>
                    {{--                    <li class="nav-item">--}}
                    {{--                        <a class="nav-link" href="#three-monthly" data-toggle="tab">Trimestriel</a>--}}
                    {{--                    </li>--}}
                    {{--                    <li class="nav-item">--}}
                    {{--                        <a class="nav-link" href="#half-yearly" data-toggle="tab">Semestriel</a>--}}
                    {{--                    </li>--}}
                                        <li class="nav-item">
                                            <a class="nav-link" href="#yearly" data-toggle="tab">Annuel</a>
                                        </li>
                </ul>
                <div class="tab-content">
                    <div class="card-subtitle">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>

                    <div id="daily" class="tab-pane active">
                        {!! Form::open(['url'=>route('vente.journaliere'),'class'=>'daily-form',"id"=>'daily-form']) !!}
                        <section class="card">
                            <header class="card-header">
                                <div class="card-actions">
                                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                    <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                                </div>

                                <h2 class="card-title"> Consulter Ventes </h2>
                                <p class="card-subtitle">
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif
                                    </p>
                            </header>
                            <div class="card-body">
                                <div class="form-row">

                                    <div class="col">
                                        <div class="form-group">
                                            <label class="col-form-label" for="date_jour">Date </label>
                                            <div class="input-group">
														<span class="input-group-prepend">
															<span class="input-group-text">
																<i class="fas fa-calendar-alt"></i>
															</span>
														</span>
                                                <input type="text" name="date_jour"
                                                       value="{{ old('date_jour',request('date_jour')) }}"
                                                       id="date_jour" class="form-control" autocomplete="off"
                                                       required
                                                       data-plugin-datepicker
                                                       data-plugin-options='{"orientation":"bottom","autoclose":true,"language":"fr","todayHighlight":true}'
                                                >
                                            </div>
                                            @error('date_jour')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>


                                </div>
                            </div>
                            <footer class="card-footer">
                                <div class="row ">
                                    <div class="col-sm-9 align-content-md-end">
                                        <input type="hidden" name="case" value="daily">
                                        <button type="submit" class="btn btn-primary"><i
                                                class="fas search"></i> Rechercher
                                        </button>


                                    </div>
                                </div>
                            </footer>
                        </section>
                        {!! Form::close() !!}
                    </div>


                    <div id="period" class="tab-pane">
                        {!! Form::open(['url'=>route('vente.periode'),'class'=>'weekly-form',"id"=>'period-form']) !!}
                        <section class="card">
                            <header class="card-header">
                                <div class="card-actions">
                                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                    <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                                </div>

                                <h2 class="card-title"> Consulter les ventes </h2>
                                <p class="card-subtitle">
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif
                                    </p>
                            </header>
                            <div class="card-body">
                                <div class="form-group row">
                                    <label class="col-lg-3 control-label text-lg-right pt-2">Periode</label>
                                    <div class="col-lg-9">
                                        <div class="input-daterange input-group" data-plugin-datepicker required

                                             data-plugin-options='{"orientation":"bottom","autoclose":true,"language":"fr","todayHighlight":true}'>
														<span class="input-group-prepend">
															<span class="input-group-text">
																<i class="fas fa-calendar-alt"></i>
															</span>
														</span>
                                            <input type="text" class="form-control dateRange" name="start"
                                                   autocomplete="off">
                                            <span
                                                class="input-group-text border-left-0 border-right-0 rounded-0">
															Au
														</span>
                                            <input type="text" class="form-control" name="end" autocomplete="off">
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <footer class="card-footer">
                                <div class="row ">
                                    <div class="col-sm-9 align-content-md-end">
                                        <input type="hidden" name="case" value="period">
                                        <button type="submit" class="btn btn-primary"><i
                                                class="fas search"></i> Rechercher
                                        </button>


                                    </div>
                                </div>
                            </footer>
                        </section>
                        {!! Form::close() !!}
                    </div>
                    <div id="monthly" class="tab-pane ">
                        {!! Form::open(['url'=>route('vente.month'),'class'=>'monthly-form',"id"=>'monthly-form']) !!}
                        <section class="card">
                            <header class="card-header">
                                <div class="card-actions">
                                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                    <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                                </div>

                                <h2 class="card-title"> Consulter Ventes </h2>
                                <p class="card-subtitle">
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif
                                    </p>
                            </header>
                            <div class="card-body">
                                <div class="form-row">

                                    <div class="col">
                                        <div class="form-group">
                                            <label class="col-form-label" for="date_month">Date </label>
                                            <div class="input-group">
														<span class="input-group-prepend">
															<span class="input-group-text">
																<i class="fas fa-calendar-alt"></i>
															</span>
														</span>
                                                <input type="text" name="date_month"
                                                       value="{{ old('date_month',request('date_month')) }}"
                                                       id="date_month" class="form-control" autocomplete="off"
                                                       required
                                                       data-plugin-datepicker
                                                       data-date-view="months"
                                                       data-date-view-mode="months"
                                                       data-date-min-view-mode="months"
                                                       data-date-format="mm/yyyy"
                                                       data-plugin-options='{"orientation":"bottom","autoclose":true}'
                                                >
                                            </div>
                                            @error('date_month')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>


                                </div>
                            </div>
                            <footer class="card-footer">
                                <div class="row ">
                                    <div class="col-sm-9 align-content-md-end">
                                        <input type="hidden" name="case" value="monthly">
                                        <button type="submit" class="btn btn-primary"><i
                                                class="fas search"></i> Rechercher
                                        </button>


                                    </div>
                                </div>
                            </footer>
                        </section>
                        {!! Form::close() !!}
                    </div>
                    <div id="yearly" class="tab-pane ">
                        {!! Form::open(['url'=>route('vente.year'),'class'=>'yearly-form',"id"=>'yearly-form']) !!}
                        <section class="card">
                            <header class="card-header">
                                <div class="card-actions">
                                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                    <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                                </div>

                                <h2 class="card-title"> Consulter Ventes </h2>
                                <p class="card-subtitle">
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif
                                    </p>
                            </header>
                            <div class="card-body">
                                <div class="form-row">

                                    <div class="col">
                                        <div class="form-group">
                                            <label class="col-form-label" for="date_year">Date </label>
                                            <div class="input-group">
														<span class="input-group-prepend">
															<span class="input-group-text">
																<i class="fas fa-calendar-alt"></i>
															</span>
														</span>
                                                <input type="text" name="date_year"
                                                       value="{{ old('date_year',request('date_year')) }}"
                                                       id="date_year" class="form-control" autocomplete="off"
                                                       required
                                                       data-plugin-datepicker
                                                       data-date-view="years"
                                                       data-date-view-mode="years"
                                                       data-date-min-view-mode="years"
                                                       data-date-format="yyyy"
                                                       data-plugin-options='{"orientation":"bottom","autoclose":true}'
                                                >
                                            </div>
                                            @error('date_year')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>


                                </div>
                            </div>
                            <footer class="card-footer">
                                <div class="row ">
                                    <div class="col-sm-9 align-content-md-end">
                                        <input type="hidden" name="case" value="yearly">
                                        <button type="submit" class="btn btn-primary"><i
                                                class="fas search"></i> Rechercher
                                        </button>


                                    </div>
                                </div>
                            </footer>
                        </section>
                        {!! Form::close() !!}
                    </div>

                </div>
            </div>
        </div>

    </div>

    @include('ventes.details')


@endsection
