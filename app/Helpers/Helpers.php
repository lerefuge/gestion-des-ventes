<?php
/**
 * Created by PhpStorm.
 * User: JosiasAka
 * Date: 22/02/2018
 * Time: 15:38
 */

if(! function_exists('page_title')){
    function page_title($title=null){
        $base_title = " DEXCO ";
        if ($title){
            return $title .' | '.$base_title;
        }
    }
}


if(! function_exists('percentage')){
    /**
     * @param int $numerateur
     * @param int $denominateur
     * @param int $position
     * @return float
     */
    function percentage(int $numerateur, int $denominateur, int $position){
      $div  =  ($numerateur*100) / $denominateur;
      return round($div,$position);
    }
}

if(! function_exists('set_active_route')){
    function set_active_route($route){
        return Route::is($route) ? 'active' : '' ;
    }
}
if(! function_exists('setActive')) {
    function setActive($path, $active = 'active')
    {
        return call_user_func_array('Request::is', (array)$path) ? $active : '';
    }
}
/**
 * @param $date
 * @param string $format
 * @return bool
 */
if (! function_exists('IsDate')){

    function IsDate($date, $format = 'Y-m-d H:i:s')
    {
        $version = explode('.', phpversion());
        if (((int)$version[0] >= 5 && (int)$version[1] >= 2 && (int)$version[2] > 17)) {
            $d = \DateTime::createFromFormat($format, $date);
        } else {
            try {
                $d = new \DateTime(date($format, strtotime($date)));
            } catch (Exception $e) {
                $e->getMessage();
            }
        }
        return $d && $d->format($format) == $date;
    }
}
if (! function_exists('numberFormat')){

    function numberFormat( $number,  $decimals=0,string $dec_point=".",string $thousand_sep=","):string
    {

        return number_format($number,$decimals,$dec_point,$thousand_sep);
    }
}


