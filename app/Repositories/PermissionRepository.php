<?php


namespace App\Repositories;


use Spatie\Permission\Models\Permission;

class PermissionRepository
{


    /**
     * @var Permission
     */
    private $permission;

    /**
     * PermissionRepository constructor.
     * @param Permission $permission
     */
    public function __construct(Permission $permission)
    {

        $this->permission = $permission;
    }

    public function permission()
    {
        return $this->permission->newQuery();
    }
}
