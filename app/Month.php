<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Month extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;

    public function budget()
    {
        return $this->hasOne(Budget::class);
    }
}
