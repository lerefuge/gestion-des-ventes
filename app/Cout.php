<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cout extends Model
{
    protected $guarded=['id'];
    protected $dates =['date_debut','date_fin'];

    public function produit()
    {
        return $this->belongsTo(Produit::class);
    }
}
