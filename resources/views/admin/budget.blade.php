@extends('template.theme')


@section('font-title')

@endsection
@push('scripts')

    <script type="text/javascript">
        $(function () {
            $('.selectPicker').select2({
                title: "Liste d'élément ...",
                header: 'Selectionnez un élément dans la liste',
                liveSearch: true,
                width: 'resolve'
            });
        });

    </script>

@endpush


@section('content-body')

    <div class="row">
        <div class="col-md-5">
            @if($budget->id)
            {!! Form::open(['url'=>route('admin.enregistrement-client-edit',$budget->id),'class'=>'form-horizontal','method'=>'PUT']) !!}
                @method('put')
            @else
            {!! Form::open(['url'=>route('admin.enregistrement-budget-annuel'),'class'=>'form-horizontal']) !!}
            @endif
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title"> Enregistrement des budget </h2>

                </header>
                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="row form-group">
                        <div class="col-4">

                            <div class="form-group">
                                <label for="month_id" class="control-label">Mois </label>
                                {!! Form::select('month_id',$months->pluck('libelle','id') , $budget->month_id ,
                                ['id'=>'month_id','class'=>'form-control selectPicker','placeholder'=>"Mois",'required'=>'required',
                                'required'=>'required']) !!}
                                @error('month_id')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-4">

                            <div class="form-group">
                                <label for="produit_id" class="control-label">Ciment : </label>
                                {!! Form::select('produit_id',$produits->pluck('libelle','id') , $budget->produit_id ,
                                 ['id'=>'produit_id','class'=>'form-control selectPicker','placeholder'=>"Ciment",'required'=>'required',
                                 'required'=>'required']) !!}
                                @error('produit_id')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-4">

                            <div class="form-group">
                                <label for="annee_id" class="control-label">Année : </label>
                                {!! Form::select('annee_id',$years->pluck('libelle','id') , $budget->annee_id ,
                                 ['id'=>'annee_id','class'=>'form-control selectPicker','placeholder'=>"Année",'required'=>'required',
                                 'required'=>'required']) !!}
                                @error('annee_id')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-6">

                            <div class="form-group">
                                <label for="officiel" class="control-label"> Officiel : </label>
                                {!! Form::number('officiel',$budget->officiel,
                                ['id'=>'officiel','class'=>'form-control','required'=>'required','min'=>0,'placeholder'=>'Buget Officiel']) !!}
                                @error('officiel')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col">

                            <div class="form-group">
                                <label for="challenge" class="control-label">Challenge : </label>
                                {!! Form::number('challenge',$budget->challenge,
                                ['id'=>'challenge','class'=>'form-control','min'=>0,'placeholder'=>'Budget challenge']) !!}
                                @error('challenge')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>


                </div>
                <footer class="card-footer">
                    <div class="row justify-content-end">
                        <div class="col-sm-9">
                            @if($budget->id)
                                <button type="submit" class="btn btn-primary" data-toggle="confirmation">Mettre à jour</button>
                            @else
                                <button type="submit" class="btn btn-primary" data-toggle="confirmation">Enregistrer</button>
                                <button type="reset" class="btn btn-default">Effacer</button>
                            @endif
                        </div>
                    </div>
                </footer>
            </section>
            {!! Form::close() !!}

        </div>
        <div class="col-md-7">
            <div class="row">
                <div class="col">
                    <section class="card">
                        <header class="card-header">
                            <div class="card-actions">
                                <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                            </div>

                            <h2 class="card-title">Localités enregistrées</h2>
                        </header>
                        <div class="card-body">
                            <table class="table table-bordered table-striped mb-0 datatable-default">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Mois</th>
                                    <th>Ciment</th>
                                    <th>Budget Officiel</th>
                                    <th>Budget Challenge</th>

                                    <th class="">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($budgets as $budget)
                                    <tr>
                                        <td>{{ $loop->index + 1 }}</td>
                                        <td>{{ $budget->month->libelle }}</td>
                                        <td>{{ $budget->produit->libelle }}</td>
                                        <td>{{ $budget->officiel}}</td>
                                        <td>{{ $budget->challenge}}</td>

                                        <td class="flex text-center">
                                            <a href="{{ route('admin.enregistrement-client-edit',$budget) }}" class="on-default edit-row" data-toggle="confirmation"><i
                                                    class="fas fa-pencil-alt"></i></a>
                                            <a href="#" class="on-default remove-row"><i
                                                    class="far fa-trash-alt"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>





@endsection
