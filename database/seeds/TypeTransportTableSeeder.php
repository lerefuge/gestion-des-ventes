<?php

use Illuminate\Database\Seeder;

class TypeTransportTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $type_vehicule = [
            ['libelle'=>'PRIVÉ'],
            ['libelle'=>'MARCHÉ'],
            ['libelle'=>'MINIER'],
        ];
        DB::table('type_vehicules')->insert($type_vehicule);
    }
}
