<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DeleteFieldsOnTablePesses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pesees', function (Blueprint $table) {
            $table->dropColumn(['chauffeur','immatriculation']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pesees', function (Blueprint $table) {
            $table->string('chauffeur')->nullable();
            $table->string('immatriculation')->nullable();
        });
    }
}
