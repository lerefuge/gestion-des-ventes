<?php

namespace App\Http\Controllers;

use App\Repositories\DestinationRepository;
use App\Repositories\DistributeurRepository;
use App\Repositories\PeseeRepository;
use App\Repositories\RegionRepository;
use App\Repositories\TransporteurRepository;
use App\Repositories\TypeSubventionRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PointController extends Controller
{
    /**
     * @var TransporteurRepository
     */
    private $transporteurRepository;
    /**
     * @var TypeSubventionRepository
     */
    private $typeSubventionRepository;
    /**
     * @var PeseeRepository
     */
    private $peseeRepository;
    /**
     * @var DistributeurRepository
     */
    private $distributeurRepository;
    /**
     * @var RegionRepository
     */
    private $regionRepository;
    /**
     * @var DestinationRepository
     */
    private $destinationRepository;

    /**
     * PointController constructor.
     * @param TransporteurRepository $transporteurRepository
     * @param DistributeurRepository $distributeurRepository
     * @param RegionRepository $regionRepository
     * @param DestinationRepository $destinationRepository
     * @param TypeSubventionRepository $typeSubventionRepository
     * @param PeseeRepository $peseeRepository
     */
    public function __construct(TransporteurRepository $transporteurRepository,
                                DistributeurRepository $distributeurRepository,
                                RegionRepository $regionRepository,
                                DestinationRepository $destinationRepository,
                                TypeSubventionRepository $typeSubventionRepository,PeseeRepository $peseeRepository)
    {
        $this->middleware('auth');
        $this->transporteurRepository = $transporteurRepository;
        $this->typeSubventionRepository = $typeSubventionRepository;
        $this->peseeRepository = $peseeRepository;

        $this->distributeurRepository = $distributeurRepository;
        $this->regionRepository = $regionRepository;
        $this->destinationRepository = $destinationRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function transporteur()
    {

        $data['results']=false;
        $data['transporteurs']=$this->transporteurRepository->transporteur()->get()->sortBy('name');
        $data['type_subventions'] = $this->typeSubventionRepository->typeSubvention()->get();
        return view('points.transporteur')->with($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function pointTransporteur(Request $request)
    {
        $data['results'] =[];
        $case = $request->case;
        $data['case'] =$case;
        $transporteur_id = $request->transporteur_id;

        $type_subvention_id = $request->type_subvention_id;


        if ($case=='daily'){
            $date = Carbon::parse($request->date_pesee_entree)->format('Y-m-d');
            $points =   $this->peseeRepository->pointJourTransporteur($transporteur_id,$date,$type_subvention_id);
        }elseif ($case=='weekly'){
            $start = Carbon::parse($request->start)->format('Y-m-d');
            $end = Carbon::parse($request->end)->format('Y-m-d');
             $points =   $this->peseeRepository->pointHebdoTransporteur($transporteur_id,$start,$end,$type_subvention_id);
        }elseif ($case='monthly'){

        }
       // dd($points);
        $data['transporteurs']=$this->transporteurRepository->transporteur()->get()->sortBy('name');
        $data['type_subventions'] = $this->typeSubventionRepository->typeSubvention()->get();
        if($points->isNotEmpty()) {
            $data['results'] = $points;
        }else{
            flash('Aucune donnée correspondante a ce jour trouvée');
            return redirect()->back();
        }
        return view('points.transporteur')->with($data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function distributeur()
    {
        $data['results']=false;
        $data['distributeurs']=$this->distributeurRepository->distributeur()->get()->sortBy('nom');
        $data['type_subventions'] = $this->typeSubventionRepository->typeSubvention()->get();
        return view('points.distributeur')->with($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function pointDistributeur(Request $request)
    {
        //  dd($request->all());
        $data['results'] =[];
        $case = $request->case;
        $data['case'] =$case;
        $distributeur_id = $request->distributeur_id;

        $type_subvention_id = $request->type_subvention_id;


        if ($case=='daily'){
            $date = Carbon::parse($request->date_pesee_entree)->format('Y-m-d');
            $points =   $this->peseeRepository->pointJourDistributeur($distributeur_id,$date,$type_subvention_id);
        }
        if ($case=='weekly'){
            $start = Carbon::parse($request->start)->format('Y-m-d');
            $end = Carbon::parse($request->end)->format('Y-m-d');
            $points =   $this->peseeRepository->pointHebdoDistributeur($distributeur_id,$start,$end,$type_subvention_id);
        }
        // dd($points);
        $data['distributeurs']=$this->distributeurRepository->distributeur()->get()->sortBy('name');
        $data['type_subventions'] = $this->typeSubventionRepository->typeSubvention()->get();
        if($points->isNotEmpty()) {
            $data['results'] = $points;
        }else{
            flash('Aucune donnée correspondante a ce jour trouvée');
            return redirect()->back();
        }
        return view('points.distributeur')->with($data);
    }

    public function region()
    {
        $data['results']=false;
        $data['regions']=$this->regionRepository->region()->get()->sortBy('libelle');
        $data['type_subventions'] = $this->typeSubventionRepository->typeSubvention()->get();
        return view('points.region')->with($data);
    }

    public function localite()
    {
        $data['results']=false;
        $data['localites']=$this->destinationRepository->destination()->get()->sortBy('libelle');
        $data['type_subventions'] = $this->typeSubventionRepository->typeSubvention()->get();
        return view('points.localite')->with($data);
    }

    public function pointLocalite(Request $request)
    {
        $data['results'] =[];
        $case = $request->case;
        $data['case'] =$case;
        $localite_ids = $request->localite_ids;

        $type_subvention_id = $request->type_subvention_id;


//        if ($case=='daily'){
//            $date = Carbon::parse($request->date_pesee_entree)->format('Y-m-d');
//            $points =   $this->peseeRepository->pointJourDistributeur($distributeur_id,$date,$type_subvention_id);
//        }
//        if ($case=='weekly'){
//            $start = Carbon::parse($request->start)->format('Y-m-d');
//            $end = Carbon::parse($request->end)->format('Y-m-d');
//            $points =   $this->peseeRepository->pointHebdoDistributeur($distributeur_id,$start,$end,$type_subvention_id);
//        }
    }
}
