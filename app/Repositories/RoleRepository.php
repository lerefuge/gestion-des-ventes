<?php


namespace App\Repositories;


use Spatie\Permission\Models\Role;

class RoleRepository
{

    /**
     * @var Role
     */
    private $role;

    /**
     * RoleRepository constructor.
     * @param Role $role
     */
    public function __construct(Role $role)
    {

        $this->role = $role;
    }

    public function role()
    {
        return $this->role->newQuery();
    }
}
