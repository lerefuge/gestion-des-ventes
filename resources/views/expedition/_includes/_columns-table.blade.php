"columns": [

{{--{data: 'id', name: 'id', sortable: false, orderable: false, searchable: false},--}}
{data: 'numero_bl', name: 'numero_bl', sortable: true, orderable: true, searchable: true},
{data: 'distributeur', name: 'distributeur', sortable: true, orderable: true, searchable: true},
{data: 'transporteur', name: 'transporteur', sortable: true, orderable: true, searchable: true},
{data: 'immatriculation', name: 'immatriculation', sortable: true, orderable: true, searchable: true},
{data: 'ciment', name: 'ciment', sortable: true, orderable: true, searchable: true},
{data: 'quantite_bl', name: 'quantite_bl', sortable: true, orderable: true, searchable: true},
{data: 'quantite_pesee', name: 'quantite_pesee', sortable: true, orderable: true, searchable: true},
{data: 'ecart', name: 'ecart', sortable: true, orderable: true, searchable: true},
{data: 'date_entree', name: 'date_entree', sortable: true, orderable: true, searchable: true},
{data: 'date_sortie', name: 'date_sortie', sortable: true, orderable: true,searchable:true},
{data: 'heure_entree', name: 'heure_entree', sortable: true, orderable: true, searchable: true},
{data: 'heure_sortie', name: 'heure_sortie', sortable: true, orderable: true,searchable:true},
{data: 'tps_attente', name: 'tps_attente', sortable: true, orderable: true,searchable:true},
{data: 'debut_chargement', name: 'debut_chargement', sortable: true, orderable: true, searchable: true},
{data: 'fin_chargement', name: 'fin_chargement', sortable: true, orderable: true, searchable: true},
{data: 'tps_chargement', name: 'tps_chargement', sortable: true, orderable: true,searchable:true},
{{--{data: 'destination', name: 'destination', sortable: true, orderable: true,searchable:true},--}}
{data: 'action', name: 'action', sortable: false, orderable: false, searchable: false},
],
