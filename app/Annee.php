<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Annee extends Model
{
    protected $guarded = ['id'];

    public $timestamps = false;
}
