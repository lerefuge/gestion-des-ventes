@extends('template.theme')


@section('font-title')

@endsection

@section('css-libs')

@endsection
@push('scripts')

    <script type="text/javascript">
        (function () {

            'use strict';

            // basic

            // Select 2 Fields
            $('select[data-plugin-selectTwo]').on('change', function () {
                $(this).valid();
            });
            $('.selectPicker').select2({
                title: "Liste d'élement ...",
                placeholder: 'Selectionnez un élement dans la liste',
                liveSearch: true,allowClear: true,
                width: 'resolve'
            });

            $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
                localStorage.setItem('activeTab', $(e.target).attr('href'));
            });
            var activeTab = localStorage.getItem('activeTab');
            if (activeTab) {
                $('#navTabs a[href="' + activeTab + '"]').tab('show');
            }

        }).apply(this, [jQuery]);

    </script>
@endpush


@section('content-body')
<div class="row">
    <div class="col-8 offset-2">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
</div>
                   <div class="col-6 offset-3">

                    <div class="tabs">
                        <ul class="nav nav-tabs" id="navTabs">

                           <li class="nav-item">
                                <a class="nav-link current" href="#yearly" data-toggle="tab">Annuel</a>
                            </li>
                        </ul>
                        <div class="tab-content">

                            <div id="yearly" class="tab-pane active">
                                {!! Form::open(['url'=>route('commercial.tableau-ventes'),'class'=>'yearly-form',"id"=>'yearly-form']) !!}
                                <section class="card">
                                    <header class="card-header">
                                        <div class="card-actions">
                                            <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                            <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                                        </div>

                                        <h2 class="card-title"> Consulter Ventes </h2>
                                        <p class="card-subtitle">

                                            </p>
                                    </header>
                                    <div class="card-body">
                                        <div class="form-row">

                                            <div class="col">
                                                <div class="form-group">
                                                    <label class="col-form-label" for="date_year">Date </label>
                                                    <div class="input-group">
														<span class="input-group-prepend">
															<span class="input-group-text">
																<i class="fas fa-calendar-alt"></i>
															</span>
														</span>
                                                        <input type="text" name="date_year"
                                                               value="{{ old('date_year',request('date_year')) }}"
                                                               id="date_year" class="form-control" autocomplete="off"
                                                               required
                                                               data-plugin-datepicker
                                                               data-date-view="years"
                                                               data-date-view-mode="years"
                                                               data-date-min-view-mode="years"
                                                               data-date-format="yyyy"
                                                               data-plugin-options='{"orientation":"bottom","autoclose":true}'
                                                        >
                                                    </div>
                                                    @error('date_year')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>


                                        </div>
                                        <div class="row">
                                            <div class="col">

                                                <label class=" control-label"> Zones </label>
                                                {!! Form::select('zone_id[]',$zones->pluck('libelle','id'),request('zone_id'),['class'=>'form-control selectPicker', 'required'=>true,'multiple'=>true,'style'=>'width:98%'
                                                         ]
                                                          ) !!}

                                            </div>
                                        </div>
                                        <hr>
                                    </div>
                                    <footer class="card-footer">
                                        <div class="row ">
                                            <div class="col-sm-9 align-content-md-end">
                                                <input type="hidden" name="case" value="yearly">
                                                <button type="submit" class="btn btn-primary"><i
                                                        class="fas search"></i> Rechercher
                                                </button>


                                            </div>
                                        </div>
                                    </footer>
                                </section>
                                {!! Form::close() !!}
                            </div>

                        </div>
                    </div>
                </div>
<hr>

<table class="table small table-bordered table-condensed  mb-0 ">
    <tbody>
    <tr class="text-center">
        <th scope="col">&nbsp;</th>
        <th scope="col">&nbsp;QUALITE</th>
        <th scope="col">JANVIER&nbsp;</th>
        <th scope="col">FEVRIER&nbsp;</th>
        <th scope="col">MARS&nbsp;</th>
        <th scope="col">AVRIL&nbsp;</th>
        <th scope="col">MAI&nbsp;</th>
        <th scope="col">JUIN&nbsp;</th>
        <th scope="col">JUILLET&nbsp;</th>
        <th scope="col">AOUT&nbsp;</th>
        <th scope="col">&nbsp;SEPTEMBRE</th>
        <th scope="col">&nbsp;OCTOBRE</th>
        <th scope="col">NOVEMBRE&nbsp;</th>
        <th scope="col">DECEMBRE&nbsp;</th>
        <th scope="col">TOTAUX</th>
    </tr>
    <tr class="text-center">

        <th scope="row" rowspan="5">&nbsp;SAC</th>
        <th scope="row">CPJ 32.5</th>
        <td>{{   number_format(data_get($expedies, '01.sacs.32_5'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '02.sacs.32_5'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '03.sacs.32_5'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '04.sacs.32_5'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '05.sacs.32_5'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '06.sacs.32_5'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '07.sacs.32_5'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '08.sacs.32_5'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '09.sacs.32_5'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '10.sacs.32_5'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '11.sacs.32_5'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '12.sacs.32_5'),2,',',' ') }}</td>
        <th>&nbsp; @php($total_annee_32_sac = data_get($expedies, '01.sacs.32_5') + data_get($expedies, '02.sacs.32_5')+ data_get($expedies, '03.sacs.32_5')+ data_get($expedies, '04.sacs.32_5')+ data_get($expedies, '05.sacs.32_5')+ data_get($expedies, '06.sacs.32_5')+ data_get($expedies, '07.sacs.32_5')+ data_get($expedies, '08.sacs.32_5')+ data_get($expedies, '09.sacs.32_5')+ data_get($expedies, '10.sacs.32_5')+ data_get($expedies, '11.sacs.32_5')+ data_get($expedies, '12.sacs.32_5'))
           {{   number_format($total_annee_32_sac,2,',',' ') }}
        </th>
    </tr>
    <tr class="text-center">
        <th scope="row">&nbsp;CPJ 42.5 </th>
        <td>{{   number_format(data_get($expedies, '01.sacs.42_5'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '02.sacs.42_5'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '03.sacs.42_5'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '04.sacs.42_5'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '05.sacs.42_5'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '06.sacs.42_5'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '07.sacs.42_5'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '08.sacs.42_5'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '09.sacs.42_5'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '10.sacs.42_5'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '11.sacs.42_5'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '12.sacs.42_5'),2,',',' ') }}</td>
        <th >
            @php($total_annee_42_sac = data_get($expedies, '01.sacs.42_5') + data_get($expedies, '02.sacs.42_5')+ data_get($expedies, '03.sacs.42_5')+ data_get($expedies, '04.sacs.42_5')+ data_get($expedies, '05.sacs.42_5')+ data_get($expedies, '06.sacs.42_5')+ data_get($expedies, '07.sacs.42_5')+ data_get($expedies, '08.sacs.42_5')+ data_get($expedies, '09.sacs.42_5')+ data_get($expedies, '10.sacs.42_5')+ data_get($expedies, '11.sacs.42_5')+ data_get($expedies, '12.sacs.42_5'))
            {{   number_format($total_annee_42_sac,2,',',' ') }}

        </th>
    </tr>
    <tr class="text-center">
        <th scope="row">CPA ROBUSTO&nbsp;</th>
        <td>{{   number_format(data_get($expedies, '01.sacs.robusto'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '02.sacs.robusto'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '03.sacs.robusto'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '04.sacs.robusto'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '05.sacs.robusto'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '06.sacs.robusto'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '07.sacs.robusto'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '08.sacs.robusto'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '09.sacs.robusto'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '10.sacs.robusto'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '11.sacs.robusto'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '12.sacs.robusto'),2,',',' ') }}</td>
        <th>
            @php($total_annee_robusto_sac = data_get($expedies, '01.sacs.robusto') + data_get($expedies, '02.sacs.robusto')+ data_get($expedies, '03.sacs.robusto')+ data_get($expedies, '04.sacs.robusto')+ data_get($expedies, '05.sacs.robusto')+ data_get($expedies, '06.sacs.robusto')+ data_get($expedies, '07.sacs.robusto')+ data_get($expedies, '08.sacs.robusto')+ data_get($expedies, '09.sacs.robusto')+ data_get($expedies, '10.sacs.robusto')+ data_get($expedies, '11.sacs.robusto')+ data_get($expedies, '12.sacs.robusto'))
            {{   number_format($total_annee_robusto_sac,2,',',' ') }}

        </th>
    </tr>
    <tr class="text-center">
        <th scope="row">CHF &nbsp;</th>
        <td>{{   number_format(data_get($expedies, '01.sacs.chf'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '02.sacs.chf'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '03.sacs.chf'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '04.sacs.chf'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '05.sacs.chf'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '06.sacs.chf'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '07.sacs.chf'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '08.sacs.chf'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '09.sacs.chf'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '10.sacs.chf'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '11.sacs.chf'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '12.sacs.chf'),2,',',' ') }}</td>
        <th>
            @php($total_annee_chf_sac = data_get($expedies, '01.sacs.chf') + data_get($expedies, '02.sacs.chf')+ data_get($expedies, '03.sacs.chf')+ data_get($expedies, '04.sacs.chf')+ data_get($expedies, '05.sacs.chf')+ data_get($expedies, '06.sacs.chf')+ data_get($expedies, '07.sacs.chf')+ data_get($expedies, '08.sacs.chf')+ data_get($expedies, '09.sacs.chf')+ data_get($expedies, '10.sacs.chf')+ data_get($expedies, '11.sacs.chf')+ data_get($expedies, '12.sacs.chf'))
            {{   number_format($total_annee_chf_sac,2,',',' ') }}

           </th>
    </tr>
    <tr class="warning text-center">
        <th scope="row" >TOTAL SACS&nbsp;</th>
        <td>{{   number_format(data_get($expedies, '01.sacs.total_sac'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '02.sacs.total_sac'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '03.sacs.total_sac'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '04.sacs.total_sac'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '05.sacs.total_sac'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '06.sacs.total_sac'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '07.sacs.total_sac'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '08.sacs.total_sac'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '09.sacs.total_sac'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '10.sacs.total_sac'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '11.sacs.total_sac'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '12.sacs.total_sac'),2,',',' ') }}</td>
        <th>
            @php($total_annee_sac =$total_annee_robusto_sac+$total_annee_chf_sac+$total_annee_42_sac+$total_annee_32_sac )
            {{   number_format($total_annee_sac,2,',',' ') }}
        </th>
    </tr>
    <tr class="text-center">
        <th rowspan="5" scope="row" >VRAC&nbsp;</th>
        <th scope="row">CPJ 32.5&nbsp;</th>
        <td>{{   number_format(data_get($expedies, '01.vracs.32_5'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '02.vracs.32_5'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '03.vracs.32_5'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '04.vracs.32_5'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '05.vracs.32_5'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '06.vracs.32_5'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '07.vracs.32_5'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '08.vracs.32_5'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '09.vracs.32_5'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '10.vracs.32_5'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '11.vracs.32_5'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '12.vracs.32_5'),2,',',' ') }}</td>
        <th>&nbsp; @php($total_annee_32_vrac = data_get($expedies, '01.vracs.32_5') + data_get($expedies, '02.vracs.32_5')+ data_get($expedies, '03.vracs.32_5')+ data_get($expedies, '04.vracs.32_5')+ data_get($expedies, '05.vracs.32_5')+ data_get($expedies, '06.vracs.32_5')+ data_get($expedies, '07.vracs.32_5')+ data_get($expedies, '08.vracs.32_5')+ data_get($expedies, '09.vracs.32_5')+ data_get($expedies, '10.vracs.32_5')+ data_get($expedies, '11.vracs.32_5')+ data_get($expedies, '12.vracs.32_5'))
            {{   number_format($total_annee_32_vrac,2,',',' ') }}
        </th>
    </tr>
    <tr class="text-center">
        <th scope="row">CPJ 42.5&nbsp;</th>
        <td>{{   number_format(data_get($expedies, '01.vracs.42_5'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '02.vracs.42_5'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '03.vracs.42_5'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '04.vracs.42_5'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '05.vracs.42_5'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '06.vracs.42_5'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '07.vracs.42_5'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '08.vracs.42_5'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '09.vracs.42_5'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '10.vracs.42_5'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '11.vracs.42_5'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '12.vracs.42_5'),2,',',' ') }}</td>
        <th>&nbsp; @php($total_annee_42_vrac = data_get($expedies, '01.vracs.42_5') + data_get($expedies, '02.vracs.42_5')+ data_get($expedies, '03.vracs.42_5')+ data_get($expedies, '04.vracs.42_5')+ data_get($expedies, '05.vracs.42_5')+ data_get($expedies, '06.vracs.42_5')+ data_get($expedies, '07.vracs.42_5')+ data_get($expedies, '08.vracs.42_5')+ data_get($expedies, '09.vracs.42_5')+ data_get($expedies, '10.vracs.42_5')+ data_get($expedies, '11.vracs.42_5')+ data_get($expedies, '12.vracs.42_5'))
            {{   number_format($total_annee_42_vrac,2,',',' ') }}
        </th>
    </tr>
    <tr class="text-center">
        <th scope="row">&nbsp; CPA ROBUSTO</th>
        <td>{{   number_format(data_get($expedies, '01.vracs.robusto'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '02.vracs.robusto'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '03.vracs.robusto'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '04.vracs.robusto'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '05.vracs.robusto'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '06.vracs.robusto'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '07.vracs.robusto'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '08.vracs.robusto'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '09.vracs.robusto'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '10.vracs.robusto'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '11.vracs.robusto'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '12.vracs.robusto'),2,',',' ') }}</td>
        <th>&nbsp; @php($total_annee_robusto_vrac = data_get($expedies, '01.vracs.robusto') + data_get($expedies, '02.vracs.robusto')+ data_get($expedies, '03.vracs.robusto')+ data_get($expedies, '04.vracs.robusto')+ data_get($expedies, '05.vracs.robusto')+ data_get($expedies, '06.vracs.robusto')+ data_get($expedies, '07.vracs.robusto')+ data_get($expedies, '08.vracs.robusto')+ data_get($expedies, '09.vracs.robusto')+ data_get($expedies, '10.vracs.robusto')+ data_get($expedies, '11.vracs.robusto')+ data_get($expedies, '12.vracs.robusto'))
            {{   number_format($total_annee_robusto_vrac,2,',',' ') }}
        </th>
    </tr>
    <tr class="text-center">
        <th scope="row">&nbsp;CHF</th>
        <td>{{   number_format(data_get($expedies, '01.vracs.chf'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '02.vracs.chf'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '03.vracs.chf'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '04.vracs.chf'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '05.vracs.chf'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '06.vracs.chf'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '07.vracs.chf'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '08.vracs.chf'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '09.vracs.chf'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '10.vracs.chf'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '11.vracs.chf'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '12.vracs.chf'),2,',',' ') }}</td>
        <th>&nbsp; @php($total_annee_chf_vrac = data_get($expedies, '01.vracs.chf') + data_get($expedies, '02.vracs.chf')+ data_get($expedies, '03.vracs.chf')+ data_get($expedies, '04.vracs.chf')+ data_get($expedies, '05.vracs.chf')+ data_get($expedies, '06.vracs.chf')+ data_get($expedies, '07.vracs.chf')+ data_get($expedies, '08.vracs.chf')+ data_get($expedies, '09.vracs.chf')+ data_get($expedies, '10.vracs.chf')+ data_get($expedies, '11.vracs.chf')+ data_get($expedies, '12.vracs.chf'))
            {{   number_format($total_annee_chf_vrac,2,',',' ') }}
        </th>
    </tr>
    <tr class="warning text-center">
        <th scope="row">&nbsp;TOTAL VRAC</th>
        <td>{{   number_format(data_get($expedies, '01.vracs.total_vrac'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '02.vracs.total_vrac'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '03.vracs.total_vrac'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '04.vracs.total_vrac'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '05.vracs.total_vrac'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '06.vracs.total_vrac'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '07.vracs.total_vrac'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '08.vracs.total_vrac'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '09.vracs.total_vrac'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '10.vracs.total_vrac'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '11.vracs.total_vrac'),2,',',' ') }}</td>
        <td>{{  number_format(data_get($expedies, '12.vracs.total_vrac'),2,',',' ') }}</td>
        <th>
            @php($total_annee_vrac =$total_annee_robusto_vrac+$total_annee_chf_vrac+$total_annee_42_vrac+$total_annee_32_vrac )
            {{   number_format($total_annee_vrac,2,',',' ') }}
        </th>
    </tr>
    <tr class="info text-center">
        <th scope="row">&nbsp;</th>
        <th scope="row">&nbsp;TOTAL </th>
        <td>@php($ttx_janv =  data_get($expedies, '01.sacs.total_sac') + data_get($expedies, '01.vracs.total_vrac') )
            {{ number_format($ttx_janv,2,',',' ') }}
        </td>
        <td>@php($ttx_fev =  data_get($expedies, '02.sacs.total_sac') + data_get($expedies, '02.vracs.total_vrac') )
            {{ number_format($ttx_fev,2,',',' ') }}
        </td>
        <td>@php($ttx_mars =  data_get($expedies, '03.sacs.total_sac') + data_get($expedies, '03.vracs.total_vrac') )
            {{ number_format($ttx_mars,2,',',' ') }}
        </td>
        <td>@php($ttx_avril =  data_get($expedies, '04.sacs.total_sac') + data_get($expedies, '04.vracs.total_vrac') )
            {{ number_format($ttx_avril,2,',',' ') }}
        </td>
        <td>@php($ttx_mai =  data_get($expedies, '05.sacs.total_sac') + data_get($expedies, '05.vracs.total_vrac') )
            {{ number_format($ttx_mai,2,',',' ') }}
        </td>
        <td>@php($ttx_juin =  data_get($expedies, '06.sacs.total_sac') + data_get($expedies, '06.vracs.total_vrac') )
            {{ number_format($ttx_juin,2,',',' ') }}
        </td>
        <td>@php($ttx_juillet =  data_get($expedies, '07.sacs.total_sac') + data_get($expedies, '07.vracs.total_vrac') )
            {{ number_format($ttx_juillet,2,',',' ') }}
        </td>
        <td>@php($ttx_aout =  data_get($expedies, '08.sacs.total_sac') + data_get($expedies, '08.vracs.total_vrac') )
            {{ number_format($ttx_aout,2,',',' ') }}
        </td>
        <td>@php($ttx_sept =  data_get($expedies, '09.sacs.total_sac') + data_get($expedies, '09.vracs.total_vrac') )
            {{ number_format($ttx_sept,2,',',' ') }}
        </td>
        <td>@php($ttx_oct =  data_get($expedies, '10.sacs.total_sac') + data_get($expedies, '10.vracs.total_vrac') )
            {{ number_format($ttx_oct,2,',',' ') }}
        </td>
        <td>@php($ttx_nov =  data_get($expedies, '11.sacs.total_sac') + data_get($expedies, '11.vracs.total_vrac') )
            {{ number_format($ttx_nov,2,',',' ') }}
        </td>
        <td>@php($ttx_dec =  data_get($expedies, '12.sacs.total_sac') + data_get($expedies, '12.vracs.total_vrac') )
            {{ number_format($ttx_dec,2,',',' ') }}
        </td>

        <th>
            @php($total_annuel =$ttx_janv+$ttx_fev+$ttx_mars+$ttx_avril+$ttx_mai+$ttx_juin+$ttx_juillet+$ttx_aout+$ttx_sept+$ttx_oct+$ttx_nov+$ttx_dec )
            {{   number_format($total_annuel,2,',',' ') }}
        </th>
    </tr>
    </tbody>
</table>

@endsection
