<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tonnage extends Model
{
    protected $guarded=['id'];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function pesee()
    {
        return $this->belongsTo(Pesee::class);
    }
}
