<?php


namespace App\Repositories;


use App\Vehicule;

class VehiculeRepository
{

    /**
     * @var Vehicule
     */
    private $vehicule;

    /**
     * VehiculeRepository constructor.
     * @param Vehicule $vehicule
     */
    public function __construct(Vehicule $vehicule)
    {

        $this->vehicule = $vehicule;
    }

    public function vehicule()
    {
        return $this->vehicule->newQuery();
    }
}
