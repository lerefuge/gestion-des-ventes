<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="sidebar-light sidebar-left-big-icons">
<head>

    <!-- Basic -->
    <meta charset="UTF-8">

    <title></title>
    <meta name="keywords" content="HTML5 Admin Template"/>
    <meta name="description" content="Porto Admin - Responsive HTML5 Template">
    <meta name="author" content="okler.net">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>

    <!-- Web Fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light"
          rel="stylesheet" type="text/css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/all.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('porto/css/theme.css') }}"/>
    <link rel="stylesheet" href="{{ asset('porto/css/custom.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/porto-vendors.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/notify.css') }}"/>


    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{ asset('css/porto-theme.css') }}"/>
    @notifyCss

@yield('css-libs')
<!-- Head Libs -->
    <script src="{{ asset('porto/vendor/modernizr/modernizr.js')}}"></script>
    {{--    <script src="{{ asset('porto/vendor/jquery/jquery.min.js') }}"></script>--}}
</head>
<body>
@include('notify::messages')
{{--    <div class="row justify-content-center">--}}
{{--        <div class="col-md-8">--}}
{{--            <div class="card">--}}
{{--                <div class="card-header">{{ __('Login') }}</div>--}}

{{--                <div class="card-body">--}}
{{--                    <form method="POST" action="{{ route('utilisateur.login') }}">--}}
{{--                        @csrf--}}

{{--                        <div class="form-group row">--}}
{{--                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>--}}

{{--                                @error('email')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row">--}}
{{--                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">--}}

{{--                                @error('password')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row">--}}
{{--                            <div class="col-md-6 offset-md-4">--}}
{{--                                <div class="form-check">--}}
{{--                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>--}}

{{--                                    <label class="form-check-label" for="remember">--}}
{{--                                        {{ __('Remember Me') }}--}}
{{--                                    </label>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row mb-0">--}}
{{--                            <div class="col-md-8 offset-md-4">--}}
{{--                                <button type="submit" class="btn btn-primary">--}}
{{--                                    {{ __('Login') }}--}}
{{--                                </button>--}}

{{--                                @if (Route::has('password.request'))--}}
{{--                                    <a class="btn btn-link" href="{{ route('password.request') }}">--}}
{{--                                        {{ __('Forgot Your Password?') }}--}}
{{--                                    </a>--}}
{{--                                @endif--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

    <section class="body-sign">
        <div class="center-sign">
            <a href="/" class="logo float-left">
                <img src="{{ asset('images/logo-cimaf.jpg') }}" height="54" alt="Porto Admin" />
            </a>

            <div class="panel card-sign">
                <div class="card-title-sign mt-3 text-right">
                    <h2 class="title text-uppercase font-weight-bold m-0"><i class="fas fa-user mr-1"></i> Connexion</h2>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('utilisateur.login') }}">
                        @csrf
                        <div class="form-group mb-3">
                            <label for="email">Identifiant:</label>
                            <div class="input-group">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                <span class="input-group-append">
										<span class="input-group-text">
											<i class="fas fa-user"></i>
										</span>
									</span>
                            </div>
                        </div>

                        <div class="form-group mb-3">
{{--                            <div class="clearfix">--}}
{{--                                <label class="float-left">Password</label>--}}
{{--                                <a href="pages-recover-password.html" class="float-right">Lost Password?</a>--}}
{{--                            </div>--}}
                            <label for="password">Mot de passe</label>
                            <div class="input-group">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <span class="input-group-append">
										<span class="input-group-text">
											<i class="fas fa-lock"></i>
										</span>
									</span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="checkbox-custom checkbox-default">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label for="RememberMe">Rester connecté</label>
                                </div>
                            </div>
                            <div class="col-sm-6 text-right">
                                <button type="submit" class="btn btn-primary mt-2">Se connecter</button>
                            </div>
                        </div>

{{--                        <span class="mt-3 mb-3 line-thru text-center text-uppercase">--}}
{{--								<span>or</span>--}}
{{--							</span>--}}

{{--                        <div class="mb-1 text-center">--}}
{{--                            <a class="btn btn-facebook mb-3 ml-1 mr-1" href="#">Connect with <i class="fab fa-facebook-f"></i></a>--}}
{{--                            <a class="btn btn-twitter mb-3 ml-1 mr-1" href="#">Connect with <i class="fab fa-twitter"></i></a>--}}
{{--                        </div>--}}

{{--                        <p class="text-center">Don't have an account yet? <a href="pages-signup.html">Sign Up!</a></p>--}}

                    </form>
                </div>
            </div>

            <p class="text-center text-muted mt-3 mb-3">&copy; Copyright {{ env('START_DATE') == Carbon\Carbon::now()->format('Y') ? Carbon\Carbon::now()->format('Y') : env('START_DATE').'-'.Carbon\Carbon::now()->format('Y') }}. All Rights Reserved.</p>
        </div>
    </section>
<script src="{{ asset('js/porto-vendors.js') }}"></script>
<script src="{{ asset('js/porto-theme.js') }}"></script>
<script src="{{ asset('js/icheck.min.js') }}"></script>
<script src="{{ asset('js/notify.js') }}"></script>

<!-- Theme Base, Components and Settings -->

@yield('js-links')
@stack('scripts')
@stack('script')
@notifyJs
<script>
    // Maintain Scroll Position
    if (typeof localStorage !== 'undefined') {
        if (localStorage.getItem('sidebar-left-position') !== null) {
            var initialPosition = localStorage.getItem('sidebar-left-position'),
                sidebarLeft = document.querySelector('#sidebar-left .nano-content');

            sidebarLeft.scrollTop = initialPosition;
        }
    }

    (function ($) {

        'use strict';

        var flashInit = function () {

            $('#flash-overlay-modal').modal();

        };

        $(function () {
            flashInit();
        });

    }).apply(this, [jQuery]);

    (function ($) {
        'use strict';

        var bootstrapConfirmInit = function () {
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]',
                // other options
                animation: true,
                placement: 'top',
                trigger: 'click',
                title: 'Confirmez-vous cette action ?',
                btnOkLabel: '<i class="icon-ok-sign icon-white"></i> Oui',
                btnCancelLabel: '<i class="icon-remove-sign"></i> Non',
                singleton: true
            });
        };

        $(function () {
            bootstrapConfirmInit();
        })

    }).apply(this, [jQuery]);
</script>

<script src="{{ asset('js/bootstrap-confirmation.min.js') }}"></script>
<script src="{{ asset('js/popper.min.js') }}"></script>
</body>
</html>
