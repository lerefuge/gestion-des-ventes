<?php

namespace App\Providers;

use App\Client;
use App\Observers\ClientObserver;
use Illuminate\Support\Carbon;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        ini_set("pcre.backtrack_limit", "5000000");
        //ini_set("memory_limit", 512);
        ini_set("max_execution_time", 14400);

        Carbon::setLocale('fr');

        $this->_observer();


    }

    private function _observer()
    {
        Client::observe(ClientObserver::class);
    }
}
