@extends('template.theme')


@section('font-title')

@endsection
@push('scripts')

    <script type="text/javascript">
        $(function () {
            $('.selectPicker').select2({
                title: "Liste d'élément ...",
                header: 'Selectionnez un élément dans la liste',
                liveSearch: true,
                width: 'resolve'
            });
        });

    </script>

@endpush


@section('content-body')

    <div class="row">
        <div class="col-md-5">
            @if($region->id)
            {!! Form::open(['url'=>route('admin.enregistrement-region-edit',$region->id),'class'=>'form-horizontal','method'=>'PUT']) !!}
                @method('put')
            @else
            {!! Form::open(['url'=>route('admin.enregistrement-region'),'class'=>'form-horizontal']) !!}
            @endif
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title"> Enregistrement des régions </h2>
                    <p class="card-subtitle">
                        Basic validation will display a label with the error after the form control.
                    </p>
                </header>
                <div class="card-body">

                    <div class="form-group row">
                        <label class="col-sm-4 control-label text-sm-right pt-2"> Chef lieu de region <span
                                class="required">*</span></label>
                        <div class="col-sm-8">
                            {!! Form::select('chef_lieu_id',$chefLieux->pluck('libelle','id'),$region->chef_lieu_id,['class'=>'form-control selectPicker', 'required'=>true,'id'=>'','placeholder'=>'Selectionner un élément dans la liste'
                                     ,'show-tick',' data-live-search' => true, 'data-live-search-normalize'=>true,]
                                     ) !!}

                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 control-label text-sm-right pt-2"> Région <span
                                class="required">*</span></label>
                        <div class="col-sm-8">
                            {!! Form::select('region_id[]',$regions->pluck('libelle','id'),null,['class'=>'form-control selectPicker', 'required'=>true,'id'=>'','multiple','placeholder'=>'Selectionner un élément dans la liste'
                                    ,'show-tick',' data-live-search' => true, 'data-live-search-normalize'=>true,]
                                    ) !!}</div>
                    </div>

                </div>
                <footer class="card-footer">
                    <div class="row justify-content-end">
                        <div class="col-sm-9">
                            @if($region->id)
                                <button type="submit" class="btn btn-primary" data-toggle="confirmation">Mettre à jour</button>
                            @else
                                <button type="submit" class="btn btn-primary" data-toggle="confirmation">Enregistrer</button>
                                <button type="reset" class="btn btn-default">Effacer</button>
                            @endif
                        </div>
                    </div>
                </footer>
            </section>
            {!! Form::close() !!}

        </div>
        <div class="col-md-7">
            <div class="row">
                <div class="col">
                    <section class="card">
                        <header class="card-header">
                            <div class="card-actions">
                                <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                            </div>

                            <h2 class="card-title">Régions enregistrées</h2>
                        </header>
                        <div class="card-body">
                            <table class="table table-bordered table-striped mb-0 datatable-default">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Chef lieu</th>
                                    <th>Région</th>

                                    <th class="">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($liste_regions as $region)
                                    <tr>
                                        <td>{{ $loop->index + 1 }}</td>
                                        <td>{{ $region->chefLieu->libelle ?? '' }}</td>
                                        <td>{{ $region->libelle }}</td>
                                        <td class="flex text-center">
                                            @hasanyrole('Super Admin|Admins')
                                            <a href="{{ route('admin.enregistrement-region-edit',$region) }}" class="on-default edit-row" data-toggle="confirmation"><i
                                                    class="fas fa-pencil-alt"></i></a>
                                            <a href="#" class="on-default remove-row"><i
                                                    class="far fa-trash-alt"></i></a>
                                            @endhasanyrole
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>





@endsection
