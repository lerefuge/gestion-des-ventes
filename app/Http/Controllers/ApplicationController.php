<?php

namespace App\Http\Controllers;

use App\Annee;
use App\Charts\DailyChart;
use App\Charts\VenteAnnuelleCharts;
use App\Repositories\BudgetRepository;
use App\Repositories\ChefLieuDistrictRepository;
use App\Repositories\MonthRepository;
use App\Repositories\PeseeRepository;
use App\Traits\ExportableGraphData;
use ArielMejiaDev\LarapexCharts\LarapexChart;
use Carbon\CarbonImmutable;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;


class ApplicationController extends Controller
{
    use ExportableGraphData;

    /**
     * @var PeseeRepository
     */
    private $peseeRepository;
    /**
     * @var MonthRepository
     */
    private $monthRepository;
    /**
     * @var BudgetRepository
     */
    private $budgetRepository;
    /**
     * @var ChefLieuDistrictRepository
     */
    private $chefLieuDistrictRepository;

    /**
     * ApplicationController constructor.
     * @param PeseeRepository $peseeRepository
     * @param MonthRepository $monthRepository
     * @param BudgetRepository $budgetRepository
     * @param ChefLieuDistrictRepository $chefLieuDistrictRepository
     */
    public function __construct(PeseeRepository $peseeRepository, MonthRepository $monthRepository, BudgetRepository $budgetRepository, ChefLieuDistrictRepository $chefLieuDistrictRepository)
    {
        $this->middleware('auth');
        $this->peseeRepository = $peseeRepository;
        $this->monthRepository = $monthRepository;
        $this->budgetRepository = $budgetRepository;
        $this->chefLieuDistrictRepository = $chefLieuDistrictRepository;
    }

    public function index()
    {
        $currentYear = Annee::where('current', '=', true)->first()->libelle;
        // dd($currentYear);
        $now = now()->subDays(1);
        $day = now()->subDays(1)->format('Y-m-d');
        $month = now()->format('m');
        $data['days'] = now()->subDay()->daysInMonth;
        $start_month = Carbon::parse($now)->firstOfMonth()->format('Y-m-d');
        $end_month = Carbon::parse($now)->endOfMonth()->format('Y-m-d');

        $start = Carbon::parse($now)->firstOfMonth()->format('d/m/Y');
        $fin = Carbon::parse($now)->endOfMonth()->format('d/m/Y');
        $chart = new VenteAnnuelleCharts;

        $date_pesee = $this->_graphDays($start_month, $end_month);
        $expedies = $this->_getTotalExpedies($start_month, $end_month);
        $chart->labels($date_pesee);
        $chart->label('Expedés (Tonnes)');
        $chart->dataset("Ventes du {$start} au {$fin}", 'column', $expedies);
        $data['page_header_title'] = 'Tableaux de bord';
        $data['chart'] = $chart;
        $data['periode'] = " Point des ventes de la période du {$start} au {$fin} ";

        $realises = [
            $this->budgetMensuel('01', $currentYear),
            $this->budgetMensuel('02', $currentYear),
            $this->budgetMensuel('03', $currentYear),
            $this->budgetMensuel('04', $currentYear),
            $this->budgetMensuel('05', $currentYear),
            $this->budgetMensuel('06', $currentYear),
            $this->budgetMensuel('07', $currentYear),
            $this->budgetMensuel('08', $currentYear),
            $this->budgetMensuel('09', $currentYear),
            $this->budgetMensuel('10', $currentYear),
            $this->budgetMensuel('11', $currentYear),
            $this->budgetMensuel('12', $currentYear),

        ];
        //Budget
        $budgetCharts = new VenteAnnuelleCharts;
        $dailyData_32 = $this->_getDailyExpedies32($start_month, $end_month)->pluck('expedies', 'date');
        $dailyData_42 = $this->_getDailyExpedies42($start_month, $end_month)->pluck('expedies', 'date');

        $dailyDays = array_merge($dailyData_32->keys()->toArray(), $dailyData_42->keys()->toArray());
        $budgetCharts->labels($this->getBudgetMonthName());
        $budgetCharts->dataset('Budget 2020', 'column', $this->budget());
        $budgetCharts->dataset('Réalisé 2020', 'column', $realises);
        $budgetCharts->label('Volume (Tonnes)');
        $budgetCharts->displayAxes(true);
        $data['budgetCharts'] = $budgetCharts;
        /// Points detaillé par qualité
        $expedies_32_sac = $this->_getTotalExpedies32($start_month, $end_month);
        $expedies_42_sac = $this->_getTotalExpedies42($start_month, $end_month);
        $expedies_CPAsac = $this->_getTotalExpediesCPASAC($start_month, $end_month);
        $expedies_CHF_vrac = $this->_getTotalExpediesCHF($start_month, $end_month);
        $expedies_42Vrac = $this->_getTotalExpedies42VRAC($start_month, $end_month);
        $expedies_RobustoVrac = $this->_getTotalExpediesCPAVRAC($start_month, $end_month);
        $dailyDetailCharts = new DailyChart;
        $dailyDetailCharts->labels($date_pesee);

        // dd($expedies_42_sac,$expedies_CPAsac);
        $dailyDetailCharts->dataset('32.5 SAC', 'bar', $dailyData_32->values());
        //        $dailyDetailCharts->dataset('42.5 SAC', 'column', $dailyData_42->values());
        //        $dailyDetailCharts->dataset('ROBUSTO SAC', 'column', $expedies_CPAsac);
        //        $dailyDetailCharts->dataset('ROBUSTO VRAC', 'column', $expedies_RobustoVrac);
        //        $dailyDetailCharts->dataset('42.5 VRAC', 'column', $expedies_42Vrac);
        //        $dailyDetailCharts->dataset('CHF', 'column', $expedies_CHF_vrac);
        //        $dailyDetailCharts->displayAxes(true);
        //        $dailyDetailCharts->title("Ciment Expédiés par Qualités");
        // $dailyDetailCharts->label('Expedés (Tonnes)');

        $data['totaux'] = [
            'sacs' => [
                'jour' => [
                    '32_5' => $this->_getTotalExpedies32(now()->subDay()->format('Y-m-d'))->sum() ?? 0,
                    '42_5' => $this->_getTotalExpedies42(now()->subDay()->format('Y-m-d'))->sum() ?? 0,
                    'cpa' => $this->_getTotalExpediesCPASAC(now()->subDay()->format('Y-m-d'))->sum() ?? 0,
                    'chf' => 0,
                ],
                'mois' => [
                    '32_5' => $expedies_32_sac->sum() ?? 0,
                    '42_5' => $expedies_42_sac->sum() ?? 0,
                    'cpa' => $expedies_CPAsac->sum() ?? 0,
                    'chf' => 0,
                ],
                'annee' => [
                    '32_5' => $this->_getTotalExpedies32(now()->subDay()->firstOfYear()->format('Y-m-d'), now()->subDays(1)->format('Y-m-d'))->sum() ?? 0,
                    '42_5' => $this->_getTotalExpedies42(now()->subDay()->firstOfYear()->format('Y-m-d'), now()->subDays(1)->format('Y-m-d'))->sum() ?? 0,
                    'cpa' => $this->_getTotalExpediesCPASAC(now()->subDay()->firstOfYear()->format('Y-m-d'), now()->subDays(1)->format('Y-m-d'))->sum() ?? 0,
                    'chf' => 0,
                ]
            ],
            'vrac' => [
                'jour' => [
                    '32_5' => 0,
                    '42_5' => $this->_getTotalExpedies42VRAC(now()->subDay()->format('Y-m-d'))->sum() ?? 0,
                    'cpa' => $this->_getTotalExpediesCPAVRAC(now()->subDay()->format('Y-m-d'))->sum() ?? 0,
                    'chf' => $this->_getTotalExpediesCHF(now()->subDay()->format('Y-m-d'))->sum() ?? 0,
                ],
                'mois' => [
                    '32_5' => 0,
                    '42_5' => $expedies_42Vrac->sum() ?? 0,
                    'cpa' => $expedies_RobustoVrac->sum() ?? 0,
                    'chf' => $expedies_CHF_vrac->sum() ?? 0,
                ],
                'annee' => [
                    '32_5' => 0,
                    '42_5' => $this->_getTotalExpedies42VRAC(now()->subDay()->firstOfYear()->format('Y-m-d'), now()->subDays(1)->format('Y-m-d'))->sum() ?? 0,
                    'cpa' => $this->_getTotalExpediesCPAVRAC(now()->subDay()->firstOfYear()->format('Y-m-d'), now()->subDays(1)->format('Y-m-d'))->sum() ?? 0,
                    'chf' => $this->_getTotalExpediesCHF(now()->subDay()->firstOfYear()->format('Y-m-d'), now()->subDays(1)->format('Y-m-d'))->sum() ?? 0,
                ]
            ]

        ];
        $monthlyPieCharts = (new LarapexChart())->setType('pie')
            ->setTitle('Expédiés mensuels')
            ->setDataset($this->_getTotalMonthlyExpediesDataGroupByProduit($currentYear, $month)->toArray())
            // ->setColors(['#dc3545', '#ffc73c'])
            ->setLabels($this->_getTotalMonthlyExpediesNameGroupByProduit($currentYear, $month)->toArray());
        $data['monthlyPieCharts'] = $monthlyPieCharts;
        $yearlyPieCharts = (new LarapexChart())->setType('donut')
            ->setTitle('Expédiés annuels')
            ->setDataset($this->_getTotalMonthlyExpediesDataGroupByProduit($currentYear)->toArray())
            // ->setColors(['#dc3545', '#ffc73c'])
            ->setLabels($this->_getTotalMonthlyExpediesNameGroupByProduit($currentYear)->toArray());
        $data['yearlyPieCharts'] = $yearlyPieCharts;
        $data['dailyDetailCharts'] = $dailyDetailCharts;

//        $expediesParDistrict = $this->expediesParDistrict($currentYear, $month);
//        $expediesParDistrictAnnuel = $this->expediesParDistrict($currentYear);
//        $data['expedition_mensuelles'] = $expediesParDistrict;
//        $data['expedition_mensuelles_orphans'] = $this->expediesOrphans($currentYear, $month);
//        // dd($data['expedition_mensuelles_orphans']);
//        $data['expedition_annuelles'] = $expediesParDistrictAnnuel;
//        $data['somme_expedition_mensuelles'] = $expediesParDistrict;
//        $data['expedition_annuelles_orphans'] = $this->expediesOrphans($currentYear);
//        $data['somme_totale_expedies_par_district_par_mois'] = $this->sommeTotaleExpediesParDistrictParMois($currentYear, $month);
//        $data['somme_totale_expedies_par_district_par_annee'] = $this->sommeTotaleExpediesParDistrictParAnnee($currentYear);
        $expediesParDistrict = $this->expediesParDistrict($currentYear, $month);
        $expediesParDistrictAnnuel = $this->expediesParDistrict($currentYear);
        $expediesParDistributeur = $this->expediesParDistributeur($currentYear, $month);
        $expediesParDistributeurAnnuel = $this->expediesParDistributeur($currentYear);
        $data['expedition_mensuelles'] = $expediesParDistrict;
        $data['expedition_mensuelles_orphans'] = $this->expediesOrphans($currentYear, $month);
        $data['expedition_mensuelle_distributeurs'] = $expediesParDistributeur;
        $data['expedition_mensuelle_distributeur_orphans'] = $this->expediesDistributeurOrphans($currentYear, $month);
        $data['expedition_annuelles'] = $expediesParDistrictAnnuel;
        $data['expedition_annuelles_orphans'] = $this->expediesOrphans($currentYear);

        $data['somme_expedition_mensuelles'] = $expediesParDistrict;
        $data['somme_totale_expedies_par_district_par_mois'] = $this->sommeTotaleExpediesParDistrictParMois($currentYear, $month);
        $data['somme_totale_expedies_par_district_par_annee'] = $this->sommeTotaleExpediesParDistrictParAnnee($currentYear);
        $data['somme_totale_expedies_par_distributeur_par_mois'] = $this->sommeTotaleExpediesParDistributeurParMois($currentYear, $month);
        $data['somme_totale_expedies_par_distributeur_par_annee'] = $this->sommeTotaleExpediesParDistributeurParAnnee($currentYear);
        $data['show_daily'] = true;
        $data['detail_budgets'] = $this->detailBudget();
        $data['budget_boucle'] = $this->detailRealiseBudget();

        return view('application.index')->with($data);
    }

    public function historique(Request $request)
    {
        $data['show_daily'] = null;
        $data['detail_budgets'] = $this->detailBudget();
        $raw_date = explode('/', $request->date_month);
        $an = $raw_date[1];
        $mois = $raw_date[0];
        $unformated_date = CarbonImmutable::createFromFormat('Y-m-d', $an . '-' . $mois . "-01");
        //   $created_date =->toDateString();

        $currentYear = $this->_currentYear();
        //  dd($unformated_date,$unformated_date->subDays(1),$unformated_date->format('m'));
        $month = $unformated_date->format('m');
        $data['days'] = $unformated_date->daysInMonth;
        $start_month = Carbon::parse($unformated_date)->firstOfMonth()->format('Y-m-d');
        $end_month = Carbon::parse($unformated_date)->endOfMonth()->format('Y-m-d');

        $start = Carbon::parse($unformated_date)->firstOfMonth()->format('d/m/Y');
        $fin = Carbon::parse($unformated_date)->endOfMonth()->format('d/m/Y');
        $chart = new VenteAnnuelleCharts;

        $date_pesee = $this->_graphDays($start_month, $end_month);
        $expedies = $this->_getTotalExpedies($start_month, $end_month);
        $chart->labels($date_pesee);
        $chart->label('Expedés (Tonnes)');
        $chart->dataset("Ventes du {$start} au {$fin}", 'column', $expedies);
        $data['page_header_title'] = 'Tableaux de bord';
        $data['chart'] = $chart;
        $data['periode'] = " Point des ventes de la période du {$start} au {$fin} ";

        $realises = [
            $this->budgetMensuel('01', $currentYear),
            $this->budgetMensuel('02', $currentYear),
            $this->budgetMensuel('03', $currentYear),
            $this->budgetMensuel('04', $currentYear),
            $this->budgetMensuel('05', $currentYear),
            $this->budgetMensuel('06', $currentYear),
            $this->budgetMensuel('07', $currentYear),
            $this->budgetMensuel('08', $currentYear),
            $this->budgetMensuel('09', $currentYear),
            $this->budgetMensuel('10', $currentYear),
            $this->budgetMensuel('11', $currentYear),
            $this->budgetMensuel('12', $currentYear),

        ];
        //Budget
        $budgetCharts = new VenteAnnuelleCharts;
        $dailyData_32 = $this->_getDailyExpedies32($start_month, $end_month)->pluck('expedies', 'date');
        $dailyData_42 = $this->_getDailyExpedies42($start_month, $end_month)->pluck('expedies', 'date');

        $dailyDays = array_merge($dailyData_32->keys()->toArray(), $dailyData_42->keys()->toArray());
        $budgetCharts->labels($this->getBudgetMonthName());
        $budgetCharts->dataset('Budget 2020', 'column', $this->budget());
        $budgetCharts->dataset('Réalisé 2020', 'column', $realises);
        $budgetCharts->label('Volume (Tonnes)');
        $budgetCharts->displayAxes(true);
        $data['budgetCharts'] = $budgetCharts;
        /// Points detaillé par qualité
        $expedies_32_sac = $this->_getTotalExpedies32($start_month, $end_month);
        $expedies_42_sac = $this->_getTotalExpedies42($start_month, $end_month);
        $expedies_CPAsac = $this->_getTotalExpediesCPASAC($start_month, $end_month);
        $expedies_CHF_vrac = $this->_getTotalExpediesCHF($start_month, $end_month);
        $expedies_42Vrac = $this->_getTotalExpedies42VRAC($start_month, $end_month);
        $expedies_RobustoVrac = $this->_getTotalExpediesCPAVRAC($start_month, $end_month);
        $dailyDetailCharts = new DailyChart;
        $dailyDetailCharts->labels($date_pesee);

        // dd($expedies_42_sac,$expedies_CPAsac);
        $dailyDetailCharts->dataset('32.5 SAC', 'bar', $dailyData_32->values());
        //        $dailyDetailCharts->dataset('42.5 SAC', 'column', $dailyData_42->values());
        //        $dailyDetailCharts->dataset('ROBUSTO SAC', 'column', $expedies_CPAsac);
        //        $dailyDetailCharts->dataset('ROBUSTO VRAC', 'column', $expedies_RobustoVrac);
        //        $dailyDetailCharts->dataset('42.5 VRAC', 'column', $expedies_42Vrac);
        //        $dailyDetailCharts->dataset('CHF', 'column', $expedies_CHF_vrac);
        //        $dailyDetailCharts->displayAxes(true);
        //        $dailyDetailCharts->title("Ciment Expédiés par Qualités");
        // $dailyDetailCharts->label('Expedés (Tonnes)');

        $data['totaux'] = [
            'sacs' => [
                'jour' => [
                    '32_5' => $this->_getTotalExpedies32(now()->subDay()->format('Y-m-d'))->sum() ?? 0,
                    '42_5' => $this->_getTotalExpedies42(now()->subDay()->format('Y-m-d'))->sum() ?? 0,
                    'cpa' => $this->_getTotalExpediesCPASAC(now()->subDay()->format('Y-m-d'))->sum() ?? 0,
                    'chf' => 0,
                ],
                'mois' => [
                    '32_5' => $expedies_32_sac->sum() ?? 0,
                    '42_5' => $expedies_42_sac->sum() ?? 0,
                    'cpa' => $expedies_CPAsac->sum() ?? 0,
                    'chf' => 0,
                ],
                'annee' => [
                    '32_5' => $this->_getTotalExpedies32($unformated_date->firstOfYear()->format('Y-m-d'), $unformated_date->format('Y-m-d'))->sum() ?? 0,
                    '42_5' => $this->_getTotalExpedies42($unformated_date->firstOfYear()->format('Y-m-d'), $unformated_date->format('Y-m-d'))->sum() ?? 0,
                    'cpa' => $this->_getTotalExpediesCPASAC($unformated_date->firstOfYear()->format('Y-m-d'), $unformated_date->format('Y-m-d'))->sum() ?? 0,
                    'chf' => 0,
                ]
            ],
            'vrac' => [
                'jour' => [
                    '32_5' => 0,
                    '42_5' => $this->_getTotalExpedies42VRAC(now()->subDay()->format('Y-m-d'))->sum() ?? 0,
                    'cpa' => $this->_getTotalExpediesCPAVRAC(now()->subDay()->format('Y-m-d'))->sum() ?? 0,
                    'chf' => $this->_getTotalExpediesCHF(now()->subDay()->format('Y-m-d'))->sum() ?? 0,
                ],
                'mois' => [
                    '32_5' => 0,
                    '42_5' => $expedies_42Vrac->sum() ?? 0,
                    'cpa' => $expedies_RobustoVrac->sum() ?? 0,
                    'chf' => $expedies_CHF_vrac->sum() ?? 0,
                ],
                'annee' => [
                    '32_5' => 0,
                    '42_5' => $this->_getTotalExpedies42VRAC($unformated_date->firstOfYear()->format('Y-m-d'), $unformated_date->format('Y-m-d'))->sum() ?? 0,
                    'cpa' => $this->_getTotalExpediesCPAVRAC($unformated_date->firstOfYear()->format('Y-m-d'), $unformated_date->format('Y-m-d'))->sum() ?? 0,
                    'chf' => $this->_getTotalExpediesCHF($unformated_date->firstOfYear()->format('Y-m-d'), $unformated_date->format('Y-m-d'))->sum() ?? 0,
                ]
            ]

        ];
        $monthlyPieCharts = (new LarapexChart())->setType('pie')
            ->setTitle('Expédiés mensuels')
            ->setDataset($this->_getTotalMonthlyExpediesDataGroupByProduit($currentYear, $month)->toArray())
            // ->setColors(['#dc3545', '#ffc73c'])
            ->setLabels($this->_getTotalMonthlyExpediesNameGroupByProduit($currentYear, $month)->toArray());
        $data['monthlyPieCharts'] = $monthlyPieCharts;
        $yearlyPieCharts = (new LarapexChart())->setType('donut')
            ->setTitle('Expédiés annuels')
            ->setDataset($this->_getTotalMonthlyExpediesDataGroupByProduit($currentYear)->toArray())
            // ->setColors(['#dc3545', '#ffc73c'])
            ->setLabels($this->_getTotalMonthlyExpediesNameGroupByProduit($currentYear)->toArray());
        $data['yearlyPieCharts'] = $yearlyPieCharts;
        $data['dailyDetailCharts'] = $dailyDetailCharts;

        $expediesParDistrict = $this->expediesParDistrict($currentYear, $month);
        $expediesParDistrictAnnuel = $this->expediesParDistrict($currentYear);
        $expediesParDistributeur = $this->expediesParDistributeur($currentYear, $month);
        $expediesParDistributeurAnnuel = $this->expediesParDistributeur($currentYear);
        $data['expedition_mensuelles'] = $expediesParDistrict;
        $data['expedition_mensuelles_orphans'] = $this->expediesOrphans($currentYear, $month);
        $data['expedition_mensuelle_distributeurs'] = $expediesParDistributeur;
        $data['expedition_mensuelle_distributeur_orphans'] = $this->expediesDistributeurOrphans($currentYear, $month);
        $data['expedition_annuelles'] = $expediesParDistrictAnnuel;
        $data['expedition_annuelles_orphans'] = $this->expediesOrphans($currentYear);

        $data['somme_expedition_mensuelles'] = $expediesParDistrict;
        $data['somme_totale_expedies_par_district_par_mois'] = $this->sommeTotaleExpediesParDistrictParMois($currentYear, $month);
        $data['somme_totale_expedies_par_district_par_annee'] = $this->sommeTotaleExpediesParDistrictParAnnee($currentYear);
        $data['somme_totale_expedies_par_distributeur_par_mois'] = $this->sommeTotaleExpediesParDistributeurParMois($currentYear, $month);
        $data['somme_totale_expedies_par_distributeur_par_annee'] = $this->sommeTotaleExpediesParDistributeurParAnnee($currentYear);
        return view('application.index')->with($data);
    }


    private function _currentYear()
    {
        return Annee::where('current', '=', true)->first()->libelle;
    }
}
