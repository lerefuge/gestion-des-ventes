<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        Permission::create(['name' => 'create']);
        Permission::create(['name' => 'read']);
        Permission::create(['name' => 'update']);
        Permission::create(['name' => 'delete']);

        // create roles and assign created permissions

        // this can be done as separate statements
//        $role = Role::create(['name' => 'admin']);
//        $role = Role::create(['name' => 'superviseur']);
//        $role = Role::create(['name' => 'guest']);
//        $role->givePermissionTo('edit articles');

        // or may be done by chaining
        $role = Role::create(['name' => 'admin'])
            ->givePermissionTo(['create', 'read','update','delete']);
        $role = Role::create(['name' => 'superviseur'])
            ->givePermissionTo(['create', 'read','update']);
            Role::create(['name' => 'guest'])
            ->givePermissionTo(['read']);

//        $role = Role::create(['name' => 'super-admin']);
//        $role->givePermissionTo(Permission::all());
    }
}
