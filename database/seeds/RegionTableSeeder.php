<?php

use Illuminate\Database\Seeder;

class RegionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $regions = [
            ['libelle' => 'Iffou'],
            ['libelle' => 'Bélier'],
            ['libelle' => 'Moronou'],
            ['libelle' => 'Indénié-Djuablin'],
            ['libelle' => 'Sud-Comoé'],
            ['libelle' => 'Folon'],
            ['libelle' => 'Kabadougou'],
            ['libelle' => 'Gôh'],
            ['libelle' => 'Lôh-Djiboua'],
            ['libelle' => 'Mé'],
            ['libelle' => 'Grands Ponts'],
            ['libelle' => 'Tonkpi'],
            ['libelle' => 'Cavally'],
            ['libelle' => 'Guémon'],
            ['libelle' => 'Marahoué'],
            ['libelle' => 'Poro'],
            ['libelle' => 'Tchologo'],
            ['libelle' => 'Bagoué'],
            ['libelle' => 'Nawa'],
            ['libelle' => 'San-Pédro'],
            ['libelle' => 'Gbôklé'],
            ['libelle' => 'Hambol'],
            ['libelle' => 'Gbêkê'],
            ['libelle' => 'Béré'],
            ['libelle' => 'Worodougou'],
            ['libelle' => 'Bounkani'],
            ['libelle' => 'Gontougo'],
            ['libelle' => 'N’ZI'],
            ['libelle' => 'HAUT-SASSANDRA'],
            ['libelle' => 'AGNÉBY-TIASSA'],
            ['libelle' => 'BAFING'],
        ];
        DB::table('regions')->insert($regions);
    }
}
