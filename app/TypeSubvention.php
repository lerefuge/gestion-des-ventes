<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Str;

class TypeSubvention extends Model
{
    protected $guarded = ['id'];
    use SoftDeletes;

    public function setLibelleAttribute($value)
    {
        return $this->attributes['libelle'] = STR::upper($value);
    }

    public function subvention()
    {
        return $this->hasOne(Subvention::class);
    }
}
