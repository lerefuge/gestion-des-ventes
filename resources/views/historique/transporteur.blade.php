@extends('template.theme')


@section('font-title')

@endsection
@push('scripts')

    <script type="text/javascript">
        (function () {

            'use strict';

            // basic
            $("#weekly-form").validate({
                highlight: function (label) {
                    $(label).closest('.form-group').removeClass('has-success').addClass('has-error');
                },
                success: function (label) {
                    $(label).closest('.form-group').removeClass('has-error');
                    label.remove();
                },
                errorPlacement: function (error, element) {
                    var placement = element.closest('.input-group');
                    if (!placement.get(0)) {
                        placement = element;
                    }
                    if (error.text() !== '') {
                        placement.after(error);
                    }
                }
            });

            // validation summary
            var $summaryForm = $("#summary-form");
            $summaryForm.validate({
                errorContainer: $summaryForm.find('div.validation-message'),
                errorLabelContainer: $summaryForm.find('div.validation-message ul'),
                wrapper: "li"
            });

            // checkbox, radio and selects
            $("#chk-radios-form, #selects-form").each(function () {
                $(this).validate({
                    highlight: function (element) {
                        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                    },
                    success: function (element) {
                        $(element).closest('.form-group').removeClass('has-error');
                    },
                    errorPlacement: function (error, element) {
                        var placement = $(element).parent();

                        placement.append(error);
                    }
                });
            });

            // Select 2 Fields
            $('select[data-plugin-selectTwo]').on('change', function () {
                $(this).valid();
            });



        }).apply(this, [jQuery]);
        $(document).ready(function () {

            $('.selectPicker').select2({
                title: "Liste d'élement ...",
                header: 'Selectionnez un élement dans la liste',
                liveSearch: true,
                width: 'resolve'
            });
            $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
                localStorage.setItem('activeTab', $(e.target).attr('href'));
            });
            var activeTab = localStorage.getItem('activeTab');
            if (activeTab) {
                $('#navTabs a[href="' + activeTab + '"]').tab('show');
            }
        });
    </script>

@endpush


@section('content-body')

    <div class="row">
        <div class="col-lg-10 offset-1">
            <div class="tabs">
                <ul class="nav nav-tabs" id="navTabs">
                    <li class="nav-item active">
                        <a class="nav-link" href="#daily" data-toggle="tab"> Journalier</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#weekly" data-toggle="tab">Periode</a>
                    </li>
{{--                    <li class="nav-item">--}}
{{--                        <a class="nav-link" href="#monthly" data-toggle="tab">Mensuel</a>--}}
{{--                    </li>--}}
{{--                    <li class="nav-item">--}}
{{--                        <a class="nav-link" href="#three-monthly" data-toggle="tab">Trimestriel</a>--}}
{{--                    </li>--}}
{{--                    <li class="nav-item">--}}
{{--                        <a class="nav-link" href="#half-yearly" data-toggle="tab">Semestriel</a>--}}
{{--                    </li>--}}
{{--                    <li class="nav-item">--}}
{{--                        <a class="nav-link" href="#yearly" data-toggle="tab">Annuel</a>--}}
{{--                    </li>--}}
                </ul>
                <div class="tab-content">
                    <div class="card-subtitle">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>

                    <div id="daily" class="tab-pane active">
                        {!! Form::open(['url'=>route('historique.transporteur'),'class'=>'daily-form',"id"=>'daily-form']) !!}
                        <section class="card">
                            <header class="card-header">
                                <div class="card-actions">
                                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                    <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                                </div>

                                <h2 class="card-title"> Consulter Point transporteur </h2>
                                <p class="card-subtitle">
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif
                                    </p>
                            </header>
                            <div class="card-body">
                                <div class="form-row">
                                    <div class="col">
                                        <div class="form-group">

                                            <label for="transporteur_id" class="control-label">Transporteur
                                                : </label>
                                            {!! Form::select('transporteur_id[]', $transporteurs->pluck('nom','id'),
                                           request('transporteur_id'), ['transporteur_id'=>'transporteur_id','class'=>'form-control selectPicker
                                             show-tick',' data-live-search' => true, 'multiple', 'data-live-search-normalize'=>true,'title'=>'Selectionner un transporteur',
                                           ]) !!}
                                            @error('transporteur_id','transporteur')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label class="col-form-label" for="date_pesee_entree">Date entrée </label>
                                            <div class="input-group">
														<span class="input-group-prepend">
															<span class="input-group-text">
																<i class="fas fa-calendar-alt"></i>
															</span>
														</span>
                                                <input type="text" name="date_pesee_entree"
                                                       value="{{ old('date_pesee_entree',request('date_pesee_entree')) }}"
                                                       id="date_pesee_entree" class="form-control" autocomplete="off"
                                                       required
                                                       data-plugin-datepicker
                                                       data-plugin-options='{"orientation":"bottom","autoclose":true,"language":"fr","todayHighlight":true}'
                                                >
                                            </div>
                                            @error('date_pesee_entree','transporteur')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col">
                                        <label for="type_subvention_id" class="control-label">Type Subvention : </label>
                                        <div class="form-group">
                                            <select name="type_subvention_id" id="type_subvention_id"
                                                    class="form-control" title="Type Subvention"
                                                    placeholder="Type Subvention">
                                                <option value="">Selectionner a subvention SVP</option>
                                                @foreach( $type_subventions as $type_subvention)
                                                    <option
                                                        value="{{ $type_subvention->id }}" {{ request('type_subvention_id')== $type_subvention->id ? 'selected' : '' }}>{{ $type_subvention->libelle }}</option>
                                                @endforeach
                                            </select>
                                            @error('type_subvention_id','transporteur')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <footer class="card-footer">
                                <div class="row ">
                                    <div class="col-sm-9 align-content-md-end">
                                        <input type="hidden" name="case" value="daily">
                                        <button type="submit" class="btn btn-primary"><i
                                                class="fas search"></i> Rechercher
                                        </button>


                                    </div>
                                </div>
                            </footer>
                        </section>
                        {!! Form::close() !!}
                    </div>
                    <div id="weekly" class="tab-pane">
                        {!! Form::open(['url'=>route('historique.transporteur'),'class'=>'weekly-form',"id"=>'weekly-form']) !!}
                        <section class="card">
                            <header class="card-header">
                                <div class="card-actions">
                                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                    <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                                </div>

                                <h2 class="card-title"> Consulter Point transporteur </h2>
                                <p class="card-subtitle">
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif
                                    </p>
                            </header>
                            <div class="card-body">
                                <div class="form-row">
                                    <div class="col">

                                        <div class="form-group row">
                                            <label
                                                class="col-lg-4 control-label text-lg-right pt-2">Transporteur</label><br>
                                            <div class="col-lg-8">


                                                {!! Form::select('transporteur_id[]', $transporteurs->pluck('nom','id'),
                                               request('transporteur_id'), ['transporteur_id'=>'transporteur_id','class'=>'form-control populate','data-role'=>'multiselect
                                                 ','  data-plugin-selectTwo' => true, 'multiple', 'title'=>'Selectionner un transporteur',
                                                ]) !!}
                                                @error('transporteur_id','transporteur')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-5">
                                        <div class="form-group row">
                                            <label class="col-lg-3 control-label text-lg-right pt-2">Periode</label>
                                            <div class="col-lg-9">
                                                <div class="input-daterange input-group" data-plugin-datepicker required

                                                     data-plugin-options='{"orientation":"bottom","autoclose":true,"language":"fr","todayHighlight":true}'>
														<span class="input-group-prepend">
															<span class="input-group-text">
																<i class="fas fa-calendar-alt"></i>
															</span>
														</span>
                                                    <input type="text" class="form-control dateRange" name="start" autocomplete="off">
                                                    <span
                                                        class="input-group-text border-left-0 border-right-0 rounded-0">
															Au
														</span>
                                                    <input type="text" class="form-control" name="end" autocomplete="off">
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class=" col">
                                        <label for="type_subvention_id" class="control-label">Type Subvention : </label>
                                        <div class="form-group">
                                            <select name="type_subvention_id" id="type_subvention_id"
                                                    class="form-control" title="Type Subvention"
                                                    placeholder="Type Subvention">
                                                <option value="">Selectionner a subvention SVP</option>
                                                @foreach( $type_subventions as $type_subvention)
                                                    <option
                                                        value="{{ $type_subvention->id }}" {{ request('type_subvention_id')== $type_subvention->id ? 'selected' : '' }}>{{ $type_subvention->libelle }}</option>
                                                @endforeach
                                            </select>
                                            @error('type_subvention_id','transporteur')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <footer class="card-footer">
                                <div class="row ">
                                    <div class="col-sm-9 align-content-md-end">
                                        <input type="hidden" name="case" value="weekly">
                                        <button type="submit" class="btn btn-primary"><i
                                                class="fas search"></i> Rechercher
                                        </button>


                                    </div>
                                </div>
                            </footer>
                        </section>
                        {!! Form::close() !!}
                    </div>
                    <div id="monthly" class="tab-pane">
                        <p>Recent</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitat.</p>
                    </div>
                    <div id="three-monthly" class="tab-pane">
                        <p>Recent</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitat.</p>
                    </div>
                    <div id="half-yearly" class="tab-pane">
                        <p>Recent</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitat.</p>
                    </div>
                    <div id="yearly" class="tab-pane">
                        <p>Recent</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitat.</p>
                    </div>

                </div>
            </div>
        </div>

    </div>
    {{--//@dump($results)--}}
    @includeWhen( (isset($results) && !empty($results)) ? true : false ,'historique.details.transporteur')


@endsection
