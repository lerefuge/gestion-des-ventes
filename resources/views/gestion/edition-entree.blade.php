@extends('template.theme')


@section('font-title')

@endsection
@push('scripts')

    <script type="text/javascript">

        $(function () {
            $('.selectPicker').select2({
                title: "Liste d'élement ...",
                header: 'Selectionnez un élement dans la liste',
                liveSearch: true,
                width: 'resolve'
            });
            var field__caisse = $('#field_caisse');
            var field_facturee = $('#field_facture');
            let type_vehicule_id_field = $('#type_vehicule_id');
            var type_subvention_id = $('#type_subvention_id');
            var prix_base = $('#prix_base');
            var prix_negocie = $('#prix_negocie');
            var montant_supporte = $('#montant_supporte');
            var num_bon_caisse_field = $('#numero_bon_caisse');
            var $field_caisse = document.querySelector('#field_caisse');
            var $montant_supporte_cimaf = document.querySelector('#montant_supporte_cimaf');
            var $prix_negocie = document.querySelector('#prix_negocie'); // prix du voyage
            var $montant_supporte = document.querySelector('#montant_supporte'); // montant supporté par le client
            var $avance = document.querySelector('#avance'); // Avance versee au client
            var $reste = document.querySelector('#reste'); // Avance versee au client
            $field_caisse.style.display = "{{ $entree->subvention &&  $entree->subvention->statut ==true ? 'inline': 'none' }}";
            var radio = $('input.paiementStatus');
            // num_bon_caisse_field.attr('required', 'required');
            @if($entree->subvention && $entree->subvention->statut==true )
                num_bon_caisse_field.removeAttr('disabled');
            @else
                num_bon_caisse_field.attr('disabled', 'disabled');
            @endif


            $('#montant_supporte').on('blur',function(e) {
              let prix_voyage = parseInt($prix_negocie.value);
              let  prix_client = parseInt(e.target.value);
                let mtn = prix_voyage - prix_client;
                $montant_supporte_cimaf.value =  '';    //parseInt()
                $montant_supporte_cimaf.value =  mtn;    //parseInt()
                $avance.value = mtn;
                $avance.setAttribute('max',mtn);

                //console.log($avance);
            }).trigger('change');

            $('#avance').on('blur',function(e) {
                $reste.value=$montant_supporte_cimaf.value - $avance.value;
            }).trigger('change');






            type_vehicule_id_field.on('change', function (e) {
                e.preventDefault();
                let type_vehicule_id = e.target.value;

                num_bon_caisse_field.attr('disabled', 'disabled');
                if (type_vehicule_id === '1') {
                    num_bon_caisse_field.removeAttr('required');
                    num_bon_caisse_field.attr('disabled', 'disabled');
                    type_subvention_id.removeAttr('required');
                    type_subvention_id.attr('disabled', 'disabled');
                    prix_base.removeAttr('required');
                    prix_base.attr('disabled', 'disabled');
                    prix_negocie.removeAttr('required');
                    prix_negocie.attr('disabled', 'disabled');
                    montant_supporte.removeAttr('required');
                    montant_supporte.attr('disabled', 'disabled');
                    radio.attr('disabled', 'disabled')

                } else {
                    num_bon_caisse_field.attr('required', 'required');
                   // num_bon_caisse_field.removeAttr('disabled');
                    type_subvention_id.attr('required', 'required');
                    type_subvention_id.removeAttr('disabled');
                    prix_base.attr('required', 'required');
                    prix_base.removeAttr('disabled');
                    prix_negocie.attr('required', 'required');
                    prix_negocie.removeAttr('disabled');
                    montant_supporte.attr('required', 'required');
                    montant_supporte.removeAttr('disabled');
                    radio.removeAttr('disabled')
                }
            }).trigger('change');
            @if($entree->subvention && $entree->subvention->statut==true )
            num_bon_caisse_field.removeAttr('disabled');
            @else
            num_bon_caisse_field.attr('disabled', 'disabled');
            @endif

            radio.on('change', function (event) {

                let status = event.target.value;
                // console.log(event)
                //
                if (status === 'unpaid') {
                    $field_caisse.style.display = 'none';
                    num_bon_caisse_field.removeAttr('required');
                    num_bon_caisse_field.attr('disabled', 'disabled');
                } else if (status === 'paid') {
                    $field_caisse.style.display = 'inline';
                    num_bon_caisse_field.attr('required', 'required');
                    num_bon_caisse_field.removeAttr('disabled');

                }
            }).trigger('checked',);

        })
        ;

    </script>

@endpush


@section('content-body')

    <div class="row">
        <div class="col-md-12 ">
            @if(is_null($entree->numero_bl))
                {!! Form::open(['url'=>route('gestion.recherche'),'class'=>'form-horizontal']) !!}
            @else
                {!! Form::open(['url'=>'#']) !!}
            @endif
            <section class="card">
                <header class="card-header">


                    <h2 class="card-title"><a href="{{ url()->previous() }}"> <em class="fa fa-arrow-left"></em></a>
                        Editer une entrée </h2>
                    <div class="card-subtitle">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                </header>
                <div class="card-body">
                    <div class="row form-group">
                        <div class="col-2">
                            <div class="form-group">
                                <label class="col-form-label" for="numero_bl">N°BL</label>
                                <input type="text" name="numero_bl" value="{{ old('numero_bl',$entree->numero_bl) }}"
                                       id="numero_bl" class="form-control"
                                       autocomplete="off" {{ $entree->numero_bl ? 'required readonly': 'required' }}
                                >

                                @error('numero_bl')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group">

                                <label for="ditributeur_id" class="control-label">Distributeur : </label><br>
                                {!! Form::select('ditributeur_id', $entree->distributeur()->get()->pluck('nom','id'),
                                $entree->distributeur_id, ['ditributeur_id'=>'ditributeur_id','class'=>'form-control
                                 show-tick',' data-live-search' => true, 'data-live-search-normalize'=>true,
                                'required'=>'required', 'readonly'=>true]) !!}



                                @error('ditributeur_id')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group">

                                <label for="transporteur_id" class="control-label">Transporteur : </label><br>
                                {!! Form::select('transporteur_id', $entree->transporteur()->get()->pluck('nom','id'),
                                $entree->distributeur_id, ['transporteur_id'=>'transporteur_id','class'=>'form-control
                                 show-tick',' data-live-search' => true, 'data-live-search-normalize'=>true,
                                'required'=>'required', 'readonly'=>true]) !!}



                                @error('transporteur_id')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group">

                                <label for="destination_id" class="control-label">Destination : </label><br>
                                {!! Form::select('destination_id', $entree->destination()->get()->pluck('libelle','id'),
                                $entree->distributeur_id, ['destination_id'=>'destination_id','class'=>'form-control
                                 show-tick',' data-live-search' => true, 'data-live-search-normalize'=>true,
                                'required'=>'required', 'readonly'=>true]) !!}



                                @error('destination_id')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group">

                                <label for="produit_id" class="control-label">Produit : </label><br>
                                {!! Form::select('produit_id', $entree->produit()->get()->pluck('libelle','id'),
                                $entree->produit_id, ['produit_id'=>'produit_id','class'=>'form-control
                                 show-tick',' data-live-search' => true, 'data-live-search-normalize'=>true,
                                'required'=>'required', 'readonly'=>true]) !!}



                                @error('produit_id')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-1">
                            <div class="form-group">
                                <label for="quantite" class="control-label">Quantité : </label><br>


                                {!! Form::text('quantite', $entree->quantite,
                                ['id'=>'quantite','class'=>'form-control',
                                'required'=>'required', 'readonly'=>true]) !!}
                                @error('quantite')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">


                        <div class="col-2">
                            <div class="form-group">
                                <label class="col-form-label" for="date_pesee_entree">Date entrée </label>
                                <div class="input-group">
														<span class="input-group-prepend">
															<span class="input-group-text">
																<i class="fas fa-calendar-alt"></i>
															</span>
														</span>
                                    <input type="text" name="date_pesee_entree"
                                           value="{{ old('date_pesee_entree',$entree->date_entree->format('Y-m-d')) }}"
                                           id="date_pesee_entree" class="form-control" autocomplete="off" required
                                           readonly
                                    >
                                </div>
                                @error('date_pesee_entree')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group">
                                <label class="col-form-label" for="date_pesee_sortie">Date sortie </label>
                                <div class="input-group">
														<span class="input-group-prepend">
															<span class="input-group-text">
																<i class="fas fa-calendar-alt"></i>
															</span>
														</span>
                                    <input type="text" name="date_pesee_sortie"
                                           value="{{ old('date_pesee_sortie',$entree->date_sortie->format('Y-m-d')) }}"
                                           id="date_pesee_sortie" class="form-control" autocomplete="off" required
                                           readonly
                                    >
                                </div>
                                @error('date_pesee_sortie')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group">
                                <label class="col-form-label" for="heure_pesee_entree">Heure entrée </label>
                                <div class="input-group">
														<span class="input-group-prepend">
															<span class="input-group-text">
																<i class="fas fa-clock"></i>
															</span>
														</span>
                                    <input type="text" name="heure_pesee_entree"
                                           value="{{ old('heure_pesee_entree',$entree->heure_pesee_entree) }}"
                                           id="heure_pesee_entree" class="form-control" autocomplete="off" required
                                           readonly
                                    >
                                </div>
                                @error('heure_pesee_entree')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group">
                                <label class="col-form-label" for="heure_pesee_sortie">Heure sortie </label>
                                <div class="input-group">
														<span class="input-group-prepend">
															<span class="input-group-text">
																<i class="fas fa-clock"></i>
															</span>
														</span>
                                    <input type="text" name="heure_pesee_sortie"
                                           value="{{ old('heure_pesee_sortie',$entree->heure_pesee_sortie) }}"
                                           id="heure_pesee_sortie" class="form-control" autocomplete="off" required
                                           readonly
                                    >
                                </div>
                                @error('heure_pesee_sortie')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-2">
                            <div class="form-group">
                                <label for="poids_pesee_entree" class="control-label">Poids entrée : </label>

                                <div class="input-group">
														<span class="input-group-prepend">
															<span class="input-group-text">
																<i class="fas fa-weight-hanging"></i>
															</span>
														</span>
                                    {!! Form::text('poids_pesee_entree', $entree->poids_entree,
                                    ['poids_pesee_entree'=>'poids_pesee_entree','class'=>'form-control',
                                    'required'=>'required', 'readonly'=>true]) !!}
                                </div>
                                @error('poids_pesee_entree')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group">
                                <label for="poids_pesee_sortie" class="control-label">Poids sortie </label>
                                <div class="input-group">
														<span class="input-group-prepend">
															<span class="input-group-text">
																<i class="fas fa-weight"></i>
															</span>
														</span>


                                    {!! Form::text('poids_pesee_sortie', $entree->poids_sortie,
                                    ['poids_pesee_sortie'=>'poids_pesee_sortie','class'=>'form-control',
                                    'required'=>'required', 'readonly'=>true]) !!}
                                    @error('poids_pesee_sortie')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                        </div>
                        <div class="col-2">
                            <div class="form-group">
                                <label class="col-form-label" for="poids_net">Poids net </label>
                                <div class="input-group">
														<span class="input-group-prepend">
															<span class="input-group-text">
																<i class="fas fa-weight"></i>
															</span>
														</span>


                                    <input type="text" name="poids_net"
                                           value="{{ $entree->poids_net }}"
                                           id="poids_net" class="form-control" autocomplete="off" required
                                           readonly
                                    >
                                </div>

                                @error('poids_net')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>

                </div>
                @if(is_null($entree->numero_bl))
                    <footer class="card-footer">
                        <div class="row justify-content-end">

                            <button type="submit" data-toggle="confirmation" class="btn btn-dark"><i
                                    class="fas fa-save"></i> Mettre à jour
                            </button>


                        </div>
                    </footer>
                @endif
            </section>
            {!! Form::close() !!}

        </div>
    </div>
    <hr>


    {!! Form::open(['url'=>route('gestion.charge-livraison'),'method'=>'POST']) !!}
    <div class="col-12">
        <div class="row">
            <div class="col col-3">

                <section class="card">
                    <header class="card-header">
                        <h2 class="card-title"> Transporteur</h2>
                    </header>
                    <div class="card-body">

                        <div class="form-group">

                            <label for="transporteur_id" class="control-label">Nom du Transporteur : </label><br>
                            {!! Form::select('transporteur_id', $entree->transporteur()->get()->pluck('nom','id'),
                            $entree->distributeur_id, ['transporteur_id'=>'transporteur_id','class'=>'form-control
                             show-tick',' data-live-search' => true, 'data-live-search-normalize'=>true,
                            'required'=>'required', 'readonly'=>true]) !!}



                            @error('transporteur_id')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="type_vehicule_id" class="control-label">Type de transporteur : </label>
                            {!! Form::select('type_vehicule_id',$type_vehicules->pluck('libelle','id'), request('type_vehicule_id',$entree->vehicule->type_vehicule_id ?? 1),
                            ['id'=>'type_vehicule_id','class'=>'form-control select2','data-plugin-multiselect']) !!}
                            @error('type_vehicule_id','charges')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="chauffeur" class="control-label"> Conducteur : </label><br>


                            {!! Form::text('chauffeur', $entree->vehicule->chauffeur,
                            ['chauffeur'=>'chauffeur','class'=>'form-control',
                            'required'=>'required', 'readonly'=>true]) !!}
                            @error('chauffeur')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-row">

                            <div class="col">
                                <div class="form-group">
                                    <label for="immatriculation" class="control-label">Vehicule : </label><br>


                                    {!! Form::text('immatriculation', $entree->vehicule->immatriculation,
                                    ['immatriculation'=>'immatriculation','class'=>'form-control',
                                    'required'=>'required', 'readonly'=>true]) !!}
                                    @error('immatriculation')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="contact" class="control-label">Contacts : </label><br>


                                    {!! Form::text('contact', $entree->vehicule->contact,
                                    ['id'=>'contact','class'=>'form-control',
                                    'required'=>'required', 'readonly'=>true]) !!}
                                    @error('contact')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>


                        </div>

                    </div>

                </section>

            </div>
            <div class="col col-2">

                <section class="card">
                    <header class="card-header">
                        <h2 class="card-title"> Type de subvention</h2>
                    </header>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="type_subvention_id" class="control-label">Type Subvention : </label>
                            {!! Form::select('type_subvention_id',$type_subventions->pluck('libelle','id'),request('type_subvention_id', $entree->subvention->type_subvention_id ?? 1),
                            ['id'=>'type_subvention_id','class'=>'form-control select2']) !!}
                            @error('type_subvention_id','charges')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                </section>

            </div>
            <div class="col col-4 ">


                <section class="card">
                    <header class="card-header">


                        <h2 class="card-title"> Entrer les charges liées au transport </h2>
                        <div class="card-subtitle">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </div>
                    </header>
                    <div class="card-body">
                        <div class="row form-group">
                            <div class="col-6">

                                <div class="form-group">
                                    <label for="prix_base" class="control-label">Tarif de base : </label>
                                    {!! Form::text('prix_base', $entree->subvention->prix_base ?? null,
                                    ['id'=>'prix_base','class'=>'form-control','placeholder'=>"10000",
                                    'required'=>'required']) !!}
                                    @error('prix_base','charges')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-6">

                                <div class="form-group">
                                    <label for="prix_negocie" class="control-label">Prix du voyage : </label>
                                    {!! Form::text('prix_negocie', $entree->subvention->prix_negocie ?? null ,
                                    ['id'=>'prix_negocie','class'=>'form-control', 'placeholder'=>"10000",'required'=>'required']) !!}
                                    @error('prix_negocie','charges')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col">

                                <div class="form-group">
                                    <label for="montant_supporte" class="control-label">Mnt supporté par le client : </label><br>


                                    {!! Form::text('montant_supporte', $entree->subvention->montant_supporte ?? null ,
                                    ['id'=>'montant_supporte','class'=>'form-control', 'placeholder'=>"10000",'required'=>'required']) !!}
                                    @error('montant_supporte','charges')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col">

                                <div class="form-group">
                                    <label for="montant_supporte_cimaf" class="control-label">Mnt supporté par CIMAF: </label><br>


                                    {!! Form::text('montant_supporte_cimaf', $entree->subvention->montant_supporte_cimaf ?? null ,
                                    ['id'=>'montant_supporte_cimaf','class'=>'form-control', 'placeholder'=>"0",'required'=>'required','readonly'=>'readonly']) !!}
                                    @error('montant_supporte_cimaf','charges')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col">

                                <div class="form-group">
                                    <label for="avance" class="control-label">Remis : </label><br>


                                    {!! Form::number('avance', $entree->subvention->avance ?? null ,
                                    ['id'=>'avance','class'=>'form-control', 'min'=>'0', 'step'=>1000, 'placeholder'=>"10000",'required'=>'required']) !!}
                                    @error('avance','charges')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col">

                                <div class="form-group">
                                    <label for="reste" class="control-label">Reste: </label><br>


                                    {!! Form::text('reste', $entree->subvention->reste ?? null ,
                                    ['id'=>'reste','class'=>'form-control', 'placeholder'=>"0",'required'=>'required','readonly'=>'readonly']) !!}
                                    @error('reste','charges')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <div class="radio-custom radio-success">
                                        <input type="radio" class="paiementStatus" value="{{ old('statut','paid') }}"
                                               id="paid" name="statut"
                                               @if( $entree->subvention &&  $entree->subvention->statut ==true ) checked @endif>
                                        <label for="paid">Payé</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col">

                                <div class="form-group">
                                    <div class="radio-custom radio-danger">
                                        <input type="radio" class="paiementStatus" value="{{ old('statut','unpaid') }}"
                                               id="unpaid" name="statut"
                                               @if( $entree->subvention &&  $entree->subvention->statut ==false ) checked
                                            @endif @if( !$entree->subvention) checked
                                            @endif>
                                        <label for="unpaid">Non Payé</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col">

                                <div class="form-group" id="field_caisse"
                                     style="display: {{$entree->subvention &&  $entree->subvention->statut==true ? 'inline' :'none' }}">
                                    <label for="numero_bon_caisse" class="control-label">N° Bon de
                                        Caisse ou N° Facture: </label><br>


                                    {!! Form::text('numero_bon_caisse', $entree->subvention->numero_bon_caisse ?? null ,
                                    ['id'=>'numero_bon_caisse','class'=>'form-control']) !!}
                                    @error('numero_bon_caisse','charges')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                        </div>

                    </div>

                </section>
                {!! Form::close() !!}
            </div>
            <div class="col col-3">

                <section class="card">
                    <header class="card-header">
                        <h2 class="card-title">Client</h2>
                    </header>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="client_id" class="control-label">Client : </label>
                            {!! Form::select('client_id',$clients->pluck('nom','id'),request('client_id',$entree->tonnage ? $entree->tonnage->client_id : null),
                            ['id'=>'client_id','class'=>'form-control selectPicker','placeholder'=>'Selectionner un client dans la liste','title'=>'Selectionner un client dans la liste']) !!}
                            @error('client_id','charges')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-row">
                            <label class="col-form-label" for="date_livraison">Date</label>
                            <div class="input-group">
														<span class="input-group-prepend">
															<span class="input-group-text">
																<i class="fas fa-calendar-alt"></i>
															</span>
														</span>
                                <input type="text" name="date_livraison"
                                       value="{{ old('date_livraison',request('date_livraison',$entree->tonnage ? $entree->tonnage->date : null)) }}"
                                       id="date_livraison"
                                       data-plugin-datepicker class="form-control" autocomplete="off" required
                                       readonly
                                       data-plugin-options='{"orientation":"bottom","autoclose":true,"language":"en","todayHighlight":true,"format":"yyyy-mm-dd"}'>
                            </div>
                            @error('date_livraison')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror

                            {!! Form::hidden('quantite', $entree->quantite,
                               ['id'=>'quantite']) !!}
                        </div>
                    </div>

                </section>

            </div>
        </div>
        <div class="row justify-content-end">
            {!! Form::hidden('pesee_id',$entree->id) !!}
            <button type="submit" class="btn btn-dark" data-toggle="confirmation"><i
                    class="fas fa-save"></i> Enregistrer
            </button>
        </div>
    </div>
    {!! Form::close() !!}

@endsection
