@extends('template.theme')


@section('font-title')

@endsection

@section('css-libs')
    {{--<link rel="stylesheet" href="{{ asset('css/charts.css') }}">--}}
    {{--<link rel="stylesheet" href="{{ asset('css/chart-style.css') }}">--}}
    <style>
        .highcharts-figure, .highcharts-data-table table {
            min-width: 310px;
            max-width: 800px;
            margin: 1em auto;
        }

        #container {
            height: 400px;
        }

        .highcharts-data-table table {
            font-family: Verdana, sans-serif;
            border-collapse: collapse;
            border: 1px solid #EBEBEB;
            margin: 10px auto;
            text-align: center;
            width: 100%;
            max-width: 500px;
        }

        .highcharts-data-table caption {
            padding: 1em 0;
            font-size: 1.2em;
            color: #555;
        }

        .highcharts-data-table th {
            font-weight: 600;
            padding: 0.5em;
        }

        .highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
            padding: 0.5em;
        }

        .highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
            background: #f8f8f8;
        }

        .highcharts-data-table tr:hover {
            background: #f1f7ff;
        }
    </style>
@endsection
@push('scripts')
    <script src="{{ asset('js/charts.js') }}" type="text/javascript" defer="true"></script>
    <script src="{{ asset('js/charts-script.js') }}" type="text/javascript" defer="true"></script>
    {!! $chart->script() !!}
    {!! $dailyDetailCharts->script() !!}
    {!! $budgetCharts->script() !!}
    <script src="{{ $monthlyPieCharts->cdn() }}"></script>
    {{ $monthlyPieCharts->script() }}
    {{ $yearlyPieCharts->script() }}

    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>


@endpush
@section('page-header')
    <header class="page-header">
        <h2>{{ $page_header_title ?? '' }}</h2>
    </header>
@endsection

@section('content-body')

    {{--    {{ dd($budget_boucle) }}--}}
{{--    <div class="sidebar-widget widget-stats">--}}
{{--        <div class="widget-header">--}}
{{--            <h6>Evolution Budget</h6>--}}
{{--        </div>--}}
{{--        <div class="widget-content">--}}
{{--            <ul class="row">--}}
{{--                @foreach ($detail_budgets as $detail_budget)--}}

{{--                    @foreach($budget_boucle as  $boucle)--}}
{{--                        @if(Str::contains($boucle->produit,'CPJ 42.5'))--}}
{{--                            @php--}}
{{--                                $expedie_cpj_42 = ''--}}
{{--                            --}}
{{--                            @endphp--}}
{{--                            <li class="col-3">--}}
{{--                                <span class="stats-title">{{ $detail_budget->produit  }} {{ $boucle->produit }}</span>--}}
{{--                                <span--}}
{{--                                    class="stats-complete">{{ (($boucle->realise*100) /$detail_budget->budget) }}</span>--}}
{{--                                <div class="progress">--}}
{{--                                    <div class="progress-bar  " role="progressbar" aria-valuenow="{{ (($boucle->realise*100) /$detail_budget->budget) }}" aria-valuemin="0"--}}
{{--                                         aria-valuemax="{{ $detail_budget->budget }}" style="width: {{ (($boucle->realise*100) /$detail_budget->budget) }}%;">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                        @endif--}}
{{--                    @endforeach--}}

{{--                @endforeach--}}
{{--            </ul>--}}
{{--        </div>--}}
{{--    </div>--}}
    {{-- <h4 class="mt-0 mb-0">Flot Charts</h4>
    <p class="mb-4">Flot is a pure JavaScript plotting library for jQuery, with a focus on simple usage, attractive looks and interactive features.</p> --}}
    <div class="row">
        <div class="col col-6 offset-3">
            {!! Form::open(['url'=>route('application.historique'),'class'=>'monthly-form',"id"=>'monthly-form']) !!}
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title"> Consultation Historique </h2>
                    <p class="card-subtitle">

                    </p>
                </header>
                <div class="card-body">
                    <div class="form-row">
                        <div class="col">
                            <div class="form-group">
                                <label class="col-form-label" for="date_month">Date </label>
                                <div class="input-group">
														<span class="input-group-prepend">
															<span class="input-group-text">
																<i class="fas fa-calendar-alt"></i>
															</span>
														</span>
                                    <input type="text" name="date_month"
                                           value="{{ old('date_month',request('date_month')) }}"
                                           id="date_month" class="form-control" autocomplete="off"
                                           required
                                           data-plugin-datepicker
                                           data-date-view="months"
                                           data-date-view-mode="months"
                                           data-date-min-view-mode="months"
                                           data-date-format="mm/yyyy"
                                           data-plugin-options='{"orientation":"bottom","autoclose":true}'
                                    >
                                </div>
                                @error('date_month')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    {{--                    <div class="row">--}}
                    {{--                        <div class="col">--}}

                    {{--                            <label class=" control-label"> Zones </label>--}}
                    {{--                            {!! Form::select('zone_id[]',$zones->pluck('libelle','id'),request('zone_id'),['class'=>'form-control selectPicker', 'multiple'=>true,'style'=>'width:98%'--}}
                    {{--                                     ]--}}
                    {{--                                      ) !!}--}}

                    {{--                        </div>--}}
                    {{--                    </div>--}}
                    <hr>
                </div>
                <footer class="card-footer">
                    <div class="row ">
                        <div class="col-sm-9 align-content-md-end">
                            <input type="hidden" name="case" value="monthly">
                            <button type="submit" data-toggle="confirmation" class="btn btn-primary"><i
                                    class="fas search"></i> Rechercher
                            </button>


                        </div>
                    </div>
                </footer>
            </section>
            {!! Form::close() !!}
        </div>
    </div>
    <div class="row">
        <div class="col-md-7">
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>
                    <h2 class="card-title">Histogramme {{ $periode }}</h2>
                    <p class="card-subtitle"></p>
                </header>
                <div class="card-body">
                    {!! $chart->container() !!}
                </div>
            </section>
        </div>
        <div class="col-md-5">
            <section class="card card-primary">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title"> Budget annuel 2020 </h2>
                    <p class="card-subtitle"></p>
                </header>
                <div class="card-body">
                    {!! $budgetCharts->container() !!}
                </div>
            </section>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <section class="card card-info">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title">Ventes Mensuelles </h2>
                    <p class="card-subtitle"></p>
                </header>
                <div class="card-body">
                    {!! $monthlyPieCharts->container() !!}
                </div>
            </section>
        </div>
        <div class="col-md-6">
            <section class="card card-warning">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title">Ventes Annuelles </h2>
                    <p class="card-subtitle"></p>
                </header>
                <div class="card-body">
                    {!! $yearlyPieCharts->container() !!}
                </div>
            </section>
        </div>
    </div>
    {{--    <div class="row">--}}
    {{--        <div class="col-md-12">--}}
    {{--            <section class="card">--}}
    {{--                <header class="card-header">--}}
    {{--                    <div class="card-actions">--}}
    {{--                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>--}}
    {{--                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>--}}
    {{--                    </div>--}}

    {{--                    <h2 class="card-title">Histogramme  {{ $periode }}</h2>--}}
    {{--                    <p class="card-subtitle"></p>--}}
    {{--                </header>--}}
    {{--                <div class="card-body">--}}
    {{--                    {!! $dailyDetailCharts->container() !!}--}}
    {{--                </div>--}}
    {{--            </section>--}}
    {{--        </div>--}}
    {{--    </div>--}}
    <div class="row">

        <div class="col-md">
            <section class="card">

                <div class="card-body table-responsive">

                    <table class="table small table-bordered table-condensed table-striped mb-0 " id="">
                        <thead>

                        <th colspan="2"></th>
                        @if($show_daily)
                            <th>JOUR</th>
                        @endif
                        <th>MOIS</th>
                        <th>ANNEE</th>
                        </thead>
                        <tbody>

                        <tr valign="middle" class="text-center">
                            <th rowspan="5" valign="midlle">SAC</th>
                            <th>CPJ 32.5</th>
                            @if($show_daily)
                                <td>{{ $jour_32 = trim(data_get($totaux, 'sacs.jour.32_5'),'[]')  }}</td>
                            @endif
                            <td>{{ $mois_32 = trim(data_get($totaux, 'sacs.mois.32_5'),'[]')  }}</td>
                            <td>{{ $annee_32 = trim(data_get($totaux, 'sacs.annee.32_5'),'[]')  }}</td>

                        </tr>
                        <tr class="text-center">

                            <th>CPJ 42.5</th>
                            @if($show_daily)
                                <td>{{ $jour_42 =trim(data_get($totaux, 'sacs.jour.42_5'),'[]')}}</td>
                            @endif
                            <td>{{ $mois_42 = trim(data_get($totaux, 'sacs.mois.42_5'),'[]')}}</td>
                            <td>{{ $annee_42 = trim(data_get($totaux, 'sacs.annee.42_5'),'[]')}}</td>

                        </tr>
                        <tr class="text-center">
                            <th>CPA ROBUSTO</th>
                            @if($show_daily)
                                <td>{{ $jour_cpa = trim(data_get($totaux, 'sacs.jour.cpa'),'[]') ??  0 }}</td>
                            @endif
                            <td>{{ $mois_cpa = trim(data_get($totaux, 'sacs.mois.cpa'),'[]') ??  0 }}</td>
                            <td>{{ $annee_cpa = trim(data_get($totaux, 'sacs.annee.cpa'),'[]') ??  0 }}</td>

                        </tr>
                        <tr class="text-center">

                            <th>CHF</th>
                            @if($show_daily)
                                <td>0</td>
                            @endif
                            <td>0</td>
                            <td>0</td>
                        </tr>
                        <tr class="text-center info">

                            <th>TOTAL SAC</th>
                            @if($show_daily)
                                <th> @php($jour_sac = $jour_32+$jour_42+$jour_cpa)  {{ number_format($jour_sac,2,',',' ') }}</th>
                            @endif
                            <th> @php($mois_sac = $mois_32+$mois_42+$mois_cpa)  {{ number_format($mois_sac,2,',',' ') }}</th>
                            <th> @php($annee_sac = $annee_32+$annee_42+$annee_cpa)  {{ number_format($annee_sac,2,',',' ') }}</th>

                        </tr>
                        <tr class="text-center">
                            <th rowspan="5">VRAC</th>
                            <th>CPJ 32.5</th>
                            @if($show_daily)
                                <td>{{ $jour_32_vrac = trim(data_get($totaux, 'vrac.jour.32_5'),'[]')  }}</td>
                            @endif
                            <td>{{ $mois_32_vrac = trim(data_get($totaux, 'vrac.mois.32_5'),'[]')  }}</td>
                            <td>{{ $annee_32_vrac = trim(data_get($totaux, 'vrac.annee.32_5'),'[]')  }}</td>


                        </tr>
                        <tr class="text-center">

                            <th>CPJ 42.5</th>
                            @if($show_daily)
                                <td>{{ $jour_42_vrac = trim(data_get($totaux, 'vrac.jour.42_5'),'[]')  }}</td>
                            @endif
                            <td>{{ $mois_42_vrac = trim(data_get($totaux, 'vrac.mois.42_5'),'[]')  }}</td>
                            <td>{{ $annee_42_vrac = trim(data_get($totaux, 'vrac.annee.42_5'),'[]')  }}</td>

                        </tr>
                        <tr class="text-center">

                            <th>CPA ROBUSTO</th>
                            @if($show_daily)
                                <td>{{ $jour_cpa_vrac = trim(data_get($totaux, 'vrac.jour.cpa'),'[]')  }}</td>
                            @endif
                            <td>{{ $mois_cpa_vrac = trim(data_get($totaux, 'vrac.mois.cpa'),'[]')  }}</td>
                            <td>{{ $annee_cpa_vrac = trim(data_get($totaux, 'vrac.annee.cpa'),'[]')  }}</td>

                        </tr>
                        <tr class="text-center">

                            <th>CHF</th>
                            @if($show_daily)
                                <td>{{ $jour_chf_vrac = trim(data_get($totaux, 'vrac.jour.chf'),'[]')  }}</td>
                            @endif
                            <td>{{ $mois_chf_vrac = trim(data_get($totaux, 'vrac.mois.chf'),'[]')  }}</td>
                            <td>{{ $annee_chf_vrac = trim(data_get($totaux, 'vrac.annee.chf'),'[]')  }}</td>

                        </tr>
                        <tr class="text-center primary">

                            <th>TOTAL VRAC</th>
                            @if($show_daily)
                                <td> @php($jour_vrac = $jour_32_vrac+$jour_42_vrac+$jour_cpa_vrac+$jour_chf_vrac)  {{ number_format($jour_vrac,2,',',' ') }}</td>
                            @endif
                            <td> @php($mois_vrac = $mois_32_vrac + $mois_42_vrac+ $mois_cpa_vrac+ $mois_chf_vrac)  {{ number_format($mois_vrac,2,',',' ') }}</td>
                            <td> @php($annee_vrac = $annee_32_vrac + $annee_42_vrac + $annee_cpa_vrac + $annee_chf_vrac)  {{ number_format($annee_vrac,2,',',' ') }}</td>
                        </tr>
                        <tr class="text-center ">

                            <th></th>
                            <th>TOTAL EXPEDIES</th>
                            @if($show_daily)
                                <th> @php($expedie_jour = $jour_vrac+$jour_sac) {{ number_format($expedie_jour,2,',',' ') }} </th>
                            @endif
                            <th> @php($expedie_mois = $mois_vrac+$mois_sac) {{ number_format($expedie_mois,2,',',' ') }} </th>
                            <th> @php($expedie_annee = $annee_vrac+$annee_sac) {{ number_format($expedie_annee,2,',',' ') }} </th>

                        </tr>
                        </tbody>
                    </table>

                </div>
            </section>
        </div>
        <div class="col-md">
            <div class="row">
                <div class="col">
                    <section class="card card-warning">
                        <header class="card-header">
                            <div class="card-actions">
                                <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                            </div>

                            <h2 class="card-title"> Apperçu des ventes Mensuelle par Zone </h2>
                            <p class="card-subtitle"></p>
                        </header>
                        <div class="card-body table-responsive">


                            <table class="table small table-bordered table-condensed table-striped mb-0 " id="">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Chef lieu</th>
                                    <th>Expedies</th>
                                    <th>Pourcentage</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php(  $somme_expedition_mensuelle =$somme_percentage_annuel=$somme_percentage_mensuel =0)
                                @php( $total_mois = $somme_totale_expedies_par_district_par_mois['expedies'] + $somme_totale_expedies_par_district_par_mois['orphans'])
                                @php( $total_annee = $somme_totale_expedies_par_district_par_annee['expedies'] + $somme_totale_expedies_par_district_par_annee['orphans'])

                                @foreach($expedition_mensuelles as $expedition_mensuelle)
                                    <tr>

                                        <td>{{ $loop->index +1 }}</td>
                                        <td>{{ $expedition_mensuelle->chef_lieu }}</td>
                                        <td>{{  number_format($expedition_mensuelle->expedies,2,',',' ') }}</td>
                                        <td> @if($expedition_mensuelle->expedies>0)
                                                @php($percentage = percentage($expedition_mensuelle->expedies, $total_mois, 2))
                                                {{ number_format($percentage,2,',',' ')}} %
                                                @php ($somme_percentage_mensuel+=$percentage)
                                            @endif
                                        </td>

                                    </tr>
                                    @php ($somme_expedition_mensuelle +=$expedition_mensuelle->expedies)
                                @endforeach
                                {{-- @dd($expedition_mensuelles) --}}
                                @if(!$expedition_mensuelles->isEmpty() )
                                <tr>
                                    <td>{{ sizeof($expedition_mensuelles) +1 }}</td>
                                    <td colspan="1">Autre</td>
                                    <td> {{ number_format($expedition_mensuelles_orphans,2,',',' ') }}</td>

                                    <td>  @if($expedition_mensuelle->expedies>0)

                                       {{ number_format((100-$somme_percentage_mensuel),2,',',' ') }} %
                                            @endif
                                    </td>
                                </tr>

                                <tr>


                                    <td>  @if(@isset($expedition_mensuelle) && $expedition_mensuelle->expedies>0)

                                            {{ number_format((100-$somme_percentage_mensuel),2,',',' ') }} %
                                    @endif
                                    </td>
                                </tr>
                                <tr>

                                    <th colspan="2" class="text-right">TOTAL</th>
                                    @php($sommeTotale =  $somme_expedition_mensuelle + $expedition_mensuelles_orphans )
                                    <th colspan="2"
                                        class="text-center"> {{ number_format($sommeTotale,2,',',' ') }}</th>
                                </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </section>
                </div>
                <div class="col">
                    <section class="card card-primary">
                        <header class="card-header">
                            <div class="card-actions">
                                <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                            </div>

                            <h2 class="card-title"> Apperçu des ventes Annuelle par Zone </h2>
                            <p class="card-subtitle"></p>
                        </header>
                        <div class="card-body table-responsive">


                            <table class="table small table-bordered table-condensed table-striped mb-0 " id="">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Chef lieu</th>
                                    <th>Expedies</th>
                                    <th>Pourcentage</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php($somme_expedition_annuelle =0)
                                @foreach($expedition_annuelles as $expedition_annuelle)
                                    <tr>

                                        <td>{{ $loop->index +1 }}</td>
                                        <td>{{ $expedition_annuelle->chef_lieu }}</td>
                                        <td>{{  number_format($expedition_annuelle->expedies,2,',',' ') }}</td>
                                        <td> @if($expedition_annuelle->expedies>0)
                                                @php($percentage = percentage($expedition_annuelle->expedies, $total_annee, 2))
                                                {{ number_format($percentage,2,',',' ')}} %
                                                @php ($somme_percentage_annuel+=$percentage)
                                            @endif
                                        </td>
                                    </tr>
                                    @php ($somme_expedition_annuelle +=$expedition_annuelle->expedies)
                                @endforeach
                                <tr>
                                    <td>{{ sizeof($expedition_annuelles) +1 }}</td>
                                    <td colspan="1">Autre</td>
                                    <td> {{ number_format($expedition_annuelles_orphans,2,',',' ') }}</td>
                                    <td>


                                        {{ number_format((100-$somme_percentage_annuel),2,',',' ') }} %

                                    </td>
                                </tr>
                                <tr>
                                    <th colspan="2" class="text-right">TOTAL</th>
                                    @php($sommeTotaleAnnuelle =  $somme_expedition_annuelle + $expedition_annuelles_orphans )
                                    <th colspan="2"
                                        class="text-center"> {{ number_format($sommeTotaleAnnuelle,2,',',' ') }}</th>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </section>
                </div>
            </div>


        </div>

    </div>
    <div class="row">

        <div class="col">
            <section class="card card-featured-dark">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title"> Apperçu des ventes Mensuelle par Distributeur </h2>
                    <p class="card-subtitle"></p>
                </header>
                <div class="card-body table-responsive">


                    <table class="table small table-bordered table-condensed table-striped mb-0 " id="">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Chef lieu</th>
                            <th>Distributeur</th>
                            <th>Expedies</th>
                            <th>Pourcentage</th>
                        </tr>
                        </thead>
                        {{--                                <tbody>--}}
                        {{--                                @php(  $somme_expedition_mensuelle =$somme_percentage_annuel=$somme_percentage_mensuel =0)--}}
                        {{--                                @php( $total_mois = $somme_totale_expedies_par_distributeur_par_mois['expedies'] + $somme_totale_expedies_par_distributeur_par_mois['orphans'])--}}
                        {{--                                @php( $total_annee = $somme_totale_expedies_par_distributeur_par_annee['expedies'] + $somme_totale_expedies_par_distributeur_par_annee['orphans'])--}}

                        {{--                                @foreach($expedition_mensuelle_distributeurs as $expedition_mensuelle_distributeur)--}}
                        {{--                                    <tr>--}}

                        {{--                                        <td>{{ $loop->index +1 }}</td>--}}
                        {{--                                        <td>{{ $expedition_mensuelle_distributeur->chef_lieu }}</td>--}}
                        {{--                                        <td>{{ $expedition_mensuelle_distributeur->distributeur }}</td>--}}
                        {{--                                        <td>{{  number_format($expedition_mensuelle_distributeur->expedies,2,',',' ') }}</td>--}}
                        {{--                                        <td> @if($expedition_mensuelle_distributeur->expedies>0)--}}
                        {{--                                                @php($percentage = percentage($expedition_mensuelle_distributeur->expedies, $total_mois, 2))--}}
                        {{--                                                {{ number_format($percentage,2,',',' ')}} %--}}
                        {{--                                                @php ($somme_percentage_mensuel+=$percentage)--}}
                        {{--                                            @endif--}}
                        {{--                                        </td>--}}

                        {{--                                    </tr>--}}
                        {{--                                    @php ($somme_expedition_mensuelle +=$expedition_mensuelle_distributeur->expedies)--}}
                        {{--                                @endforeach--}}
                        {{--                                <tr>--}}
                        {{--                                    <td >{{ sizeof($expedition_mensuelle_distributeurs) +1 }}</td>--}}
                        {{--                                    <td colspan="2">Autre</td>--}}
                        {{--                                    <td> {{ number_format($expedition_mensuelle_distributeur_orphans,2,',',' ') }}</td>--}}
                        {{--                                    <td>  @if(@isset($expedition_mensuelle_distributeur) && $expedition_mensuelle_distributeur->expedies>0)--}}

                        {{--                                            {{ number_format((100-$somme_percentage_mensuel),2,',',' ') }} %--}}
                        {{--                                    @endif--}}
                        {{--                                </tr><tr>--}}
                        {{--                                    <th colspan="3" class="text-right">TOTAL</th>--}}
                        {{--                                    @php($sommeTotale =  $expedition_mensuelle_distributeurs + $expedition_mensuelle_distributeur_orphans )--}}
                        {{--                                    <th colspan="2" class="text-center"> {{ number_format($sommeTotale,2,',',' ') }}</th>--}}
                        {{--                                </tr>--}}
                        {{--                                </tbody>--}}
                    </table>
                </div>
            </section>
        </div>
        {{--                <div class="col">--}}
        {{--                    <section class="card card-primary">--}}
        {{--                        <header class="card-header">--}}
        {{--                            <div class="card-actions">--}}
        {{--                                <a href="#" class="card-action card-action-toggle" data-card-toggle></a>--}}
        {{--                                <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>--}}
        {{--                            </div>--}}

        {{--                            <h2 class="card-title"> Apperçu des ventes Annuelle par Zone </h2>--}}
        {{--                            <p class="card-subtitle"></p>--}}
        {{--                        </header>--}}
        {{--                        <div class="card-body table-responsive">--}}


        {{--                            <table class="table small table-bordered table-condensed table-striped mb-0 " id="">--}}
        {{--                                <thead>--}}
        {{--                                <tr>--}}
        {{--                                    <th>#</th>--}}
        {{--                                    <th>Chef lieu</th>--}}
        {{--                                    <th>Expedies</th>--}}
        {{--                                    <th>Pourcentage</th>--}}
        {{--                                </tr>--}}
        {{--                                </thead>--}}
        {{--                                <tbody>--}}
        {{--                                @php($somme_expedition_annuelle =0)--}}
        {{--                                @foreach($expedition_annuelles as $expedition_annuelle)--}}
        {{--                                    <tr>--}}

        {{--                                        <td>{{ $loop->index +1 }}</td>--}}
        {{--                                        <td>{{ $expedition_annuelle->chef_lieu }}</td>--}}
        {{--                                        <td>{{  number_format($expedition_annuelle->expedies,2,',',' ') }}</td>--}}
        {{--                                        <td> @if($expedition_annuelle->expedies>0)--}}
        {{--                                                @php($percentage = percentage($expedition_annuelle->expedies, $total_annee, 2))--}}
        {{--                                                {{ number_format($percentage,2,',',' ')}} %--}}
        {{--                                                @php ($somme_percentage_annuel+=$percentage)--}}
        {{--                                            @endif--}}
        {{--                                        </td>--}}
        {{--                                    </tr>--}}
        {{--                                    @php ($somme_expedition_annuelle +=$expedition_annuelle->expedies)--}}
        {{--                                @endforeach--}}
        {{--                                <tr>--}}
        {{--                                    <td >{{ sizeof($expedition_annuelles) +1 }}</td>--}}
        {{--                                    <td colspan="1">Autre</td>--}}
        {{--                                    <td> {{ number_format($expedition_annuelles_orphans,2,',',' ') }}</td>--}}
        {{--                                    <td>--}}


        {{--                                        {{ number_format((100-$somme_percentage_annuel),2,',',' ') }} %--}}

        {{--                                    </td>--}}
        {{--                                </tr><tr>--}}
        {{--                                    <th colspan="2"  class="text-right">TOTAL</th>--}}
        {{--                                    @php($sommeTotaleAnnuelle =  $somme_expedition_annuelle + $expedition_annuelles_orphans )--}}
        {{--                                    <th colspan="2" class="text-center"> {{ number_format($sommeTotaleAnnuelle,2,',',' ') }}</th>--}}
        {{--                                </tr>--}}
        {{--                                </tbody>--}}
        {{--                            </table>--}}
        {{--                        </div>--}}
        {{--                    </section>--}}
        {{--                </div>--}}


    </div>
@endsection
