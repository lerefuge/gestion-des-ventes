<?php

namespace App\Http\Controllers;

use App\Repositories\DestinationRepository;
use App\Repositories\PeseeRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class DetailsController extends Controller
{
    /**
     * @var DestinationRepository
     */
    private $destinationRepository;
    /**
     * @var PeseeRepository
     */
    private $peseeRepository;

    /**
     * DetailsController constructor.
     * @param DestinationRepository $destinationRepository
     * @param PeseeRepository $peseeRepository
     */
    public function __construct(DestinationRepository $destinationRepository,PeseeRepository $peseeRepository)
    {

        $this->destinationRepository = $destinationRepository;
        $this->peseeRepository = $peseeRepository;
    }

    public function localite()
    {
        $data['localites'] = $this->destinationRepository->destination()->get()->sortBy('libelle');
        $month = now()->subMonth();
        $data['days']=$month->daysInMonth;

        $date_month = Carbon::parse($month)->format('Y-m-d');
        $start_month = $date_month."-01";

        $end_month = Carbon::parse($start_month)->endOfMonth()->format('Y-m-d');
        $start = Carbon::parse($start_month)->format('d/m/Y');
        $fin = Carbon::parse($end_month)->format('d/m/Y');
        $data['data_table_url'] = route('datatable.detail-mensuel', ['debut' => strtotime($start_month), 'fin' => strtotime($end_month)]);
        $data['periode'] = " Point des ventes de la période du {$start} au {$fin} ";
        $data['localite_ids'] = NULL;
        return view('details.localites')->with($data);
    }

    public function getDetailLocalite(Request $request)
    {
       // dd($request->all());
        $period = $request->case;
        switch ($period):
            case  'monthly';
                $date_send = explode('/', $request->date_month);
                $correct_date = $date_send[1] . '-' . $date_send['0'] . '-01';
                $month = $correct_date;
                $date_month = Carbon::parse($month)->format('Y-m-d');
                $start_month = $date_month;
                $end_month = Carbon::parse($start_month)->endOfMonth()->format('Y-m-d');
                $start = Carbon::parse($start_month)->format('d/m/Y');

                $fin = Carbon::parse($end_month)->format('d/m/Y');
                $data['data_table_url'] = route('datatable.detail-mensuel', ['debut' => strtotime($start_month), 'fin' => strtotime($end_month)]);
                $data['periode'] = " Point des ventes de la période du {$start} au {$fin} ";
                $data['days'] = Carbon::parse($month)->daysInMonth;
                break;
            case 'yearly';
            $start_year = "01-01-" . $request->date_year;
            $end_year = "31-12-" . $request->date_year;
            $date_year_start = Carbon::parse($start_year)->format('Y-m-d');
            $date_year_end = Carbon::parse($end_year)->format('Y-m-d');
            $start = Carbon::parse($date_year_start)->format('d/m/Y');
            $fin = Carbon::parse($date_year_end)->format('d/m/Y');
            $data['periode'] = " Point des ventes de la période du {$start} au {$fin} ";
            $data['data_table_url'] = route('datatable.detail-annuelle', ['debut' => strtotime($date_year_start), 'fin' => strtotime($date_year_end)]);
                       break;
        endswitch;
        $data['localites'] = $this->destinationRepository->destination()->get()->sortBy('libelle');
        return view('details.localites')->with($data);
    }
}
