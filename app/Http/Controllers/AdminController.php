<?php

namespace App\Http\Controllers;

use App\Annee;
use App\Budget;
use App\ChefLieuDistrict;
use App\Client;
use App\Cout;
use App\Distributeur;
use App\District;
use App\Produit;
use App\Region;
use App\Repositories\BudgetRepository;
use App\Repositories\ChefLieuDistrictRepository;
use App\Repositories\ClientRepository;
use App\Repositories\CoutRepository;
use App\Repositories\DestinationRepository;
use App\Repositories\DistributeurRepository;
use App\Repositories\DistrictRepository;
use App\Repositories\MonthRepository;
use App\Repositories\PermissionRepository;
use App\Repositories\ProduitRepository;
use App\Repositories\RegionRepository;
use App\Repositories\RoleRepository;
use App\Repositories\TransporteurRepository;
use App\Repositories\TypeVehiculeRepository;
use App\Repositories\UserRepository;
use App\Transporteur;
use App\User;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Spatie\Permission\Exceptions\PermissionAlreadyExists;
use Spatie\Permission\Guard;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AdminController extends Controller
{
    /**
     * @var ProduitRepository
     */
    private $produitRepository;
    /**
     * @var RegionRepository
     */
    private $regionRepository;
    /**
     * @var DestinationRepository
     */
    private $destinationRepository;
    /**
     * @var CoutRepository
     */
    private $coutRepository;
    /**
     * @var TypeVehiculeRepository
     */
    private $typeVehiculeRepository;
    /**
     * @var DistributeurRepository
     */
    private $distributeurRepository;
    /**
     * @var TransporteurRepository
     */
    private $transporteurRepository;
    /**
     * @var ClientRepository
     */
    private $clientRepository;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var RoleRepository
     */
    private $roleRepository;
    /**
     * @var PermissionRepository
     */
    private $permissionRepository;
    /**
     * @var MonthRepository
     */
    private $monthRepository;
    /**
     * @var BudgetRepository
     */
    private $budgetRepository;
    /**
     * @var DistrictRepository
     */
    private $districtRepository;
    /**
     * @var ChefLieuDistrictRepository
     */
    private $chefLieuDistrictRepository;

    /**
     * AdminController constructor.
     * @param ProduitRepository $produitRepository
     * @param RegionRepository $regionRepository
     * @param TransporteurRepository $transporteurRepository
     * @param UserRepository $userRepository
     * @param DestinationRepository $destinationRepository
     * @param MonthRepository $monthRepository
     * @param DistributeurRepository $distributeurRepository
     * @param BudgetRepository $budgetRepository
     * @param ClientRepository $clientRepository
     * @param RoleRepository $roleRepository
     * @param PermissionRepository $permissionRepository
     * @param DistrictRepository $districtRepository
     * @param ChefLieuDistrictRepository $chefLieuDistrictRepository
     * @param CoutRepository $coutRepository
     * @param TypeVehiculeRepository $typeVehiculeRepository
     */
    public function __construct(ProduitRepository $produitRepository,
                                RegionRepository $regionRepository,
                                TransporteurRepository $transporteurRepository,
                                UserRepository $userRepository,
                                DestinationRepository $destinationRepository,
                                MonthRepository $monthRepository,
                                DistributeurRepository $distributeurRepository,
                                BudgetRepository $budgetRepository,
                                ClientRepository $clientRepository,
                                RoleRepository $roleRepository,
                                PermissionRepository $permissionRepository,
                                DistrictRepository $districtRepository,
                                ChefLieuDistrictRepository $chefLieuDistrictRepository,
                                CoutRepository $coutRepository,
                                TypeVehiculeRepository $typeVehiculeRepository)
    {
        $this->middleware('auth');
        $this->produitRepository = $produitRepository;
        $this->regionRepository = $regionRepository;
        $this->destinationRepository = $destinationRepository;
        $this->coutRepository = $coutRepository;
        $this->typeVehiculeRepository = $typeVehiculeRepository;
        $this->distributeurRepository = $distributeurRepository;
        $this->transporteurRepository = $transporteurRepository;
        $this->clientRepository = $clientRepository;
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;
        $this->permissionRepository = $permissionRepository;
        $this->monthRepository = $monthRepository;
        $this->budgetRepository = $budgetRepository;
        $this->districtRepository = $districtRepository;
        $this->chefLieuDistrictRepository = $chefLieuDistrictRepository;
    }

    public function produits()
    {
        $data['produit'] = new Produit();
        $data['page_header_title'] = 'Qualités de ciments';
        $data['produits'] = $this->produitRepository->produit()->get();
        return view('admin.qualite-de-ciment')->with($data);
    }

    public function postProduits(Request $request)
    {
        // dd($request->all());
        $validator = validator($request->all(), [
            'qualite' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->_redirectWhenFail($validator);
        }
        try {
            \DB::beginTransaction();
        } catch (Exception $e) {
        }
        try {
            $d = ['libelle' => $request->get('qualite')];
            if ($this->produitRepository->produit()->create($d)) {
                flash('Produit enregistré', 'success');
                \DB::commit();
                return redirect()->route('admin.enregistrement-produit');
            }
            flash('Echec enregistrement de produit', 'warning');
            \DB::rollBack();
            return redirect()->route('admin.enregistrement-produit');
        } catch (Exception $exception) {
            \DB::rollBack();
            flash('Echec enregistrement de produit', 'info');
            return redirect()->back();
        }
    }

    private function _redirectWhenFail($validator = null)
    {
        flash('Veuillez corriger le erreurs SVP !', 'warning');
        $redirect = redirect()->back()->withInput();
        if ($validator)
            $redirect = $redirect->withErrors($validator)->withInput();
        return $redirect;
    }

    public function editProduits($produit_id)
    {
        $data['page_header_title'] = 'Qualités de ciments';
        $data['produits'] = $this->produitRepository->produit()->get();
        $data['produit'] = $this->produitRepository->produit()->find($produit_id);
        return view('admin.qualite-de-ciment')->with($data);
    }

    /**
     * @param Request $request
     * @param $produit_id
     * @return RedirectResponse
     * @throws Exception
     */
    public function updateProduits(Request $request, $produit_id)
    {
        $produit = $this->produitRepository->produit()->find($produit_id);
        $validator = validator($request->all(), [
            'qualite' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->_redirectWhenFail($validator);
        }
        \DB::beginTransaction();
        try {
            $d = ['libelle' => $request->get('qualite')];
            if ($produit->update($d)) {
                flash('Produit mis à jour', 'success');
                DB::commit();
                return redirect()->route('admin.enregistrement-produit');
            }
            flash('Echec modification du produit', 'warning');
            DB::rollBack();
            return redirect()->route('admin.enregistrement-produit');
        } catch (Exception $exception) {
            DB::rollBack();
            flash('Echec modification du produit', 'info');
            return redirect()->back();
        }
    }

    public function region()
    {
        $data['region'] = new Region();
        $data['chefLieux'] = $this->chefLieuDistrictRepository->chefLieuDistrict()->get();

        $data['regions'] = $this->regionRepository->region()->with(['chefLieu'])->doesntHave('chefLieu')->get()->sortBy('libelle');
        $data['liste_regions'] = $this->regionRepository->region()->with(['chefLieu'])->get()->sortBy('libelle');
        return view('admin.region')->with($data);
    }

    public function postRegion(Request $request)
    {
        //dd($request->all());
        $validator = validator($request->all(), [
            'region_id' => 'required',
            'chef_lieu_id' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->_redirectWhenFail($validator);
        }
        \DB::beginTransaction();
        try {
            $region_ids = $request->region_id;
            $chef_lieu_id = $request->chef_lieu_id;
            $regions = $this->regionRepository->region()->whereIn('id', $region_ids);
            if ($regions) {
                $regions->update(['chef_lieu_id' => $chef_lieu_id]);
                flash('Region enregistrée', 'success');
                DB::commit();
                return redirect()->route('admin.enregistrement-region');
            }
            flash('Echec enregistrement de la région', 'warning');
            DB::rollBack();
            return redirect()->route('admin.enregistrement-region');
        } catch (Exception $exception) {
            DB::rollBack();
            flash('Echec enregistrement de la region', 'info');
            return redirect()->back();
        }
    }


    public function editRegion($region_id)
    {
        $data['region'] = $this->regionRepository->region()->find($region_id);
        $data['regions'] = $this->regionRepository->region()->get()->sortBy('libelle');
        return view('admin.region')->with($data);
    }

    public function patchRegion(Request $request, $region_id)
    {
        $region = $this->regionRepository->region()->find($region_id);

        $validator = validator($request->all(), [
            'libelle' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->_redirectWhenFail($validator);
        }
        DB::beginTransaction();
        try {
            $d = ['libelle' => $request->get('libelle')];
            if ($region->update($d)) {
                flash('Region enregistrée', 'success');
                DB::commit();
                return redirect()->route('admin.enregistrement-region');
            }
            flash('Echec enregistrement de la région', 'warning');
            DB::rollBack();
            return redirect()->route('admin.enregistrement-region');
        } catch (Exception $exception) {
            DB::rollBack();
            flash('Echec enregistrement de la region', 'info');
            return redirect()->back();
        }
    }

    public function type_vehicule()
    {
        $data['type_vehicule'] = new Region();
        $data['type_vehicules'] = $this->typeVehiculeRepository->typeVehicule()->get()->sortBy('libelle');
        return view('admin.type-vehicule')->with($data);
    }

    public function postTypeVehicule(Request $request)
    {
        //dd($request->all());
        $validator = validator($request->all(), [
            'libelle' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->_redirectWhenFail($validator);
        }
        DB::beginTransaction();
        try {
            $d = ['libelle' => $request->get('libelle')];
            if ($this->typeVehiculeRepository->typeVehicule()->create($d)) {
                flash('Type de vehicule enregistré', 'success');
                DB::commit();
                return redirect()->route('admin.enregistrement-type_vehicule');
            }
            flash('Echec enregistrement type véhicule', 'warning');
            DB::rollBack();
            return redirect()->route('admin.enregistrement-type_vehicule');
        } catch (Exception $exception) {
            DB::rollBack();
            flash('Echec enregistrement du type du vehicule', 'info');
            return redirect()->back();
        }
    }


    public function editTypeVehicule($type_id)
    {
        $data['type_vehicule'] = $this->typeVehiculeRepository->typeVehicule()->find($type_id);
        $data['type_vehicules'] = $this->typeVehiculeRepository->typeVehicule()->get()->sortBy('libelle');
        return view('admin.type-vehicule')->with($data);
    }

    public function patchTypeVehicule(Request $request, $type_id)
    {
        $region = $this->typeVehiculeRepository->typeVehicule()->find($type_id);

        $validator = validator($request->all(), [
            'libelle' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->_redirectWhenFail($validator);
        }
        DB::beginTransaction();
        try {
            $d = ['libelle' => $request->get('libelle')];
            if ($region->update($d)) {
                flash('Region enregistrée', 'success');
                DB::commit();
                return redirect()->route('admin.enregistrement-type_vehicule');
            }
            flash('Echec enregistrement de la région', 'warning');
            DB::rollBack();
            return redirect()->route('admin.enregistrement-type_vehicule');
        } catch (Exception $exception) {
            DB::rollBack();
            flash('Echec enregistrement de la region', 'info');
            return redirect()->back();
        }
    }

    public function client()
    {
        $data['client'] = new Client();
        $data['clients'] = $this->clientRepository->client()->with(['region', 'distributeur', 'destination'])->get();
        $data['regions'] = $this->regionRepository->region()->orderBy('libelle')->get();
        $data['destinations'] = $this->destinationRepository->destination()->get()->sortBy('libelle');
        $data['distributeurs'] = $this->distributeurRepository->distributeur()->whereNotNull('code')->get()->sortBy('nom');
        return view('admin.client')->with($data);
    }

    public function postClient(Request $request)
    {
        // dd($request->all());
        $validator = validator($request->all(), [
            'region_id' => 'required',
            'destination_id' => 'required',
            'distributeur_id' => 'required',
            'nom' => 'required',
            'contacts' => 'required',
        ], [
            'region_id.required' => "Selectionnez la région SVP",
            'distributeur_id.required' => "Selectionnez le distributeur auquel il est rattaché",
            'destination_id.required' => 'Selectionnez la region',
            'nom.required' => 'Precisez le nom du client',
            'contacts.required' => 'Precisez le/les contacts du client',
        ]);

        if ($validator->fails()) {
            return $this->_redirectWhenFail($validator);
        }
        DB::beginTransaction();
        try {
            $d = [
                'nom' => $request->get('nom'),
                'contact' => $request->get('contacts'),
                'region_id' => $request->get('region_id'),
                'destination_id' => $request->get('destination_id'),
                'distributeur_id' => $request->get('distributeur_id'),
            ];
            if ($this->clientRepository->client()->create($d)) {
                flash('Client enregistré', 'success');
                DB::commit();
                return redirect()->route('admin.enregistrement-client');
            }
            flash('Echec enregistrement client', 'warning');
            DB::rollBack();
            return redirect()->route('admin.enregistrement-client');
        } catch (Exception $exception) {
            //dd($exception->getMessage());
            DB::rollBack();
            flash('Echec enregistrement du client', 'warning');
            return redirect()->back();
        }
    }

    public function editClient($client_id)
    {
        $data['client'] = $this->clientRepository->client()->find($client_id);
        $data['clients'] = $this->clientRepository->client()->get();
        $data['regions'] = $this->regionRepository->region()->orderBy('libelle')->get();
        $data['destinations'] = $this->destinationRepository->destination()->get()->sortBy('libelle');
        $data['distributeurs'] = $this->distributeurRepository->distributeur()->whereNotNull('code')->get()->sortBy('nom');
        return view('admin.client')->with($data);
    }

    public function patchClient(Request $request, $client_id)
    {
        $client = $this->clientRepository->client()->where('id', '=', $client_id)->first();
        // dd($client);
        $validator = validator($request->all(), [
            'region_id' => 'required',
            'destination_id' => 'required',
            'distributeur_id' => 'required',
            'nom' => 'required',
            'contacts' => 'required',
        ], [
            'region_id.required' => "Selectionnez la région SVP",
            'distributeur_id.required' => "Selectionnez le distributeur auquel il est rattaché",
            'destination_id.required' => 'Selectionnez la region',
            'nom.required' => 'Precisez le nom du client',
            'contacts.required' => 'Precisez le/les contacts du client',
        ]);

        if ($validator->fails()) {
            return $this->_redirectWhenFail($validator);
        }
        DB::beginTransaction();
        try {
            $d = [
                'nom' => $request->get('nom'),
                'contact' => $request->get('contacts'),
                'region_id' => $request->get('region_id'),
                'destination_id' => $request->get('destination_id'),
                'distributeur_id' => $request->get('distributeur_id'),
            ];
            //  dd($d);
            if ($client->update($d)) {
                flash('Client enregistré', 'success');
                DB::commit();
                return redirect()->route('admin.enregistrement-client');
            }
            flash('Echec enregistrement client', 'warning');
            DB::rollBack();
            return redirect()->route('admin.enregistrement-client');
        } catch (Exception $exception) {
            //dd($exception->getMessage());
            DB::rollBack();
            flash('Echec enregistrement du client', 'warning');
            return redirect()->back();
        }
    }


    public function user()
    {
        $data['user'] = new User();
        $data['users'] = $this->userRepository->user()->get();
        return view('admin.user')->with($data);
    }

    public function postUser(Request $request)
    {
        $validator = validator($request->all(), [

            'name' => 'required',
            'email' => 'required|email|unique:users,email',
        ], [

            'name.required' => "Precisez le nom de l'utilisateur",
            'email.required' => 'Precisez identifianr utilisateur',
            'email.email' => 'Entrez une adresse mail correcte',
            'email.unique' => 'Cet addresse mail semble être déjà utilisés',
        ]);

        if ($validator->fails()) {
            return $this->_redirectWhenFail($validator);
        }
        DB::beginTransaction();
        try {
            $raw_password = "secret";
            $d = [
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($raw_password),

            ];
            if ($this->userRepository->user()->create($d)) {
                flash('Utilisateur enregistré', 'success');
                DB::commit();
                return redirect()->route('admin.enregistrement-user');
            }
            flash('Echec enregistrement utilisateur', 'warning');
            DB::rollBack();
            return redirect()->route('admin.enregistrement-user');
        } catch (Exception $exception) {
            DB::rollBack();
            flash('Echec enregistrement utilisateur', 'warning');
            return redirect()->back();
        }
    }

    public function editUser($user_id)
    {
        $data['user'] = $this->userRepository->user()->find($user_id);
        $data['users'] = $this->userRepository->user()->get();
        return view('admin.user')->with($data);
    }

    public function patchUser(Request $request, $user_id)
    {
        $user = $this->userRepository->user()->find($user_id);
        $validator = validator($request->all(), [
            'name' => 'required',
            'email' => [
                "required", "email",
                Rule::unique('users')->ignore($user->id)
            ],
        ], [

            'name.required' => "Precisez le nom de l'utilisateur",
            'email.required' => 'Precisez identifianr utilisateur',
            'email.email' => 'Entrez une adresse mail correcte',
        ]);

        if ($validator->fails()) {
            return $this->_redirectWhenFail($validator);
        }
        DB::beginTransaction();
        try {
            $d = [
                'name' => $request->name,
                'email' => $request->email,
            ];
            if ($user->update($d)) {
                flash('Utilisateur enregistré', 'success');
                DB::commit();
                return redirect()->route('admin.enregistrement-user');
            }
            flash('Echec enregistrement utilisateur', 'warning');
            DB::rollBack();
            return redirect()->route('admin.enregistrement-user');
        } catch (Exception $exception) {
            //dd($exception->getMessage());
            DB::rollBack();
            flash('Echec enregistrement utilisateur', 'warning');
            return redirect()->back();
        }
    }

    public function roles()
    {
        $data['role'] = new Role();
        $data['roles'] = Role::with(['permissions'])->get();
        return view('admin.roles')->with($data);
    }

    public function roleSave(Request $request)
    {
        $role = ['name' => $request->name, 'guard_name' => $request->guard];
        $validation = validator($request->all(), [
            'name' => 'required|unique:roles,name',
            'guard' => 'required'
        ],
            [
                'name.required' => 'Preciser le role',
                'guard.required' => 'Preciser le Guard',
                'name.unique' => 'Ce role existe déjà'
            ]
        );
        if ($validation->fails()) {
            return $this->_redirectWhenFail($validation);
        }
        Role::create($role);
        return redirect()->route('admin.enregistrement-roles');

    }

    public function roleEdit($role_id)
    {
        $data['role'] = Role::findById($role_id);
        $data['roles'] = Role::with(['permissions'])->get();
        return view('admin.roles')->with($data);
    }

    public function putRole(Request $request, $role_id)
    {
        $role = Role::findById($role_id);
        $roles = ['name' => $request->name, 'guard_name' => $request->guard];
        $validation = validator($request->all(), [
            'name' => ['required', Rule::unique('users')->ignore($role_id)],
            'guard' => 'required'
        ],
            [
                'name.required' => 'Preciser le role',
                'guard.required' => 'Preciser le Guard',
                'name.unique' => 'Ce role existe déjà'
            ]
        );
        if ($validation->fails()) {
            return $this->_redirectWhenFail($validation);
        }
        $role->update($roles);
        return redirect()->route('admin.enregistrement-roles');
    }

    public function permissions()
    {
        $data['permission'] = new Permission();
        $data['permissions'] = Permission::all();
        return view('admin.permissions')->with($data);
    }

    public function savePermission(Request $request)
    {
        //dd($request->all());
        $permissions = ['name' => $request->name, 'guard_name' => $request->guard];
        $validation = validator($request->all(), [
            'name' => 'required|unique:permissions,name',
            'guard' => 'required'
        ],
            [
                'name.required' => 'Preciser le role',
                'guard.required' => 'Preciser le Guard',
                'name.unique' => 'Ce role existe déjà'
            ]
        );
        if ($validation->fails()) {
            return $this->_redirectWhenFail($validation);
        }
        Permission::create($permissions);
        return redirect()->route('admin.enregistrement-permissions');
    }

    public function permissionsEdit(int $permission_id)
    {
        $data ['permission']  =  Permission::where('id','=',$permission_id)->first();
        $data['permissions']  =  Permission::all();
        return view('admin.permissions')->with($data);
    }

    public function permissionsPut(Request $request,int $permission_id)
    {
        $permission = Permission::where('id','=',$permission_id)->first();
        $permissions = ['name' => $request->name, 'guard_name' => $request->guard];
        $validation = validator($request->all(), [
            'name' => ['required',Rule::unique('permissions','name')->ignore($permission->id)],
            'guard' => 'required'
        ],
            [
                'name.required' => 'Preciser la permission ',
                'guard.required' => 'Preciser le Guard',
                'name.unique' => 'Cette permission existe déjà'
            ]
        );
        //dd($request->all(),$validation);
        if ($validation->fails()) {
            return $this->_redirectWhenFail($validation);
        }
        $permission->update($permissions);
        return redirect()->route('admin.enregistrement-permissions');
    }

    public function rolesPermissions($role_id)
    {
        //TODO wip
    }

    public function rolesAttribution()
    {
        $data['utilisateur'] = null;
        $data['roles'] = $this->roleRepository->role()->get();
        $data['permissions'] = $this->permissionRepository->permission()->get();
        $data['users'] = $this->userRepository->user()->with(['roles', 'permissions'])->get();
        // dd($data['users'])
        return view('admin.roles-attributions')->with($data);
    }

    public function rolesAttributionUser($user_id)
    {
        $data['roles'] = $this->roleRepository->role()->get();
        $data['permissions'] = $this->permissionRepository->permission()->get();
        $data['users'] = $this->userRepository->user()->with(['roles', 'permissions'])->get();
        $data['utilisateur'] = $this->userRepository->user()->find($user_id);

        return view('admin.roles-attributions')->with($data);
    }

    public function postRolesAttributionUser(Request $request)
    {
        $user_id = $request->user_id;
        $user = $this->userRepository->user()->find($user_id);
        $user->syncRoles($request->role_id);
        //$user->roles()
        $user->syncPermissions($request->permissions);
        return redirect()->route('admin.user-roles-attibution', $user_id);
    }

    public function distributeur()
    {

        $data['distributeur'] = new Distributeur();
        $data['distributeurs'] = $this->distributeurRepository->distributeur()->get();
        return view('admin.distributeur')->with($data);
    }

    public function postDistributeur(Request $request)
    {
        // dd($request->all());
        $validator = validator($request->all(), [
            'code' => 'required',
            'distributeur_id.*' => 'required',
            'contacts' => 'nullable',
        ], [
            'code.required' => 'Precisez le code du distributeur',
            'nom.required' => 'Precisez le nom du distributeur',
            'contacts.required' => 'Precisez le/les contacts du distributeur',
        ]);

        if ($validator->fails()) {
            return $this->_redirectWhenFail($validator);
        }
        DB::beginTransaction();
        try {
            $d = [
                'code' => $request->code,
                'contacts' => $request->contacts
            ];
            $distibuteurs = $this->distributeurRepository->distributeur()->whereIn('id', $request->distributeur_id);
            if ($distibuteurs) {
                $distibuteurs->update($d);
                flash('Distributeur enregistrée', 'success');
                DB::commit();
            }

//            if ($this->distributeurRepository->distributeur()->create($d)) {
//
//                return redirect()->route('admin.enregistrement-distributeur');
//            }
//            flash('Echec enregistrement du distributeur', 'warning');
//            DB::rollBack();
            return redirect()->route('admin.enregistrement-distributeur');
        } catch (Exception $exception) {
            DB::rollBack();
            flash('Echec enregistrement du distributeur', 'warning');
            return redirect()->back();
        }
    }

    public function editDistributeur($distributeur_id)
    {
        $data['distributeur'] = $this->distributeurRepository->distributeur()->where('id', '=', $distributeur_id)->first();

        $data['distributeurs'] = $this->distributeurRepository->distributeur()->get()->sortBy('nom');
        return view('admin.distributeur')->with($data);
    }


    public function patchDistributeur(Request $request, $distributeur_id)
    {
        $distributeur = $this->distributeurRepository->distributeur()->find($distributeur_id);
        $validator = validator($request->all(), [
            'code' => 'required',
            'distributeur_id.*' => 'required',
            'contacts' => 'required',
            'referent' => 'nullable',
        ], [
                'code.required' => 'Precisez le code du distributeur',
                'nom.required' => 'Precisez le nom du distributeur',
                'contacts.required' => 'Precisez le/les contacts du distributeur',
            ]
        );

        if ($validator->fails()) {
            return $this->_redirectWhenFail($validator);
        }
        \DB::beginTransaction();
        try {
            $d = [
                'code' => $request->code,
                'contacts' => $request->contacts,
                'referent' => $request->referent
            ];
            $distibuteurs = $this->distributeurRepository->distributeur()->whereIn('id', $request->distributeur_id);
            if ($distibuteurs) {
                $distibuteurs->update($d);
                flash('Distributeur enregistrée', 'success');
                DB::commit();
                return redirect()->route('admin.enregistrement-distributeur');
            }
            flash('Echec enregistrement du distributeur', 'warning');
            DB::rollBack();
            return redirect()->route('admin.enregistrement-distributeur');
        } catch (Exception $exception) {
            DB::rollBack();
            flash('Echec enregistrement du distributeur', 'warning');
            return redirect()->back();
        }
    }

    public function transporteur()
    {

        $data['transporteur'] = new Transporteur();
        $data['transporteurs'] = $this->transporteurRepository->transporteur()->get()->sortBy('nom');
        return view('admin.transporteur')->with($data);
    }

    public function postTransporteur(Request $request)
    {
        $validator = validator($request->all(), [
            'code' => 'required',
            'nom' => 'required',
            'contacts' => 'required',
        ], [
            'code.required' => 'Precisez le code du transporteur',
            'nom.required' => 'Precisez le nom du transporteur',
            'contacts.required' => 'Precisez le/les contacts du transporteur',
        ]);

        if ($validator->fails()) {
            return $this->_redirectWhenFail($validator);
        }
        DB::beginTransaction();
        try {
            $d = [
                'code' => $request->get('code'),
                'nom' => $request->get('nom'),
                'contacts' => $request->get('contacts')
            ];
            if ($this->transporteurRepository->transporteur()->create($d)) {
                flash('Transporteur enregistrée', 'success');
                DB::commit();
                return redirect()->route('admin.enregistrement-transporteur');
            }
            flash('Echec enregistrement du transporteur', 'warning');
            DB::rollBack();
            return redirect()->route('admin.enregistrement-transporteur');
        } catch (Exception $exception) {
            DB::rollBack();
            flash('Echec enregistrement du transporteur', 'warning');
            return redirect()->back();
        }
    }

    public function editTransporteur($transporteur_id)
    {
        $data['transporteur'] = $this->transporteurRepository->transporteur()->find($transporteur_id);
        $data['transporteurs'] = $this->transporteurRepository->transporteur()->get()->sortBy('nom');
        return view('admin.transporteur')->with($data);
    }


    public function patchTransporteur(Request $request, $transporteur_id)
    {
        $transporteur = $this->transporteurRepository->transporteur()->find($transporteur_id);
        $validator = validator($request->all(), [
            'code' => 'required',
            'nom' => 'required',
            'contacts' => 'required',
        ], [
                'code.required' => 'Precisez le code du transporteur',
                'nom.required' => 'Precisez le nom du transporteur',
                'contacts.required' => 'Precisez le/les contacts du transporteur',
            ]
        );

        if ($validator->fails()) {
            return $this->_redirectWhenFail($validator);
        }
        DB::beginTransaction();
        try {
            $d = [
                'code' => $request->get('code'),
                'nom' => $request->get('nom'),
                'contacts' => $request->get('contacts')
            ];
            if ($transporteur->update($d)) {
                flash('Transporteur enregistrée', 'success');
                DB::commit();
                return redirect()->route('admin.enregistrement-transporteur');
            }
            flash('Echec enregistrement du transporteur', 'warning');
            DB::rollBack();
            return redirect()->route('admin.enregistrement-transporteur');
        } catch (Exception $exception) {
            DB::rollBack();
            flash('Echec enregistrement du transporteur', 'warning');
            return redirect()->back();
        }
    }

    public function destination()
    {
        $data['destination'] = new Region();
        $destination = $this->destinationRepository->destination()->with('region');
        $localites = $this->destinationRepository->destination();
        $data['regions'] = $this->regionRepository->region()->orderBy('libelle');
        $data['localites'] = $localites;
        $data['localites_libres'] = $localites->doesntHave('region')->get();
        $data['destinations'] = $destination->get()->sortBy('libelle');
        return view('admin.localite')->with($data);
    }

    public function postDestination(Request $request)
    {
        $validator = validator($request->all(), [
            'localite_id.*' => 'required|integer',
            'region_id' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->_redirectWhenFail($validator);
        }
        DB::beginTransaction();
        try {
            $d = ['region_id' => $request->region_id];
            if ($destination = $this->destinationRepository->destination()->whereIn('id', $request->localite_id)) {
                $destination->update($d);
                flash('Localité enregistrée', 'success');
                DB::commit();
                return redirect()->route('admin.enregistrement-destination');
            }
            flash('Echec enregistrement de la localite', 'warning');
            DB::rollBack();
            return redirect()->route('admin.enregistrement-destination');
        } catch (Exception $exception) {
            DB::rollBack();
            flash('Echec enregistrement de la region', 'info');
            return redirect()->back();
        }
    }


    public function editDestination($destination_id)
    {
        $destination = $this->destinationRepository->destination()->with('region');
        $localites = $this->destinationRepository->destination()->get();

        $data['regions'] = $this->regionRepository->region()->orderBy('libelle');
        $data['localites_libres'] = $localites;
        $data['localites'] = $localites;

        $data['destinations'] = $destination->get()->sortBy('libelle');
        $data['destination'] = $destination->find($destination_id);
        return view('admin.localite')->with($data);
    }

    public function patchDestination(Request $request, $destination_id)
    {
        $destination = $this->destinationRepository->destination()->find($destination_id);
        $validator = validator($request->all(), [
            'localite_id.*' => 'required|integer',
            'region_id' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->_redirectWhenFail($validator);
        }
        DB::beginTransaction();
        try {
            $d = ['region_id' => $request->region_id];
            if ($destination->update($d)) {
                flash('Localité enregistrée', 'success');
                DB::commit();
                return redirect()->route('admin.enregistrement-destination');
            }
            flash('Echec enregistrement de la localité', 'warning');
            DB::rollBack();
            return redirect()->route('admin.enregistrement-destination');
        } catch (Exception $exception) {
            DB::rollBack();
            flash('Echec enregistrement de la localite', 'info');
            return redirect()->back();
        }
    }

    public function couts()
    {
        $data['produits'] = $this->produitRepository->produit();
        $data['couts'] = $this->coutRepository->cout()->with('produit')->get();
        $data['cout'] = new Cout();
        return view('admin.couts')->with($data);
    }

    public function postCoutsCiments(Request $request)
    {
        $validator = validator($request->all(), [
            'prix_usine' => 'required',
            'prix_distributeur' => 'required',
            'date_debut' => 'required',
            'date_fin' => 'required',
        ], [
            'prix_usine.required' => 'Veuillez preciser le Prix Usine',
            'prix_distributeur.required' => 'Veuillez preciser Prix Distributeur',
            'date_debut.required' => "Veuillez preciser d'entrée en vigueur",
            // 'date_fin.required'=>'Veuillez preciser ',
        ]);

        if ($validator->fails()) {
            $this->_redirectWhenFail($validator);
        }
        $d = [
            'prix_usine' => $request->get('prix_usine'),
            'prix_distributeur' => $request->get('prix_distributeur'),
            'date_debut' => $request->get('date_debut'),
            'date_fin' => $request->get('date_fin'),
            'produit_id' => $request->get('produit_id'),
        ];
        if ($this->coutRepository->cout()->create($d)) {
            flash("Enregistrement effectué avec succes");
            return redirect()->back();
        }
        flash("Désolé, echec enregistrement ", "warning");
        return redirect()->back()->withInput();
    }

    public function getBudgetAnnuel()
    {
        $data['budget'] = new Budget();
        $data['produits'] = $this->produitRepository->produit();
        $data['months'] = $this->monthRepository->month();
        $data['years'] = Annee::where('current', '=', true)->first();
        $data['budgets'] = $this->budgetRepository->budget()->with(['produit', 'month', 'annee' =>
            function ($q) {
                return $q->where('current', '=', true);
            }
        ])->get();
        return view('admin.budget')->with($data);
    }

    public function postBudgetAnnuel(Request $request)
    {
        $validator = validator($request->all(), [
            'produit_id' => 'required',
            'month_id' => 'required',
            'annee_id' => 'required',
            'officiel' => 'required',
            'challenge' => 'nullable',
        ], [
            'produit_id.required' => 'Veuillez preciser la qualité du ciment',
            'month_id.required' => 'Veuillez selectionner le mois',
            'annee_id.required' => "Veuillez preciser l'année",
            'officiel.required' => "Veuillez le budget",
            // 'date_fin.required'=>'Veuillez preciser ',
        ]);

        if ($validator->fails()) {
            return $this->_redirectWhenFail($validator);
        }
        $d = [
            'produit_id' => $request->produit_id,
            'month_id' => $request->month_id,
            'annee_id' => $request->annee_id,
            'officiel' => $request->officiel,
            'challenge' => $request->challenge,
        ];
        if ($this->budgetRepository->budget()->create($d)) {
            flash("Enregistrement effectué avec succes");
            return redirect()->back();
        }
        flash("Désolé, echec enregistrement ", "warning");
        return redirect()->back()->withInput();
    }

    public function district()
    {
        $data['district'] = new District();
        $data['districts'] = $this->districtRepository->district()->get();
        return view('admin.district')->with($data);
    }

    public function postDistrict(Request $request)
    {
        $validator = validator($request->all(), [
            'libelle' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->_redirectWhenFail($validator);
        }
        \DB::beginTransaction();
        try {
            $d = ['libelle' => $request->libelle];
            if ($this->districtRepository->district()->create($d)) {
                flash('District enregistré', 'success');
                DB::commit();
                return redirect()->route('admin.enregistrement-district');
            }
            flash('Echec enregistrement du district', 'warning');
            DB::rollBack();
            return redirect()->route('admin.enregistrement-district');
        } catch (Exception $exception) {
            \DB::rollBack();
            flash('Echec enregistrement du district', 'info');
            return redirect()->back();
        }
    }


    public function editDistrict($district_id)
    {
        $data['district'] = $this->districtRepository->district()->find($district_id);
        $data['districts'] = $this->districtRepository->district()->get();
        return view('admin.district')->with($data);
    }

    public function patchDistrict(Request $request, $district_id)
    {
        $district = $this->districtRepository->district()->find($district_id);

        $validator = validator($request->all(), [
            'libelle' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->_redirectWhenFail($validator);
        }
        \DB::beginTransaction();
        try {
            $d = ['libelle' => $request->libelle];
            if ($district->update($d)) {
                flash('District modifié', 'success');
                DB::commit();
                return redirect()->route('admin.enregistrement-district');
            }
            flash('Echec modification district', 'warning');
            \DB::rollBack();
            return redirect()->route('admin.enregistrement-district');
        } catch (Exception $exception) {
            \DB::rollBack();
            flash('Echec enregistrement district', 'info');
            return redirect()->back();
        }
    }

    public function chefLieuDistrict()
    {
        $data['chefLieu'] = new ChefLieuDistrict();
        $data['districts'] = $this->districtRepository->district()->get();
        $data['chefLieux'] = $this->chefLieuDistrictRepository->chefLieuDistrict()->get()->load(['district']);

        return view('admin.chef-lieu-district')->with($data);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws Exception
     */
    public function postChefLieuDistrict(Request $request): RedirectResponse
    {
        $validator = validator($request->all(), [
            'libelle' => 'required',
            'district_id' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->_redirectWhenFail($validator);
        }
        \DB::beginTransaction();
        try {
            $d = [
                'libelle' => $request->libelle,
                'district_id' => $request->district_id,
            ];
            if ($this->chefLieuDistrictRepository->chefLieuDistrict()->create($d)) {
                flash('Données enregistrées', 'success');
                \DB::commit();
                return redirect()->route('admin.enregistrement-chef-lieu-district');
            }
            flash('Echec enregistrement ', 'warning');
            \DB::rollBack();
            return redirect()->route('admin.enregistrement-chef-lieu-district');
        } catch (Exception $exception) {
            \DB::rollBack();
            flash('Echec enregistrement', 'info');
            return redirect()->back();
        }
    }

    public function editChefLieuDistrict(int $chef_lieu_id)
    {
        $data['chefLieu'] = $this->chefLieuDistrictRepository->chefLieuDistrict()->find($chef_lieu_id);
        $data['districts'] = $this->districtRepository->district()->get();
        $data['chefLieux'] = $this->chefLieuDistrictRepository->chefLieuDistrict()->get();
        return view('admin.chef-lieu-district')->with($data);
    }

    public function putChefLieuDistrict(Request $request, int $chef_lieu_id)
    {
        $chefLieu = $this->chefLieuDistrictRepository->chefLieuDistrict()->find($chef_lieu_id);
        $validator = validator($request->all(), [
            'libelle' => 'required',
            'district_id' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->_redirectWhenFail($validator);
        }
        \DB::beginTransaction();
        try {
            $d = [
                'libelle' => $request->libelle,
                'district_id' => $request->district_id,
            ];
            if ($chefLieu->update($d)) {
                flash('Données enregistrées', 'success');
                \DB::commit();
                return redirect()->route('admin.enregistrement-chef-lieu-district');
            }
            flash('Echec enregistrement ', 'warning');
            \DB::rollBack();
            return redirect()->route('admin.enregistrement-chef-lieu-district');
        } catch (Exception $exception) {
            \DB::rollBack();
            flash('Echec enregistrement', 'info');
            return redirect()->back();
        }
    }
}
////laraveldaily/laravel-charts
