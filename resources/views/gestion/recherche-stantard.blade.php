@extends('template.theme')


@section('font-title')

@endsection
@push('scripts')



@endpush


@section('content-body')

    <div class="row">
        <div class="col-md-6 offset-md-3">

            {!! Form::open(['url'=>route('gestion.recherche'),'class'=>'form-horizontal']) !!}

            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title"> Recherche standard </h2>
                    <div class="card-subtitle">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                </header>
                <div class="card-body">
                    <div class="row form-group">
                        <div class="col-12">
                            <div class="form-group">
                                <label class="col-form-label" for="date">Date du jour</label>
                                <div class="input-group">
														<span class="input-group-prepend">
															<span class="input-group-text">
																<i class="fas fa-calendar-alt"></i>
															</span>
														</span>
                                    <input type="text" name="date" value="{{ old('date',request('date')) }}" id="date"
                                           data-plugin-datepicker class="form-control" autocomplete="off" required
                                           readonly
                                           data-plugin-options='{"orientation":"bottom","autoclose":true,"language":"en","todayHighlight":true,"format":"yyyy-mm-dd"}'>
                                </div>
                                @error('date')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <footer class="card-footer">
                    <div class="row justify-content-end">
                        <div class="col-sm-9">
                            <button type="submit" class="btn btn-primary"><i class="fas fa-search-minus"></i> Rechercher
                            </button>

                        </div>
                    </div>
                </footer>
            </section>
            {!! Form::close() !!}

        </div>


    </div>

    @if(isset($entrees) && !empty($entrees))
        <div class="row">
            <div class="col">
                <section class="card">
                    <header class="card-header">
                        <div class="card-actions">
                            <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                            <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                        </div>

                        <h2 class="card-title">Point des ventes du {{ date('d/m/Y', strtotime(request('date'))) }}</h2>
                    </header>
                    <div class="card-body">
                        <table class="table table-bordered table-light table-striped mb-0 datatable-default">
                            <thead>
                            <tr class="small">
                                <th>#</th>
                                <th>N° BL</th>
                                <th>Distributeur</th>
                                <th>Transporteur</th>
                                <th>Type Transport</th>
                                <th>Immat.</th>
                                <th>Région</th>
                                <th>Destination</th>
                                <th>Produit</th>
                                <th>Qte <em>(T)</em></th>
                                <th>Type Subvention)</th>
                                <th>Prix de base</th>
                                <th>Prix de negocié</th>
                                <th>Montant Supporté</th>
                                <th>N° Bon de Caisse</th>
                                <th>Détails</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($entrees as $entree)
                                <tr class="text-center small @if( $entree->subvention &&  $entree->subvention->statut ==true ) success @endif">
                                    <td>{{ $loop->index + 1  }}</td>
                                    <td>{{ $entree->numero_bl }}</td>
                                    <td>{{ $entree->distributeur->nom }}</td>
                                    <td>{{ $entree->transporteur->nom }}</td>
                                    <td>{{ $entree->vehicule->typeVehicule->libelle ?? null }}</td>
                                    <td>{{ $entree->vehicule->immatriculation ?? null }}</td>
                                    <td>{{ $entree->destination->region->libelle ?? null}}</td>
                                    <td>{{ $entree->destination->libelle }}</td>
                                    <td>{{ $entree->produit->libelle }}</td>
                                    <td>{{ $entree->quantite }}</td>
                                    <td>{{ $entree->subvention->typeSubvention->libelle ?? null }}</td>
                                    <td>{{ $entree->subvention->prix_base ?? null }}</td>
                                    <td>{{ $entree->subvention->prix_negocie ?? null }}</td>
                                    <td>{{ $entree->subvention->montant_supporte ?? null }}</td>
                                    <td>{{ $entree->subvention->numero_bon_caisse ?? null }}</td>
                                    <td class="center">
                                        <a href="{{ route('gestion.edition-entree',$entree) }}"
                                          target="_top" class="on-default edit-row"><i
                                                class="fas fa-eye"></i></a>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </section>
            </div>
        </div>
    @endif

@endsection
