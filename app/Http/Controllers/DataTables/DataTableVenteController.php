<?php

namespace App\Http\Controllers\DataTables;

use App\Traits\CanUsedRoles;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Auth\AuthManager;
use App\Http\Controllers\Controller;
use App\Repositories\PeseeRepository;
use Illuminate\Database\Eloquent\Collection;
use Yajra\DataTables\Facades\DataTables;

class DataTableVenteController extends Controller
{
    use CanUsedRoles;

    /**
     * @var PeseeRepository
     */
    private $peseeRepository;

    /**
     *@var AuthManager
     */
    private $authManager;

    /**
     * VenteController constructor.
     * @param PeseeRepository $peseeRepository
     * @param AuthManager $authManager
     */
    public function __construct(PeseeRepository $peseeRepository,AuthManager $authManager)
    {

        $this->authManager = $authManager;
       // $this->middleware('auth');
        $this->peseeRepository = $peseeRepository;
    }

    private function _getVentesQuery(){
        return $this->peseeRepository->pesee()->select(
            "pesees.id",
            "pesees.numero_bl",
            "pesees.qte_bl AS quantite_bl",
            "pesees.poids_entree",
            "pesees.poids_sortie",
            "pesees.qte_pesee as quantite_pesee",
            "pesees.heure_pesee_entree",
            "pesees.heure_pesee_sortie",
            "pesees.date_entree",
            "pesees.date_sortie",
            "distributeurs.nom AS distributeur",
            "transporteurs.nom as transporteur",
            "produits.libelle AS ciment",
            "vehicules.immatriculation AS immatriculation",
            "destinations.libelle AS destination"
        )
            ->join('produits', 'produits.id', '=', 'pesees.produit_id')
            ->leftJoin('distributeurs', 'distributeurs.id', '=', 'pesees.distributeur_id')
            ->leftJoin('transporteurs', 'transporteurs.id', '=', 'pesees.transporteur_id')
            ->leftJoin('vehicules', 'pesees.id', '=', 'vehicules.pesee_id')
            ->leftJoin('destinations', 'destinations.id', '=', 'pesees.destination_id')

            ;

    }

    private function _venteDataTableColumn($ventes)
    {

        return DataTables::collection($ventes)
            ->addColumn('id', function ($q) {
                return $q->id;
            })
            ->addColumn('numero_bl', function ($q) {
                return $q->numero_bl;
            })
            ->addColumn('distributeur', function ($q) {
                return "{$q->distributeur}";
            })->addColumn('transporteur', function ($q) {
                return "{$q->transporteur}";
            })
            ->addColumn('immatriculation', function ($q) {
                return "{$q->immatriculation}";
            })
            ->addColumn('ciment', function ($q) {
                return "{$q->ciment}";
            })
            ->addColumn('quantite_bl', function ($q) {
                return "{$q->quantite_bl}";
            })
            ->addColumn('quantite_pesee', function ($q) {
                return "{$q->quantite_pesee}";
            })
            ->addColumn('date_entree', function ($q) {
                return "{$q->date_entree->format('d/m/Y')}";
            })
            ->addColumn('heure_entree', function ($q) {
                return "{$q->heure_pesee_entree}";
            })
            ->addColumn('date_sortie', function ($q) {
                return $q->date_sortie->format('d/m/Y');
            })->addColumn('heure_sortie', function ($q) {
                return $q->heure_pesee_sortie;
            })

            ->addColumn('destination', function ($q) {
                return $q->destination;
            })

            ->editColumn('id', function ($q) {
                return $q->id;
            });
    }





    private function _makeDataTable(Collection $collection)
    {
        /**
         * @var AuthManager
         */
        $user =  $this->authManager->user();

        return $this->_venteDataTableColumn($collection)
            ->addColumn('action', function ($q) use($user) {
                $route = route("exploitation.edition-pesee", $q->id);
                if ($user->hasanypermission('Edition données commerciales')):
                    return '<a href="' . $route . '" target="_blank" class="btn btn-xs btn-info"><i class="fas fa-edit"></i></a>';
                endif;
//                $this->userCanUsePermissions($user,$route,['Edition données commerciales']);
            })
            ->rawColumns(['action',])
            ->escapeColumns(['action',])
            ->make(true);
    }
    public function venteJournaliere(Request $request)
    {
       
        $date = date('Y-m-d', $request->datetime);
        $zone_id = $request->zone_id;
        $query = $this->_getVentesQuery()
            ->where('date_pesee','=',$date);
            if(isset($zone_id) && !empty($zone_id)){
                $query = $query
                    ->join('regions', 'destinations.region_id', '=', 'regions.id')
                    ->join('chef_lieu_districts', 'regions.chef_lieu_id', '=', 'chef_lieu_districts.id')
                    ->whereIn('chef_lieu_districts.id',$zone_id);
            }
         $ventes =$query->get();
        return $this->_makeDataTable($ventes);
    }
    public function ventePeriodique(Request $request)
    {
        $debut = date('Y-m-d', $request->debut);
        $fin = date('Y-m-d', $request->fin);
        $zone_id = $request->zone_id;
        $query = $this->_getVentesQuery()
            ->whereBetween('date_pesee',[$debut,$fin]);
            if(isset($zone_id) && !empty($zone_id)){
                $query = $query
                    ->join('regions', 'destinations.region_id', '=', 'regions.id')
                    ->join('chef_lieu_districts', 'regions.chef_lieu_id', '=', 'chef_lieu_districts.id')
                    ->whereIn('chef_lieu_districts.id',$zone_id);
            }
         $ventes =$query->get();
        return $this->_makeDataTable($ventes);
    }
    public function venteMensuelle(Request $request)
    {
        $debut = date('Y-m-d', $request->debut);
        $fin = date('Y-m-d', $request->fin);
        $zone_id = $request->zone_id;
        $query = $this->_getVentesQuery()
            ->whereBetween('date_pesee',[$debut,$fin]);
            if(isset($zone_id) && !empty($zone_id)){
                $query = $query
                    ->join('regions', 'destinations.region_id', '=', 'regions.id')
                    ->join('chef_lieu_districts', 'regions.chef_lieu_id', '=', 'chef_lieu_districts.id')
                    ->whereIn('chef_lieu_districts.id',$zone_id);
            }
         $ventes =$query->get();
        return $this->_makeDataTable($ventes);
    }
    public function venteAnnuelle(Request $request)
    {
       
        $debut = date('Y-m-d', $request->debut);
        $fin = date('Y-m-d', $request->fin);
        $zone_id = $request->zone_id;
        $query  = $this->_getVentesQuery()
            ->whereBetween('date_pesee',[$debut,$fin]);
            if(isset($zone_id) && !empty($zone_id)){
                $query = $query
                    ->join('regions', 'destinations.region_id', '=', 'regions.id')
                    ->join('chef_lieu_districts', 'regions.chef_lieu_id', '=', 'chef_lieu_districts.id')
                    ->whereIn('chef_lieu_districts.id',$zone_id);
            }
         $ventes =$query->get();
        return $this->_makeDataTable($ventes);
    }
}
