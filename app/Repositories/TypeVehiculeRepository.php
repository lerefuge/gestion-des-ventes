<?php


namespace App\Repositories;


use App\TypeVehicule;
use App\Vehicule;

class TypeVehiculeRepository
{


    /**
     * @var TypeVehicule
     */
    private $type_vehicule;

    /**
     * TypeVehiculeRepository constructor.
     * @param TypeVehicule $typeVehicule
     */
    public function __construct(TypeVehicule $typeVehicule)
    {

        $this->type_vehicule = $typeVehicule;
    }

    public function typeVehicule()
    {
        return $this->type_vehicule->newQuery();
    }
}
