<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entite extends Model
{
    protected $guarded=['id'];
    public $timestamps = false;
}
