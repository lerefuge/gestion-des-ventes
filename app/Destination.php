<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Destination extends Model
{
    protected $guarded =['id'];


    public function setLibelleAttribute($value)
    {
        return $this->attributes['libelle'] = STR::upper($value);
    }

    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    public function pesees()
    {
       return $this->hasMany(Pesee::class);
    }

    public function sommeExpedie()
    {
       return $this->sum('qte_lb');
    }
}
