@extends('template.theme')


@section('font-title')

@endsection
@push('scripts')

    <script type="text/javascript">
        $(function () {
            $('.selectPicker').select2({
                title: "Liste d'élément ...",
                header: 'Selectionnez un élément dans la liste',
                liveSearch: true,
                width: 'resolve'
            });
        });

    </script>

@endpush


@section('content-body')

    <div class="row">
        <div class="col-md-5">
            @if($cout->id)
            {!! Form::open(['url'=>route('admin.enregistrement-destination-edit',$cout->id),'class'=>'form-horizontal','method'=>'PUT']) !!}
                @method('put')
            @else
            {!! Form::open(['url'=>route('admin.enregistrement-couts'),'class'=>'form-horizontal']) !!}
            @endif
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title"> Coûts du ciment </h2>
                    <p class="card-subtitle hide-text">
                        Basic validation will display a label with the error after the form control.
                    </p>
                </header>
                <div class="card-body">

                    <div class="form-group row">
                        <label class="col-sm-3 control-label text-sm-right pt-2"  for="region_id"> Ciment <span
                                class="required">*</span></label>
                        <div class="col-sm-9">
                            {!! Form::select('produit_id',$produits->pluck('libelle','id'),$cout->produit_id,['class'=>'form-control selectPicker', 'required'=>true,'id'=>'','placeholder'=>'Selectionnez une qualite de Ciment']) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 control-label text-lg-right pt-2">Periode</label>
                        <div class="col-lg-9">
                            <div class="input-daterange input-group" data-plugin-datepicker data-plugin-options='{"orientation":"bottom","autoclose":true,"format":"yyyy-mm-dd" }'>
														<span class="input-group-prepend">
															<span class="input-group-text">
																<i class="fas fa-calendar-alt"></i>
															</span>
														</span>
                                <input type="text" class="form-control" readonly="readonly" name="date_debut"  value="{{ old('date_debut',request('date_debut')) }}">
                                <span class="input-group-text border-left-0 border-right-0 rounded-0">
															au
														</span>
                                <input type="text" class="form-control" readonly="readonly" name="date_fin" value="{{ old('date_fin',request('date_fin')) }}">
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col ">
                            <div class="form-group">
                                <label for="prix_usine">Prix usine</label>
                                {!!  Form::number('prix_usine',$cout->prix_usine,['min'=>0,'class'=>'form-control','id'=>'prix_usine']) !!}
                                @error('prix_usine')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col ">
                            <div class="form-group">
                                <label for="prix_distributeur">Prix distributeur</label>
                                {!!  Form::number('prix_distributeur',$cout->prix_distributeur,['min'=>0,'class'=>'form-control','id'=>'prix_distributeur']) !!}
                                @error('prix_distributeur')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>

                </div>
                <footer class="card-footer">
                    <div class="row justify-content-end">
                        <div class="col-sm-9">

                            @if($cout->id)
                                <button type="submit" class="btn btn-primary" data-toggle="confirmation">Mettre à jour</button>
                            @else
                                <button type="submit" class="btn btn-primary" data-toggle="confirmation">Enregistrer</button>
                                <button type="reset" class="btn btn-default">Effacer</button>
                            @endif
                        </div>
                    </div>
                </footer>
            </section>
            {!! Form::close() !!}

        </div>
        <div class="col-md-7">
            <div class="row">
                <div class="col">
                    <section class="card">
                        <header class="card-header">
                            <div class="card-actions">
                                <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                            </div>

                            <h2 class="card-title">Coût ciment à la tonne</h2>
                        </header>
                        <div class="card-body">
                            <table class="table table-bordered table-striped mb-0 datatable-default">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Qualité</th>
                                    <th>Prix Usine</th>
                                    <th>Prix Distributeur</th>
                                    <th>Période</th>
                                    <th class="">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($couts as $cout)
                                    <tr>
                                        <td>{{ $loop->index + 1 }}</td>
                                        <td>{{ $cout->produit->libelle }}</td>
                                        <td>{{ $cout->prix_usine}}</td>
                                        <td>{{ $cout->prix_distributeur}}</td>
                                        <td class="small">Du {{ $cout->date_debut->format('d-m-Y')}} Au {{ $cout->date_fin->format('d-m-Y')}}</td>
                                        <td class="flex text-center">
                                            <a href="{{ route('admin.enregistrement-destination-edit',$cout) }}" class="on-default edit-row" data-toggle="confirmation"><i
                                                    class="fas fa-pencil-alt"></i></a>
                                            <a href="#" class="on-default remove-row"><i
                                                    class="far fa-trash-alt"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>





@endsection
