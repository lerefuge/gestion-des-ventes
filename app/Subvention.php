<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subvention extends Model
{
    use SoftDeletes;
    protected $guarded = ['id'];

    public function pesee()
    {
        return $this->belongsTo(Pesee::class);
    }

    public function typeSubvention()
    {
        return $this->belongsTo(TypeSubvention::class);
    }
}
