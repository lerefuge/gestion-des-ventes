<?php

use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            ['name' => 'create','guard_name'=>'web'],
            ['name' => 'read','guard_name'=>'web'],
            ['name' => 'update','guard_name'=>'web'],
            ['name' => 'delete','guard_name'=>'web'],

        ];
        DB::table('permissions')->insert($permissions);
    }
}
