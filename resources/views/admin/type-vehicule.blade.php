@extends('template.theme')


@section('font-title')

@endsection
@push('scripts')



@endpush


@section('content-body')

    <div class="row">
        <div class="col-md-5">
            @if($type_vehicule->id)
            {!! Form::open(['url'=>route('admin.enregistrement-type_vehicule-edit',$type_vehicule->id),'class'=>'form-horizontal','method'=>'PUT']) !!}
                @method('put')
            @else
            {!! Form::open(['url'=>route('admin.enregistrement-type_vehicule'),'class'=>'form-horizontal']) !!}
            @endif
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title"> Enregistrement des type de vehicules </h2>
{{--                    <p class="card-subtitle hide-text">--}}
{{--                        Basic validation will display a label with the error after the form control.--}}
{{--                    </p>--}}
                </header>
                <div class="card-body">
                    <div class="form-group row">
                        <label class="col-sm-4 control-label text-sm-right pt-2"> Type du vehicule  <span
                                class="required">*</span></label>
                        <div class="col-sm-8">
                            {!! Form::text('libelle',$type_vehicule->libelle,['class'=>'form-control', 'required'=>true, 'autocomplete'=>'off','id'=>'','title'=>"Enregistrer un type de vehicule",'placeholder'=>"Enregistrer un type de vehicule"]) !!}
                        </div>
                    </div>

                </div>
                <footer class="card-footer">
                    <div class="row justify-content-end">
                        <div class="col-sm-9">
                            @if($type_vehicule->id)
                                <button type="submit" class="btn btn-primary" data-toggle="confirmation">Mettre à jour</button>
                            @else
                                <button type="submit" class="btn btn-primary" data-toggle="confirmation">Enregistrer</button>
                                <button type="reset" class="btn btn-default">Effacer</button>
                            @endif
                        </div>
                    </div>
                </footer>
            </section>
            {!! Form::close() !!}

        </div>
        <div class="col-md-7">
            <div class="row">
                <div class="col">
                    <section class="card">
                        <header class="card-header">
                            <div class="card-actions">
                                <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                            </div>

                            <h2 class="card-title">Type du vehicule s enregistrées</h2>
                        </header>
                        <div class="card-body">
                            <table class="table table-bordered table-striped mb-0 datatable-default">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Type du vehicule </th>

                                    <th class="">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($type_vehicules as $type_vehicule)
                                    <tr>
                                        <td>{{ $loop->index + 1 }}</td>
                                        <td>{{ $type_vehicule->libelle }}</td>
                                        <td class="flex text-center">
                                            <a href="{{ route('admin.enregistrement-type_vehicule-edit',$type_vehicule) }}" class="on-default edit-row" data-toggle="confirmation"><i
                                                    class="fas fa-pencil-alt"></i></a>
                                            <a href="#" class="on-default remove-row"><i
                                                    class="far fa-trash-alt"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>





@endsection
