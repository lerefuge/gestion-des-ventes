<?php

use Illuminate\Database\Seeder;

class MonthTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $months = [
            ['libelle' => 'Janvier'],
            ['libelle' => 'Février'],
            ['libelle' => 'Mars'],
            ['libelle' => 'Avril'],
            ['libelle' => 'Mai'],
            ['libelle' => 'Juin'],
            ['libelle' => 'Juillet'],
            ['libelle' => 'Août'],
            ['libelle' => 'Septembre'],
            ['libelle' => 'Octobre'],
            ['libelle' => 'Novembre'],
            ['libelle' => 'Décembre'],


        ];
        DB::table('months')->insert($months);
    }
}
