@extends('template.theme')


@section('font-title')

@endsection
@push('scripts')



@endpush
@section('page-header')
    <header class="page-header">
        <h2>{{ $page_header_title ?? '' }}</h2>
    </header>
@endsection

@section('content-body')

    <div class="row">
        <div class="col-lg-5 col-xl-12  mb-4 mb-xl-0">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {!! Form::open(['url'=>route('user.profile')]) !!}
            <section class="card">
                <div class="card-body">
                    <div class="thumb-info mb-3">
                        <img src="img/!logged-user.jpg" class="rounded img-fluid" alt="John Doe">
                        <div class="thumb-info-title">
                            <span class="thumb-info-inner">M. {{ auth()->user()->name }}</span>
                            <span class="thumb-info-type">{{ auth()->user()->roles->implode('name',', ') }}</span>
                        </div>
                    </div>

                    <div class="widget-toggle-expand mb-3">
                        <div class="form-group mb-3">

                            <label for="name" class="control-label">Nom  : </label>
                            {!! Form::text('name',auth()->user()->name,
                            ['id'=>'name','class'=>'form-control form-control-lg','required'=>'required','readonly'=>true,'placeholder'=>'Nom utilisateur']) !!}
                            @error('name')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group mb-3">

                            <label for="email" class="control-label">Identifiant  : </label>
                            {!! Form::email('email',auth()->user()->email,
                            ['id'=>'email','class'=>'form-control form-control-lg','readonly'=>true,'disabled'=>'true','placeholder'=>"Identifiant utilisateur "]) !!}
                            @error('email')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group mb-3">
                            <label for="password" class="control-label">Mot de passe  : </label>
                            {!! Form::password('password',
                            ['id'=>'password','class'=>'form-control form-control-lg','placeholder'=>"Votre mot de passe "]) !!}
                            @error('password','profile')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group mb-3">
                            <label for="password_confirmation" class="control-label">Mot de passe  : </label>
                            {!! Form::password('password_confirmation',
                            ['id'=>'password_confirmation','class'=>'form-control form-control-lg','placeholder'=>"Saisissez à nouveau votre mot de passe "]) !!}
                            @error('password_confirmation','profile')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                    </div>


                    <hr class="dotted short">

                    <div class="social-icons-list">
                        <button type="submit" class="btn btn-primary  mt-2" data-toggle="confirmation">
                            Mettre a jour
                        </button>
                         </div>

                </div>
            </section>
        </div>

    </div>
@endsection
