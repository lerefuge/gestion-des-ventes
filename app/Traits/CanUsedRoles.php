<?php


namespace App\Traits;


use App\User;
use Illuminate\Auth\AuthManager;

trait CanUsedRoles
{

    /**
     * @var AuthManager
     */
    private $authManager;

    public function __construct(AuthManager $authManager)
    {
        $this->authManager = $authManager;
    }


    private function userCanUsePermissions(User $user, $route, $permission)
    {
        if ($user->hasanypermission($permission)):
            return '<a href="' . $route . '" target="_blank" class="btn btn-xs btn-info"><i class="fas fa-edit"></i></a>';
        endif;
    }
}
