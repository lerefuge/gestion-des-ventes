<div class="row">
    <div class="col">
        <section class="card">
            <header class="card-header">
                <div class="card-actions">
                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                    <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                </div>

                <h2 class="card-title">Points Ventes

                    @if($case =='daily')
                        du {{ date('d/m/Y', strtotime(request('date_jour'))) }}
                    @elseif ($case=='period')
                        du {{ date('d/m/Y', strtotime(request('start'))) }}  au {{ date('d/m/Y', strtotime(request('end'))) }}
                    @elseif ($case=='monthly')
                        d {{ Carbon\Carbon::parse(date('d/m/Y', strtotime(request('date_month'))))->format('MMMM') }}
                    @endif
                    <sup class="text-danger">*</sup></h2>
                <p class="card-subtitle text-danger">
                    NB: Sous réserve, des entrées comptabilisées
                </p>
            </header>
            <div class="card-body">
                <table class="table table-bordered table-condensed table-striped mb-0 datatable-default">
                    <thead>
                    <tr class="small">
                        <th>#</th>
                        <th>N° BL</th>
                        <th>Distributeur</th>
                        <th>Transporteur</th>
                        <th>Immat. Vehicule</th>
                        <th>Ciment</th>
                        <th>Quantité BL</th>
                        <th>Quantité Pesee</th>
                        <th>Date Entree</th>
                        <th>Heure Entree</th>
                        <th>Date Sortie</th>
                        <th>Heure Sortie</th>
                        <th>Destination</th>
                        <th></th>


                    </tr>
                    </thead>
                    <tbody>
                    @php
                        $total_tonnage = $total_prix_base = $total_prix_negocie = $total_montant_supporte = $total_montant_supporte_cimaf = 0                       @endphp
                    @foreach($results['ventes'] as $ventes)
                        <tr class="text-center small ">
                            <td>{{ $loop->index + 1  }}</td>
                            <td>{{ $ventes->numero_bl }}</td>
                            <td>{{ $ventes->distributeur }}</td>
                            <td>{{ $ventes->transporteur }}</td>
                            <td>{{ $ventes->immatriculation }}</td>
                            <td>{{ $ventes->ciment }}</td>
                            <td>{{ $ventes->qte_bl }}</td>
                            <td>{{ $ventes->qte_pesee }}</td>
                            <td>{{ $ventes->date_entree->format('d/m/Y') }} </td>
                            <td>{{ $ventes->heure_pesee_entree}}</td>
                            <td>{{ $ventes->date_sortie->format('d/m/Y') }} </td>
                            <td>{{ $ventes->heure_pesee_sortie }}</td>
                            <td>{{ $ventes->destination }}</td>
                            <td>
                                @hasanyrole('Super Admin|Admins')
                                <a href="{{ route('exploitation.edition-pesee',$ventes->id) }}"
                                   target="_top" class="on-default edit-row"><i
                                        class="fas fa-eye"></i></a>
                                @endhasanyrole
                            </td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <hr>
                <div class="row">
                    <div class="col-4">
                        <section class="card">
                            <header class="card-header">
                                <div class="card-actions">
                                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                    <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                                </div>

                                <h2 class="card-title">Expédié par qualité
                                    <sup class="text-danger">*</sup></h2>
                                <p class="card-subtitle text-danger">
                                    NB: Sous réserve, des entrées comptabilisées
                                </p>
                            </header>
                            <div class="card-body">
                            <table class="table table-bordered small table-striped table-condensed datatable-default mb-0 ">
                            <thead>
                            <tr class="small">
                                <th>#</th>
                                <th>Ciment</th>
                                <th>Quantité </th>
                            </tr>
                            </thead>
                            <tbody>
                        @foreach($results['produits'] as $vendu)
                            <tr>
                                <td>{{ $loop->index +1 }}</td>
                                <td>{{$vendu->libelle}}</td>
                                <td class="text-center">{{  number_format($vendu->livre,2,',',' ')}}</td>
                            </tr>

                            @endforeach
                            <tr>
                                <td colspan="2" class="text-right">Total</td>
                                <td class="text-center">{{ number_format($results['produits']->sum('livre'),2,',',' ') }}</td>
                            </tr>
                            </tbody>
                        </table>
                            </div>
                        </section>
                    </div>
                    @if($case='daily' || $case='period' || $case='monthly')
{{--                    <div class="col-4">--}}
{{--                        <section class="card">--}}
{{--                            <header class="card-header">--}}
{{--                                <div class="card-actions">--}}
{{--                                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>--}}
{{--                                    <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>--}}
{{--                                </div>--}}

{{--                                <h2 class="card-title">Expédié par Distributeur--}}
{{--                                    <sup class="text-danger">*</sup></h2>--}}
{{--                                <p class="card-subtitle text-danger">--}}
{{--                                    NB: Sous réserve, des entrées comptabilisées--}}
{{--                                </p>--}}
{{--                            </header>--}}
{{--                            <div class="card-body">--}}
{{--                            <table class="table table-bordered small table-striped table-condensed datatable-default mb-0 ">--}}
{{--                            <thead>--}}
{{--                            <tr class="small">--}}
{{--                                <th>#</th>--}}
{{--                                <th>Distributeur</th>--}}
{{--                                <th>Ciment</th>--}}
{{--                                <th>Quantité </th>--}}
{{--                            </tr>--}}
{{--                            </thead>--}}
{{--                            <tbody>--}}
{{--                        @foreach($results['distributeurs'] as $distributeur)--}}
{{--                            <tr>--}}
{{--                                <td>{{ $loop->index +1 }}</td>--}}
{{--                                <td>{{$distributeur->nom}}</td>--}}
{{--                                <td>{{$distributeur->libelle}}</td>--}}
{{--                                <td class="text-center">{{ $distributeur->livre}}</td>--}}
{{--                            </tr>--}}

{{--                            @endforeach--}}
{{--                        <tr>--}}
{{--                            <td colspan="3" class="text-right">Total</td>--}}
{{--                            <td class="text-center">{{ numberFormat($results['distributeurs']->sum('livre'),0,' ',' ') }}</td>--}}
{{--                        </tr>--}}
{{--                            </tbody>--}}
{{--                        </table>--}}
{{--                            </div>--}}
{{--                        </section>--}}
{{--                    </div>--}}
{{--                    <div class="col-4">--}}
{{--                        <section class="card">--}}
{{--                            <header class="card-header">--}}
{{--                                <div class="card-actions">--}}
{{--                                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>--}}
{{--                                    <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>--}}
{{--                                </div>--}}

{{--                                <h2 class="card-title">Expédié par Transporteurs--}}
{{--                                    <sup class="text-danger">*</sup></h2>--}}
{{--                                <p class="card-subtitle text-danger">--}}
{{--                                    NB: Sous réserve, des entrées comptabilisées--}}
{{--                                </p>--}}
{{--                            </header>--}}
{{--                            <div class="card-body">--}}
{{--                            <table class="table table-bordered small table-striped table-condensed datatable-default mb-0 ">--}}
{{--                            <thead>--}}
{{--                            <tr class="small">--}}
{{--                                <th>#</th>--}}
{{--                                <th>Transporteur</th>--}}
{{--                                <th>Ciment</th>--}}
{{--                                <th>Quantité </th>--}}
{{--                            </tr>--}}
{{--                            </thead>--}}
{{--                            <tbody>--}}
{{--                        @foreach($results['transporteurs'] as $transporteur)--}}
{{--                            <tr>--}}
{{--                                <td>{{ $loop->index +1 }}</td>--}}
{{--                                <td>{{$transporteur->nom}}</td>--}}
{{--                                <td>{{$transporteur->libelle}}</td>--}}
{{--                                <td class="text-center">{{$transporteur->livre}}</td>--}}
{{--                            </tr>--}}

{{--                            @endforeach--}}
{{--                        <tr>--}}
{{--                            <td colspan="3" class="text-right">Total</td>--}}
{{--                            <td class="text-center">{{ numberFormat($results['transporteurs']->sum('livre'),0,' ',' ') }}</td>--}}
{{--                        </tr>--}}
{{--                            </tbody>--}}
{{--                        </table>--}}
{{--                            </div>--}}
{{--                        </section>--}}
{{--                    </div>--}}
                    @endif
                </div>
            </div>
        </section>
    </div>
</div>
