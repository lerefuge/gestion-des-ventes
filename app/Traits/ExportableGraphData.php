<?php


namespace App\Traits;


use App\Repositories\BudgetRepository;
use App\Repositories\MonthRepository;
use App\Repositories\PeseeRepository;
use Illuminate\Support\Facades\DB;

trait ExportableGraphData
{

    /**
     * @var PeseeRepository
     */
    private $peseeRepository;
    /**
     * @var BudgetRepository
     */
    private $budgetRepository;
    /**
     * @var MonthRepository
     */
    private $monthRepository;

    /**
     * ExportableGraphData constructor.
     * @param PeseeRepository $peseeRepository
     * @param BudgetRepository $budgetRepository
     * @param MonthRepository $monthRepository
     */
    public function __construct(PeseeRepository $peseeRepository, BudgetRepository $budgetRepository, MonthRepository $monthRepository)
    {

        $this->peseeRepository = $peseeRepository;
        $this->budgetRepository = $budgetRepository;
        $this->monthRepository = $monthRepository;
    }

    public function _graphDays($start, $end)
    {
        return $this->peseeRepository->pesee()->select('date_pesee')->distinct()
            ->whereBetween('pesees.date_pesee', [$start, $end])
            ->orderBy('date_pesee')
            ->get()
            ->map(function ($query) {
                return $query->date_pesee->format('D d');
            })->toArray();
    }


    public function _getTotalExpedies($start, $end = null)
    {
        $query = $this->peseeRepository->pesee()
            ->join('produits', 'produits.id', '=', 'pesees.produit_id')
            //->join('destinations', 'destinations.id', '=', 'pesees.destination_id')
            ->groupBy('pesees.date_pesee');
        if (!is_null($end)) {
            $query = $query->whereBetween('pesees.date_pesee', [$start, $end]);
        } else {
            $query = $query->where('pesees.date_pesee', '=', $start);
        }
        $query = $query
            ->addSelect(DB::raw('SUM(qte_bl) AS expedies'))
            ->orderBy('pesees.date_pesee')->get('expedies')->map(function ($q) {
                return round($q->expedies, 2);
            });
        return $query;
    }

    public function _getTotalExpedies32($start, $end = null)
    {
        $query = $this->peseeRepository->pesee()
            ->join('produits', 'produits.id', '=', 'pesees.produit_id')
            //->join('destinations', 'destinations.id', '=', 'pesees.destination_id')
            ->groupBy('pesees.date_pesee');
        if (!is_null($end)) {
            $query = $query->whereBetween('pesees.date_pesee', [$start, $end]);
        } else {
            $query = $query->where('pesees.date_pesee', '=', $start);
        }
        $query = $query
            ->where('produits.id', '=', '2') // CPJ 32.5 SAC
            ->addSelect(DB::raw('SUM(qte_bl) AS expedies'))
            ->orderBy('pesees.date_pesee')->get('expedies')->map(function ($q) {
                return round($q->expedies, 2);
            });
        return $query;
    }

    public function _getTotalExpedies42($start, $end = null)
    {
        $query = $this->peseeRepository->pesee()
            ->join('produits', 'produits.id', '=', 'pesees.produit_id')
            //->join('destinations', 'destinations.id', '=', 'pesees.destination_id')
            ->groupBy('pesees.date_pesee');
        if (!is_null($end)) {
            $query = $query->whereBetween('pesees.date_pesee', [$start, $end]);
        } else {
            $query = $query->where('pesees.date_pesee', '=', $start);
        }
        $query = $query
            ->where('produits.id', '=', '1') //CPJ 42.5 SAC
            ->addSelect(DB::raw('SUM(qte_bl) AS expedies'))
            ->orderBy('pesees.date_pesee')->get('expedies')->map(function ($q) {
                return round($q->expedies, 2);
            });
        return $query;
    }

    public function _getTotalExpediesCPASAC($start, $end = null)
    {
        $query = $this->peseeRepository->pesee()
            ->join('produits', 'produits.id', '=', 'pesees.produit_id')
            //->join('destinations', 'destinations.id', '=', 'pesees.destination_id')
            ->groupBy('pesees.date_pesee');
        if (!is_null($end)) {
            $query = $query->whereBetween('pesees.date_pesee', [$start, $end]);
        } else {
            $query = $query->where('pesees.date_pesee', '=', $start);
        }
        $query = $query
            ->where('produits.id', '=', '3') //CPA 42.5 SAC
            ->addSelect(DB::raw('SUM(qte_bl) AS expedies'))
            ->orderBy('pesees.date_pesee')->get('expedies')->map(function ($q) {
                $expedies = $q->expedies ?? '10';
                return round($expedies, 2);

            });
        return $query;
    }

    public function _getTotalExpediesCHF($start, $end = null)
    {
        $query = $this->peseeRepository->pesee()
            ->join('produits', 'produits.id', '=', 'pesees.produit_id')
            //->join('destinations', 'destinations.id', '=', 'pesees.destination_id')
            ->groupBy('pesees.date_pesee');
        if (!is_null($end)) {
            $query = $query->whereBetween('pesees.date_pesee', [$start, $end]);
        } else {
            $query = $query->where('pesees.date_pesee', '=', $start);
        }
        $query = $query
            ->where('produits.id', '=', '6') //CHF VRAC
            ->addSelect(DB::raw('SUM(qte_bl) AS expedies'))
            ->orderBy('pesees.date_pesee')->get('expedies')->map(function ($q) {
                return round($q->expedies, 2);
            });
        return $query;
    }

    public function _getTotalExpedies42VRAC($start, $end = null)
    {
        $query = $this->peseeRepository->pesee()
            ->join('produits', 'produits.id', '=', 'pesees.produit_id')
            //->join('destinations', 'destinations.id', '=', 'pesees.destination_id')
            ->groupBy('pesees.date_pesee');
        if (!is_null($end)) {
            $query = $query->whereBetween('pesees.date_pesee', [$start, $end]);
        } else {
            $query = $query->where('pesees.date_pesee', '=', $start);
        }
        $query = $query
            ->where('produits.id', '=', '4') //CPJ 42.5 VRAC
            ->addSelect(DB::raw('SUM(qte_bl) AS expedies'))
            ->orderBy('pesees.date_pesee')->get('expedies')->map(function ($q) {
                return round($q->expedies, 2);
            });
        return $query;
    }

    public function _getTotalExpediesCPAVRAC($start, $end = null)
    {
        $query = $this->peseeRepository->pesee()
            ->join('produits', 'produits.id', '=', 'pesees.produit_id')
            //->join('destinations', 'destinations.id', '=', 'pesees.destination_id')
            ->groupBy('pesees.date_pesee');
        if (!is_null($end)) {
            $query = $query->whereBetween('pesees.date_pesee', [$start, $end]);
        } else {
            $query = $query->where('pesees.date_pesee', '=', $start);
        }
        $query = $query
            ->where('produits.id', '=', '5') //CPA 42.5 VRAC
            ->addSelect(\DB::raw('SUM(qte_bl) AS expedies'))
            ->orderBy('pesees.date_pesee')->get('expedies')->map(function ($q) {
                return round($q->expedies, 2);
            });
        return $query;
    }

    public function _getTotalMonthlyExpediesNameGroupByProduit($currentYear, $month = null)
    {
        $query = $this->peseeRepository->pesee()
            ->selectRaw('produits.libelle AS ciment')
            ->join('produits', 'produits.id', '=', 'pesees.produit_id')
            ->groupBy(['produits.libelle'])
            ->whereYear('date_pesee', '=', $currentYear);
        if (!is_null($month)) {
            $query = $query->whereMonth('date_pesee', '=', $month);
        }
        $query = $query
            ->addSelect(\DB::raw('SUM(qte_bl) AS expedies'))
            ->get('expedies')->map(function ($q) {

                return $q->ciment;
            });
        return $query;
    }

    public function _getTotalMonthlyExpediesDataGroupByProduit($currentYear, $month = null)
    {
        $query = $this->peseeRepository->pesee()
            ->selectRaw('produits.libelle AS ciment')
            ->join('produits', 'produits.id', '=', 'pesees.produit_id')
            ->groupBy(['produits.libelle'])
            ->whereYear('date_pesee', '=', $currentYear);
        if (!is_null($month)) {
            $query = $query->whereMonth('date_pesee', '=', $month);
        }
        $query = $query
            ->addSelect(\DB::raw('SUM(qte_bl) AS expedies'))
            ->get('expedies')->map(function ($q) {

                return round($q->expedies, 2);
            });
        return $query;
    }

    public function _queryGetTotalExpedies($start, $end, $produit_id = null)
    {
        $q = $this->peseeRepository->pesee()
            ->join('produits', 'produits.id', '=', 'pesees.produit_id')
            //->join('destinations', 'destinations.id', '=', 'pesees.destination_id')
            ->groupBy('pesees.date_pesee')
            ->whereBetween('pesees.date_pesee', [$start, $end]);
        if (!is_null($produit_id)) {
            $q = $q->where('produits.id', '=', $produit_id);
        }
        $q = $q
            ->addSelect(\DB::raw('SUM(qte_bl) AS expedies'))
            ->orderBy('pesees.date_pesee')->get('expedies')
            ->map(function ($q) {
                return round($q->expedies, 2);
            });
        return $q;
    }

    public function getBudgetMonthName()
    {
        return $this->monthRepository->month()->select('libelle AS mois')->get()
            ->sortBy('month_number')->map(function ($q) {
                return $q->mois;
            });

    }

    public function budget()
    {
        return $this->budgetRepository->budget()
            ->selectRaw("months.libelle AS month,months.libelle AS mois")
            ->join('produits', 'produits.id', '=', 'budgets.produit_id')
            ->join('months', 'months.id', '=', 'budgets.month_id')
            ->select(\DB::raw('SUM(officiel) AS budget'))
            ->groupBy(['months.libelle', 'months.month_number', 'budgets.month_id'])
            ->orderBy('budgets.month_id')
            ->get('month')->map(function ($q) {
                return $q->budget;
            });
    }

    public function budgetMensuel(string $month, $currentYear)
    {

        return $this->peseeRepository->pesee()
            ->join('produits', 'produits.id', '=', 'pesees.produit_id')
            ->addSelect(DB::raw('SUM(qte_bl) AS realise'))
            ->whereMonth('date_pesee', '=', $month)
            ->whereYear('date_pesee', '=', $currentYear)
            ->get('realise')
            ->map(function ($q) {
                return round($q->realise, 2);
            });
    }

    public function _getDailyExpedies32($start, $end = null)
    {
        $query = $this->peseeRepository->pesee()
            ->select('pesees.date_pesee AS date')
            ->join('produits', 'produits.id', '=', 'pesees.produit_id')
            ->groupBy('pesees.date_pesee');
        if (!is_null($end)) {
            $query = $query->whereBetween('pesees.date_pesee', [$start, $end]);
        } else {
            $query = $query->where('pesees.date_pesee', '=', $start);
        }
        $query = $query
            ->where('produits.id', '=', '2') // CPJ 32.5 SAC
            ->addSelect(DB::raw('SUM(qte_bl) AS expedies'))
            ->orderBy('pesees.date_pesee');
        return $query;
    }

    public function _getDailyExpedies42($start, $end = null)
    {
        $query = $this->peseeRepository->pesee()
            ->select('pesees.date_pesee AS date')
            ->join('produits', 'produits.id', '=', 'pesees.produit_id')
            ->groupBy('pesees.date_pesee');
        if (!is_null($end)) {
            $query = $query->whereBetween('pesees.date_pesee', [$start, $end]);
        } else {
            $query = $query->where('pesees.date_pesee', '=', $start);
        }
        $query = $query
            ->where('produits.id', '=', '1') // CPJ 42.5 SAC
            ->addSelect(DB::raw('SUM(qte_bl) AS expedies'))
            ->orderBy('pesees.date_pesee')->tap(function ($q) {
                return $q;
            });
        return $query;
    }

    public function expediesParDistrict($currentYear, $month = null)
    {
        $query = $this->peseeRepository->pesee()
            ->selectRaw("chef_lieu_districts.libelle AS chef_lieu")
            ->selectRaw('SUM(qte_bl) AS expedies')
            ->join('destinations', 'destinations.id', '=', 'pesees.destination_id')
            ->join('regions', 'destinations.region_id', '=', 'regions.id')
            ->join('chef_lieu_districts', 'regions.chef_lieu_id', '=', 'chef_lieu_districts.id')
            ->whereYear('date_pesee', '=', $currentYear);
        if (!is_null($month)) {
            $query = $query->whereMonth('date_pesee', '=', $month);
        }
        $query = $query->groupBy(['chef_lieu_districts.libelle'])->orderByDesc('expedies')->get();

        return $query;
    }

    public function expediesParDistributeur($currentYear, $month = null)
    {
        $query = $this->peseeRepository->pesee()
            ->selectRaw("distributeurs.nom AS distributeur")
            ->selectRaw("chef_lieu_districts.libelle AS chef_lieu")
            ->selectRaw('SUM(qte_bl) AS expedies')
            ->join('destinations', 'destinations.id', '=', 'pesees.destination_id')
            ->join('distributeurs', 'distributeurs.id', '=', 'pesees.distributeur_id')
            ->join('regions', 'destinations.region_id', '=', 'regions.id')
            ->join('chef_lieu_districts', 'regions.chef_lieu_id', '=', 'chef_lieu_districts.id')
            ->whereYear('date_pesee', '=', $currentYear);
        if (!is_null($month)) {
            $query = $query->whereMonth('date_pesee', '=', $month);
        }
        $query = $query->groupBy(['distributeur','chef_lieu_districts.libelle'])->orderByDesc('expedies')->get();

        return $query;
    }

    public function expediesOrphans($currentYear, $month = null)
    {
        $query = $this->peseeRepository->pesee()
            ->with(['destination'])->doesntHave('destination.region')
            ->whereYear('date_pesee', '=', $currentYear);
        if (!is_null($month)) {
            $query = $query->whereMonth('date_pesee', '=', $month);
        }
        $query = $query->sum('qte_bl');
        return $query;
    }

    public function expediesDistributeurOrphans($currentYear, $month = null)
    {
        $query = $this->peseeRepository->pesee()
            ->with(['destination'])->doesntHave('distributeur')
            ->whereYear('date_pesee', '=', $currentYear);
        if (!is_null($month)) {
            $query = $query->whereMonth('date_pesee', '=', $month);
        }
        $query = $query->sum('qte_bl');
        return $query;
    }


    public function sommeTotaleExpediesParDistrictParMois($currentYear, $month)
    {
        return [
            'expedies' => $this->expediesParDistrict($currentYear, $month)->sum('expedies'),
            'orphans' => $this->expediesOrphans($currentYear, $month),

        ];
    }

    public function sommeTotaleExpediesParDistributeurParMois($currentYear, $month)
    {
        return [
            'expedies' => $this->expediesParDistributeur($currentYear, $month)->sum('expedies'),
             'orphans' => 0


        ];
    }

    public function sommeTotaleExpediesParDistributeurParAnnee($currentYear)
    {
        return [
            'expedies' => $this->expediesParDistributeur($currentYear)->sum('expedies'),
             'orphans' => 0
        ];
    }

    public function sommeTotaleExpediesParDistrictParAnnee($currentYear)
    {
        return [
            'expedies' => $this->expediesParDistrict($currentYear)->sum('expedies'),
            'orphans' => $this->expediesOrphans($currentYear),

        ];
    }

    public function detailBudget()
    {
        return $this->budgetRepository->budget()
            ->selectRaw('produits.libelle as produit')
            ->selectRaw(DB::raw('Sum(budgets.officiel) as budget'))
            ->join('annees','annees.id','=','budgets.annee_id')
            ->join('produits','produits.id','=','budgets.produit_id')
            ->where('annees.current',true)

            ->groupBy(['produits.libelle'])
            ->get()
          //  ->sum('officiel')
            ;

    }
    public function detailRealiseBudget()
    {
        return $this->peseeRepository->pesee()
            ->selectRaw('produits.libelle as produit')
            ->selectRaw(DB::raw('Sum(pesees.qte_bl) as realise'))
            ->join('produits','produits.id','=','pesees.produit_id')
             ->groupBy(['produits.libelle','produit_id'])
            ->get()
            ;

    }

}
