<?php

use Illuminate\Database\Seeder;

class ProduitTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('produits')->insert(['libelle'=>'CIMENT CPJ 32.5 SAC']);
        DB::table('produits')->insert(['libelle'=>'CIMENT CPJ 42.5 SAC']);
        DB::table('produits')->insert(['libelle'=>'CPA ROBUSTO SAC']);
    }
}
