<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }
    private function _redirectWhenFail($validator = null,$bags = null)
    {
        flash('Veuillez corriger le erreurs SVP !', 'warning');
        $redirect = redirect()->back()->withInput();
        if ($validator)
            $redirect = $redirect->withErrors($validator,$bags)->withInput();
        return $redirect;
    }

    public function profile ()
    {
        return view('users.profile');
    }

    public function updateProfile(Request $request)
    {
        $user = auth()->user();
        $validator  = validator($request->all(),['password'=>['required','min:6','confirmed']],
            [
                'password.required'=>'Saisisser votre nouveau mot de passe',
                'password.min'=>"Votre mot de passe doit être d'aumoins 6 caractères",
                "password.confirmed"=>"Les deux mots de passes ne sont pas identiques"
            ]
        );
        //dd($request->all(),$validator->failed(),$user);


        if ($validator->fails()){
            return $this->_redirectWhenFail($validator,'profile');
        }
        $user->update([
            'password'=>Hash::make($request->password),
            'default_password'=>false
        ]);
        flash('Votre mot de passe a bien été mis à jour.', 'success');
        return redirect()->route('user.profile');

    }
}
