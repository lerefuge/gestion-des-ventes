<?php

/**
 *  Route liées aux action du service commercial
 */
Route::group([ 'prefix' => 'service-expedition' , 'middleware' => 'auth' ], function () {

    Route::get('recherches','ExpeditionController@recherche')->name('expedition.recherche');
    Route::post('recherches','ExpeditionController@postRecherche')->name('expedition.recherche');

    Route::get('point-expeditions','ExpeditionController@pointExpedition')->name('expedition.expedies');
    Route::post('point-expeditions','ExpeditionController@consultation')->name('expedition.expedies');
    Route::get('camions-hors-tolerances','ExpeditionController@toleranceCamions')->name('expedition.tolerance-camions');

});
