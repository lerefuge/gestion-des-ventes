@extends('template.theme')


@section('font-title')

@endsection
@push('scripts')

    <script type="text/javascript">
        $(function () {
            $('.selectPicker').select2({
                title: "Liste d'élément ...",
                header: 'Selectionnez un élément dans la liste',
                liveSearch: true,
                width: 'resolve'
            });
        });

    </script>

@endpush


@section('content-body')


    <div class="row">

        <div class="col col-md-12">
            <div class="row">
                <div class="col">
                    <section class="card">
                        <header class="card-header">
                            <div class="card-actions">
                                <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                            </div>

                            <h2 class="card-title">Utilisateurs enregistrées</h2>
                        </header>
                        <div class="card-body">
                            <table class="table table-bordered table-striped mb-0 datatable-default">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nom</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>Permissions</th>
                                    <th class="">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{ $loop->index + 1 }}</td>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>
{{--                                            {{ $user}}--}}
                                            {{ $user->roles ? $user->roles->implode('name',', ') : '' }}
                                        </td>
                                        <td>
                                            {{ $user->permissions ?  $user->permissions->implode('name',', ') : '' }}
                                        </td>

                                        <td class="flex text-center">
                                            <a href="{{ route('admin.user-roles-attibution',$user) }}"
                                               class="on-default edit-row" data-toggle="confirmation"><i
                                                    class="fas fa-pencil-alt"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </section>
                </div>
            </div>
        </div>


    </div>

    @if($utilisateur && !empty($utilisateur))
        <div class="row">
            <div class="col col-md-6 offset-md-3">
                <div class="panel card-sign">

                    <div class="card-body">

                        {!! Form::open(['url'=>route('admin.user-roles-attibution',$utilisateur->id),'class'=>'form-horizontal','method'=>'POST']) !!}
                        @method('POST')

                        <div class="form-group mb-3">

                            <label for="name" class="control-label">Nom utilisateur : </label>
                            {!! Form::text('name',$utilisateur->name,
                            ['id'=>'name','class'=>'form-control form-control-lg','required'=>'required','placeholder'=>'Nom utilisateur','readonly'=>true]) !!}
                            @error('name')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group mb-3">

                            <label for="email" class="control-label">Identifiant utilisateur : </label>
                            {!! Form::email('email',$utilisateur->email,
                            ['id'=>'email','class'=>'form-control form-control-lg','placeholder'=>"Identifiant utilisateur ",'readonly'=>true]) !!}
                            @error('email')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="row form-group">
                            <div class="col-6">

                                <div class="form-group">
                                    <label for="role_id" class="control-label">Role </label>
                                    {!! Form::select('role_id',$roles->pluck('name','id'), $utilisateur->roles->pluck('id')->toArray(),
                                    ['id'=>'role_id',
                                    'class'=>'form-control ',
                                    'title'=>"Role",
                                    'placeholder'=>"Selectionner un role",
                                    'required'=>'required',
                                    ]) !!}
                                    @error('role_id')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-6">

                                <div class="form-group">
                                    <label for="permissions" class="control-label">Permission : </label>
                                    {!! Form::select('permissions[]',$permissions->pluck('name','id') ,$utilisateur->permissions->pluck('id')->toArray() ,
                                     ['id'=>'permissions','class'=>'form-control selectPicker','title'=>"Permission utilisateur",'required'=>'required','multiple'=>true,
                                     'required'=>'required']) !!}
                                    @error('permissions')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-sm-6 text-right">

                                {!! Form::hidden('user_id',$utilisateur->id) !!}
                                <button type="submit" class="btn btn-warning  mt-2" data-toggle="confirmation">
                                    Attribuer
                                </button>

                            </div>
                        </div>


                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    @endif



@endsection
