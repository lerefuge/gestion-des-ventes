<?php

use Illuminate\Database\Seeder;

class TypeSubventionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $type_subvention = [
            ['libelle'=>'CASH'],
            ['libelle'=>'FACTURE'],
        ];
        DB::table('type_subventions')->insert($type_subvention);
    }
}
