<div class="sidebar-widget widget-tasks">
    <div class="widget-header">
        <h6>Administation</h6>
        <div class="widget-toggle">+</div>
    </div>
    <div class="widget-content">
        <ul class="list-unstyled m-0">
            <li><a href="#">Porto HTML5 Template</a></li>
            <li><a href="#">Tucson Template</a></li>
            <li><a href="#">Porto Admin</a></li>
        </ul>
    </div>
</div>

<hr class="separator" />

<div class="sidebar-widget widget-stats">
    <div class="widget-header">
        <h6>Company Stats</h6>
        <div class="widget-toggle">+</div>
    </div>
    <div class="widget-content">
        <ul>
            <li>
                <span class="stats-title">Stat 1</span>
                <span class="stats-complete">85%</span>
                <div class="progress">
                    <div class="progress-bar progress-bar-primary progress-without-number" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 85%;">
                        <span class="sr-only">85% Complete</span>
                    </div>
                </div>
            </li>
            <li>
                <span class="stats-title">Stat 2</span>
                <span class="stats-complete">70%</span>
                <div class="progress">
                    <div class="progress-bar progress-bar-primary progress-without-number" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%;">
                        <span class="sr-only">70% Complete</span>
                    </div>
                </div>
            </li>
            <li>
                <span class="stats-title">Stat 3</span>
                <span class="stats-complete">2%</span>
                <div class="progress">
                    <div class="progress-bar progress-bar-primary progress-without-number" role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="100" style="width: 2%;">
                        <span class="sr-only">2% Complete</span>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>
