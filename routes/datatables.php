<?php
//Route::group(['middleware'=>'auth'], function () {
    Route::namespace('DataTables')->group(function () {
       Route::prefix('ventes')->group(function(){
           Route::post('journaliere/{datetime}','DataTableVenteController@venteJournaliere')->name('datatable.vente-jour');
           Route::post('periodique/{debut}/{fin}','DataTableVenteController@ventePeriodique')->name('datatable.vente-periode');
           Route::post('mensuelle/{debut}/{fin}','DataTableVenteController@venteMensuelle')->name('datatable.vente-mensuelle');
           Route::post('annuelle/{debut}/{fin}','DataTableVenteController@venteAnnuelle')->name('datatable.vente-annuelle');
       });

       Route::prefix('expeditions')->group(function(){
           Route::post('journaliere/{datetime}','DataTableExpeditionController@expediesJournaliere')->name('datatable.expeditions-jour');
           Route::post('mensuelle/{debut}/{fin}','DataTableExpeditionController@expediesMensuelle')->name('datatable.expeditions-mensuelle');
           Route::post('annuelle/{debut}/{fin}','DataTableExpeditionController@expediesAnnuelle')->name('datatable.expeditions-annuelle');
       });

       Route::prefix('details')->group(function(){
           Route::post('mensuelle/{debut}/{fin}','DataTableDetailsController@detailMensuelle')->name('datatable.detail-mensuel');
           Route::post('annuelle/{debut}/{fin}','DataTableDetailsController@detailAnnuelle')->name('datatable.detail-annuelle');
       });
    });
//});
