<?php

namespace App\Http\Controllers;

use App\Imports\MassDataPeseeImport;
use App\Imports\PeseesImport;
use App\Imports\TablePeseeImport;
use App\Repositories\ChargeRepository;
use App\Repositories\ChauffeurRepository;
use App\Repositories\ClientRepository;
use App\Repositories\DestinationRepository;
use App\Repositories\DistributeurRepository;
use App\Repositories\PeseeRepository;
use App\Repositories\ProduitRepository;
use App\Repositories\RegionRepository;
use App\Repositories\SubventionRepository;
use App\Repositories\TonnageRepository;
use App\Repositories\TransporteurRepository;
use App\Repositories\TypeSubventionRepository;
use App\Repositories\TypeVehiculeRepository;
use App\Repositories\VehiculeRepository;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Facades\Excel;

class GestionController extends Controller
{

    /**
     * @var ProduitRepository
     */
    private $produitRepository;
    /**
     * @var ChauffeurRepository
     */
    private $chauffeurRepository;
    /**
     * @var RegionRepository
     */
    private $regionRepository;
    /**
     * @var TransporteurRepository
     */
    private $transporteurRepository;
    /**
     * @var DistributeurRepository
     */
    private $distributeurRepository;
    /**
     * @var PeseeRepository
     */
    private $peseeRepository;
    /**
     * @var DestinationRepository
     */
    private $destinationRepository;
    /**
     * @var ChargeRepository
     */
    private $chargeRepository;
    /**
     * @var VehiculeRepository
     */
    private $vehiculeRepository;
    /**
     * @var TypeSubventionRepository
     */
    private $typeSubventionRepository;
    /**
     * @var TypeVehiculeRepository
     */
    private $typeVehiculeRepository;
    /**
     * @var SubventionRepository
     */
    private $subventionRepository;
    /**
     * @var ClientRepository
     */
    private $clientRepository;
    /**
     * @var TonnageRepository
     */
    private $tonnageRepository;

    /**
     * GestionController constructor.
     * @param ProduitRepository $produitRepository
     * @param ChauffeurRepository $chauffeurRepository
     * @param RegionRepository $regionRepository
     * @param TransporteurRepository $transporteurRepository
     * @param DistributeurRepository $distributeurRepository
     * @param PeseeRepository $peseeRepository
     * @param ChargeRepository $chargeRepository
     * @param VehiculeRepository $vehiculeRepository
     * @param TonnageRepository $tonnageRepository
     * @param ClientRepository $clientRepository
     * @param TypeVehiculeRepository $typeVehiculeRepository
     * @param TypeSubventionRepository $typeSubventionRepository
     * @param SubventionRepository $subventionRepository
     * @param DestinationRepository $destinationRepository
     */
    public function __construct(ProduitRepository $produitRepository,
                                ChauffeurRepository $chauffeurRepository,
                                RegionRepository $regionRepository,
                                TransporteurRepository $transporteurRepository,
                                DistributeurRepository $distributeurRepository,
                                PeseeRepository $peseeRepository,
                                ChargeRepository $chargeRepository,
                                VehiculeRepository $vehiculeRepository,
                                TonnageRepository $tonnageRepository,
                                ClientRepository $clientRepository,
                                TypeVehiculeRepository $typeVehiculeRepository,
                                TypeSubventionRepository $typeSubventionRepository,
                                SubventionRepository $subventionRepository,
                                DestinationRepository $destinationRepository)
    {
        $this->middleware('auth');
        $this->produitRepository = $produitRepository;
        $this->chauffeurRepository = $chauffeurRepository;
        $this->regionRepository = $regionRepository;
        $this->transporteurRepository = $transporteurRepository;
        $this->distributeurRepository = $distributeurRepository;
        $this->peseeRepository = $peseeRepository;
        $this->destinationRepository = $destinationRepository;
        $this->chargeRepository = $chargeRepository;
        $this->vehiculeRepository = $vehiculeRepository;
        $this->typeSubventionRepository = $typeSubventionRepository;
        $this->typeVehiculeRepository = $typeVehiculeRepository;
        $this->subventionRepository = $subventionRepository;
        $this->clientRepository = $clientRepository;
        $this->tonnageRepository = $tonnageRepository;
    }
    use Importable;

    private function _redirectWhenFail($validator = null,$bags = null)
    {
        flash('Veuillez corriger le erreurs SVP !', 'warning');
        $redirect = redirect()->back()->withInput();
        if ($validator)
            $redirect = $redirect->withErrors($validator,$bags)->withInput();
        return $redirect;
    }

    public function index()
    {
        return view('gestion.chargement-fichier');
    }

    public function traitement(Request $request)
    {
        Excel::import(new PeseesImport(
            $this->produitRepository,
            $this->regionRepository,
            $this->subventionRepository,
            $this->transporteurRepository,
            $this->distributeurRepository,
            $this->destinationRepository,
            $this->peseeRepository,
            $this->vehiculeRepository,
            $request->only(['date'])
        ), request()->file('vente')->getRealPath())
//        ->onQueue('high')
//        ->delay(now()->addSeconds(5))
        ;
        flash('Importation effectué avec succès', 'info');
        return redirect()->back();
    }

    public function tableTolerance()
    {
        return view('gestion.chargement-table-tolerance');
    }

    public function chargementTableTolerance(Request $request)
    {
        Excel::import(new TablePeseeImport($this->peseeRepository),$request->file('table_tolerance')->getRealPath())
      //  ->onQueue('low')
       // ->delay(now()->addSeconds(5))
        ;
        flash('Importation effectué avec succès', 'info');
        return redirect()->back();
    }

    public function loadMassData()
    {
        return view('gestion.chargement-mass-data');

    }

    public function chargeMassData(Request $request)
    {
       Excel::queueImport(
            new MassDataPeseeImport(
                $this->produitRepository,
                $this->regionRepository,
                $this->subventionRepository,
                $this->transporteurRepository,
                $this->distributeurRepository,
                $this->destinationRepository,
                $this->peseeRepository,
                $this->vehiculeRepository
            ),$request->file('table_pesee_mensuelle')->getRealPath())
         //   ->onQueue('high')
           ->delay(now()->addSeconds(5))
           //->onConnection('database')
           // ->onQueue('mass-load')
       ;
        flash('Importation effectué avec succès', 'info');
        return redirect()->back();
    }
    public function recherche()
    {
        $data['entrees'] = [];
        return view('gestion.recherche-stantard')->with($data);
    }

    public function postRecherche(Request $request)
    {
        $date = $request->get('date');
        $validator = validator($request->all(), ['date' => 'required|date'], ['date.required' => 'Veuillez selectionner une date SVP']);
        if ($validator->fails()) {
            return $this->_redirectWhenFail($validator);
        }
        $result = $this->peseeRepository->pesee()->whereDate('date_pesee', '=', $date)->orderBy('heure_pesee_sortie','ASC')->get()->load(['transporteur', 'produit', 'distributeur', 'destination', 'destination.region', 'charges', 'vehicule', 'vehicule.typeVehicule', 'subvention', 'subvention.typeSubvention', 'produit.couts' => function ($query) use ($date) {
            return $query->where('date_debut', '<=', $date)
                ->where('date_fin', '>=', $date);
        }]);
        if ($result->isNotEmpty()) {
            $data['entrees'] = $result;
            return view('gestion.recherche-stantard')->with($data);
        } else {
            flash('Desolé, aucune entrée trouvées pour cette date', 'info');
            return redirect()->back();
        }
    }
    public function postAdvanced(Request $request)
    {
        $date = $request->get('date_jour');
        $immatriculation = $request->get('immatriculation');
        $validator = validator($request->all(), ['date_jour' => 'required|date'], ['date_jour.sometimes' => 'Veuillez selectionner une date SVP']);
        if ($validator->fails()) {
            return $this->_redirectWhenFail($validator,'advanced');
        }
        $result = $this->peseeRepository->pesee()
            ->leftJoin('vehicules', 'vehicules.pesee_id','=','pesees.id')
            ->whereDate('date_pesee', '=', $date)
            ->where('vehicules.immatriculation', '=', $immatriculation)
            ->orderBy('heure_pesee_sortie','ASC')->get()->load(['transporteur', 'produit', 'distributeur', 'destination', 'destination.region', 'charges', 'vehicule', 'vehicule.typeVehicule', 'subvention', 'subvention.typeSubvention', 'produit.couts' => function ($query) use ($date) {

                return $query->where('date_debut', '<=', $date)
                ->where('date_fin', '>=', $date);
        }]);
        if ($result->isNotEmpty()) {
            $data['entrees'] = $result;
            return view('gestion.recherche-stantard')->with($data);
        } else {
            flash('Desolé, aucune entrée trouvées pour cette date', 'info');
            return redirect()->back();
        }
    }

    public function editionEntree($entree)
    {
        $result = $this->peseeRepository->pesee()
            ->with(['transporteur', 'produit','distributeur', 'destination', 'vehicule','subvention.typeSubvention'])->where('id', '=', $entree)->first();
        if ($result) {
            $data['clients'] = $this->clientRepository->client();
            $data['type_vehicules'] = $this->typeVehiculeRepository->typeVehicule();
            $data['type_vehicules'] = $this->typeVehiculeRepository->typeVehicule();
            $data['type_subventions'] = $this->typeSubventionRepository->typeSubvention();
            $data['entree'] = $result;
            return view('gestion.edition-entree')->with($data);
        } else {
            flash('Desolé, aucune entrée trouvées pour cette date', 'info');
            return redirect()->back();
        }
    }

    public function chargeLivraison(Request $request)
    {

        $validator = validator($request->all(), [
            'type_vehicule_id' => 'required',
            'type_subvention_id' => Rule::requiredIf($request->get('type_vehicule_id') != 1),
            'prix_base' => Rule::requiredIf($request->get('type_vehicule_id') != 1),
            'prix_negocie' => Rule::requiredIf($request->get('type_vehicule_id') != 1),
            'montant_supporte' => Rule::requiredIf($request->get('type_vehicule_id') != 1),
            'numero_bon_caisse' =>[ Rule::requiredIf($request->get('type_subvention_id') == 1),'sometimes'],
        ],
            [
                'type_vehicule_id.required' => 'Veuillez preciser le type du véhicule',
                'type_subvention_id.required' => 'Veuillez preciser le type de la subvention',
                'prix_base.required' => 'Veuillez preciser le prix de base de ce trajet',
                'prix_negocie.required' => 'Veuillez preciser le prix negocié pour ce trajet',
                'montant_supporte.required' => 'Veuillez preciser le montant supporté',
                'numero_bon_caisse.required' => 'Veuillez preciser le numéro du bon de caisse',
            ]
        );
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator, 'charges')->withInput();
        }
        $pesee_id = $request->get('pesee_id');
        if($request->get('client_id')){
            $tonnage =['pesee_id'=>$pesee_id,'client_id'=>$request->get('client_id'),'date'=>$request->get('date_livraison'),'quantite'=>$request->get('quantite')];
            $this->tonnageRepository->tonnage()->updateOrCreate(['pesee_id'=>$pesee_id,'client_id'=>$request->get('client_id')],$tonnage);
        }
        $vehicule = $this->vehiculeRepository->vehicule()->where('pesee_id', '=', $pesee_id)->first();
        if ($vehicule) {
            $type_vehicule_id = $request->get('type_vehicule_id');


            $vehicule->update(['type_vehicule_id' => $type_vehicule_id]);
            if ($type_vehicule_id != 1) {
                $type_subvention_id = $request->get('type_subvention_id');
                $prix_base = $request->get('prix_base');
                $prix_negocie = $request->get('prix_negocie');
                $montant_supporte = $request->get('montant_supporte');
                $num_bom = [  'numero_bon_caisse' =>  $request->get('numero_bon_caisse') , 'statut' => $request->statut== TRUE ,];
                $data = [
                    'type_subvention_id' => $type_subvention_id,
                    'prix_base' => $prix_base,
                    'prix_negocie' => $prix_negocie,
                    'montant_supporte_cimaf' => $request->montant_supporte_cimaf,
                    'numero_bon_caisse' => NULL,
                    'statut' => FALSE,
                    'montant_supporte' => $montant_supporte,
                    'avance' => $request->avance,
                    'reste' => $request->reste,
                    'pesee_id' => $pesee_id
                ];
                if ($request->statut=='paid'){
                    $array = array_merge($data,$num_bom);
                    if ($this->subventionRepository->subvention()->updateOrCreate(['pesee_id' => $pesee_id], $array)) {

                        flash("subvention enregistrée");
                        return redirect()->route('gestion.edition-entree', $pesee_id);
                    }
                }else{
                    if ($this->subventionRepository->subvention()->updateOrCreate(['pesee_id' => $pesee_id], $data)) {

                        flash("subvention enregistrée");
                        return redirect()->route('gestion.edition-entree', $pesee_id);
                    }
                }


            }else{
                $data = [
                    'type_subvention_id' => NULL,
                    'prix_base' => NULL,
                    'prix_negocie' => NULL,
                    'montant_supporte' => NULL,
                    'numero_bon_caisse' => NULL,
                    'avance' => NULL,
                    'reste' => NULL,
                    'pesee_id' => $pesee_id
                ];

                if ($this->subventionRepository->subvention()->updateOrCreate(['pesee_id' => $pesee_id], $data)) {
                    flash("subvention enregistrée");
                    return redirect()->route('gestion.edition-entree', $pesee_id);
                }
            }
            flash("subvention enregistrée");
            return redirect()->route('gestion.edition-entree', $pesee_id);
        }
    }
}
