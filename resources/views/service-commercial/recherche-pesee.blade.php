@extends('template.theme')


@section('font-title')

@endsection
@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {

            'use strict';

            let ajaxSelectPicker = function () {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                var numero_bon_livraison = {
                    ajax: {
                        url: "{{ route('recherche.load-bon-livraison') }}",
                        type: 'POST',
                        dataType: 'json',
                        // Use "@{{{q}}}" as a placeholder and Ajax Bootstrap Select will
                        // automatically replace it with the value of the search query.
                        data: {
                            q: '@{{{q}}}'
                        }
                    },
                    locale: {
                        emptyTitle: 'Select and Begin Typing'
                    },
                    log: 1,
                    templates: {
                        // The placeholder for status updates pertaining to the list and request.
                        status: '<div class="status"></div>',
                    },

                    preprocessData: function (data) {
                        var i, l = data.length,
                            array = [];
                        if (l) {
                            for (i = 0; i < l; i++) {
                                array.push($.extend(true, data[i], {
                                    text: data[i].numero_bl,
                                    value: data[i].id,
                                    // data: {
                                    //     subtext: data[i].numero_bl
                                    // }
                                }));
                            }
                        }
                        // You must always return a valid array when processing data. The
                        // data argument passed is a clone and cannot be modified directly.
                        return array;
                    },
                    preserveSelected: true,
                    requestDelay: 3000,
                    bindEvent : 'keyup'
                };
                $('.select-picker')
                    .selectpicker({
                        liveSearch: true,
                    })
                    .ajaxSelectPicker(numero_bon_livraison)
                    .filter('.with-ajax')
                    .selectpicker('refresh');

            };


            ajaxSelectPicker();
            $('select').trigger('change');
            $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
                localStorage.setItem('activeTab', $(e.target).attr('href'));
            });
            var activeTab = localStorage.getItem('activeTab');
            if (activeTab) {
                $('#navTabs a[href="' + activeTab + '"]').tab('show');
            }
        });
    </script>
@endpush


@section('content-body')
    <div class="row">
        <div class="col-6 offset-3">
            <div class="tabs">
                <ul class="nav nav-tabs" id="navTabs">
                    <li class="nav-item ">
                        <a class="nav-link" href="#daily" data-toggle="tab"> Journalière</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#period" data-toggle="tab">Periode</a>
                    </li>

                </ul>
                <div class="tab-content">
                    <div class="card-subtitle">

                    </div>

                    <div id="daily" class="tab-pane active">
                        {!! Form::open(['url'=>route('commercial.recherche'),'class'=>'daily-form',"id"=>'daily-form']) !!}
                        <section class="card">
                            <header class="card-header">
                                <div class="card-actions">
                                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                    <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                                </div>

                                <h2 class="card-title"> Consulter Ventes </h2>

                            </header>
                            <div class="card-body">
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <div class="form-row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label class="col-form-label" for="pesee_id">N° Bon de livraison</label>
                                            <div class="input-group">
                                                {!! Form::select('pesee_id', [], request('pesee_id',null),
                                                ['id'=>'num_bl_id',
                                                'class'=>'form-control select-picker selectPicker with-ajax ',
                                                'data-live-search' => true,
                                                'autofocus',
                                                'data-live-search-normalize'=>true,
                                                'required'=>'required',
                                                'placeholder'=>'Numéro Bon de livraison']
                                                ) !!}
                                            </div>
                                            @error('num_bl')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <footer class="card-footer">
                                <div class="row ">
                                    <div class="col-sm-9 align-content-md-end">
                                        <input type="hidden" name="case" value="numero_bl">
                                        <button type="submit" class="btn btn-primary"><i
                                                class="fas search"></i> Rechercher
                                        </button>


                                    </div>
                                </div>
                            </footer>
                        </section>
                        {!! Form::close() !!}
                    </div>


                    <div id="period" class="tab-pane">
                        {!! Form::open(['url'=>route('commercial.recherche'),'class'=>'weekly-form',"id"=>'period-form']) !!}
                        <section class="card">
                            <header class="card-header">
                                <div class="card-actions">
                                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                    <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                                </div>

                                <h2 class="card-title"> Consulter les ventes </h2>
                                <p class="card-subtitle">

                                    </p>
                            </header>
                            <div class="card-body">
                                <div class="col">
                                    <div class="form-group">
                                        <label class="col-form-label" for="date_jour">Date </label>
                                        <div class="input-group">
														<span class="input-group-prepend">
															<span class="input-group-text">
																<i class="fas fa-calendar-alt"></i>
															</span>
														</span>
                                            <input type="text" name="date_jour"
                                                   value="{{ old('date_jour',request('date_jour')) }}"
                                                   id="date_jour" class="form-control" autocomplete="off"
                                                   required
                                                   data-plugin-datepicker
                                                   data-plugin-options='{"orientation":"bottom","autoclose":true,"language":"fr","todayHighlight":true}'
                                            >
                                        </div>
                                        @error('date_jour')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <footer class="card-footer">
                                <div class="row ">
                                    <div class="col-sm-9 align-content-md-end">
                                        <input type="hidden" name="case" value="period">
                                        <button type="submit" class="btn btn-primary"><i
                                                class="fas search"></i> Rechercher
                                        </button>


                                    </div>
                                </div>
                            </footer>
                        </section>
                        {!! Form::close() !!}
                    </div>


                </div>
            </div>
        </div>

    </div>



    @if(isset($expedies) && !empty($expedies))
        <div class="row">
            <div class="col">
                <section class="card">
                    <header class="card-header">
                        {{-- <div class="card-actions">
                            <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                            <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                        </div> --}}

                        <h2 class="card-title">
                            @if($case=='period')
                                @can('Suppression données commerciales')
                                <form method="post" action="{{ route('exploitation.deletion-pesee-by-date') }}"
                                      class="form-inline pull-right">
                                    @method('delete')
                                    @csrf
                                    <input type="hidden" name="date_pesee_del" value="{{ $date_pesee}}">
                                    <button type="submit" data-toggle="confirmation" class="btn btn-xs btn-danger"><i
                                            class="fa fa-trash"></i>
                                    </button>
                                </form>
                                @endcan
                            @endif

                        </h2>
                    </header>
                    <div class="card-body">
                        <table class="table table-bordered table-light table-striped mb-0 datatable-default">
                            <thead>
                            <tr class="small">

                                <th>N° BL</th>
                                <th>Distributeur</th>
                                <th>Transporteur</th>
                                <th>Immatriculation.</th>
                                <th>Produit</th>
                                <th>Qte BL <em>(T)</em></th>
                                <th>Qte Pesée <em>(T)</em></th>
                                <th>Date entrée</th>
                                <th>Heure entrée</th>
                                <th>Date Sortie</th>
                                <th>Heure Sortie</th>
                                <th>Destination</th>
                                <th>Zone</th>
                                <th>Détails</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($expedies as $expedie)
                                <tr
                                    class="text-center small ">

                                    <td>{{ $expedie->numero_bl }}</td>
                                    <td>{{ $expedie->distributeur->nom }}</td>
                                    <td>{{ $expedie->transporteur->nom }}</td>
                                    <td>{{ $expedie->vehicule->immatriculation ?? null }}</td>
                                    <td>{{ $expedie->produit->libelle }}</td>
                                    <td>{{ $expedie->qte_bl }}</td>
                                    <td>{{ $expedie->qte_pesee }}</td>
                                    <td>{{ $expedie->date_entree->format('d/mY') }}</td>
                                    <td>{{ $expedie->heure_pesee_entree }}</td>
                                    <td>{{ $expedie->date_sortie->format('d/mY') }}</td>
                                    <td>{{ $expedie->heure_pesee_sortie }}</td>
                                    <td>{{ $expedie->destination->libelle }}</td>
                                    <td>{{ '' }}</td>

                                    <td class="center">
{{--                                        <a href="{{ route('exploitation.edition-pesee',$expedies) }}" target="_top"--}}
{{--                                           class="on-default edit-row"><i class="fas fa-eye"></i></a>--}}
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </section>
            </div>
        </div>

    @endif

@endsection
