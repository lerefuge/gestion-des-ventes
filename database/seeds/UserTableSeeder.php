<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $user =  DB::table('users')->insert([
            'name'=>'aka josias',
            'email'=>'aka.josias@cimafsp.sp',
            'password'=> Hash::make('josiasaka13'),
        ]);

    }
}
