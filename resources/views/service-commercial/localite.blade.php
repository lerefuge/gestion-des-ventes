@extends('template.theme')


@section('font-title')

@endsection
@push('scripts')

    <script type="text/javascript">
       function matchCustom(params, data) {
            // If there are no search terms, return all of the data
            if ($.trim(params.term) === '') {
                return data;
            }

            // Do not display the item if there is no 'text' property
            if (typeof data.text === 'undefined') {
                return null;
            }

            // `params.term` should be the term that is used for searching
            // `data.text` is the text that is displayed for the data object
            if (data.text.indexOf(params.term) > -1) {
                var modifiedData = $.extend({}, data, true);
                modifiedData.text += ' (matched)';

                // You can return modified objects from here
                // This includes matching the `children` how you want in nested data sets
                return modifiedData;
            }

            // Return `null` if the term should not be displayed
            return null;
        }

       function matchStart(params, data) {
           // If there are no search terms, return all of the data
           if ($.trim(params.term) === '') {
               return data;
           }

           // Skip if there is no 'children' property
           if (typeof data.children === 'undefined') {
               return null;
           }

           // `data.children` contains the actual options that we are matching against
           var filteredChildren = [];
           $.each(data.children, function (idx, child) {
               if (child.text.toUpperCase().indexOf(params.term.toUpperCase()) == 0) {
                   filteredChildren.push(child);
               }
           });

           // If we matched any of the timezone group's children, then set the matched children on the group
           // and return the group object
           if (filteredChildren.length) {
               var modifiedData = $.extend({}, data, true);
               modifiedData.children = filteredChildren;

               // You can return modified objects from here
               // This includes matching the `children` how you want in nested data sets
               return modifiedData;
           }

           // Return `null` if the term should not be displayed
           return null;
       }

        $(function () {

            $('.selectPicker').select2({
                title: "Liste d'élément ...",
                placeholder: 'Selectionnez un élément dans la liste',
                liveSearch: true,
                width: 'resolve',
                //matcher:matchStart,
                allowClear: true
            });


        });

    </script>

@endpush


@section('content-body')

    <div class="row">
        <div class="col">
            @if($destination->id)
            {!! Form::open(['url'=>route('commercial.edition-localite',$destination->id),'class'=>'form-horizontal','method'=>'PUT']) !!}
                @method('put')
            @else
            {!! Form::open(['url'=>route('commercial.localite'),'class'=>'form-horizontal']) !!}
            @endif
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title"> Gestion des localités </h2>

                </header>
                <div class="card-body">
                    <div class="form-group ">
                        <label class=" control-label"> Localité </label>
                            {!! Form::select('localite_id[]',$localites->pluck('libelle','id'),$destination->id,['class'=>'form-control selectPicker', 'required'=>true,'id'=>'localites','multiple'=>true,
                                    ]
                                     ) !!}


                    </div>
                    <div class="form-group">
                        <label class="control-label"  for="region_id"> Region <span
                                class="required">*</span></label>

                            {!! Form::select('region_id',$regions->pluck('libelle','id'),$destination->region_id,['class'=>'form-control selectPicker', 'required'=>true,'id'=>'','placeholder'=>'Selectionnez une region']) !!}

                    </div>

                </div>
                <footer class="card-footer">
                    <div class="row justify-content-end">
                        <div class="col">
                            @if($destination->id)
                                <button type="submit" class="btn btn-primary" data-toggle="confirmation">Mettre à jour</button>
                            @else
                                <button type="submit" class="btn btn-primary" data-toggle="confirmation">Enregistrer</button>
                                <button type="reset" class="btn btn-default">Effacer</button>
                            @endif
                        </div>
                    </div>
                </footer>
            </section>
            {!! Form::close() !!}

        </div>
    </div>
    <div class="row">
        <div class="col-md-7">
            <div class="row">
                <div class="col">
                    <section class="card">
                        <header class="card-header">
                            <div class="card-actions">
                                <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                            </div>

                            <h2 class="card-title">Localités enregistrées</h2>
                        </header>
                        <div class="card-body">
                            <table class="table table-bordered table-striped mb-0 datatable-default small">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Localité</th>
                                    <th>Region</th>

                                    <th class="">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($destinations as $destination)
                                    <tr>
                                        <td>{{ $loop->index + 1 }}</td>
                                        <td>{{ $destination->libelle }}</td>
                                        <td>{{ $destination->region->libelle ?? null }}</td>
                                        <td class="flex text-center">

                                            <a href="{{ route('commercial.edition-localite',$destination) }}" class="on-default edit-row" ><i
                                                    class="fas fa-pencil-alt"></i></a>

                                            <form id="del_localite_form" action="{{ route('commercial.deletion-localite') }}" method="POST"  class="form-inline center align-content-center">
                                                @csrf
                                                @method('DELETE')
                                                {!! Form::hidden('localite_id',$destination->id) !!}

                                                <button type="submit" class="btn btn-outline-danger  btn-xs" data-toggle="confirmation"> <i
                                                        class="far fa-trash-alt"></i></button>
                                            </form>


                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="row">
                <div class="col">
                    <section class="card">
                        <header class="card-header">
                            <div class="card-actions">
                                <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                            </div>

                            <h2 class="card-title">Localités sans regions</h2>
                        </header>
                        <div class="card-body">
                            <table class="table table-bordered table-striped mb-0 datatable-default">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Localité</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($localites_libres as $libre)
                                    <tr>
                                        <td>{{ $loop->index + 1 }}</td>
                                        <td>{{ $libre->libelle }}</td>

                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>





@endsection
