@extends('template.theme')


@section('font-title')

@endsection
@push('scripts')
    <script type="text/javascript">

        $(function () {
                $('.selectPicker').select2({
                    title: "Liste d'élement ...",
                    header: 'Selectionnez un élement dans la liste',
                    liveSearch: true,
                    width: 'resolve'
                });
        })
        ;

    </script>

@endpush


@section('content-body')

    <div class="row">
        <div class="col-md-5">
            @if($distributeur->id)
            {!! Form::open(['url'=>route('admin.enregistrement-distributeur-edit',$distributeur->id),'class'=>'form-horizontal','method'=>'PUT']) !!}
                @method('put')
            @else
            {!! Form::open(['url'=>route('admin.enregistrement-distributeur'),'class'=>'form-horizontal']) !!}
            @endif
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title"> Enregistrement  distributeurs </h2>

                </header>
                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="row form-group">
                        <div class="col-4">

                            <div class="form-group">
                                <label for="code" class="control-label">CODE : </label>
                                {!! Form::text('code', $distributeur->code ,
                                ['id'=>'code','class'=>'form-control','placeholder'=>"Code Distributeur",'required'=>'required',
                                'required'=>'required']) !!}
                                @error('code')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-8">

                            <div class="form-group">
                                <label class=" control-label text-sm-right pt-2"  for="distributeur_id"> Distributeur <span
                                        class="required">*</span></label>
                                <div >
                                    {!! Form::select('distributeur_id[]',$distributeurs->pluck('nom','id'),$distributeur->id,['class'=>'form-control selectPicker', 'required'=>true,'id'=>'','multiple'=>true,'placeholder'=>'Selectionnez les distributeurs'
                                     ,'show-tick',' data-live-search' => true, 'data-live-search-normalize'=>true,
                                    ]) !!}
                                </div>
                                </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-6">

                            <div class="form-group">
                                <label for="referent" class="control-label">Personne ressource à contacter : </label>
                                {!! Form::text('referent', $distributeur->referent ,
                                ['id'=>'referent','class'=>'form-control','placeholder'=>"Personne à contacter",
                                ]) !!}
                                @error('referent')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col">

                            <div class="form-group">
                                <label for="contacts" class="control-label">Contacts : </label>
                                {!! Form::text('contacts',$distributeur->contacts,
                                ['id'=>'contacts','class'=>'form-control','placeholder'=>'Contacts du distributeur']) !!}
                                @error('contacts')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>

                </div>
                <footer class="card-footer">
                    <div class="row justify-content-end">
                        <div class="col-sm-9">
                            @if($distributeur->id)
                                <button type="submit" class="btn btn-primary" data-toggle="confirmation">Mettre à jour</button>
                            @else
                                <button type="submit" class="btn btn-primary" data-toggle="confirmation">Enregistrer</button>
                                <button type="reset" class="btn btn-default">Effacer</button>
                            @endif
                        </div>
                    </div>
                </footer>
            </section>
            {!! Form::close() !!}

        </div>
        <div class="col-md-7">
            <div class="row">
                <div class="col">
                    <section class="card">
                        <header class="card-header">
                            <div class="card-actions">
                                <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                            </div>

                            <h2 class="card-title">Distributeurs enregistrées</h2>
                        </header>
                        <div class="card-body">
                            <table class="table table-bordered table-striped mb-0 datatable-default">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Code</th>
                                    <th>NOM DISTRIBUTEUR</th>
                                    <th>Reférent</th>
                                    <th>Contact</th>

                                    <th class="">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($distributeurs as $distrib)
                                    <tr>
                                        <td>{{ $loop->index + 1 }}</td>
                                        <td>{{ $distrib->code }}</td>
                                        <td>{{ $distrib->nom }}</td>
                                        <td>{{ $distrib->referent }}</td>
                                        <td>{{ $distrib->contacts }}</td>
                                        <td class="flex text-center">
                                            @hasanyrole('Super Admin|Admins')
                                            <a href="{{ route('admin.enregistrement-distributeur-edit',$distrib) }}" class="on-default edit-row" data-toggle="confirmation"><i
                                                    class="fas fa-pencil-alt"></i></a>
                                            <a href="#" class="on-default remove-row"><i
                                                    class="far fa-trash-alt"></i></a>
                                            @endhasanyrole
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>





@endsection
