<?php

namespace App\Http\Controllers;

use App\Repositories\PeseeRepository;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Builder;
class CommercialController extends Controller
{
    /**
     * @var Builder
     */
    private $builder;
    /**
     * @var PeseeRepository
     */
    private $peseeRepository;

    public function __construct(Builder $builder,PeseeRepository $peseeRepository)
    {

        $this->builder = $builder;
        $this->peseeRepository = $peseeRepository;
    }

    public function venteMensuelles(Request $request)
    {
         $month = Carbon::now()->subMonths(1);
        $roots= $month->format('Y-m');
        $nbre_of_days = $month->daysInMonth;
        $start = $roots.'-01';
        $end = $roots.'-'.$nbre_of_days;
        $ventes = $this->_ventes($start,$end)->get();
      // dd($ventes);
        try {
            dd(DataTables::of($ventes));

        } catch (\Exception $e) {
            dd($e->getMessage());
        }
        $data['tableHtml'] =  $this->builder->columns([

            ['data' => 'ciment', 'footer' => 'Ciment'],
            ['data' => 'expedie', 'footer' => 'Expediés'],
    ]);

        return view('service-commercial.vente-mensuel')->with($data);
    }

    public function _ventes(string $periodStart, string $periodEnd=null)
    {
       $query =  $this->peseeRepository->pesee()
        ->select(
            "produits.libelle AS ciment"
        )
        ->selectRaw(DB::raw('SUM(pesees.qte_bl) AS expedie'))
        ->groupBy('produits.libelle')
        ->join('produits', 'produits.id', '=', 'pesees.produit_id');
       if (!is_null( $periodEnd)){
           $query = $query->whereBetween('date_pesee',[$periodStart,$periodEnd]);
       }else{
           $query = $query->where('date_pesee', '=', $periodStart);
       }
       return $query ;
    }
}
