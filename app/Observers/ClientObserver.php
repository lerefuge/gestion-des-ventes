<?php

namespace App\Observers;

use App\Client;
use App\Repositories\DestinationRepository;
use App\Repositories\DistributeurRepository;
use App\Repositories\RegionRepository;

class ClientObserver
{

    /**
     * @var RegionRepository
     */
    private $regionRepository;
    /**
     * @var DestinationRepository
     */
    private $destinationRepository;
    /**
     * @var DistributeurRepository
     */
    private $distributeurRepository;

    /**
     * ClientObserver constructor.
     * @param RegionRepository $regionRepository
     * @param DestinationRepository $destinationRepository
     * @param DistributeurRepository $distributeurRepository
     */
    public function __construct(RegionRepository $regionRepository, DestinationRepository $destinationRepository, DistributeurRepository $distributeurRepository)
    {

        $this->regionRepository = $regionRepository;
        $this->destinationRepository = $destinationRepository;
        $this->distributeurRepository = $distributeurRepository;
    }
    /**
     * Handle the client "created" event.
     *
     * @param  \App\Client  $client
     * @return void
     */
    public function created(Client $client)
    {

        $this->_generateCLientMatricule($client);
    }

    /**
     * Handle the client "updated" event.
     *
     * @param  \App\Client  $client
     * @return void
     */
    public function updated(Client $client)
    {
        $region = $this->regionRepository->region()->find($client->region_id)->id;
        $destination = $this->destinationRepository->destination()->find($client->destination_id)->id;
        $distributeur = $this->distributeurRepository->distributeur()->find($client->distributeur_id)->id;
       // dd($client);

        if ($client->id < 9) {
            $client_id = "000{$client->id}";
        }elseif ($client->id > 9 && $client->id <100) {
            $client_id = "00{$client->id}";
        } else {
            $client_id = $client->id;
        }
        $region_id = $region < 9  ? "0{$region}" : $region;
        $destination_id = $destination < 9  ? "0{$destination}" : $destination;
        $distributeur_id = $distributeur < 9  ? "0{$distributeur}" : $distributeur;
        $client->matricule ="{$region_id}-{$destination_id}-{$distributeur_id}-$client_id";
    }

    public function saving(Client $client)
    {
       // dd($client);
        $region = $this->regionRepository->region()->find($client->region_id)->id;
        $destination = $this->destinationRepository->destination()->find($client->destination_id)->id;
        $distributeur = $this->distributeurRepository->distributeur()->find($client->distributeur_id)->id;


        if ($client->id < 9) {
            $client_id = "000{$client->id}";
        }elseif ($client->id > 9 && $client->id <100) {
            $client_id = "00{$client->id}";
        } else {
            $client_id = $client->id;
        }
        $region_id = $region < 9  ? "0{$region}" : $region;
        $destination_id = $destination < 9  ? "0{$destination}" : $destination;
        $distributeur_id = $distributeur < 9  ? "0{$distributeur}" : $distributeur;
        $client->matricule ="{$region_id}-{$destination_id}-{$distributeur_id}-$client_id";
       // $client->update();
    }

    /**
     * Handle the client "deleted" event.
     *
     * @param  \App\Client  $client
     * @return void
     */
    public function deleted(Client $client)
    {
        //
    }

    /**
     * Handle the client "restored" event.
     *
     * @param  \App\Client  $client
     * @return void
     */
    public function restored(Client $client)
    {
        //
    }

    /**
     * Handle the client "force deleted" event.
     *
     * @param  \App\Client  $client
     * @return void
     */
    public function forceDeleted(Client $client)
    {
        //
    }

    private function _generateCLientMatricule (Client $client){
        $region = $this->regionRepository->region()->find($client->region_id)->id;
        $destination = $this->destinationRepository->destination()->find($client->destination_id)->id;
        $distributeur = $this->distributeurRepository->distributeur()->find($client->distributeur_id)->id;
       // dd($destination);
        if ($client->id < 9) {
            $client_id = "000{$client->id}";
        }elseif ($client->id > 9 && $client->id <100) {
            $client_id = "00{$client->id}";
        } else {
            $client_id = $client->id;
        }
        $region_id = $region < 9  ? "0{$region}" : $region;
        $destination_id = $destination < 9  ? "0{$destination}" : $destination;
        $distributeur_id = $distributeur < 9  ? "0{$distributeur}" : $distributeur;
        $client->matricule ="{$region_id}-{$destination_id}-{$distributeur_id}-$client_id";
        $client->save();
    }
}
