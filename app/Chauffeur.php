<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Chauffeur extends Model
{
    protected $guarded=['id'];


    public function setNomAttribute($value)
    {
        return $this->attributes['nom'] = STR::upper($value);
    }
}
