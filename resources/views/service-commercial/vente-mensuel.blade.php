@extends('template.theme')


@section('font-title')

@endsection

@section('css-libs')

@endsection
@push('scripts')

    <script type="text/javascript">
        (function () {

            'use strict';

            // basic

            // Select 2 Fields
            $('select[data-plugin-selectTwo]').on('change', function () {
                $(this).valid();
            });
            $('.selectPicker').select2({
                title: "Liste d'élement ...",
                header: 'Selectionnez un élement dans la liste',
                liveSearch: true,
                width: 'resolve'
            });
            $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
                localStorage.setItem('activeTab', $(e.target).attr('href'));
            });
            var activeTab = localStorage.getItem('activeTab');
            if (activeTab) {
                $('#navTabs a[href="' + activeTab + '"]').tab('show');
            }

        }).apply(this, [jQuery]);

    </script>
    {!! $tableHtml->scripts() !!}
@endpush


@section('content-body')

    <section class="card ">
        <header class="card-header">
            <div class="card-actions">
                <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
            </div>

            <h2 class="card-title">Title</h2>
        </header>
        <div class="card-body">
            {!! $tableHtml->table(['class' => 'table table-bordered'], true) !!}
        </div>
    </section>


@endsection
