<?php


namespace App\Repositories;


use App\Client;

class ClientRepository
{
    /**
     * @var Client
     */
    private $client;

    /**
     * ClientRepository constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {

        $this->client = $client;
    }

    public function client()
    {
        return $this->client->newQuery();}
}
