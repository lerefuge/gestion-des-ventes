<?php


namespace App\Repositories;


use App\Destination;

class DestinationRepository
{
    /**
     * @var Destination
     */
    private $destination;

    /**
     * DestinationRepository constructor.
     * @param Destination $destination
     */
    public function __construct(Destination $destination)
    {

        $this->destination = $destination;
    }

    public function destination()
    {
        return $this->destination->newQuery();
    }

}
