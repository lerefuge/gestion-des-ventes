<?php


namespace App\Repositories;


use App\District;

class DistrictRepository
{

    /**
     * @var District
     */
    private $district;

    /**
     * DistrictRepository constructor.
     * @param District $district
     */
    public function __construct(District $district)
    {

        $this->district = $district;
    }

    public function district()
    {
        return $this->district->newQuery();
    }
}
