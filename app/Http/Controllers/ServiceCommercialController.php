<?php

namespace App\Http\Controllers;

use App\Distributeur;
use App\Region;
use App\Repositories\ChefLieuDistrictRepository;
use App\Repositories\DestinationRepository;
use App\Repositories\DistributeurRepository;
use App\Repositories\PeseeRepository;
use App\Repositories\RegionRepository;
use App\Traits\ExportableGraphData;
use App\Traits\SnipetsTraits;
use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Http\Request;


class ServiceCommercialController extends Controller
{

    use SnipetsTraits, ExportableGraphData;
    /**
     * @var Carbon
     */
    private $date;

    /**
     * @var Carbon
     */
    private $yesterday;

    /**
     * @var PeseeRepository
     */
    private $peseeRepository;
    /**
     * @var Request
     */
    private $period;
    /**
     * @var ChefLieuDistrictRepository
     */
    private $chefLieuDistrictRepository;
    /**
     * @var DestinationRepository
     */
    private $destinationRepository;
    /**
     * @var RegionRepository
     */
    private $regionRepository;
    /**
     * @var DistributeurRepository
     */
    private $distributeurRepository;

    /**
     * ServiceCommercialController constructor.
     * @param PeseeRepository $peseeRepository
     * @param ChefLieuDistrictRepository $chefLieuDistrictRepository
     * @param DestinationRepository $destinationRepository
     * @param RegionRepository $regionRepository
     * @param DistributeurRepository $distributeurRepository
     */
    public function __construct(PeseeRepository $peseeRepository,
                                ChefLieuDistrictRepository $chefLieuDistrictRepository,
                                DestinationRepository $destinationRepository,
                                RegionRepository $regionRepository,
                                DistributeurRepository $distributeurRepository)
    {

        $this->date = now();
        $this->yesterday = $this->date->subDay();
        $this->peseeRepository = $peseeRepository;
        $this->chefLieuDistrictRepository = $chefLieuDistrictRepository;
        $this->destinationRepository = $destinationRepository;
        $this->regionRepository = $regionRepository;
        $this->distributeurRepository = $distributeurRepository;
    }
    /**
     * @param string $date
     * @param string $dateFormat
     * @return string
     */
    private function parsedDate(string  $date, $dateFormat = 'Y-m-d')
    {
        return Carbon::parse($date)->format($dateFormat);
    }
    public function recherche()
    {
        $data['zones'] = $this->chefLieuDistrictRepository->chefLieuDistrict();
        return view('service-commercial.recherche-pesee')->with($data);
    }

    public function recherchePesee(Request $request)
    {
        $case = $request->case;
        $data['case'] = $case;
        $data['zones'] = $this->chefLieuDistrictRepository->chefLieuDistrict();
        if ($case == 'numero_bl') {
            $pesee_id = $request->get('pesee_id');
            $validator = validator($request->all(), ['pesee_id' => 'required|integer'], ['pesee_id.required' => 'Veuillez selectionner le numero du BL']);
            if ($validator->fails()) {
                $this->_redirectWhenFail("Veuillez corriger le erreurs SVP !", 'warning', $validator);
            }
            $result = $this->peseeRepository->pesee()->where('pesees.id', '=', $pesee_id)
                ->with(['produit', 'distributeur', 'destination', 'vehicule', 'transporteur'])
                ->get();
            if ($result) {
                $data['expedies'] = $this->peseeRepository->pesee()->where('pesees.id', '=', $pesee_id)->get();
                return view('service-commercial.recherche-pesee')->with($data);
            } else {
                flash('Desolé, aucune entrée trouvées pour cette date', 'info');
                return redirect()->back();
            }
        } elseif ($case == 'period') {
            $date = Carbon::parse($request->date_jour)->format('Y-m-d');
            $data['date_pesee'] = $date;
            $validator = validator($request->all(), ['date_jour' => 'required'], ['date_jour.required' => 'Veuillez selectionner la date du jour']);
            if ($validator->fails()) {
                $this->_redirectWhenFail("Veuillez corriger le erreurs SVP !", 'warning', $validator);
            }
            $result = $this->peseeRepository->pesee()->where('pesees.date_pesee', '=', $date)
                ->with(['produit', 'distributeur', 'destination', 'vehicule', 'transporteur'])
                ->get();
            if ($result) {
                $data['expedies'] = $result;
                return view('service-commercial.recherche-pesee')->with($data);
            } else {
                flash('Desolé, aucune entrée trouvées pour cette date', 'info');
                return redirect()->back();
            }
        } else
            return view('service-commercial.recherche-pesee')->with($data);
    }

    public function pesee()
    {
        $date = $this->parsedDate($this->yesterday);
        $day = $this->yesterday->format('d/m/Y');
        $data['zone_id'] =$zone_id = null;
        $data['zones'] = $this->chefLieuDistrictRepository->chefLieuDistrict();
        $data['data_table_url'] = route('datatable.vente-jour', ['datetime'=>strtotime($date)]);
        $data['periode'] = " Point des ventes de la journée du {$day} ";
        $data['expedies'] = $this->peseeRepository->expedies($date, $date,$zone_id);
        return view('service-commercial.pesee')->with($data);
    }



    public function consultation(Request $request)
    {
        $this->period = $request->case;
        $data['zone_id'] = $zone_id = $request->zone_id;
        switch ($this->period):
            case  'daily';
                $date = $this->parsedDate($this->yesterday);
                $day = $this->yesterday->format('d/m/Y');
                $data['periode'] = " Point des ventes de la journée du {$day} ";
                $data['data_table_url'] = route('datatable.vente-jour', [ 'datetime'=>strtotime($date)]);
                $data['expedies'] = $this->peseeRepository->expedies($date, $date, $zone_id);
                break;
            case  'period';
                $start_period = $this->parsedDate($request->start, 'Y-m-d');
                $end_period = $this->parsedDate($request->end, 'Y-m-d');
                $data['data_table_url'] = route('datatable.vente-periode', ['debut' => strtotime($start_period), 'fin' => strtotime($end_period)]);
                $start = $this->parsedDate($request->start, 'd/m/Y');
                $fin = $this->parsedDate($request->end, 'd/m/Y');
                $data['periode'] = " Point des ventes de la période du {$start} au {$fin} ";
                $data['expedies'] = $this->peseeRepository->expedies($start_period, $end_period, $zone_id);
                break;
            case  'monthly';
                $date_send = explode('/', $request->date_month);
                $correct_date = $date_send[1] . '-' . $date_send['0'] . '-01';
                $month = $correct_date;
                $date_month = Carbon::parse($month)->firstOfMonth()->format('Y-m-d');
                $start_month = $date_month;
                $end_month = Carbon::parse($start_month)->endOfMonth()->format('Y-m-d');
                $data['data_table_url'] = route('datatable.vente-mensuelle', ['debut' => strtotime($start_month), 'fin' => strtotime($end_month)]);
                $start = Carbon::parse($start_month)->format('d/m/Y');
                $fin = Carbon::parse($end_month)->format('d/m/Y');
                $data['periode'] = " Point des ventes de la période du {$start} au {$fin} ";
                $data['expedies'] = $this->peseeRepository->expedies($start_month, $end_month, $zone_id);
                break;
            case  'yearly';
                $start_year = "01-01-" . $request->date_year;
                $year = Carbon::parse($start_year);
                $date_year_start = $year->startOfYear()->format('Y-m-d');
                $date_year_end =$year->endOfYear()->format('Y-m-d');              
                $data['data_table_url'] = route('datatable.vente-annuelle', ['debut' => strtotime($date_year_start), 'fin' => strtotime($date_year_end)]);
                $start = Carbon::parse($date_year_start)->format('d/m/Y');
                $fin = Carbon::parse($date_year_end)->format('d/m/Y');
                $data['periode'] = " Point des ventes de la période du {$start} au {$fin} ";
                $data['expedies'] = $this->peseeRepository->expedies($date_year_start, $date_year_end, $zone_id);
                break;
            default:
                $date = Carbon::parse($request->date_jour)->format('Y-m-d');
                $day = Carbon::parse($date)->format('d/m/Y');
                $data['periode'] = " Point des ventes de la journée du {$day} ";
                $data['data_table_url'] = route('datatable.vente-jour', strtotime($date));
                $data['expedies'] = $this->peseeRepository->expedies($date, $date, $zone_id);
        endswitch;
        $data['zones'] = $this->chefLieuDistrictRepository->chefLieuDistrict();
        return view('service-commercial.pesee')->with($data);
    }

    public function resumeDesVentes()
    {
        $data['expedies'] = $this->_tableauResumeVentes();
        $data['zones'] = $this->chefLieuDistrictRepository->chefLieuDistrict();
        return view('service-commercial.tableau-resume-ventes')->with($data);
    } 
    
    public function resumeDesVentesJournaliere()
    {
        $data['nbr_jours'] = $this->date->daysInMonth;
        $data['month'] = $this->date->format('m');
        $data['year'] =  $this->date->format('Y');
        // $data['zones'] = $this->chefLieuDistrictRepository->chefLieuDistrict();
        return view('service-commercial.tableau-resume-ventes-journalieres')->with($data);
    }
    
    public function resumeDesVentesJournalierePost(Request $request)
    {
        $date_send = explode('/', $request->date_month);
        $correct_date = $date_send[1] . '-' . $date_send['0'] . '-01';
        $month = $correct_date;
        $date = Carbon::parse($month);
        $data['nbr_jours'] = $date->daysInMonth;
        $data['month'] = $date->format('m');
        $data['year'] =  $this->date->format('Y');
        //$data['zones'] = $this->chefLieuDistrictRepository->chefLieuDistrict();
        return view('service-commercial.tableau-resume-ventes-journalieres')->with($data);
    }

    public function pointResumeDesVentes(Request $request)
    {
        $year = $request->date_year ?? null;
        $zone_id = $request->zone_id;
        $data['expedies'] = $this->_tableauResumeVentes($year,$zone_id);
        $data['zones'] = $this->chefLieuDistrictRepository->chefLieuDistrict();
        return view('service-commercial.tableau-resume-ventes')->with($data);
    }

    private function _tableauResumeVentes($year = null,array $destination_id=null)
    {
        if ($year == null) {
            $year = $this->date->format('Y');
        }
        $arr = [];
        for ($i = 1; $i <= 12; $i++) {
            $i = ($i < 10) ? "0{$i}" : "{$i}";
            $arr[$i] = [
                'sacs' => [
                    '32_5' => $this->_expedie("{$i}", [2], $year,$destination_id)->sum(),// CPJ 32.5
                    '42_5' => $this->_expedie("{$i}", [1], $year,$destination_id)->sum(),// CPJ 42.5
                    'robusto' => $this->_expedie("{$i}", [3], $year,$destination_id)->sum(),// Robusto
                    'chf' => 0,// CHF
                    'total_sac' => $this->_expedie("{$i}", [1, 2, 3], $year,$destination_id)->sum()

                ],
                'vracs' => [
                    '32_5' => 0,// CPJ 32.5
                    '42_5' => $this->_expedie("{$i}", [4], $year,$destination_id)->sum(),// CPJ 42.5
                    'robusto' => $this->_expedie("{$i}", [5], $year,$destination_id)->sum(),// Robusto
                    'chf' => $this->_expedie("{$i}", [6], $year,$destination_id)->sum(),// CHF
                    'total_vrac' => $this->_expedie("{$i}", [4, 5, 6], $year,$destination_id)->sum()
                ],
            ];

        }
        return $arr;

    }

    /**
     * @param $month
     * @param array $produit_id
     * @param null $year
     * @param null $destination_id
     * @return \App\Pesee|\App\Pesee[]|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Query\Builder|\Illuminate\Database\Query\Builder[]|\Illuminate\Support\Collection
     */
    private function _expedie($month, array $produit_id, $year = null, $destination_id = null)
    {
        $query = $this->peseeRepository->pesee()
            ->join('produits', 'produits.id', '=', 'pesees.produit_id')
            ->groupBy('pesees.date_pesee');
        if (!is_null($year)) {
            $query = $query->whereYear('pesees.date_pesee', $year);
        } else {
            $query = $query->whereYear('pesees.date_pesee', now()->format('Y'));
        }

        if (!is_null($destination_id) && !empty($destination_id)) {
            $query = $query->join('destinations', 'destinations.id', '=', 'pesees.destination_id')
                ->join('regions', 'destinations.region_id', '=', 'regions.id')
                ->join('chef_lieu_districts', 'regions.chef_lieu_id', '=', 'chef_lieu_districts.id')
                ->whereIn('pesees.destination_id', $destination_id);
        }
        $query = $query->whereMonth('pesees.date_pesee', $month)
            ->whereIn('produits.id', $produit_id)
            ->addSelect(DB::raw('SUM(qte_bl) AS expedies'))
            ->orderBy('pesees.date_pesee')->get('expedies')->map(function ($q) {
                return round($q->expedies, 2);
            });
        return $query;
    }

    public function localites()
    {
        $data['destination'] = new Region();
        $destination = $this->destinationRepository->destination()->with('region');
        $localites = $this->destinationRepository->destination();
        $data['regions'] = $this->regionRepository->region()->orderBy('libelle');
        $data['localites'] = $localites;
        $data['localites_libres'] = $localites->doesntHave('region')->get();
        $data['destinations'] = $destination->get()->sortBy('libelle');
        return view('service-commercial.localite')->with($data);

    }

    public function enregistrementLocalites(Request $request)
    {
        $validator = validator($request->all(), [
            'localite_id.*' => 'required|integer',
            'region_id' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->_redirectWhenFail("Veuillez corriger le erreurs SVP !", 'warning', $validator);
        }
        DB::beginTransaction();
        try {
            $d = [
                'region_id' => $request->region_id
            ];
            if ($destination = $this->destinationRepository->destination()->whereIn('id', $request->localite_id)) {
                $destination->update($d);
                flash('Localité enregistrée', 'success');
                DB::commit();
                return redirect()->route('commercial.localite');
            }
            flash('Echec enregistrement de la localite', 'warning');
            DB::rollBack();
            return redirect()->route('commercial.localite');
        } catch (Exception $exception) {
            DB::rollBack();
            flash('Echec enregistrement de la localité', 'info');
            return redirect()->back();
        }
    }

    public function editionLocalites(int $localite)
    {
        $destination = $this->destinationRepository->destination()->with('region');
        $localites = $this->destinationRepository->destination()->get();
        $data['regions'] = $this->regionRepository->region()->orderBy('libelle');
        $data['localites_libres'] = $localites;
        $data['localites'] = $localites;
        $data['destinations'] = $destination->get()->sortBy('libelle');
        $data['destination'] = $destination->find($localite);
        return view('service-commercial.localite')->with($data);
    }

    public function updateLocalites(Request $request, int $localite)
    {
        $destination = $this->destinationRepository->destination()->find($localite);
        $validator = validator($request->all(), [
            'localite_id.*' => 'required|integer',
            'region_id' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->_redirectWhenFail("Veuillez corriger le erreurs SVP !", 'warning', $validator);
        }
        \DB::beginTransaction();
        try {
            $d = ['region_id' => $request->region_id];
            if ($destination->update($d)) {
                flash('Localité enregistrée', 'success');
                DB::commit();
                return redirect()->route('commercial.localite');
            }
            flash('Echec enregistrement de la localité', 'warning');
            DB::rollBack();
            return redirect()->route('commercial.localite');
        } catch (Exception $exception) {
            DB::rollBack();
            flash('Echec enregistrement de la localite', 'info');
            return redirect()->back();
        }
    }

    public function deletionLocalites(Request $request)
    {
        $localite = $request->localite_id;
        $destination = $this->destinationRepository->destination()->where('id', '=', $localite);

        if ($destination) {
            $destination->delete();
            flash('Localité supprimée', 'info');
        }
        return redirect()->route('commercial.localite');
    }

    public function distributeur()
    {
        $data['distributeur'] = new Distributeur();
        $data['distributeurs'] = $this->distributeurRepository->distributeur()->get();
        return view('service-commercial.distributeur')->with($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws Exception
     */
    public function enregistrementDistributeur(Request $request)
    {
        $validator = validator($request->all(), [
            'code' => 'required',
            'distributeur_id.*' => 'required',
            'contacts' => 'nullable',
            'referent' => 'nullable',
        ], [
            'code.required' => 'Precisez le code du distributeur',
            'nom.required' => 'Precisez le nom du distributeur',
            'contacts.required' => 'Precisez le/les contacts du distributeur',
        ]);

        if ($validator->fails()) {
            return $this->_redirectWhenFail($validator);
        }
        DB::beginTransaction();
        try {
            $d = [
                'code' => $request->code,
                'contacts' => $request->contacts,
                'referent' => $request->referent
            ];
            $distibuteurs = $this->distributeurRepository->distributeur()->whereIn('id', $request->distributeur_id);
            if ($distibuteurs) {
                $distibuteurs->update($d);
                flash('Distributeur enregistrée', 'success');
                DB::commit();
            }
            return redirect()->route('commercial.distributeur');
        } catch (Exception $exception) {
            DB::rollBack();
            flash('Echec enregistrement du distributeur', 'warning');
            return redirect()->back();
        }
    }

    public function editionDistributeur(int $distributeur_id)
    {
        $data['distributeur'] = $this->distributeurRepository->distributeur()->where('id', '=', $distributeur_id)->first();
        $data['distributeurs'] = $this->distributeurRepository->distributeur()->get()->sortBy('nom');
        return view('service-commercial.distributeur')->with($data);
    }

    public function updateDistributeur(Request $request, int $distributeur_id)
    {
        $distributeur = $this->distributeurRepository->distributeur()->find($distributeur_id);
        $validator = validator($request->all(), [
            'code' => 'required',
            'distributeur_id.*' => 'required',
            'contacts' => 'required',
            'referent' => 'nullable',
        ], [
                'code.required' => 'Precisez le code du distributeur',
                'nom.required' => 'Precisez le nom du distributeur',
                'contacts.required' => 'Precisez le/les contacts du distributeur',
            ]
        );

        if ($validator->fails()) {
            return $this->_redirectWhenFail($validator);
        }
        \DB::beginTransaction();
        try {
            $d = [
                'code' => $request->code,
                'contacts' => $request->contacts,
                'referent' => $request->referent
            ];
            $distibuteurs = $this->distributeurRepository->distributeur()->whereIn('id', $request->distributeur_id);
            if ($distibuteurs) {
                $distibuteurs->update($d);
                flash('Distributeur enregistrée', 'success');
                DB::commit();
                return redirect()->route('commercial.distributeur');
            }
            flash('Echec enregistrement du distributeur', 'warning');
            DB::rollBack();
            return redirect()->route('commercial.distributeur');
        } catch (Exception $exception) {
            DB::rollBack();
            flash('Echec enregistrement du distributeur', 'warning');
            return redirect()->back();
        }
    }

    public function deleteDistributeur(Request $request)
    {
        $distributeur = $request->distributeur_id;
        $distrib = $this->distributeurRepository->distributeur()->where('id', '=', $distributeur);

        if ($distrib) {
            $distrib->delete();
            flash('Distributeur supprimé', 'info');
        }
        return redirect()->route('commercial.distributeur');
    }
}
