<ul class="nav nav-main">
    <li>
        <a class="nav-link {{ setActive(['index']) }}" href="{{ route('application.index') }}">
            <i class="fas fa-home" aria-hidden="true"></i>
            <span> Tableau de bord </span>
        </a>
    </li>
{{--    <li class="nav-parent">--}}
{{--        <a class="nav-link" href="#">--}}
{{--            <i class="fas fa-align-left" aria-hidden="true"></i>--}}
{{--            <span>Menu Levels</span>--}}
{{--        </a>--}}
{{--        <ul class="nav nav-children">--}}
{{--            <li>--}}
{{--                <a>--}}
{{--                    First Level--}}
{{--                </a>--}}
{{--            </li>--}}
{{--            <li class="nav-parent">--}}
{{--                <a class="nav-link" href="#">--}}
{{--                    Second Level--}}
{{--                </a>--}}
{{--                <ul class="nav nav-children">--}}
{{--                    <li>--}}
{{--                        <a>--}}
{{--                            Second Level Link #1--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li>--}}
{{--                        <a>--}}
{{--                            Second Level Link #2--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li class="nav-parent">--}}
{{--                        <a class="nav-link" href="#">--}}
{{--                            Third Level--}}
{{--                        </a>--}}
{{--                        <ul class="nav nav-children">--}}
{{--                            <li>--}}
{{--                                <a>--}}
{{--                                    Third Level Link #1--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <a>--}}
{{--                                    Third Level Link #2--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--            </li>--}}
{{--        </ul>--}}
{{--    </li>--}}
    @hasanyrole('Adminstration|Sce Commercial')
    <li class="nav-parent">
        <a class="nav-link" href="#">
            <i class="fas fa-align-left" aria-hidden="true"></i>
            <span>Département commercial</span>
        </a>
        <ul class="nav nav-children">
            <li>
                <a href="{{ route('commercial.recherche') }}">
                    <i class="fas fa-search" aria-hidden="true"></i>
                    Recherches
                </a>
            </li>
            <li>
                <a href="{{ route('commercial.pesee') }}">
                    <i class="fas fa-search-plus" aria-hidden="true"></i>
                    Consulter les pesées
                </a>
            </li>

            <li class="nav-parent">
                <a class="nav-link" href="#">
                    <i class="fas fa-business-time" aria-hidden="true"></i>
                    Analyses des données
                </a>
                <ul class="nav nav-children">
                    <li>
                        <a href="{{ route('commercial.synthese') }}">
                            Synthese Données commerciales
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('commercial.tableau-ventes') }}">
                            Données commerciales mesuelles
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('commercial.tableau-ventes-journaliere') }}">
                            Données commerciales journalières
                        </a>
                    </li>
{{--                    <li>--}}
{{--                        <a>--}}
{{--                            Second Level Link #2--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li class="nav-parent">--}}
{{--                        <a class="nav-link" href="#">--}}
{{--                            Classement--}}
{{--                        </a>--}}
{{--                        <ul class="nav nav-children">--}}
{{--                            <li>--}}
{{--                                <a>--}}
{{--                                    Distributeur--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <a>--}}
{{--                                    Third Level Link #2--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </li>--}}
                </ul>
            </li>
            <li class="nav-parent">
                <a class="nav-link" href="#">
                    <i class="fas fa-save" aria-hidden="true"></i>
                    Enregistrements
                </a>
                <ul class="nav nav-children">
                    <li>
                        <a href="{{ route('commercial.localite') }}">
                            Localités
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('commercial.distributeur') }}">
                            Distributeur
                        </a>
                    </li>

                </ul>
            </li>
        </ul>
    </li>
    @endhasanyrole
    @hasanyrole('Adminstration|Expédition')
    <li class="nav-parent">
        <a class="nav-link" href="#">
            <i class="fas fa-align-left" aria-hidden="true"></i>
            <span>Expéditions</span>
        </a>
        <ul class="nav nav-children">
            <li>
                <a href="{{ route('expedition.recherche') }}">
                    <i class="fas fa-search" aria-hidden="true"></i>
                    Recherches
                </a>
            </li>
            <li>
                <a href="{{ route('expedition.expedies') }}">
                    <i class="fas fa-search-plus" aria-hidden="true"></i>
                    Consulter les expédiés
                </a>
            </li><li>
                <a href="{{ route('expedition.tolerance-camions') }}">
                    <i class="fas fa-search-plus" aria-hidden="true"></i>
                    Camions hors tolerance
                </a>
            </li>
        </ul>
    </li>
    @endhasanyrole
    @hasanyrole('Adminstration')
    <li>
        <a class="nav-link " href="{{ route('gestion.recherche') }}">
            <i class="fas fa-search" aria-hidden="true"></i>
            <span> Recherche  </span>
        </a>
    </li>
    <li>
        <a class="nav-link " href="{{ route('vente.index') }}">
            <i class="fas fa-money-bill" aria-hidden="true"></i>
            <span> Ventes  </span>
        </a>
    </li>
    <li>
        <a class="nav-link " href="{{ route('pesee.journaliere') }}">
            <i class="fas fa-calendar-day" aria-hidden="true"></i>
            <span> Pesée Journaliere  </span>
        </a>
    </li>


    <li class="nav-parent  ">
        <a class="nav-link" href="#">
            <i class="fas fa-table" aria-hidden="true"></i>
            <span>Détails</span>
        </a>
        <ul class="nav nav-children">

            <li>
                <a class="nav-link" href="{{ route('detail.localite') }}">
                    Localite
                </a>
            </li>
        </ul>
    </li>

    <li class="nav-parent">
        <a class="nav-link" href="#">
            <i class="fas fa-chart-area" aria-hidden="true"></i>
            <span>Service Commercial</span>
        </a>
        <ul class="nav nav-children">
            <li>
                <a class="nav-link" href="{{route('sce-commercial.vente-mensuelle')}}">
                    Ventes Mensuelles
                </a>
            </li>


        </ul>
    </li>
{{--    @hasanyrole('Super Admin|Admins|Superviseur')--}}
    <li class="nav-parent  ">
        <a class="nav-link" href="#">
            <i class="fas fa-copy" aria-hidden="true"></i>
            <span>Chargement de fichier</span>
        </a>
        <ul class="nav nav-children">

            <li>
                <a class="nav-link" href="{{ route('gestion.load-file') }}">
                    Table de pesees
                </a>
            </li>
            <li>
                <a class="nav-link" href="{{ route('gestion.load-table-tolerance') }}">
                    Table de tolerance
                </a>
            </li>
            <li>
                <a class="nav-link" href="{{ route('gestion.load-mass-datas') }}">
                    Chargement mensuel
                </a>
            </li>
        </ul>
    </li>
{{--    @endhasanyrole--}}

    <li>
        <a class="nav-link " href="{{ route('gestion.recherche') }}">
            <i class="fas fa-search" aria-hidden="true"></i>
            <span> Pesées Journalieres  </span>
        </a>
    </li>

    <li class="nav-parent  ">
        <a class="nav-link" href="#">
            <i class="fas fa-list-alt" aria-hidden="true"></i>
            <span>Historiques</span>
        </a>
        <ul class="nav nav-children">
            <li>
                <a class="nav-link" href="{{ route('historique.transporteur') }}">
                    Transporteur
                </a>
            </li>
        </ul>
    </li>

    <li class="nav-parent  ">
        <a class="nav-link" href="#">
            <i class="fas fa-copy" aria-hidden="true"></i>
            <span>Points</span>
        </a>
        <ul class="nav nav-children">

            <li>
                <a class="nav-link" href="{{ route('point.distributeur') }}">
                    Distributeur
                </a>
            </li>
            <li>
                <a class="nav-link" href="{{ route('point.transporteur') }}">
                    Transporteur
                </a>
            </li>
            <li>
                <a class="nav-link" href="{{ route('point.localite') }}">
                    Localite
                </a>
            </li>
        </ul>
    </li>
{{--    @hasanyrole('Super Admin|Admins|Superviseur')--}}
    <li class="nav-parent  ">
        <a class="nav-link" href="#">
            <i class="fas fa-copy" aria-hidden="true"></i>
            <span>Administration</span>
        </a>
        <ul class="nav nav-children">
            <li>
                <a class="nav-link" href="{{ route('admin.enregistrement-produit') }}">
                    Qualité de Ciment
                </a>
            </li>
            <li>
                <a class="nav-link" href="{{ route('admin.enregistrement-district') }}">
                    Enregistrement des districts
                </a>
            </li>
            <li>
                <a class="nav-link" href="{{ route('admin.enregistrement-chef-lieu-district') }}">
                    Enregistrement chef lieu de districts
                </a>
            </li>
            <li>
                <a class="nav-link" href="{{ route('admin.enregistrement-region') }}">
                    Region
                </a>
            </li>
            <li>
                <a class="nav-link" href="{{ route('admin.enregistrement-type_vehicule') }}">
                    Type de vehicule
                </a>
            </li>
            <li>
                <a class="nav-link" href="{{ route('admin.enregistrement-destination') }}">
                    Localités
                </a>
            </li>
            <li>
                <a class="nav-link" href="{{ route('admin.enregistrement-couts') }}">
                    Coûts ciments
                </a>
            </li>
            <li>
                <a class="nav-link" href="{{ route('admin.enregistrement-distributeur') }}">
                    Gerer les distributeurs
                </a>
            </li>
            <li>
                <a class="nav-link" href="{{ route('admin.enregistrement-transporteur') }}">
                    Gerer les transporteurs
                </a>
            </li>
            <li>
                <a class="nav-link" href="{{ route('admin.enregistrement-client') }}">
                    Gerer les clients
                </a>
            </li>
{{--            @hasanyrole('Super Admin|Admins')--}}
            <li>
                <a class="nav-link" href="{{ route('admin.enregistrement-user') }}">
                    Gerer les utilisateurs
                </a>
            </li>
{{--            @endhasanyrole--}}
{{--            @hasanyrole('Super Admin')--}}
            <li>
                <a class="nav-link" href="{{ route('admin.enregistrement-roles') }}">
                    Enregistrement roles
                </a>
            </li>
            <li>
                <a class="nav-link" href="{{ route('admin.enregistrement-permissions') }}">
                    Enregistrement permissios
                </a>
            </li>
            <li>
                <a class="nav-link" href="{{ route('admin.user-roles') }}">
                    Gerer les roles utilisateurs
                </a>
            </li>

            <li>
                <a class="nav-link" href="{{ route('admin.enregistrement-budget-annuel') }}">
                    Enregistrement budget Annuel
                </a>
            </li>
{{--            @endhasanyrole--}}
        </ul>
    </li>
{{--    @endhasanyrole--}}

    <li class="nav-parent">
        <a class="nav-link" href="#">
            <i class="fas fa-tasks" aria-hidden="true"></i>
            <span>Menu de gestion</span>
        </a>
        <ul class="nav nav-children">
            <li>
                <a class="nav-link" href="{{route('recherche.pesee')}}">
                    Pesess
                </a>
            </li>


        </ul>
    </li>
    @endhasanyrole
</ul>
